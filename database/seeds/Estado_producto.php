<?php

use Illuminate\Database\Seeder;

class Estado_producto extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('estado_producto')->insert([

        	'id_EstadoProducto'=>1,
        	'cantidad'=>200,
            'id_Producto'=>1,
            'id_Posibles_Estados'=>1
        ]);
    }
}
