<?php

use Illuminate\Database\Seeder;

class Meses extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('mes')->insert([
       'idmes'=>1,
       'mes'=>'Enero'
     ]);
        DB::table('mes')->insert([
       'idmes'=>2,
       'mes'=>'Febrero'
     ]);
        DB::table('mes')->insert([
       'idmes'=>3,
       'mes'=>'Marzo'
     ]);
        DB::table('mes')->insert([
       'idmes'=>4,
       'mes'=>'Abril'
     ]);
        DB::table('mes')->insert([
       'idmes'=>5,
       'mes'=>'Mayo'
     ]);
        DB::table('mes')->insert([
       'idmes'=>6,
       'mes'=>'Junio'
     ]);
        DB::table('mes')->insert([
       'idmes'=>7,
       'mes'=>'Julio'
     ]);
        DB::table('mes')->insert([
       'idmes'=>8,
       'mes'=>'Agosto'
     ]);
        DB::table('mes')->insert([
       'idmes'=>9,
       'mes'=>'Septiembre'
     ]);
        DB::table('mes')->insert([
       'idmes'=>10,
       'mes'=>'Octubre'
     ]);
        DB::table('mes')->insert([
       'idmes'=>11,
       'mes'=>'Noviembre'
     ]);
        DB::table('mes')->insert([
       'idmes'=>12,
       'mes'=>'Diciembre'
     ]);
        
    }
}
