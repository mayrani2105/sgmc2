<?php

use Illuminate\Database\Seeder;

class Almacenes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('almacen')->insert([
       'idAlmacen'=>1,
       'descripcion'=>'Almacen central',
        'calle'=>'Rosa',
        'colonia'=>'Colonia centro',
        'id_Estados'=>16,
        'id_sucursal'=>1
     ]);
     DB::table('almacen')->insert([
     'idAlmacen'=>2,
     'descripcion'=>'Sucursal A',
      'calle'=>'Verde',
      'colonia'=>'Colonia centro',
      'id_Estados'=>16,
      'id_sucursal'=>2
   ]);

     DB::table('almacen')->insert([
     'idAlmacen'=>3,
     'descripcion'=>'Almacen C',
      'calle'=>'Verde',
      'colonia'=>'Colonia centro',
      'id_Estados'=>17,
      'id_sucursal'=>3
   ]);

     DB::table('almacen')->insert([
     'idAlmacen'=>4,
     'descripcion'=>'Almacen D',
      'calle'=>'Verde',
      'colonia'=>'Colonia centro',
      'id_Estados'=>17,
      'id_sucursal'=>4
   ]);
    }
}
