<?php

use Illuminate\Database\Seeder;

class Estados extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('estados')->insert([
       'id_estados'=>1,
       'estado'=>'Aguascalientes'
     ]);
        DB::table('estados')->insert([
       'id_estados'=>2,
       'estado'=>'Baja California'
     ]);
        DB::table('estados')->insert([
       'id_estados'=>3,
       'estado'=>'Baja California Sur'
     ]);
        DB::table('estados')->insert([
       'id_estados'=>4,
       'estado'=>'Campeche'
     ]);
        DB::table('estados')->insert([
       'id_estados'=>5,
       'estado'=>'Coahuila'
     ]);
        DB::table('estados')->insert([
       'id_estados'=>6,
       'estado'=>'Colima'
     ]);
        DB::table('estados')->insert([
       'id_estados'=>7,
       'estado'=>'Chiapas'
     ]);
        DB::table('estados')->insert([
       'id_estados'=>8,
       'estado'=>'Chihuahua'
     ]);
        DB::table('estados')->insert([
       'id_estados'=>9,
       'estado'=>'Distrito Federal'
     ]);
        DB::table('estados')->insert([
       'id_estados'=>10,
       'estado'=>'Durango'
     ]);
        DB::table('estados')->insert([
       'id_estados'=>11,
       'estado'=>'Durango'
     ]);
        DB::table('estados')->insert([
       'id_estados'=>12,
       'estado'=>'Guanajuato'
     ]);
        DB::table('estados')->insert([
       'id_estados'=>13,
       'estado'=>'Guerrero'
     ]);
        DB::table('estados')->insert([
       'id_estados'=>14,
       'estado'=>'Hidalgo'
     ]);
        DB::table('estados')->insert([
       'id_estados'=>15,
       'estado'=>'Jalisco'
     ]);
        DB::table('estados')->insert([
       'id_estados'=>16,
       'estado'=>'Edo Mexico'
     ]);
        DB::table('estados')->insert([
       'id_estados'=>17,
       'estado'=>'Michoacan'
     ]);
        DB::table('estados')->insert([
       'id_estados'=>18,
       'estado'=>'Morelos'
     ]);
        DB::table('estados')->insert([
       'id_estados'=>19,
       'estado'=>'Nuevo Leon'
     ]);
        DB::table('estados')->insert([
       'id_estados'=>20,
       'estado'=>'Oaxaca'
     ]);
        DB::table('estados')->insert([
       'id_estados'=>21,
       'estado'=>'Puebla'
     ]);
        DB::table('estados')->insert([
       'id_estados'=>22,
       'estado'=>'Queretaro'
     ]);
        DB::table('estados')->insert([
       'id_estados'=>23,
       'estado'=>'Quintana Roo'
     ]);
        DB::table('estados')->insert([
       'id_estados'=>24,
       'estado'=>'San Luis Potosi'
     ]);
        DB::table('estados')->insert([
       'id_estados'=>25,
       'estado'=>'Sinaloa'
     ]);
        DB::table('estados')->insert([
       'id_estados'=>26,
       'estado'=>'Sonora'
     ]);
        DB::table('estados')->insert([
       'id_estados'=>27,
       'estado'=>'Tabasco'
     ]);
        DB::table('estados')->insert([
       'id_estados'=>28,
       'estado'=>'Tamaulipas'
     ]);
        DB::table('estados')->insert([
       'id_estados'=>29,
       'estado'=>'Tlaxcala'
     ]);
        DB::table('estados')->insert([
       'id_estados'=>30,
       'estado'=>'Veracruz'
     ]);
        DB::table('estados')->insert([
       'id_estados'=>31,
       'estado'=>'Yucatan'
     ]);
        DB::table('estados')->insert([
       'id_estados'=>32,
       'estado'=>'Zacatecas'
     ]);
    }
}
