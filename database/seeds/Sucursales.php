<?php

use Illuminate\Database\Seeder;

class Sucursales extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('sucursal')->insert([
        	'id_sucursal'=>1,
           'descripcionS'=>"A",
        	'direccion'=>'Jose Maria',
        	'colonia'=>'Soledad',
        	'numero'=>145,
            'ciudad'=>'morelia',
        	'id_Estados'=>1
        ]);
        DB::table('sucursal')->insert([
           'id_sucursal'=>2,
           'descripcionS'=>"B",
           'direccion'=>'Miguel de Allende',
           'colonia'=>'Soledad',
           'numero'=>145,
             'ciudad'=>'morelia',
           'id_Estados'=>1
         ]);
         DB::table('sucursal')->insert([
          	'id_sucursal'=>3,
             'descripcionS'=>"C",
          	'direccion'=>'Jose Maria',
          	'colonia'=>'Soledad',
          	'numero'=>145,
              'ciudad'=>'morelia',
          	'id_Estados'=>1
          ]);
          DB::table('sucursal')->insert([
             'id_sucursal'=>4,
             'descripcionS'=>"D",
             'direccion'=>'Miguel de Allende',
             'colonia'=>'Soledad',
             'numero'=>145,
               'ciudad'=>'morelia',
             'id_Estados'=>1
           ]);

    }
}
