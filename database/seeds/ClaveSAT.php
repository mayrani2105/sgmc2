<?php

use Illuminate\Database\Seeder;

class ClaveSAT extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('clavesats')->insert([
       'idsat'=>1,
       'clavesat'=>'44121618',
        'descripcion'=>'Tijeras'
     ]);
         DB::table('clavesats')->insert([
       'idsat'=>2,
       'clavesat'=>'14111514',
        'descripcion'=>'Libretas'
     ]); 
        DB::table('clavesats')->insert([
       'idsat'=>3,
       'clavesat'=>'44121707',
        'descripcion'=>'Lapices de colores'
     ]);
        DB::table('clavesats')->insert([
       'idsat'=>4,
       'clavesat'=>'44121706',
        'descripcion'=>'Lapices de madera'
     ]);
        DB::table('clavesats')->insert([
       'idsat'=>5,
       'clavesat'=>'44121701',
        'descripcion'=>'Boligrafos'
     ]); 
        DB::table('clavesats')->insert([
       'idsat'=>6,
       'clavesat'=>'60121535',
        'descripcion'=>'Borradores de goma'
     ]); 
        DB::table('clavesats')->insert([
       'idsat'=>7,
       'clavesat'=>'14111506',
        'descripcion'=>'Hojas para impresion'
     ]); 
        DB::table('clavesats')->insert([
       'idsat'=>8,
       'clavesat'=>'60103110',
        'descripcion'=>'Juego de geometria'
     ]); 
        DB::table('clavesats')->insert([
       'idsat'=>9,
       'clavesat'=>'31201610',
        'descripcion'=>'Pegamento'
     ]);
        DB::table('clavesats')->insert([
       'idsat'=>10,
       'clavesat'=>'31201500',
        'descripcion'=>'Cinta adhesiva'
     ]);
        DB::table('clavesats')->insert([
       'idsat'=>11,
       'clavesat'=>'77201500',
        'descripcion'=>'Cinta no-adhesiva'
     ]);
        DB::table('clavesats')->insert([
       'idsat'=>12,
       'clavesat'=>'88201500',
        'descripcion'=>'Cinta superadhesiva'
     ]);
    }
}
