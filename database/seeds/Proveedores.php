<?php

use Illuminate\Database\Seeder;

class Proveedores extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
  //    for ($i=0; $i<100; $i++){
    DB::table('proveedor')->insert([
       //'idProveedor'=>,
       'claveProveedor'=>211,
        'proveedor'=>'ABASTECEDORA LUMEN SA DE CVProveedor 1',
       'rfc'=>'PAAL870478LC5',
        'telefono'=>'4439807652',
       'correo'=>'P1@gmail.com',
        'colonia'=>'Las margaritas',
        'Estado'=>'NUEVO LEON',
        'calle'=>'Obispo de la calzada',
        'pais'=>'Mexico',
        'estadoproveedor'=>1,
        'noExterior'=>'1',
        'diascredito'=>20,
        'promociones'=>10

     ]);
     DB::table('proveedor')->insert([
    //'idProveedor'=>2,
    'claveProveedor'=>213,
     'proveedor'=>'ADELANTE S.A. DE C.V.',
    'rfc'=>'MUCN721020TY7',
     'telefono'=>'4439807652',
    'correo'=>'Proveedor@gmail.com',
     'colonia'=>'FRAC. ALCE BLANCO',
     'Estado'=>'Michoacan',
     'calle'=>'NVA. STO. DOMINGO No',
     'pais'=>'Mexico',
     'estadoproveedor'=>1,
     'noExterior'=>'110210',
     'diascredito'=>20,
     'promociones'=>10

  ]);
  DB::table('proveedor')->insert([
 //'idProveedor'=>3,
 'claveProveedor'=>234,
  'proveedor'=>'AMSCAN DE MEXICO, SA DE CV',
 'rfc'=>'PAAL870478LC5',
  'telefono'=>'4439807652',
 'correo'=>'P1@gmail.com',
  'colonia'=>'Las margaritas',
  'Estado'=>'NUEVO LEON',
  'calle'=>'Obispo de la calzada',
  'pais'=>'Mexico',
   'estadoproveedor'=>1,
  'noExterior'=>'1',
  'diascredito'=>20,
  'promociones'=>10

]);
DB::table('proveedor')->insert([
//'idProveedor'=>4,
'claveProveedor'=>218,
'proveedor'=>'BIO PAPPEL SCRIBE SA DE CV/PAPEL',
'rfc'=>'MUCN721020TY7',
'telefono'=>'4439807652',
'correo'=>'Proveedor@gmail.com',
'colonia'=>'FRAC. ALCE BLANCO',
'Estado'=>'Michoacan',
'calle'=>'NVA. STO. DOMINGO No',
'pais'=>'Mexico',
 'estadoproveedor'=>1,
'noExterior'=>'110210',
'diascredito'=>20,
'promociones'=>10

]);
DB::table('proveedor')->insert([
//'idProveedor'=>5,
'claveProveedor'=>24,
'proveedor'=>'BEROKY S.A. DE C.V.',
'rfc'=>'PAAL870478LC5',
'telefono'=>'4439807652',
'correo'=>'P1@gmail.com',
'colonia'=>'Las margaritas',
'Estado'=>'NUEVO LEON',
'calle'=>'Obispo de la calzada',
'pais'=>'Mexico',
'estadoproveedor'=>1,
'noExterior'=>'1',
'diascredito'=>20,
'promociones'=>10

]);
DB::table('proveedor')->insert([
//'idProveedor'=>6,
'claveProveedor'=>21,
'proveedor'=>'ARTESANIAS DE VIDRIO',
'rfc'=>'MUCN721020TY7',
'telefono'=>'4439807652',
'correo'=>'Proveedor@gmail.com',
'colonia'=>'FRAC. ALCE BLANCO',
'Estado'=>'Michoacan',
'calle'=>'NVA. STO. DOMINGO No',
'pais'=>'Mexico',
 'estadoproveedor'=>1,
'noExterior'=>'110210',
'diascredito'=>20,
'promociones'=>10

]);
}

}
