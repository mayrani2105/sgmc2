<?php

use Illuminate\Database\Seeder;

class PedidoProductos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pedido_producto')->insert([
       'idPedido'=>1,
       'Cantidad'=>20,
       'id_Producto'=>1
     ]);
        
        DB::table('pedido_producto')->insert([
       'idPedido'=>1,
       'Cantidad'=>70,
       'id_Producto'=>6
     ]);
        
        DB::table('pedido_producto')->insert([
       'idPedido'=>2,
       'Cantidad'=>10,
       'id_Producto'=>2
     ]);
        
        DB::table('pedido_producto')->insert([
       'idPedido'=>2,
       'Cantidad'=>8,
       'id_Producto'=>7
     ]);
        
        DB::table('pedido_producto')->insert([
       'idPedido'=>3,
       'Cantidad'=>50,
       'id_Producto'=>3
     ]);
        
        DB::table('pedido_producto')->insert([
       'idPedido'=>3,
       'Cantidad'=>11,
       'id_Producto'=>8
     ]);
     
        DB::table('pedido_producto')->insert([
       'idPedido'=>4,
       'Cantidad'=>15,
       'id_Producto'=>4
     ]);
        
        DB::table('pedido_producto')->insert([
       'idPedido'=>4,
       'Cantidad'=>5,
       'id_Producto'=>9
     ]);
        
        DB::table('pedido_producto')->insert([
       'idPedido'=>5,
       'Cantidad'=>60,
       'id_Producto'=>5
     ]);

         DB::table('pedido_producto')->insert([
       'idPedido'=>5,
       'Cantidad'=>6,
       'id_Producto'=>10
     ]);
        
    //Seeds para todos los pedidos registrados hasta el momento.
          DB::table('pedido_producto')->insert([
       'idPedido'=>6,
       'Cantidad'=>25,
       'id_Producto'=>1
     ]);

         DB::table('pedido_producto')->insert([
       'idPedido'=>6,
       'Cantidad'=>60,
       'id_Producto'=>2
     ]);
        
          DB::table('pedido_producto')->insert([
       'idPedido'=>7,
       'Cantidad'=>10,
       'id_Producto'=>3
     ]);

         DB::table('pedido_producto')->insert([
       'idPedido'=>7,
       'Cantidad'=>15,
       'id_Producto'=>4
     ]);
        
          DB::table('pedido_producto')->insert([
       'idPedido'=>8,
       'Cantidad'=>10,
       'id_Producto'=>5
     ]);

         DB::table('pedido_producto')->insert([
       'idPedido'=>8,
       'Cantidad'=>22,
       'id_Producto'=>6
     ]);
        
          DB::table('pedido_producto')->insert([
       'idPedido'=>9,
       'Cantidad'=>20,
       'id_Producto'=>7
     ]);

         DB::table('pedido_producto')->insert([
       'idPedido'=>9,
       'Cantidad'=>13,
       'id_Producto'=>8
     ]);
        
          DB::table('pedido_producto')->insert([
       'idPedido'=>10,
       'Cantidad'=>12,
       'id_Producto'=>9
     ]);

         DB::table('pedido_producto')->insert([
       'idPedido'=>10,
       'Cantidad'=>12,
       'id_Producto'=>10
     ]);
        
          DB::table('pedido_producto')->insert([
       'idPedido'=>11,
       'Cantidad'=>12,
       'id_Producto'=>11
     ]);

         DB::table('pedido_producto')->insert([
       'idPedido'=>11,
       'Cantidad'=>6,
       'id_Producto'=>12
     ]);
        
          DB::table('pedido_producto')->insert([
       'idPedido'=>12,
       'Cantidad'=>23,
       'id_Producto'=>13
     ]);

         DB::table('pedido_producto')->insert([
       'idPedido'=>12,
       'Cantidad'=>9,
       'id_Producto'=>14
     ]);
        
          DB::table('pedido_producto')->insert([
       'idPedido'=>13,
       'Cantidad'=>6,
       'id_Producto'=>15
     ]);

         DB::table('pedido_producto')->insert([
       'idPedido'=>13,
       'Cantidad'=>20,
       'id_Producto'=>16
     ]);
        
          DB::table('pedido_producto')->insert([
       'idPedido'=>14,
       'Cantidad'=>17,
       'id_Producto'=>17
     ]);

         DB::table('pedido_producto')->insert([
       'idPedido'=>14,
       'Cantidad'=>9,
       'id_Producto'=>18
     ]);
        
          DB::table('pedido_producto')->insert([
       'idPedido'=>15,
       'Cantidad'=>6,
       'id_Producto'=>19
     ]);

         DB::table('pedido_producto')->insert([
       'idPedido'=>15,
       'Cantidad'=>7,
       'id_Producto'=>20
     ]);
        
          DB::table('pedido_producto')->insert([
       'idPedido'=>16,
       'Cantidad'=>20,
       'id_Producto'=>21
     ]);

         DB::table('pedido_producto')->insert([
       'idPedido'=>16,
       'Cantidad'=>6,
       'id_Producto'=>22
     ]);
        
        
    }
}
