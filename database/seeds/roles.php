<?php

use Illuminate\Database\Seeder;

class roles extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {



      DB::table('rol')->insert([
       'id_rol'=>1,
       'descripcion'=>'jefe de operaciones'

     ]);
      DB::table('rol')->insert([
          'id_rol'=>2,
          'descripcion'=>'Administrador'

        ]);
       DB::table('rol')->insert([
            'id_rol'=>3,
            'descripcion'=>'Jefe Almacen'

          ]);
          DB::table('rol')->insert([
              'id_rol'=>4,
              'descripcion'=>'Distribuidora'

            ]);
          DB::table('rol')->insert([
              'id_rol'=>5,
              'descripcion'=>'Checador'

            ]);
            DB::table('rol')->insert([
                'id_rol'=>6,
                'descripcion'=>'Compras'

              ]);
              DB::table('rol')->insert([
                  'id_rol'=>7,
                  'descripcion'=>'Ventas'

                ]);
                DB::table('privilegios')->insert([
               'id_privilegio'=>1,
               'descripcion'=>'Modulo Usuarios'
                 ]);
                DB::table('privilegios')->insert([
                'id_privilegio'=>2,
                'descripcion'=>'Modulo Inventario'
              ]);
              DB::table('privilegios')->insert([
              'id_privilegio'=>3,
              'descripcion'=>'Modulo Proveedores'
              ]);
              DB::table('privilegios')->insert([
              'id_privilegio'=>4,
              'descripcion'=>'Modulo Pedidos'
              ]);
              DB::table('privilegios')->insert([
              'id_privilegio'=>5,
              'descripcion'=>'Modulo Productos'
              ]);
              DB::table('privilegios')->insert([
              'id_privilegio'=>6,
              'descripcion'=>'Modulo Almacenes'
              ]);

              //privilegios para administrador
              DB::table('funcionesrol')->insert([
             'id_rol'=>2,
             'id_privilegio'=>1,
             'ver'=>1,
             'editar'=>1,
             'borrar'=>1
               ]);
            DB::table('funcionesrol')->insert([
               'id_rol'=>2,
              'id_privilegio'=>2,
              'ver'=>1,
              'editar'=>1,
              'borrar'=>1
                ]);
              DB::table('funcionesrol')->insert([
                 'id_rol'=>2,
               'id_privilegio'=>3,
               'ver'=>1,
               'editar'=>1,
               'borrar'=>1
                 ]);
              DB::table('funcionesrol')->insert([
               'id_rol'=>2,
                'id_privilegio'=>4,
                'ver'=>1,
                'editar'=>1,
                'borrar'=>1
                  ]);
              DB::table('funcionesrol')->insert([
                  'id_rol'=>2,
                 'id_privilegio'=>5,
                 'ver'=>1,
                 'editar'=>1,
                 'borrar'=>1
                   ]);
              DB::table('funcionesrol')->insert([
                 'id_rol'=>2,
                  'id_privilegio'=>6,
                  'ver'=>1,
                  'editar'=>1,
                  'borrar'=>1
                    ]);

          //privilegios para jefe de Operaciones
          DB::table('funcionesrol')->insert([
            'id_rol'=>1,
         'id_privilegio'=>1,
         'ver'=>1,
         'editar'=>1,
         'borrar'=>0
           ]);
        DB::table('funcionesrol')->insert([
             'id_rol'=>1,
          'id_privilegio'=>2,
          'ver'=>1,
          'editar'=>1,
          'borrar'=>1
            ]);
          DB::table('funcionesrol')->insert([
        'id_rol'=>1,
           'id_privilegio'=>3,
           'ver'=>1,
           'editar'=>1,
           'borrar'=>1
             ]);
          DB::table('funcionesrol')->insert([
        'id_rol'=>1,
            'id_privilegio'=>4,
            'ver'=>1,
            'editar'=>1,
            'borrar'=>1
              ]);
          DB::table('funcionesrol')->insert([
            'id_rol'=>1,
             'id_privilegio'=>5,
             'ver'=>1,
             'editar'=>1,
             'borrar'=>1
               ]);
          DB::table('funcionesrol')->insert([
            'id_rol'=>1,
              'id_privilegio'=>6,
              'ver'=>1,
              'editar'=>1,
              'borrar'=>1
                ]);

            //Privilegios para Jefe de almacen
            DB::table('funcionesrol')->insert([
        'id_rol'=>3,
           'id_privilegio'=>1,
           'ver'=>0,
           'editar'=>0,
           'borrar'=>0
             ]);
          DB::table('funcionesrol')->insert([
        'id_rol'=>3,
            'id_privilegio'=>2,
            'ver'=>1,
            'editar'=>1,
            'borrar'=>0
              ]);
            DB::table('funcionesrol')->insert([
            'id_rol'=>3,
             'id_privilegio'=>3,
             'ver'=>1,
             'editar'=>0,
             'borrar'=>0
               ]);
            DB::table('funcionesrol')->insert([
            'id_rol'=>3,
              'id_privilegio'=>4,
              'ver'=>1,
              'editar'=>0,
              'borrar'=>0
                ]);
            DB::table('funcionesrol')->insert([
               'id_rol'=>3,
               'id_privilegio'=>5,
               'ver'=>1,
               'editar'=>0,
               'borrar'=>0
                 ]);
            DB::table('funcionesrol')->insert([
              'id_rol'=>3,
                'id_privilegio'=>6,
                'ver'=>1,
                'editar'=>0,
                'borrar'=>0
                  ]);

            //Privilegios para Distribuidora

                DB::table('funcionesrol')->insert([
              'id_rol'=>4,
               'id_privilegio'=>1,
               'ver'=>0,
               'editar'=>0,
               'borrar'=>0
                 ]);
              DB::table('funcionesrol')->insert([
              'id_rol'=>4,
                'id_privilegio'=>2,
                'ver'=>1,
                'editar'=>0,
                'borrar'=>0
                  ]);
                DB::table('funcionesrol')->insert([
                'id_rol'=>4,
                 'id_privilegio'=>3,
                 'ver'=>1,
                 'editar'=>0,
                 'borrar'=>0
                   ]);
                DB::table('funcionesrol')->insert([
                'id_rol'=>4,
                  'id_privilegio'=>4,
                  'ver'=>1,
                  'editar'=>0,
                  'borrar'=>0
                    ]);
                DB::table('funcionesrol')->insert([
                   'id_rol'=>4,
                   'id_privilegio'=>5,
                   'ver'=>0,
                   'editar'=>0,
                   'borrar'=>0
                     ]);
                DB::table('funcionesrol')->insert([
                  'id_rol'=>4,
                    'id_privilegio'=>6,
                    'ver'=>1,
                    'editar'=>0,
                    'borrar'=>0
                      ]);

                //privilegios para checador
                DB::table('funcionesrol')->insert([
              'id_rol'=>5,
               'id_privilegio'=>1,
               'ver'=>0,
               'editar'=>0,
               'borrar'=>0
                 ]);
              DB::table('funcionesrol')->insert([
              'id_rol'=>5,
                'id_privilegio'=>2,
                'ver'=>0,
                'editar'=>0,
                'borrar'=>0
                  ]);
                DB::table('funcionesrol')->insert([
                'id_rol'=>5,
                 'id_privilegio'=>3,
                 'ver'=>0,
                 'editar'=>0,
                 'borrar'=>0
                   ]);
                DB::table('funcionesrol')->insert([
                'id_rol'=>3,
                  'id_privilegio'=>4,
                  'ver'=>1,
                  'editar'=>0,
                  'borrar'=>0
                    ]);
                DB::table('funcionesrol')->insert([
                   'id_rol'=>3,
                   'id_privilegio'=>5,
                   'ver'=>0,
                   'editar'=>0,
                   'borrar'=>0
                     ]);
                DB::table('funcionesrol')->insert([
                  'id_rol'=>3,
                    'id_privilegio'=>6,
                    'ver'=>1,
                    'editar'=>0,
                    'borrar'=>0
                      ]);


                //privilegios para Compras

                      DB::table('funcionesrol')->insert([
                    'id_rol'=>6,
                     'id_privilegio'=>1,
                     'ver'=>0,
                     'editar'=>0,
                     'borrar'=>0
                       ]);
                    DB::table('funcionesrol')->insert([
                    'id_rol'=>6,
                      'id_privilegio'=>2,
                      'ver'=>1,
                      'editar'=>1,
                      'borrar'=>1
                        ]);
                      DB::table('funcionesrol')->insert([
                      'id_rol'=>6,
                       'id_privilegio'=>3,
                       'ver'=>1,
                       'editar'=>1,
                       'borrar'=>1
                         ]);
                      DB::table('funcionesrol')->insert([
                      'id_rol'=>6,
                        'id_privilegio'=>4,
                        'ver'=>1,
                        'editar'=>1,
                        'borrar'=>0
                          ]);
                      DB::table('funcionesrol')->insert([
                         'id_rol'=>6,
                         'id_privilegio'=>5,
                         'ver'=>1,
                         'editar'=>0,
                         'borrar'=>0
                           ]);
                      DB::table('funcionesrol')->insert([
                        'id_rol'=>6,
                          'id_privilegio'=>6,
                          'ver'=>1,
                          'editar'=>0,
                          'borrar'=>0
                            ]);

                      //privilegios para Ventas
                      DB::table('funcionesrol')->insert([
                    'id_rol'=>7,
                     'id_privilegio'=>1,
                     'ver'=>0,
                     'editar'=>0,
                     'borrar'=>0
                       ]);
                    DB::table('funcionesrol')->insert([
                    'id_rol'=>7,
                      'id_privilegio'=>2,
                      'ver'=>0,
                      'editar'=>0,
                      'borrar'=>0
                        ]);
                      DB::table('funcionesrol')->insert([
                      'id_rol'=>7,
                       'id_privilegio'=>3,
                       'ver'=>1,
                       'editar'=>0,
                       'borrar'=>0
                         ]);
                      DB::table('funcionesrol')->insert([
                      'id_rol'=>7,
                        'id_privilegio'=>4,
                        'ver'=>1,
                        'editar'=>1,
                        'borrar'=>0
                          ]);
                      DB::table('funcionesrol')->insert([
                         'id_rol'=>7,
                         'id_privilegio'=>5,
                         'ver'=>0,
                         'editar'=>0,
                         'borrar'=>0
                           ]);
                      DB::table('funcionesrol')->insert([
                        'id_rol'=>7,
                          'id_privilegio'=>6,
                          'ver'=>1,
                          'editar'=>0,
                          'borrar'=>0
                            ]);


    }
}
