<?php

use Illuminate\Database\Seeder;

class Privilegios extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
  DB::table('privilegios')->insert([
 'id_privilegio'=>1,
 'descripcion'=>'Modulo Usuarios'
   ]);
  DB::table('privilegios')->insert([
  'id_privilegio'=>2,
  'descripcion'=>'Modulo Inventario'
]);
DB::table('privilegios')->insert([
'id_privilegio'=>3,
'descripcion'=>'Modulo Proveedores'
]);
DB::table('privilegios')->insert([
'id_privilegio'=>4,
'descripcion'=>'Modulo Pedidos'
]);
DB::table('privilegios')->insert([
'id_privilegio'=>5,
'descripcion'=>'Modulo Productos'
]);
DB::table('privilegios')->insert([
'id_privilegio'=>6,
'descripcion'=>'Modulo Almacenes'
]);
    }
}
