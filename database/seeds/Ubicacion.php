<?php

use Illuminate\Database\Seeder;

class Ubicacion extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ubicacion')->insert([
        	'id_ubicacion'=>1,
            'nivel'=>1,
            'piso'=>1,
            'anaquel'=>1,
            'pasillo'=>'A'
        ]);
        
        DB::table('ubicacion')->insert([
        	'id_ubicacion'=>2,
            'nivel'=>1,
            'piso'=>1,
            'anaquel'=>2,
            'pasillo'=>'A'
        ]);
    }
}
