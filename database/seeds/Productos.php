<?php

use Illuminate\Database\Seeder;

class Productos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){ 
      DB::table('producto')->insert([
        //  'id_Producto' => 1,
          'descripcion'=>'Notas PIMACO ',
          'unidadbase' => 'unidad',
          'costo'=>7.00,
          'precioreal'=>15.00,
          'clave'=>93867,
          'idMarca'=>1,
          'idsat'=>4,
          'idSubCategoria'=>2,
          'idEstado'=>1
        ]);
      DB::table('producto')->insert([
        //  'id_Producto' => 1,
          'descripcion'=>'Colores 12 largos acuarelables',
          'unidadbase' => 'unidad',
          'costo'=>88.19,
          'precioreal'=>90,
          'clave'=>69370,
          'idMarca'=>2,
          'idsat'=>3,
          'idSubCategoria'=>2,
          'idEstado'=>1
        ]);

        DB::table('producto')->insert([
        //  'id_Producto' => 1,
          'descripcion'=>'Lapices de grafito goldfaber',
          'unidadbase' => 'unidad',
          'costo'=>140,
          'precioreal'=>150,
          'clave'=>69370,
          'idMarca'=>2,
          'idsat'=>4,
          'idSubCategoria'=>3,
          'idEstado'=>1
        ]);

        DB::table('producto')->insert([
        //  'id_Producto' => 1,
          'descripcion'=>'Marcador tinta china PITT BIG BRUSH',
          'unidadbase' => 'base',
          'costo'=>287.63,
          'precioreal'=>300.00,
          'clave'=>003130,
          'idMarca'=>3,
          'idsat'=>5,
          'idSubCategoria'=>2,
          'idEstado'=>1
        ]);
        

     for ($i=0; $i<5; $i++){
       DB::table('producto')->insert([
        //	'id_Producto' => 1,
        	'descripcion'=>'A CARBON PITT MONOCHROME C/10',
        	'unidadbase' => 'unidad',
        	'costo'=>7.00,
          'precioreal'=>30.00,
        	'clave'=>000672,
        	'idMarca'=>4,
          'idsat'=>5,
          'idSubCategoria'=>58,
        	'idEstado'=>1
        ]);

       DB::table('producto')->insert([
        //	'id_Producto' => 2,
        	'descripcion'=>'A COLORES 12 LARGOS FABER CASTELL',
        	'unidadbase' => 'base',
        	'costo'=>5.00,
          'precioreal'=>24.00,
        	'clave'=>012303,
        	'idMarca'=>5,
        	'idsat'=>3,
        	'idSubCategoria'=>6,
        	'idEstado'=>1
        ]);
      }

      for ($i=0; $i<5; $i++){
        DB::table('producto')->insert([
         //	'id_Producto' => 2,
           'descripcion'=>'A COLORES 48 LARGOS ACUARELABLES FABER CASTELL',
           'unidadbase' => 'base',
           'costo'=>730.00,
           'precioreal'=>794.00,
           'clave'=>012300,
           'idMarca'=>5,
           'idsat'=>5,
           'idSubCategoria'=>6,
           'idEstado'=>1
         ]);
         DB::table('producto')->insert([
          //	'id_Producto' => 2,
            'descripcion'=>'A COLORES 48 LARGOS FABER CASTELL',
            'unidadbase' => 'base',
            'costo'=>680.00,
            'precioreal'=>710.00,
            'clave'=>012301,
            'idMarca'=>5,
            'idsat'=>5,
            'idSubCategoria'=>6,
            'idEstado'=>1
          ]);
      }
      for ($i=0; $i<3; $i++){
         DB::table('producto')->insert([
        'descripcion'=>'BLOCK ISOMETRICO 50 HJS ORGANIFORMAS PI50*',
        'unidadbase' => 'unidad',
        'costo'=>10.09,
        'precioreal'=>15.00,
        'clave'=>011305,
        'idMarca'=>6,
        'idsat'=>2,
        'idSubCategoria'=>37,
        'idEstado'=>1
      ]);

     DB::table('producto')->insert([
      //	'id_Producto' => 2,
        'descripcion'=>'BOLIGRAFO PILOT POP LOL PASTEL',
        'unidadbase' => 'unidad',
        'costo'=>5.00,
        'precioreal'=>24.00,
        'clave'=>1240,
        'idMarca'=>3,
        'idsat'=>4,
        'idSubCategoria'=>61,
        'idEstado'=>1
      ]);
    }
  }
    

}
