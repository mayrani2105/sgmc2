<?php

use Illuminate\Database\Seeder;

class TipoPedido extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_pedido')->insert([
       'id_Tipo_Pedido'=>1,
       'descripcion'=>'Entrada'
     ]);

         DB::table('tipo_pedido')->insert([
       'id_Tipo_Pedido'=>2,
       'descripcion'=>'Salida'
     ]);

    }
}
