<?php

use Illuminate\Database\Seeder;

class Inventario extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Inventario sucursal A
        DB::table('inventario') ->Insert([
        	'id_inventario'=>1,
        	'cantidad'=>340,
        	'cantMaxima'=>12035,
        	'cantMin'=>20,
          'entrada'=>0,
          'salida'=>0,
          'bloqueado'=>0,
          'disponible'=>340,
          'id_ubicacion'=>1,
          'idAlmacen'=>1,
          'estadoinventario'=>1,
        	'id_Producto'=>1
        ]);
         DB::table('inventario') ->Insert([
            'id_inventario'=>2,
        	'cantidad'=>120,
        	'cantMaxima'=>12035,
        	'cantMin'=>20,
          'entrada'=>0,
          'salida'=>0,
          'bloqueado'=>0,
          'disponible'=>120,
          'id_ubicacion'=>1,
          'idAlmacen'=>1,
          'estadoinventario'=>1,
        	'id_Producto'=>2
        ]);
         DB::table('inventario') ->Insert([
            'id_inventario'=>3,
            'cantidad'=>15,
            'cantMaxima'=>125,
            'cantMin'=>10,
            'entrada'=>0,
            'salida'=>0,
            'bloqueado'=>0,
            'disponible'=>15,
            'id_ubicacion'=>1,
            'idAlmacen'=>1,
          'estadoinventario'=>1,
            'id_Producto'=>3
        ]);
         DB::table('inventario') ->Insert([
            'id_inventario'=>4,
            'cantidad'=>200,
            'cantMaxima'=>185,
            'cantMin'=>50,
            'entrada'=>0,
            'salida'=>0,
            'bloqueado'=>0,
            'disponible'=>200,
            'id_ubicacion'=>1,
            'idAlmacen'=>1,
          'estadoinventario'=>1,
            'id_Producto'=>4
        ]);
         DB::table('inventario') ->Insert([
            'id_inventario'=>5,
            'cantidad'=>102,
            'cantMaxima'=>12035,
            'cantMin'=>20,
            'entrada'=>0,
            'salida'=>0,
            'bloqueado'=>0,
            'disponible'=>102,
            'id_ubicacion'=>1,
            'idAlmacen'=>1,
          'estadoinventario'=>1,
            'id_Producto'=>5
        ]);
         DB::table('inventario') ->Insert([
            'id_inventario'=>6,
            'cantidad'=>104,
            'cantMaxima'=>12035,
            'cantMin'=>20,
            'entrada'=>0,
            'salida'=>0,
            'bloqueado'=>0,
            'disponible'=>104,
            'id_ubicacion'=>1,
            'idAlmacen'=>1,
          'estadoinventario'=>1,
            'id_Producto'=>6
        ]);
         DB::table('inventario') ->Insert([
            'id_inventario'=>7,
            'cantidad'=>150,
            'cantMaxima'=>125,
            'cantMin'=>10,
            'entrada'=>0,
            'salida'=>0,
            'bloqueado'=>0,
            'disponible'=>15,
            'id_ubicacion'=>1,
            'idAlmacen'=>1,
          'estadoinventario'=>1,
            'id_Producto'=>7
        ]);
         DB::table('inventario') ->Insert([
            'id_inventario'=>8,
            'cantidad'=>200,
            'cantMaxima'=>185,
            'cantMin'=>50,
            'entrada'=>0,
            'salida'=>0,
            'bloqueado'=>0,
            'disponible'=>200,
            'id_ubicacion'=>1,
            'idAlmacen'=>1,
          'estadoinventario'=>1,
            'id_Producto'=>8
        ]);
         DB::table('inventario') ->Insert([
            'id_inventario'=>9,
            'cantidad'=>510,
            'cantMaxima'=>12035,
            'cantMin'=>20,
            'entrada'=>0,
            'salida'=>0,
            'bloqueado'=>0,
            'disponible'=>510,
            'id_ubicacion'=>1,
            'idAlmacen'=>1,
          'estadoinventario'=>1,
            'id_Producto'=>9
        ]);
         DB::table('inventario') ->Insert([
            'id_inventario'=>10,
            'cantidad'=>104,
            'cantMaxima'=>12035,
            'cantMin'=>20,
            'entrada'=>0,
            'salida'=>0,
            'bloqueado'=>0,
            'disponible'=>104,
            'id_ubicacion'=>1,
            'idAlmacen'=>1,
          'estadoinventario'=>1,
            'id_Producto'=>10
        ]);
         DB::table('inventario') ->Insert([
            'id_inventario'=>11,
            'cantidad'=>150,
            'cantMaxima'=>125,
            'cantMin'=>10,
            'entrada'=>0,
            'salida'=>0,
            'bloqueado'=>0,
            'disponible'=>150,
            'id_ubicacion'=>1,
            'idAlmacen'=>1,
          'estadoinventario'=>1,
            'id_Producto'=>11
        ]);
         DB::table('inventario') ->Insert([
            'id_inventario'=>12,
            'cantidad'=>200,
            'cantMaxima'=>185,
            'cantMin'=>50,
            'entrada'=>0,
            'salida'=>0,
            'bloqueado'=>0,
            'disponible'=>200,
            'id_ubicacion'=>1,
            'idAlmacen'=>1,
          'estadoinventario'=>1,
            'id_Producto'=>12
        ]);
         DB::table('inventario') ->Insert([
            'id_inventario'=>13,
            'cantidad'=>410,
            'cantMaxima'=>12035,
            'cantMin'=>20,
            'entrada'=>0,
            'salida'=>0,
            'bloqueado'=>0,
            'disponible'=>410,
            'id_ubicacion'=>1,
            'idAlmacen'=>1,
          'estadoinventario'=>1,
            'id_Producto'=>13
        ]);
         DB::table('inventario') ->Insert([
            'id_inventario'=>14,
            'cantidad'=>104,
            'cantMaxima'=>12035,
            'cantMin'=>20,
            'entrada'=>0,
            'salida'=>0,
            'bloqueado'=>0,
            'disponible'=>104,
            'id_ubicacion'=>1,
            'idAlmacen'=>1,
          'estadoinventario'=>1,
            'id_Producto'=>14
        ]);
         DB::table('inventario') ->Insert([
            'id_inventario'=>15,
            'cantidad'=>150,
            'cantMaxima'=>125,
            'cantMin'=>10,
            'entrada'=>0,
            'salida'=>0,
            'bloqueado'=>0,
            'disponible'=>15,
            'id_ubicacion'=>1,
            'idAlmacen'=>1,
          'estadoinventario'=>1,
            'id_Producto'=>15
        ]);
         DB::table('inventario') ->Insert([
            'id_inventario'=>16,
            'cantidad'=>200,
            'cantMaxima'=>185,
            'cantMin'=>50,
            'entrada'=>0,
            'salida'=>0,
            'bloqueado'=>0,
            'disponible'=>200,
            'id_ubicacion'=>1,
            'idAlmacen'=>1,
          'estadoinventario'=>1,
            'id_Producto'=>16
        ]);
        DB::table('inventario') ->Insert([
            'id_inventario'=>17,
            'cantidad'=>200,
            'cantMaxima'=>185,
            'cantMin'=>50,
            'entrada'=>0,
            'salida'=>0,
            'bloqueado'=>0,
            'disponible'=>200,
            'id_ubicacion'=>1,
            'idAlmacen'=>1,
          'estadoinventario'=>1,
            'id_Producto'=>17
        ]);
        DB::table('inventario') ->Insert([
            'id_inventario'=>18,
            'cantidad'=>200,
            'cantMaxima'=>185,
            'cantMin'=>50,
            'entrada'=>0,
            'salida'=>0,
            'bloqueado'=>0,
            'disponible'=>200,
            'id_ubicacion'=>1,
            'idAlmacen'=>1,
          'estadoinventario'=>1,
            'id_Producto'=>18
        ]);
        DB::table('inventario') ->Insert([
            'id_inventario'=>19,
            'cantidad'=>200,
            'cantMaxima'=>185,
            'cantMin'=>50,
            'entrada'=>0,
            'salida'=>0,
            'bloqueado'=>0,
            'disponible'=>200,
            'id_ubicacion'=>1,
            'idAlmacen'=>1,
          'estadoinventario'=>1,
            'id_Producto'=>19
        ]);
        DB::table('inventario') ->Insert([
            'id_inventario'=>20,
            'cantidad'=>200,
            'cantMaxima'=>185,
            'cantMin'=>50,
            'entrada'=>0,
            'salida'=>0,
            'bloqueado'=>0,
            'disponible'=>200,
            'id_ubicacion'=>1,
            'idAlmacen'=>1,
          'estadoinventario'=>1,
            'id_Producto'=>20
        ]);
        DB::table('inventario') ->Insert([
            'id_inventario'=>21,
            'cantidad'=>200,
            'cantMaxima'=>185,
            'cantMin'=>50,
            'entrada'=>0,
            'salida'=>0,
            'bloqueado'=>0,
            'disponible'=>200,
            'id_ubicacion'=>1,
            'idAlmacen'=>1,
          'estadoinventario'=>1,
            'id_Producto'=>21
        ]);
        DB::table('inventario') ->Insert([
            'id_inventario'=>22,
            'cantidad'=>200,
            'cantMaxima'=>185,
            'cantMin'=>50,
            'entrada'=>0,
            'salida'=>0,
            'bloqueado'=>0,
            'disponible'=>200,
            'id_ubicacion'=>1,
            'idAlmacen'=>1,
          'estadoinventario'=>1,
            'id_Producto'=>22
        ]);
        
        
        
        //Inventario sucursal B
        
        DB::table('inventario') ->Insert([
        	'id_inventario'=>23,
        	'cantidad'=>340,
        	'cantMaxima'=>12035,
        	'cantMin'=>20,
          'entrada'=>0,
          'salida'=>0,
          'bloqueado'=>0,
          'disponible'=>340,
          'id_ubicacion'=>1,
          'idAlmacen'=>2,
          'estadoinventario'=>1,
        	'id_Producto'=>1
        ]);
         DB::table('inventario') ->Insert([
            'id_inventario'=>24,
        	'cantidad'=>120,
        	'cantMaxima'=>12035,
        	'cantMin'=>20,
          'entrada'=>0,
          'salida'=>0,
          'bloqueado'=>0,
          'disponible'=>120,
          'id_ubicacion'=>1,
          'idAlmacen'=>2,
          'estadoinventario'=>1,
        	'id_Producto'=>2
        ]);
         DB::table('inventario') ->Insert([
            'id_inventario'=>25,
            'cantidad'=>150,
            'cantMaxima'=>175,
            'cantMin'=>10,
            'entrada'=>0,
            'salida'=>0,
            'bloqueado'=>0,
            'disponible'=>15,
            'id_ubicacion'=>1,
            'idAlmacen'=>2,
          'estadoinventario'=>1,
            'id_Producto'=>3
        ]);
         DB::table('inventario') ->Insert([
            'id_inventario'=>26,
            'cantidad'=>200,
            'cantMaxima'=>185,
            'cantMin'=>50,
            'entrada'=>0,
            'salida'=>0,
            'bloqueado'=>0,
            'disponible'=>200,
            'id_ubicacion'=>1,
            'idAlmacen'=>2,
          'estadoinventario'=>1,
            'id_Producto'=>4
        ]);
         DB::table('inventario') ->Insert([
            'id_inventario'=>27,
            'cantidad'=>102,
            'cantMaxima'=>12035,
            'cantMin'=>20,
            'entrada'=>0,
            'salida'=>0,
            'bloqueado'=>0,
            'disponible'=>102,
            'id_ubicacion'=>1,
            'idAlmacen'=>2,
          'estadoinventario'=>1,
            'id_Producto'=>5
        ]);
         DB::table('inventario') ->Insert([
            'id_inventario'=>28,
            'cantidad'=>104,
            'cantMaxima'=>12035,
            'cantMin'=>20,
            'entrada'=>0,
            'salida'=>0,
            'bloqueado'=>0,
            'disponible'=>104,
            'id_ubicacion'=>1,
            'idAlmacen'=>2,
          'estadoinventario'=>1,
            'id_Producto'=>6
        ]);
         DB::table('inventario') ->Insert([
            'id_inventario'=>29,
            'cantidad'=>150,
            'cantMaxima'=>125,
            'cantMin'=>10,
            'entrada'=>0,
            'salida'=>0,
            'bloqueado'=>20,
            'disponible'=>130,
            'id_ubicacion'=>1,
            'idAlmacen'=>2,
          'estadoinventario'=>1,
            'id_Producto'=>7
        ]);
         DB::table('inventario') ->Insert([
            'id_inventario'=>30,
            'cantidad'=>200,
            'cantMaxima'=>185,
            'cantMin'=>50,
            'entrada'=>0,
            'salida'=>0,
            'bloqueado'=>13,
            'disponible'=>187,
            'id_ubicacion'=>1,
            'idAlmacen'=>2,
          'estadoinventario'=>1,
            'id_Producto'=>8
        ]);
         DB::table('inventario') ->Insert([
            'id_inventario'=>31,
            'cantidad'=>510,
            'cantMaxima'=>12035,
            'cantMin'=>20,
            'entrada'=>0,
            'salida'=>0,
            'bloqueado'=>12,
            'disponible'=>498,
            'id_ubicacion'=>1,
            'idAlmacen'=>2,
          'estadoinventario'=>1,
            'id_Producto'=>9
        ]);
         DB::table('inventario') ->Insert([
            'id_inventario'=>32,
            'cantidad'=>104,
            'cantMaxima'=>12035,
            'cantMin'=>20,
            'entrada'=>0,
            'salida'=>0,
            'bloqueado'=>12,
            'disponible'=>92,
            'id_ubicacion'=>1,
            'idAlmacen'=>2,
          'estadoinventario'=>1,
            'id_Producto'=>10
        ]);
         DB::table('inventario') ->Insert([
            'id_inventario'=>33,
            'cantidad'=>150,
            'cantMaxima'=>125,
            'cantMin'=>10,
            'entrada'=>0,
            'salida'=>0,
            'bloqueado'=>12,
            'disponible'=>138,
            'id_ubicacion'=>1,
            'idAlmacen'=>2,
          'estadoinventario'=>1,
            'id_Producto'=>11
        ]);
         DB::table('inventario') ->Insert([
            'id_inventario'=>34,
            'cantidad'=>200,
            'cantMaxima'=>185,
            'cantMin'=>50,
            'entrada'=>0,
            'salida'=>0,
            'bloqueado'=>6,
            'disponible'=>194,
            'id_ubicacion'=>1,
            'idAlmacen'=>2,
          'estadoinventario'=>1,
            'id_Producto'=>12
        ]);
         DB::table('inventario') ->Insert([
            'id_inventario'=>35,
            'cantidad'=>410,
            'cantMaxima'=>12035,
            'cantMin'=>20,
            'entrada'=>0,
            'salida'=>0,
            'bloqueado'=>23,
            'disponible'=>387,
            'id_ubicacion'=>1,
            'idAlmacen'=>2,
          'estadoinventario'=>1,
            'id_Producto'=>13
        ]);
        
         DB::table('inventario') ->Insert([
            'id_inventario'=>36,
            'cantidad'=>410,
            'cantMaxima'=>12035,
            'cantMin'=>20,
            'entrada'=>0,
            'salida'=>0,
            'bloqueado'=>9,
            'disponible'=>401,
            'id_ubicacion'=>1,
            'idAlmacen'=>2,
          'estadoinventario'=>1,
            'id_Producto'=>14
        ]);
        
         DB::table('inventario') ->Insert([
            'id_inventario'=>37,
            'cantidad'=>15,
            'cantMaxima'=>125,
            'cantMin'=>10,
            'entrada'=>0,
            'salida'=>0,
            'bloqueado'=>0,
            'disponible'=>15,
            'id_ubicacion'=>1,
            'idAlmacen'=>2,
          'estadoinventario'=>1,
            'id_Producto'=>15
        ]);
         DB::table('inventario') ->Insert([
            'id_inventario'=>38,
            'cantidad'=>200,
            'cantMaxima'=>185,
            'cantMin'=>50,
            'entrada'=>0,
            'salida'=>0,
            'bloqueado'=>0,
            'disponible'=>200,
            'id_ubicacion'=>1,
            'idAlmacen'=>2,
          'estadoinventario'=>1,
            'id_Producto'=>16
        ]);
        DB::table('inventario') ->Insert([
            'id_inventario'=>39,
            'cantidad'=>200,
            'cantMaxima'=>185,
            'cantMin'=>50,
            'entrada'=>0,
            'salida'=>0,
            'bloqueado'=>0,
            'disponible'=>200,
            'id_ubicacion'=>1,
            'idAlmacen'=>1,
          'estadoinventario'=>1,
            'id_Producto'=>17
        ]);
        DB::table('inventario') ->Insert([
            'id_inventario'=>40,
            'cantidad'=>200,
            'cantMaxima'=>185,
            'cantMin'=>50,
            'entrada'=>0,
            'salida'=>0,
            'bloqueado'=>0,
            'disponible'=>200,
            'id_ubicacion'=>1,
            'idAlmacen'=>2,
          'estadoinventario'=>1,
            'id_Producto'=>18
        ]);
        DB::table('inventario') ->Insert([
            'id_inventario'=>41,
            'cantidad'=>200,
            'cantMaxima'=>185,
            'cantMin'=>50,
            'entrada'=>0,
            'salida'=>0,
            'bloqueado'=>0,
            'disponible'=>200,
            'id_ubicacion'=>1,
            'idAlmacen'=>2,
          'estadoinventario'=>1,
            'id_Producto'=>19
        ]);
        DB::table('inventario') ->Insert([
            'id_inventario'=>42,
            'cantidad'=>200,
            'cantMaxima'=>185,
            'cantMin'=>50,
            'entrada'=>0,
            'salida'=>0,
            'bloqueado'=>0,
            'disponible'=>200,
            'id_ubicacion'=>1,
            'idAlmacen'=>1,
          'estadoinventario'=>1,
            'id_Producto'=>20
        ]);
        DB::table('inventario') ->Insert([
            'id_inventario'=>43,
            'cantidad'=>200,
            'cantMaxima'=>185,
            'cantMin'=>50,
            'entrada'=>0,
            'salida'=>0,
            'bloqueado'=>0,
            'disponible'=>200,
            'id_ubicacion'=>1,
            'idAlmacen'=>2,
          'estadoinventario'=>1,
            'id_Producto'=>21
        ]);
        DB::table('inventario') ->Insert([
            'id_inventario'=>44,
            'cantidad'=>200,
            'cantMaxima'=>185,
            'cantMin'=>50,
            'entrada'=>0,
            'salida'=>0,
            'bloqueado'=>0,
            'disponible'=>200,
            'id_ubicacion'=>1,
            'idAlmacen'=>2,
          'estadoinventario'=>1,
            'id_Producto'=>22
        ]);
        
        
        
        
        
        
        
        
        


    }
}
