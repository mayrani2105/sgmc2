<?php

use Illuminate\Database\Seeder;

class estado extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('status')->insert([
     'idEstado'=>1,
     'status'=>'Disponible',
     'descripcionEstado'=>'Producto esta al cien'
   ]);
   DB::table('status')->insert([
  'idEstado'=>2,
  'status'=>'Baja',
  'descripcionEstado'=>'Producto esta dado de baja'
]);
    }
}
