<?php

use Illuminate\Database\Seeder;

class privi extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
DB::table('tipomovimiento')->insert([
   'descripcion'=>'Entrada'
   ]);
DB::table('tipomovimiento')->insert([
   'descripcion'=>'Modificacion'
]);
DB::table('tipomovimiento')->insert([
  'descripcion'=>'Eliminacion'
]);
DB::table('tipomovimiento')->insert([
 'descripcion'=>'Creacion'
]);
DB::table('tipomovimiento')->insert([
'descripcion'=>'Salida'
]);
DB::table('tipo_notificacion')->insert([
'idtipo_n'=>1,
'descripcion'=>"Orden de compra por surtir"
]);
DB::table('tipo_notificacion')->insert([
'idtipo_n'=>2,
'descripcion'=>"Orden de compra que voy a recibir"
]);
DB::table('tipo_notificacion')->insert([
'idtipo_n'=>3,
'descripcion'=>"Orden de compra fallida,u incompleta"
]);

    }
}
