<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $this->call(Sucursales::class);
        $this->call(Estados::class);
        $this->call(UsersTableSeeder::class);
        $this->call(roles::class);
        $this->call(Meses::class);
        $this->call(PosiblesEstados::class);
        $this->call(ClaveSAT::class);
        $this->call(Proveedores::class);
        $this->call(Marcas::class);
        $this->call(Almacenes::class);
        $this->call(Categoria::class);
        $this->call(Estado_producto::class);
        $this->call(Ubicacion::class);
        $this->call(Productos::class);
        $this->call(Inventario::class);
        $this->call(estado::class);
        $this->call(TipoPedido::class);
        $this->call(StatusPedido::class);
        $this->call(Pedido::class);
        $this->call(privi::class);
        $this->call(PedidoProductos::class);
        $this->call(UsuarioPedido::class);



    }
}
