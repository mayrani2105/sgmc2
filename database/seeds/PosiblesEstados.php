<?php

use Illuminate\Database\Seeder;

class PosiblesEstados extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posibles_estados')->insert([
       'id_Posibles_Estados'=>1,
       'descripcion'=>'Apartado'
     ]);
        DB::table('posibles_estados')->insert([
       'id_Posibles_Estados'=>2,
       'descripcion'=>'En transito'
     ]);
        DB::table('posibles_estados')->insert([
       'id_Posibles_Estados'=>3,
       'descripcion'=>'Disponible'
     ]);

    }
}
