<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//se crea usuario administrador
DB::table('users')->insert([
'id'=>1,
'name' => 'administrador',
'email' => 'admin@admin.com',
'email_verified_at'=>null,
'password' => bcrypt('admin'),
'remember_token'=>null,
'usuario'=>'Administrador1',
'aPaterno'=>'palido',
'aMaterno'=>'gatos',
'calle'=>'el puesto',
'colonia'=>'lejano',
'ciudad'=>'lejos',
'estado'=>'cercano',
'permisos'=>'111111111111111101',
'estadousuario'=>'1',
'id_rol'=>'2',
'id_sucursal'=>1
]);
//se crea jefe de Operaciones
DB::table('users')->insert([
'id'=>2,
'name' => 'JefeOperaciones1',
'email' => 'spolloman@hotmail.com',
'email_verified_at'=>null,
'password' => bcrypt('admin'),
'remember_token'=>null,
'usuario'=>'Operaciones1',
'aPaterno'=>'palido',
'aMaterno'=>'gatos',
'calle'=>'el puesto',
'colonia'=>'lejano',
'ciudad'=>'lejos',
'estado'=>'cercano',
'permisos'=>'011111111011111111',
'estadousuario'=>'1',
'id_rol'=>'1',
'id_sucursal'=>1]);
//Se crea Jefe de Almacen
DB::table('users')->insert([
'id'=>9,
'name' => 'JefeAlmacen',
'email' => 'jefe@hotmail.com',
'email_verified_at'=>null,
'password' => bcrypt('admin'),
'remember_token'=>null,
'usuario'=>'Almacen1',
'aPaterno'=>'palido',
'aMaterno'=>'gatos',
'calle'=>'el puesto',
'colonia'=>'lejano',
'ciudad'=>'lejos',
'estado'=>'cercano',
'permisos'=>'011111101001010101',
'estadousuario'=>'1',
'id_rol'=>'3',
'id_sucursal'=>1]);
DB::table('users')->insert([
'id'=>10,
'name' => 'JefeAlmacen3',
'email' => 'jefe3@hotmail.com',
'email_verified_at'=>null,
'password' => bcrypt('admin'),
'remember_token'=>null,
'usuario'=>'Almacen3',
'aPaterno'=>'palido',
'aMaterno'=>'gatos',
'calle'=>'el puesto',
'colonia'=>'lejano',
'ciudad'=>'lejos',
'estado'=>'cercano',
'permisos'=>'011111101001010101',
'estadousuario'=>'1',
'id_rol'=>'3',
'id_sucursal'=>2]);
DB::table('users')->insert([
'id'=>3,
'name' => 'JefeAl2',
'email' => 'jefe23@hotmail.com',
'email_verified_at'=>null,
'password' => bcrypt('admin'),
'remember_token'=>null,
'usuario'=>'Almacen2',
'aPaterno'=>'palido',
'aMaterno'=>'gatos',
'calle'=>'el puesto',
'colonia'=>'lejano',
'ciudad'=>'lejos',
'estado'=>'cercano',
'permisos'=>'011111101001010101',
'estadousuario'=>'1',
'id_rol'=>'3',
'id_sucursal'=>2]);
// se crea usuario Distribuidora
DB::table('users')->insert([
'id'=>4,
'name' => 'Distribuidor',
'email' => 'Distribuidor@hotmail.com',
'email_verified_at'=>null,
'password' => bcrypt('admin'),
'remember_token'=>null,
'usuario'=>'Distribuidor1',
'aPaterno'=>'palido',
'aMaterno'=>'gatos',
'calle'=>'el puesto',
'colonia'=>'lejano',
'ciudad'=>'lejos',
'estado'=>'cercano',
'permisos'=>'0111100010100101011',
'estadousuario'=>'1',
'id_rol'=>'4',
'id_sucursal'=>1]);
// se crea usuario checador
DB::table('users')->insert([
'id'=>5,
'name' => 'checador',
'email' => 'checa@hotmail.com',
'email_verified_at'=>null,
'password' => bcrypt('admin'),
'remember_token'=>null,
'usuario'=>'checa1',
'aPaterno'=>'palido',
'aMaterno'=>'gatos',
'calle'=>'el puesto',
'colonia'=>'lejano',
'ciudad'=>'lejos',
'estado'=>'cercano',
'permisos'=>'000000001010010101',
'estadousuario'=>'1',
'id_rol'=>'5',
'id_sucursal'=>1]);
// se crea usuario Compras
DB::table('users')->insert([
'id'=>6,
'name' => 'compras',
'email' => 'compra@hotmail.com',
'email_verified_at'=>null,
'password' => bcrypt('admin'),
'remember_token'=>null,
'usuario'=>'compra1',
'aPaterno'=>'palido',
'aMaterno'=>'gatos',
'calle'=>'el puesto',
'colonia'=>'lejano',
'ciudad'=>'lejos',
'estado'=>'cercano',
'permisos'=>'011111101100010101',
'estadousuario'=>'1',
'id_rol'=>'6',
'id_sucursal'=>1]);
// se creea usuario Ventas
DB::table('users')->insert([
'id'=>7,
'name' => 'ventas',
'email' => 'venta@hotmail.com',
'email_verified_at'=>null,
'password' => bcrypt('admin'),
'remember_token'=>null,
'usuario'=>'venta1',
'aPaterno'=>'palido',
'aMaterno'=>'gatos',
'calle'=>'el puesto',
'colonia'=>'lejano',
'ciudad'=>'lejos',
'estado'=>'cercano',
'permisos'=>'000110001110100101',
'estadousuario'=>'1',
'id_rol'=>'6',
'id_sucursal'=>1]);

    }
}
