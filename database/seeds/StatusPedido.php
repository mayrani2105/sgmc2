<?php

use Illuminate\Database\Seeder;
class StatusPedido extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('status_pedido')->insert([
       'idStatusPedido'=>1,
       'descripcion'=>'En captura'
     ]);
        
           DB::table('status_pedido')->insert([
       'idStatusPedido'=>2,
       'descripcion'=>'Creado'
     ]);
        
        DB::table('status_pedido')->insert([
       'idStatusPedido'=>3,
       'descripcion'=>'Exitoso'
     ]);
        
        DB::table('status_pedido')->insert([
       'idStatusPedido'=>4,
       'descripcion'=>'Cancelado'
     ]);
        
           DB::table('status_pedido')->insert([
       'idStatusPedido'=>5,
       'descripcion'=>'En espera'
     ]);
         
    

    }
}
