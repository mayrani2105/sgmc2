<?php

use Illuminate\Database\Seeder;

class Categoria extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('categoria')->insert([
       'idCategoria'=>1,
       'descripcion'=>'Arte'
     ]);
     DB::table('subcategoria')->insert([
   'idSubCategoria'=>1,
   'descripcion'=>'Accesorios de arte',
    'idCategoria'=>1
  ]);
  DB::table('subcategoria')->insert([
'idSubCategoria'=>2,
'descripcion'=>'Acuarelas',
 'idCategoria'=>1
]);

DB::table('subcategoria')->insert([
'idSubCategoria'=>3,
'descripcion'=>'Bastidores',
'idCategoria'=>1
]);

DB::table('subcategoria')->insert([
'idSubCategoria'=>4,
'descripcion'=>'Caballetes',
'idCategoria'=>1
]);
DB::table('subcategoria')->insert([
'idSubCategoria'=>5,
'descripcion'=>'Carbon',
'idCategoria'=>1
]);
DB::table('subcategoria')->insert([
'idSubCategoria'=>6,
'descripcion'=>'Colores',
'idCategoria'=>1
]);
DB::table('subcategoria')->insert([
'idSubCategoria'=>7,
'descripcion'=>'Estilografos',
'idCategoria'=>1
]);
DB::table('subcategoria')->insert([
'idSubCategoria'=>8,
'descripcion'=>'Lienzos',
'idCategoria'=>1
]);
DB::table('subcategoria')->insert([
'idSubCategoria'=>9,
'descripcion'=>'Pinceles',
'idCategoria'=>1
]);
DB::table('subcategoria')->insert([
'idSubCategoria'=>10,
'descripcion'=>'Pintura',
'idCategoria'=>1
]);
DB::table('subcategoria')->insert([
'idSubCategoria'=>11,
'descripcion'=>'Rodetes',
'idCategoria'=>1
]);
DB::table('subcategoria')->insert([
'idSubCategoria'=>12,
'descripcion'=>'Rotuladores',
'idCategoria'=>1
]);
DB::table('subcategoria')->insert([
'idSubCategoria'=>13,
'descripcion'=>'Tizas',
'idCategoria'=>1
]);


        DB::table('categoria')->insert([
       'idCategoria'=>2,
       'descripcion'=>'Articulos de Limpieza'
     ]);

     DB::table('subcategoria')->insert([
  // 'idSubCategoria'=>2,
   'descripcion'=>'art varios',
    'idCategoria'=>2
  ]);
  DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'Desechables',
 'idCategoria'=>2
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'gel Antibacterial',
'idCategoria'=>2
]);




        DB::table('categoria')->insert([
       'idCategoria'=>3,
       'descripcion'=>'Articulos Escolares'
     ]);
     DB::table('subcategoria')->insert([
  // 'idSubCategoria'=>2,
   'descripcion'=>'Art Preescolar',
    'idCategoria'=>3
  ]);
  DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'Borradores',
 'idCategoria'=>3
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'Gomas',
'idCategoria'=>3
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'Juegos de Geometria',
'idCategoria'=>3
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'Reglas',
'idCategoria'=>3
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'Sacapuntas',
'idCategoria'=>3
]);
// Categoria 4
        DB::table('categoria')->insert([
       'idCategoria'=>4,
       'descripcion'=>'Articulos Temporales'
     ]);
     DB::table('subcategoria')->insert([
  // 'idSubCategoria'=>2,
   'descripcion'=>'Agendas',
    'idCategoria'=>4
  ]);
  DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'Art de muertos',
 'idCategoria'=>4
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'art fiestas patrias',
'idCategoria'=>4
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'art navideños',
'idCategoria'=>4
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'art san valentin',
'idCategoria'=>4
]);



//categoria 5
DB::table('categoria')->insert([
'idCategoria'=>5,
'descripcion'=>'Computo y consumibles'
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'accesorios computo',
'idCategoria'=>5
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'calculadoras',
'idCategoria'=>5
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'cartucho de tinta',
'idCategoria'=>5
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'cintas para equipos',
'idCategoria'=>5
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'discos',
'idCategoria'=>5
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'equipos',
'idCategoria'=>5
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'memorias',
'idCategoria'=>5
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'pilas',
'idCategoria'=>5
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'Toner',
'idCategoria'=>5
]);

//categoria 6
DB::table('categoria')->insert([
'idCategoria'=>6,
'descripcion'=>'Cuadernos'
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'blocks',
'idCategoria'=>6
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'cuaderno collage',
'idCategoria'=>6
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'cuaderno frances',
'idCategoria'=>6
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'cuaderno italiano',
'idCategoria'=>6
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'cuaderno prof economico',
'idCategoria'=>6
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'cuaderno profesional',
'idCategoria'=>6
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'memorias',
'idCategoria'=>5
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'cuaderno especiales',
'idCategoria'=>6
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'cuaderno preescolar',
'idCategoria'=>6
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'forros para cuaderno',
'idCategoria'=>6
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'libretas ejecutivas',
'idCategoria'=>6
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'repuestos de hoja',
'idCategoria'=>6
]);

//CATEGORIA 7
DB::table('categoria')->insert([
'idCategoria'=>7,
'descripcion'=>'Encuadernacion'
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'arillos',
'idCategoria'=>7
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'contac',
'idCategoria'=>7
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'engargoladoras',
'idCategoria'=>7
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'enmicadoras',
'idCategoria'=>7
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'forros',
'idCategoria'=>7
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'guillotinas',
'idCategoria'=>7
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'micass',
'idCategoria'=>7
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'pastas p/engargolar',
'idCategoria'=>7
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'plastico cristal',
'idCategoria'=>7
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'postes de aluminio',
'idCategoria'=>7
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'refuerzos para hoja',
'idCategoria'=>7
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'trituradoras de papel',
'idCategoria'=>7
]);
// Categoria 8

DB::table('categoria')->insert([
'idCategoria'=>8,
'descripcion'=>'Escritura'
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'boligrafos',
'idCategoria'=>8
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'boligrafos finos',
'idCategoria'=>8
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'colores',
'idCategoria'=>8
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'lapiz de colores',
'idCategoria'=>8
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'crayones',
'idCategoria'=>8
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'marcador de agua',
'idCategoria'=>8
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'marcador industrial',
'idCategoria'=>8
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'marcador permanente',
'idCategoria'=>8
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'marcador pintarron',
'idCategoria'=>8
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'marcador vidrio',
'idCategoria'=>8
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'marcatextos',
'idCategoria'=>8
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'minas',
'idCategoria'=>8
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'repuestos de escritura',
'idCategoria'=>8
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'portaminas',
'idCategoria'=>8
]);
//categoria 9

DB::table('categoria')->insert([
'idCategoria'=>9,
'descripcion'=>'Formas'
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'cajas de empaque',
'idCategoria'=>9
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'formas contables',
'idCategoria'=>9
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'libros contables',
'idCategoria'=>9
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'rollos',
'idCategoria'=>9
]);

// categoria 10

DB::table('categoria')->insert([
'idCategoria'=>10,
'descripcion'=>'Manualidades merceria y fiesta'
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'adornos',
'idCategoria'=>10
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'art de corte y confección',
'idCategoria'=>10
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'bolsa y cajas de regalo',
'idCategoria'=>10
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'calcomanias',
'idCategoria'=>10
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'desechables fiesta',
'idCategoria'=>10
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'foamy',
'idCategoria'=>10
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'globos',
'idCategoria'=>10
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'invitaciones',
'idCategoria'=>10
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'material de repujado',
'idCategoria'=>10
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'merceria',
'idCategoria'=>10
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'moños y listones',
'idCategoria'=>10
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'pelotas',
'idCategoria'=>10
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'pistola silicon',
'idCategoria'=>10
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'plastilina y masas',
'idCategoria'=>10
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'plumas',
'idCategoria'=>10
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'pvc',
'idCategoria'=>10
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'rafias',
'idCategoria'=>10
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'silicon',
'idCategoria'=>10
]);
DB::table('subcategoria')->insert([
// 'idSubCategoria'=>2,
'descripcion'=>'unicel',
'idCategoria'=>10
]);
    }
}
