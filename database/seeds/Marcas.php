<?php

use Illuminate\Database\Seeder;

class Marcas extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('marca')->insert([
       'idMarca'=>1,
       'marca'=>'Norma',
        'idProveedor'=>1
     ]);
     DB::table('marca')->insert([
    'idMarca'=>2,
    'marca'=>'ZEB',
     'idProveedor'=>2
  ]);
    DB::table('marca')->insert([
   'idMarca'=>3,
   'marca'=>'Bic',
    'idProveedor'=>3
 ]);
 DB::table('marca')->insert([
'idMarca'=>4,
'marca'=>'Faber Caste',
 'idProveedor'=>5
]);
DB::table('marca')->insert([
'idMarca'=>5,
'marca'=>'ROD',
'idProveedor'=>4
]);
DB::table('marca')->insert([
'idMarca'=>6,
'marca'=>'Pil',
'idProveedor'=>6
]);

}}
