<?php

use Illuminate\Database\Seeder;

class Pedido extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //De entradas
        DB::table('pedido')->insert([
        'idPedido'=>1,
        'folio'=>'P1',
        'origen'=>'Sucursal A',
        'destino'=>'Sucursal B',
        'created_at'=>'2019-01-25 14:00:00',
        'updated_at'=>null,
        'idStatusPedido'=>1,
        'id_Tipo_Pedido'=>1
      ]);

         DB::table('pedido')->insert([
        'idPedido'=>2,
        'folio'=>'P2',
        'origen'=>'Sucursal A',
        'destino'=>'Sucursal B',
        'created_at'=>'2019-02-25 14:00:00',
        'updated_at'=>null,
        'idStatusPedido'=>2,
        'id_Tipo_Pedido'=>1
      ]);

         DB::table('pedido')->insert([
        'idPedido'=>3,
        'folio'=>'P3',
        'origen'=>'Sucursal A',
        'destino'=>'Sucursal B',
        'created_at'=>'2019-03-25 14:00:00',
        'updated_at'=>null,
        'idStatusPedido'=>3,
        'id_Tipo_Pedido'=>1
      ]);

         DB::table('pedido')->insert([
        'idPedido'=>4,
        'folio'=>'P4',
        'origen'=>'Sucursal A',
        'destino'=>'Sucursal B',
        'created_at'=>'2019-04-25 14:00:00',
        'updated_at'=>null,
        'idStatusPedido'=>4,
        'id_Tipo_Pedido'=>1
      ]);
        
        

         DB::table('pedido')->insert([
        'idPedido'=>5,
        'folio'=>'P5',
        'origen'=>'Sucursal A',
        'destino'=>'Sucursal B',
        'created_at'=>'2019-05-25 14:00:00',
        'updated_at'=>null,
        'idStatusPedido'=>1,
        'id_Tipo_Pedido'=>1
      ]);

         DB::table('pedido')->insert([
        'idPedido'=>6,
        'folio'=>'P6',
        'origen'=>'Sucursal A',
        'destino'=>'Sucursal B',
        'created_at'=>'2019-06-25 14:00:00',
        'updated_at'=>null,
        'idStatusPedido'=>2,
        'id_Tipo_Pedido'=>1
      ]);

         DB::table('pedido')->insert([
        'idPedido'=>7,
        'folio'=>'P7',
        'origen'=>'Sucursal A',
        'destino'=>'Sucursal B',
        'created_at'=>'2019-07-25 14:00:00',
        'updated_at'=>null,
        'idStatusPedido'=>3,
        'id_Tipo_Pedido'=>1
      ]);

         DB::table('pedido')->insert([
        'idPedido'=>8,
        'folio'=>'P8',
        'origen'=>'Sucursal A',
        'destino'=>'Sucursal B',
        'created_at'=>'2019-08-25 14:00:00',
        'updated_at'=>null,
        'idStatusPedido'=>4,
        'id_Tipo_Pedido'=>1
      ]);
        
         //De salidas origen B

         DB::table('pedido')->insert([
        'idPedido'=>9,
        'folio'=>'P9',
        'origen'=>'Sucursal B',
        'destino'=>'Sucursal A',
        'created_at'=>'2019-09-25 14:00:00',
        'updated_at'=>null,
        'idStatusPedido'=>5,
        'id_Tipo_Pedido'=>2
      ]);

         DB::table('pedido')->insert([
        'idPedido'=>10,
        'folio'=>'P10',
        'origen'=>'Sucursal B',
        'destino'=>'Cliente B',
        'created_at'=>'2019-10-25 14:00:00',
        'updated_at'=>null,
        'idStatusPedido'=>5,
        'id_Tipo_Pedido'=>2
      ]);

         DB::table('pedido')->insert([
        'idPedido'=>11,
        'folio'=>'P11',
        'origen'=>'Sucursal B',
        'destino'=>'Cliente A',
        'created_at'=>'2019-11-25 14:00:00',
        'updated_at'=>null,
        'idStatusPedido'=>5,
        'id_Tipo_Pedido'=>2
      ]);

         DB::table('pedido')->insert([
        'idPedido'=>12,
        'folio'=>'P12',
        'created_at'=>'2019-01-25 14:00:00',
        'updated_at'=>null,
        'origen'=>'Sucursal B',
        'destino'=>'Sucursal A',
        'idStatusPedido'=>5,
        'id_Tipo_Pedido'=>2
      ]);

         DB::table('pedido')->insert([
        'idPedido'=>13,
        'folio'=>'P13',
        'origen'=>'Sucursal B',
        'destino'=>'Cliente B',
        'created_at'=>'2019-02-25 14:00:00',
        'updated_at'=>null,
        'idStatusPedido'=>5,
        'id_Tipo_Pedido'=>2
      ]);

         DB::table('pedido')->insert([
        'idPedido'=>14,
        'folio'=>'P14',
        'origen'=>'Sucursal B',
        'destino'=>'Cliente A',
        'created_at'=>'2019-03-25 14:00:00',
        'updated_at'=>null,
        'idStatusPedido'=>5,
        'id_Tipo_Pedido'=>2
      ]);

         DB::table('pedido')->insert([
        'idPedido'=>15,
        'folio'=>'P15',
        'created_at'=>'2019-04-25 14:00:00',
        'updated_at'=>null,
        'origen'=>'Sucursal B',
        'destino'=>'Sucursal A',
        'idStatusPedido'=>1,
        'id_Tipo_Pedido'=>1
      ]);
        
        //De salidas origen B

         DB::table('pedido')->insert([
        'idPedido'=>16,
        'folio'=>'P16',
        'created_at'=>'2019-05-25 14:00:00',
        'updated_at'=>null,
        'origen'=>'Sucursal A',
        'destino'=>'Cliente B',
        'idStatusPedido'=>5,
        'id_Tipo_Pedido'=>2
      ]);

         DB::table('pedido')->insert([
        'idPedido'=>17,
        'folio'=>'P17',
        'origen'=>'Sucursal A',
        'destino'=>'Cliente B',
        'created_at'=>'2019-06-25 14:00:00',
        'updated_at'=>null,
        'idStatusPedido'=>5,
        'id_Tipo_Pedido'=>2
      ]);

         DB::table('pedido')->insert([
        'idPedido'=>18,
        'folio'=>'P18',
        'origen'=>'Sucursal B',
        'destino'=>'Sucursal A',
        'created_at'=>'2019-07-25 14:00:00',
        'updated_at'=>null,
        'idStatusPedido'=>5,
        'id_Tipo_Pedido'=>2
      ]);

         DB::table('pedido')->insert([
        'idPedido'=>19,
        'folio'=>'P19',
        'origen'=>'Sucursal A',
        'destino'=>'Sucursal B',
        'created_at'=>'2019-08-25 14:00:00',
        'updated_at'=>null,
        'idStatusPedido'=>5,
        'id_Tipo_Pedido'=>2
      ]);
        
                 DB::table('pedido')->insert([
        'idPedido'=>20,
        'folio'=>'P20',
        'origen'=>'Sucursal A',
        'destino'=>'Sucursal B',
        'created_at'=>'2019-09-25 14:00:00',
        'updated_at'=>null,
        'idStatusPedido'=>5,
        'id_Tipo_Pedido'=>2
      ]);
        
         DB::table('pedido')->insert([
        'idPedido'=>21,
        'folio'=>'P21',
        'origen'=>'Sucursal A',
        'destino'=>'Cliente B',
        'created_at'=>'2019-10-25 14:00:00',
        'updated_at'=>null,
        'idStatusPedido'=>5,
        'id_Tipo_Pedido'=>2
      ]);

         DB::table('pedido')->insert([
        'idPedido'=>22,
        'folio'=>'P22',
        'origen'=>'Sucursal A',
        'destino'=>'Sucursal B',
        'created_at'=>'2019-11-25 14:00:00',
        'updated_at'=>null,
        'idStatusPedido'=>5,
        'id_Tipo_Pedido'=>2
      ]);

    }
}
