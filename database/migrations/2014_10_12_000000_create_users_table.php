<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
         Schema::disableForeignKeyConstraints();
            $table->Increments('id');
            $table->string('name');
            $table->string('email',90)->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
            $table->string('usuario',50);
            $table->string('aPaterno',50);
            $table->string('aMaterno',50);
            $table->string('calle',50);
            $table->string('colonia',50);
            $table->string('ciudad',50);
            $table->string('estado',50);
            $table->string("permisos",50);
            $table->binary('estadousuario');
            $table->unsignedInteger("id_rol");
            $table->unsignedInteger('id_sucursal');
            $table->foreign('id_sucursal')->references('id_sucursal')->on('sucursal');
            $table->foreign('id_rol')->references('id_rol')->on('rol');

            //no se si el usuario debe estar asociado a la sucursal


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      DB::statement('SET FOREIGN_KEY_CHECKS = 0');
  Schema::dropIfExists('users');
  DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
