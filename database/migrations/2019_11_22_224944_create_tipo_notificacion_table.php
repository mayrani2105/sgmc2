<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTipoNotificacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipo_notificacion', function (Blueprint $table) {
            Schema::disableForeignKeyConstraints();
            $table->Increments('idtipo_n');
            $table->string('Descripcion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

      DB::statement('SET FOREIGN_KEY_CHECKS = 0');
    Schema::dropIfExists('tipo_notificacion');
    DB::statement('SET FOREIGN_KEY_CHECKS = 1');

    }
}
