<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubcategoriaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subcategoria', function (Blueprint $table) {
            Schema::disableForeignKeyConstraints();
            $table->Increments('idSubCategoria');
            $table->string('descripcion',120);
            $table->unsignedInteger('idCategoria');
            $table->foreign('idCategoria')->references('idCategoria')->on('categoria');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      DB::statement('SET FOREIGN_KEY_CHECKS = 0');
 Schema::dropIfExists('subcategoria');
 DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
