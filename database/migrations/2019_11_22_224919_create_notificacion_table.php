<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notificacions', function (Blueprint $table) {
            $table->Increments('idNotificacion');
            $table->string('origen',120);
            $table->string('destino',120);
            $table->unsignedInteger('idtipo_n');
            $table->unsignedInteger('idPedido');
            $table->foreign('idPedido')->references('idPedido')->on('pedido');
            $table->foreign('idtipo_n')->references('idtipo_n')->on('tipo_notificacion');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

  Schema::dropIfExists('notificacions');

    }
}
