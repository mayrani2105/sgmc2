<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePedidoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedido', function (Blueprint $table) {
            Schema::disableForeignKeyConstraints();
            $table->Increments('idPedido');
            $table->string('folio',50);
            $table->string('origen',120);
            $table->string('destino',120);
            $table->timestamps();
            $table->unsignedInteger('idStatusPedido');
            $table->unsignedInteger('id_Tipo_Pedido');
            $table->foreign('id_Tipo_Pedido')->references('id_Tipo_Pedido')->on('tipo_pedido');
            $table->foreign('idStatusPedido')->references('idStatusPedido')->on('status_pedido');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      DB::statement('SET FOREIGN_KEY_CHECKS = 0');
  Schema::dropIfExists('pedido');
  DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
