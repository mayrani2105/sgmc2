<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFuncionesrolTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('funcionesrol', function (Blueprint $table) {
            Schema::disableForeignKeyConstraints();
            $table->unsignedInteger('id_rol');
            $table->unsignedInteger('id_privilegio');
            $table->binary('ver');
            $table->binary('editar');
            $table->binary('borrar');
            $table->foreign('id_rol')->references('id_rol')->on('rol');
            $table->foreign('id_privilegio')->references('id_privilegio')->on('privilegios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('funcionesrol');
    }
}
