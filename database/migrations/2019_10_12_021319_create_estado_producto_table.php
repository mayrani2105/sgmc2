<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstadoProductoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estado_producto', function (Blueprint $table) {
  Schema::disableForeignKeyConstraints();
            $table->Increments('id_EstadoProducto');
            $table->integer('cantidad');
            $table->unsignedInteger('id_Producto');
            $table->unsignedInteger('id_Posibles_Estados');
            $table->foreign('id_Producto')->references('id_Producto')->on('producto');

$table->foreign('id_Posibles_Estados')->references('id_Posibles_Estados')->on('posibles_estados');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
    Schema::dropIfExists('estado_producto');
    DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
