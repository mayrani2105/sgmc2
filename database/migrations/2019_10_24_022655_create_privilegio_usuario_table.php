<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrivilegioUsuarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('privilegio_usuario', function (Blueprint $table) {
            Schema::disableForeignKeyConstraints();
            $table->Increments('id_priviuser');
            $table->unsignedInteger('id_usuario');
            $table->unsignedInteger('id_privilegio');
            $table->binary('ver');
            $table->binary('editar');
            $table->binary('borrar');
            $table->foreign('id_usuario')->references('id')->on('users');
            $table->foreign('id_privilegio')->references('id_privilegio')->on('privilegios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('privilegio_usuario');
    }
}
