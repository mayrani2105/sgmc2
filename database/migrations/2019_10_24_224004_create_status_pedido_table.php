<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStatusPedidoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('status_pedido', function (Blueprint $table) {
            $table->Increments('idStatusPedido');
            $table->string('descripcion',110);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      DB::statement('SET FOREIGN_KEY_CHECKS = 0');
  Schema::dropIfExists('status_pedido');
  DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
