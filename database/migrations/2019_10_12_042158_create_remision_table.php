<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRemisionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('remision', function (Blueprint $table) {
          Schema::disableForeignKeyConstraints();
            $table->Increments('idRemision');
            $table->string('rfc',15);
            $table->DateTime('fecha');
            $table->unsignedInteger('idPedido');
        $table->foreign('idPedido')->references('idPedido')->on('pedido');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('remision');
    }
}
