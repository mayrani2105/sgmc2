<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlmacenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('almacen', function (Blueprint $table) {
            $table->Increments('idAlmacen');
            $table->string('descripcion',100);
            $table->string('calle',150);
            $table->string('colonia',100);
            $table->unsignedInteger('id_sucursal');
            $table->unsignedInteger('id_Estados');
            $table->foreign('id_Estados')->references('id_Estados')->on('estados');
            $table->foreign('id_sucursal')->references('id_sucursal')->on('sucursal');
  Schema::disableForeignKeyConstraints();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('almacen');
    }
}
