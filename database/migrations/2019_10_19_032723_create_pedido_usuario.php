<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePedidoUsuario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedido_usuario', function (Blueprint $table) {
          Schema::disableForeignKeyConstraints();
          $table->unsignedInteger('idPedido');
          $table->unsignedInteger('id_usuario');
          $table->foreign('id_usuario')->references('id')->on('users');
          $table->foreign('idPedido')->references('idPedido')->on('pedido');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedido_usuario');
    }
}
