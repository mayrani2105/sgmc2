<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('producto', function (Blueprint $table) {
Schema::disableForeignKeyConstraints();
            $table->Increments('id_Producto');
            $table->string('descripcion',200);
            $table->string('unidadbase',20);
            $table->float('costo');
            $table->float('precioreal');
            $table->string('clave',60);

            $table->unsignedInteger('idMarca');
            $table->unsignedInteger('idsat');

            $table->unsignedInteger('idSubCategoria');
            $table->unsignedInteger('idEstado');

  $table->foreign('idMarca')->references('idMarca')->on('marca');
  $table->foreign('idsat')->references('idsat')->on('clavesats');

  $table->foreign('idSubCategoria')->references('idSubCategoria')->on('subcategoria');
  $table->foreign('idEstado')->references('idEstado')->on('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
    Schema::dropIfExists('producto');
    DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
