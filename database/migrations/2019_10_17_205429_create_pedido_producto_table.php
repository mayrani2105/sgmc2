<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePedidoProductoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedido_producto', function (Blueprint $table) {
            Schema::disableForeignKeyConstraints();
            $table->unsignedInteger('idPedido');
            $table->integer('Cantidad');
            $table->unsignedInteger('id_Producto');
            $table->foreign('id_Producto')->references('id_Producto')->on('producto');
            $table->foreign('idPedido')->references('idPedido')->on('pedido');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedido_producto');
    }
}
