<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrivilegiosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('privilegios', function (Blueprint $table) {
  Schema::disableForeignKeyConstraints();
            $table->Increments('id_privilegio');
            $table->String('descripcion',100);
      
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
   Schema::dropIfExists('privilegios');
   DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
