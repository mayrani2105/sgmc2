<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePosiblesEstadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posibles_estados', function (Blueprint $table) {
            $table->Increments('id_Posibles_Estados');
            $table->string('descripcion',255);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
    Schema::dropIfExists('posibles_estados');
    DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
