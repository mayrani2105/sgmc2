<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProveedorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proveedor', function (Blueprint $table) {
            $table->Increments('idProveedor');
            Schema::disableForeignKeyConstraints();
            $table->integer("claveProveedor");
            $table->string("proveedor",60)->unique();
            $table->string("rfc");
            $table->char("telefono",15);
            $table->string("correo",50);
            $table->string("colonia",255);
            $table->string("Estado",255);
            $table->string("calle",255);
            $table->integer('diascredito');
            $table->integer('promociones');
            $table->string("pais",255);
            $table->binary('estadoproveedor');
            $table->string("noExterior",60);


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proveedor');
    }
}
