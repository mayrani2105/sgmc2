<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMarcaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marca', function (Blueprint $table) {
  Schema::disableForeignKeyConstraints();
            $table->Increments('idMarca');
            $table->string('marca',60)->unique();
        // $table->unique(['idMarca', 'marca']);
            $table->unsignedInteger('idProveedor');
            $table->foreign('idProveedor')->references('idProveedor')->on('proveedor');

        });
//  DB::unprepared('ALTER TABLE `marca` DROP PRIMARY KEY, ADD PRIMARY KEY (  `idMarca` ,  `marca`,`idProveedor` )');
}
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      DB::statement('SET FOREIGN_KEY_CHECKS = 0');
  Schema::dropIfExists('marca');
  DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
