<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInventarioTable extends Migration
{
    /**
     * Run the migrations.
     *	'id_ubicacion'=>1,	'idSubCategoria'=>1,
     * @return void
     */
    public function up()
    {
        Schema::create('inventario', function (Blueprint $table) {
Schema::disableForeignKeyConstraints();
            $table->Increments('id_inventario');
            $table->integer('cantidad');
            $table->integer('cantMaxima');
            $table->integer('cantMin');
            $table->binary('estadoinventario');
            $table->integer('entrada');
            $table->integer('salida');
            $table->integer('bloqueado');
            $table->integer('disponible');
            $table->unsignedInteger('idAlmacen');
            $table->unsignedInteger('id_ubicacion');
            $table->foreign('id_ubicacion')->references('id_ubicacion')->on('ubicacion');
            $table->unsignedInteger('id_Producto');
            $table->foreign('idAlmacen')->references('idAlmacen')->on('almacen');
            $table->foreign('id_Producto')->references('id_Producto')->on('producto');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      DB::statement('SET FOREIGN_KEY_CHECKS = 0');
 Schema::dropIfExists('inventario');
 DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
