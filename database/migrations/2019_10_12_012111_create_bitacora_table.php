<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBitacoraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bitacora', function (Blueprint $table) {
            Schema::disableForeignKeyConstraints();
            $table->Increments('id_bitacora');
            $table->string("datomodificado");
            $table->string("modulo");
            $table->timestamps();
            $table->unsignedInteger('id_movimiento');
            $table->unsignedInteger('id_usuario');
            $table->foreign('id_usuario')->references('id')->on('users');
            $table->foreign('id_movimiento')->references('id_movimiento')->on('tipomovimiento');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bitacora');
    }
}
