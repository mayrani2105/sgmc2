<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSucursalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sucursal', function (Blueprint $table) {
    Schema::disableForeignKeyConstraints();
            $table->Increments('id_sucursal');
            $table->string('descripcionS');
            $table->string('direccion',70);
            $table->string('colonia',50);
            $table->integer('numero');
            $table->string('ciudad',60);
            $table->unsignedInteger('id_Estados');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
    Schema::dropIfExists('sucursal');
    DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
