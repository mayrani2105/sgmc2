<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>SMGC | Panel de Jefe de Operaciones</title>

  <!-- Bootstrap core CSS -->
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet"
  href="{{ asset('assets/css/bootstrap.min.css') }}" />  <link rel="stylesheet" href="{{ asset('assets/css/simple-sidebar.css') }}" />
<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}" />
  <script src="https://kit.fontawesome.com/da3b6a12fa.js"></script>

  <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
  <script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

  <script src="https://code.highcharts.com/highcharts.js"></script>
  <script src="https://code.highcharts.com/modules/data.js"></script>
  <script src="https://code.highcharts.com/modules/drilldown.js"></script>

  <!-- Custom styles for this template -->
  <link href="/css/simple-sidebar.css" rel="stylesheet">
  <link rel="stylesheet" href="{{ asset('css/simple-sidebar.css') }}" />
  <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
  <link rel="icon" href="/favicon.ico" type="image/x-icon">
  <div class="d-flex" id="wrapper">

    <!-- Sidebar -->

    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>

    <div class=" border-right" id="sidebar-wrapper" style="  background-color:  #1b3a81;" >
      <div class="sidebar-heading bg-white"><a href=""><img src="{{ asset('assets/Images/logo.jpg') }}" style="max-height: 60px;"></a></div>
      <div class="list-group list-group-flush" id="sidebar-list">

    <a href="#multiCollapseExample2" class=" list-group-item list-group-item-action bg-transparent"id="list-element" data-toggle="collapse" data-target="#multiCollapseExample2" aria-expanded="false" aria-controls="multiCollapseExample2"><i class="fas fa-dolly"></i> Inventarios</a>
          <div class="col">
            <div class="collapse multi-collapse" id="multiCollapseExample2">


                          <a  href="Productos" class="list-group-item list-group-item-action bg-transparent" id="list-element" ><i class="fas fa-book-reader"></i> Ver inventario</a>
                              <a  href=""class="list-group-item list-group-item-action bg-transparent" id="list-element" ><i class="fas fa-clipboard-list"></i>  Modificar inventario</a>

            </div>
          </div>

        <a href="#multiCollapseExample1" class=" list-group-item list-group-item-action bg-transparent"id="list-element" data-toggle="collapse" data-target="#multiCollapseExample1" aria-expanded="false" aria-controls="multiCollapseExample1"><i class="fas fa-user-shield"></i> Roles</a>
        <div class="col">
          <div class="collapse multi-collapse" id="multiCollapseExample1">
        <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element" ><i class="fas fa-clipboard-list"></i>  Modificar roles</a>
</div>
</div>

		<a href="#multiCollapseExample3" class=" list-group-item list-group-item-action bg-transparent"id="list-element" data-toggle="collapse" data-target="#multiCollapseExample3" aria-expanded="false" aria-controls="multiCollapseExample3"><i class="fas fa-truck"></i> Pedidos</a>

    <div class="col">
      <div class="collapse multi-collapse" id="multiCollapseExample3">
        <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-clipboard-list"></i> Modificar Pedido</a>
      <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element" ><i class="fas fa-book-reader"></i> Ver Pedidos</a>
          <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-truck"></i> Generar Cancelacion</a>
        </div>
      </div>
  <a href="#multiCollapseExample4" class=" list-group-item list-group-item-action bg-transparent"id="list-element" data-toggle="collapse" data-target="#multiCollapseExample4" aria-expanded="false" aria-controls="multiCollapseExample4"><i class="fas fa-box"></i> Productos</a>

  <div class="col">
    <div class="collapse multi-collapse" id="multiCollapseExample4">
              <a href="JefeOp1" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-barcode"></i> Agregar Producto</a>
            <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element" ><i class="fas fa-clipboard-list"></i> Modificar Producto</a>
                <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-trash-restore"></i> Eliminar Producto</a>
</div>
</div>
              <a href="#multiCollapseExample5" class=" list-group-item list-group-item-action bg-transparent"id="list-element" data-toggle="collapse" data-target="#multiCollapseExample5" aria-expanded="false" aria-controls="multiCollapseExample5"><i class="fas fa-chart-bar fa-1x"></i> Reportes</a>
              <div class="col">
                <div class="collapse multi-collapse" id="multiCollapseExample5">
                    <a class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-user fa-1x"></i>Reporte inventarios</a>
</div>
</div>
                  <a href="#multiCollapseExample6" class=" list-group-item list-group-item-action bg-transparent"id="list-element" data-toggle="collapse" data-target="#multiCollapseExample6" aria-expanded="false" aria-controls="multiCollapseExample6"><i class="fas fa-chart-bar fa-1x"></i> Proveedores</a>
                  <div class="col">
                    <div class="collapse multi-collapse" id="multiCollapseExample6">
                      <a  href="Proveedor"class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-user fa-1x"></i>Ver Proveedores</a>
                      <a href="JefeOp1" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-barcode"></i> Agregar Proveedor</a>
                    <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element" ><i class="fas fa-clipboard-list"></i> Modificar Proveedor</a>
                        <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-trash-restore"></i> Eliminar Proveedor</a>
</div>
</div>
      <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-file-alt"></i> Marbete</a>
              <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-user  fa-1"></i> Perfil</a>
  <a href="{{ route('logout') }}" class="list-group-item list-group-item-action bg-transparent" id="list-element" >

<i class="fas fa-sign-out-alt fa-1x"></i> Cerrar Sesión</a>
      </div>
    </div>
    <div id="page-content-wrapper">

      <nav class="navbar navbar-expand-lg navbar-light bg-white border-bottom">
        <button class="btn btn-white" id="menu-toggle"><i class="fas fa-bars fa-1x"></i></button>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
</head>
  <style>
hello:hover{
    background-color:  #fff;
    width: 100%;
    height: 100%;
  }
  </style>
<body>



<h4 class="text-primary">  SGMC</h4>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
            <li class="nav-item active">
              <a class="nav-link" href=" "><i class="fas fa-user-tie fa-1x"></i> Jefe de Operaciones</a>
            </li>
          </ul>
        </div>
      </nav>

  <div class="container" style="margin-top:10rem;">
      @yield('content')
          @endif
</body>
</html>
