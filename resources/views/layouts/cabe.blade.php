<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>SMGC | Panel de Administración</title>

  <!-- Bootstrap core CSS -->

  <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}" />
  <link rel="stylesheet" href="{{ asset('assets/css/simple-sidebar.css') }}" />




  <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

  <!-- Custom styles for this template -->
  <link rel="stylesheet" href="{{ asset('assets/css/menu.css') }}">
  <link rel="shortcut icon" href="{{{ asset('assets/Images/favicon.ico') }}}">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.20/datatables.min.css"/>
 <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">

<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/dataTables.bootstrap4.min.css') }}">

  <!--<link rel="icon" href="{{ asset('assets/Images/favicon.ico') }}">-->
  <div class="d-flex" id="wrapper">
     <meta name="csrf-token" content="{{ csrf_token() }}" />


         <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>






       <script src="https://kit.fontawesome.com/da3b6a12fa.js"></script>
     <script src="{{asset('assets/js/bootstrap.bundle.min.js')}}"></script>
   <script src="https://code.highcharts.com/highcharts.js"></script>
  <script src="https://code.highcharts.com/modules/data.js"></script>
  <script src="https://code.highcharts.com/modules/drilldown.js"></script>
  <script src="https://code.highcharts.com/modules/exporting.js"></script>
  <script src="https://code.highcharts.com/modules/export-data.js"></script>

</head>
    <!-- Sidebar -->
<body>

    @if (Route::has('register'))
    <!--<div class="alert alert-success" role="alert">
    {{ session('status') }}
    </div>-->

@switch(Auth::user()->permisos)
@case("111111111111111101")
    <div class="border-right" id="sidebar-wrapper" style="
     background-color:  #344478; " >
      <div class="sidebar-heading bg-white">
		  <a href="{{route('home')}}"><img src="{{ asset('assets/Images/logo.jpg') }}" style="max-height: 60px;"></a>
	  </div>
      <div class="list-group list-group-flush" id="sidebar-list"style="">

    	<a href="Usuario" class=" list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-users"></i> Usuario</a>


		<a href="#multiCollapseExample2" class=" list-group-item list-group-item-action bg-transparent"id="list-element" data-toggle="collapse" data-target="#multiCollapseExample2" aria-expanded="false" aria-controls="multiCollapseExample2"><i class="fas fa-dolly"></i> Inventarios</a>
        <div class="col">
        	<div class="collapse multi-collapse" id="multiCollapseExample2">
				<a  href="ModificarInv" class="list-group-item list-group-item-action bg-transparent" id="list-element" ><i class="fas fa-book-reader"></i> Ver inventario</a>
			</div>
        </div>

		<a href="#multiCollapseExample4" class=" list-group-item list-group-item-action bg-transparent"id="list-element" data-toggle="collapse" data-target="#multiCollapseExample4" aria-expanded="false" aria-controls="multiCollapseExample4"><i class="fas fa-box"></i> Productos</a>
		<div class="col">
    		<div class="collapse multi-collapse" id="multiCollapseExample4">
				<a href="{{route('CatalogProduc')}}" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-barcode"></i> Ver Productos</a>
        	<a href="{{route('lineas')}}" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-barcode"></i> Lineas y Sublineas </a>


    	</div>
		</div>
  <a href="{{route('clave-sat')}}" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-scroll"></i> Cátalogo SAT </a>

		<a href="#multiCollapseExample3" class=" list-group-item list-group-item-action bg-transparent"id="list-element" data-toggle="collapse" data-target="#multiCollapseExample3" aria-expanded="false" aria-controls="multiCollapseExample3"><i class="fas fa-truck"></i> Pedidos y Compras</a>

    	<div class="col">
      		<div class="collapse multi-collapse" id="multiCollapseExample3">

        <a href="{{route('verPedidosReal')}}" class="list-group-item list-group-item-action bg-transparent" id="list-element" ><i class="fas fa-book-reader"></i>  Pedidos</a>
        		<a href="{{route('verPedidos')}}" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-clipboard-list"></i> Órdenes de Compra</a>
        	</div>
      	</div>

        <a href="#multiCollapseExample6" class=" list-group-item list-group-item-action bg-transparent"id="list-element" data-toggle="collapse" data-target="#multiCollapseExample6" aria-expanded="false" aria-controls="multiCollapseExample6"><i class="fas fa-chart-bar fa-1x"></i> Proveedores</a>
        <div class="col">
        	<div class="collapse multi-collapse" id="multiCollapseExample6">
            	<a  href="{{route('lproveedor')}}"class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-user fa-1x"></i>Ver Proveedores</a>
			</div>
		</div>

     <a href="#multiCollapseExample7" class=" list-group-item list-group-item-action bg-transparent"id="list-element" data-toggle="collapse" data-target="#multiCollapseExample7" aria-expanded="false" aria-controls="multiCollapseExample7"><i class="fas fa-dolly"></i> Sucursales</a>
        <div class="col">
          <div class="collapse multi-collapse" id="multiCollapseExample7">
        <a  href="Sucursales" class="list-group-item list-group-item-action bg-transparent" id="list-element" ><i class="fas fa-book-reader"></i> Ver sucursales</a>
      </div>
        </div>



		<a href="#multiCollapseExample5" class=" list-group-item list-group-item-action bg-transparent"id="list-element" data-toggle="collapse" data-target="#multiCollapseExample5" aria-expanded="false" aria-controls="multiCollapseExample5"><i class="fas fa-chart-bar fa-1x"></i> Reportes</a>
        <div class="col">
        	<div class="collapse multi-collapse" id="multiCollapseExample5">
          <a href="{{route('reporteU')}}" class="list-group-item list-group-item-action bg-transparent" id="list-element"><env class="fas fa-file-archive"></env> Bitacoras Usuarios</a>
				<a href="reportes" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-file-archive"></i> Reportes Inventario</a>

			</div>
		</div>

      </div>
    </div>
    <div id="page-content-wrapper">

      <nav class="navbar navbar-expand-lg navbar-light bg-white border-bottom" style="-webkit-box-shadow: 0 10px 6px -6px #777;
                -moz-box-shadow: 0 10px 6px -6px #777;
                box-shadow: 0 10px 6px -6px #777;">
        <button class="btn btn-white" id="menu-toggle"><i class="fas fa-bars fa-1x"></i></button>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>

        </button>

  <style>

  .dropdown-toggle::after {
    display:none;
}

  </style>


      <h4 style="color:#344478" >  SGMC </h4>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
            <li class="nav-item active">

              <nav id="menu">
                <div class="btn-group">
                <button type="button" style="width:100px; text-align:right;"class="btn btn-transparent dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-bell campanita"></i>
                </button>
                <div class="dropdown-menu">
                 @foreach($notificacion as $noti)
                   @if($noti->idtipo_n==1)
                    <a href="#"class="dropdown-item" style="font-size:10px;"> <i class="fas fa-hand-point-right" style="color:green;"></i> <strong>Notificacion Entrada:</strong><br/>{{$noti->des}}<br/>  <strong>Destino: </strong>{{$noti->destino}}<br/>{{$noti->tiempo}}</a>
                    @else
                      <a href="#"class="dropdown-item" style="font-size:10px;"> <i class="fas fa-hand-point-left" style="color:blue;"></i> <strong>Notificacion Salida:</strong><br/> {{$noti->des}}<br/> <strong>origen:</strong>{{$noti->origen}}<br/> {{$noti->tiempo}}</a>
                    @endif
                  @endforeach
                </div>

              </div>
                <div class="btn-group">
                <button type="button" class="btn btn-transparent dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-user-tie fa-1x" ></i> Administrador</a>
                </button>
                <div class="dropdown-menu">
                  <a href="" class="dropdown-item" data-toggle="modal" data-target="#verperfil" id="list-element"style="color:#344478"><i class="fas fa-user  fa-1" ></i>Mi Perfil</a>
  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="{{ url('/logout') }}"><i class="fas fa-sign-out-alt fa-1x" ></i> Cerrar Sesión</a>

                </div>

              </div>


                <div style="font-size:12px; margin-left:115px;">{{ date('Y-m-d H:i') }} </div>
              </nav>

            </li>
          </ul>
        </div>
      </nav>
@break

@case("011111111011111111")
    <div class=" border-right" id="sidebar-wrapper" style="  background-color:  #344478;" >
      <div class="sidebar-heading bg-white">
		  <a href="{{route('home')}}"><img src="{{ asset('assets/Images/logo.jpg') }}" style="max-height: 60px;"></a>
	  </div>
      <div class="list-group list-group-flush" id="sidebar-list">

    	<a href="Usuario" class=" list-group-item list-group-item-action bg-transparent" id="list-element" hidden><i class="fas fa-users"></i> Usuario</a>

		<a href="#multiCollapseExample2" class=" list-group-item list-group-item-action bg-transparent"id="list-element" data-toggle="collapse" data-target="#multiCollapseExample2" aria-expanded="false" aria-controls="multiCollapseExample2"><i class="fas fa-dolly"></i> Inventarios</a>
        <div class="col">
        	<div class="collapse multi-collapse" id="multiCollapseExample2">
				<a  href="ModificarInv" class="list-group-item list-group-item-action bg-transparent" id="list-element" ><i class="fas fa-book-reader"></i> Ver inventario</a>
			</div>
        </div>

		<a href="#multiCollapseExample4" class=" list-group-item list-group-item-action bg-transparent"id="list-element" data-toggle="collapse" data-target="#multiCollapseExample4" aria-expanded="false" aria-controls="multiCollapseExample4"><i class="fas fa-box"></i> Productos</a>
		<div class="col">
    		<div class="collapse multi-collapse" id="multiCollapseExample4">
				<a href="{{route('CatalogProduc')}}" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-barcode"></i> Ver Productos</a>
	<a href="{{route('lineas')}}" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-barcode"></i> Lineas y Sublineas </a>
    	</div>
		</div>
  <a href="{{route('clave-sat')}}" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-scroll"></i> Cátalogo SAT </a>

		<a href="#multiCollapseExample3" class=" list-group-item list-group-item-action bg-transparent"id="list-element" data-toggle="collapse" data-target="#multiCollapseExample3" aria-expanded="false" aria-controls="multiCollapseExample3"><i class="fas fa-truck"></i>Pedidos y Compras</a>

    	<div class="col">
      		<div class="collapse multi-collapse" id="multiCollapseExample3">
				<a href="{{route('verPedidosReal')}}" class="list-group-item list-group-item-action bg-transparent" id="list-element" ><i class="fas fa-book-reader"></i>  Pedidos</a>
        		<a href="{{route('verPedidos')}}" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-clipboard-list"></i> Órdenes de Compra</a>
        	</div>
      	</div>

        <a href="#multiCollapseExample6" class=" list-group-item list-group-item-action bg-transparent"id="list-element" data-toggle="collapse" data-target="#multiCollapseExample6" aria-expanded="false" aria-controls="multiCollapseExample6"><i class="fas fa-chart-bar fa-1x"></i> Proveedores</a>
        <div class="col">
        	<div class="collapse multi-collapse" id="multiCollapseExample6">
            	<a  href="{{route('lproveedor')}}"class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-user fa-1x"></i>Ver Proveedores</a>
			</div>
		</div>

         <a href="#multiCollapseExample7" class=" list-group-item list-group-item-action bg-transparent"id="list-element" data-toggle="collapse" data-target="#multiCollapseExample7" aria-expanded="false" aria-controls="multiCollapseExample7"><i class="fas fa-dolly"></i> Sucursales</a>
        <div class="col">
          <div class="collapse multi-collapse" id="multiCollapseExample7">
        <a  href="Sucursales" class="list-group-item list-group-item-action bg-transparent" id="list-element" ><i class="fas fa-book-reader"></i> Ver sucursales</a>
      </div>
        </div>


		<a href="#multiCollapseExample5" class=" list-group-item list-group-item-action bg-transparent"id="list-element" data-toggle="collapse" data-target="#multiCollapseExample5" aria-expanded="false" aria-controls="multiCollapseExample5"><i class="fas fa-chart-bar fa-1x"></i> Reportes</a>
        <div class="col">
        	<div class="collapse multi-collapse" id="multiCollapseExample5">
                      <a href="{{route('reporteU')}}" class="list-group-item list-group-item-action bg-transparent" id="list-element"><env class="fas fa-file-archive"></env> Bitacoras Usuarios</a>
				<a href="reportes" class="list-group-item list-group-item-action bg-transparent" id="list-element"><env class="fas fa-file-archive"></env> Reportes Inventario</a>

			</div>
		</div>


      </div>
    </div>
    <div id="page-content-wrapper">

      <nav class="navbar navbar-expand-lg navbar-light bg-white border-bottom">
        <button class="btn btn-white" id="menu-toggle"><i class="fas fa-bars fa-1x"></i></button>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>

        </button>

<h4 style="color:#344478" >  SGMC </h4>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
            <li class="nav-item active">

              <nav id="menu">
                <div class="btn-group">
                <button type="button" style="width:100px; text-align:right;"class="btn btn-transparent dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-bell campanita"></i>
                </button>
                <div class="dropdown-menu">
                 @foreach($notificacion as $noti)
                   @if($noti->idtipo_n==1)
                      <a href="#"class="dropdown-item" style="font-size:10px;"> <i class="fas fa-hand-point-right" style="color:green;"></i> <strong>Notificacion Entrada:</strong><br/>{{$noti->des}}<br/>  <strong>Destino: </strong>{{$noti->destino}}<br/>{{$noti->tiempo}}</a>
                    @else
                      <a href="#"class="dropdown-item" style="font-size:10px;"> <i class="fas fa-hand-point-left" style="color:blue;"></i> <strong>Notificacion Salida:</strong><br/> {{$noti->des}}<br/> <strong>origen:</strong>{{$noti->origen}}<br/> {{$noti->tiempo}}</a>
                    @endif
                  @endforeach
                </div>

              </div>
                <div class="btn-group">
                <button type="button" class="btn btn-transparent dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-user-tie fa-1x" ></i>Jefe de operaciones</a>
                </button>
                <div class="dropdown-menu">
                  <a href="" class="dropdown-item" data-toggle="modal" data-target="#verperfil" id="list-element"style="color:#344478"><i class="fas fa-user  fa-1" ></i>Mi Perfil</a>
  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="{{ url('/logout') }}"><i class="fas fa-sign-out-alt fa-1x" ></i> Cerrar Sesión</a>

                </div>
              </div>

                <div style="font-size:12px; margin-left:140px;">{{ date('Y-m-d H:i:s') }} </div>
              </nav>

            </li>
          </ul>
        </div>
      </nav>

@break

@case("011111101001010101")
    <div class=" border-right" id="sidebar-wrapper" style="  background-color:  #344478;" >
      <div class="sidebar-heading bg-white">
		  <a href="{{route('home2')}}"><img src="{{ asset('assets/Images/logo.jpg') }}" style="max-height: 60px;"></a>
	  </div>
      <div class="list-group list-group-flush" id="sidebar-list">

    	<a href="Usuario" class=" list-group-item list-group-item-action bg-transparent" id="list-element" hidden><i class="fas fa-users"></i> Usuario</a>

		<a href="#multiCollapseExample2" class=" list-group-item list-group-item-action bg-transparent"id="list-element" data-toggle="collapse" data-target="#multiCollapseExample2" aria-expanded="false" aria-controls="multiCollapseExample2"><i class="fas fa-dolly"></i> Inventarios</a>
        <div class="col">
        	<div class="collapse multi-collapse" id="multiCollapseExample2">
				<a  href="ModificarInv" class="list-group-item list-group-item-action bg-transparent" id="list-element" ><i class="fas fa-book-reader"></i> Ver inventario</a>
			</div>
        </div>

		<a href="#multiCollapseExample4" class=" list-group-item list-group-item-action bg-transparent"id="list-element" data-toggle="collapse" data-target="#multiCollapseExample4" aria-expanded="false" aria-controls="multiCollapseExample4" hidden><i class="fas fa-box"></i> Productos</a>
		<div class="col">
    		<div class="collapse multi-collapse" id="multiCollapseExample4">
				<a href="{{route('CatalogProduc')}}" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-barcode"></i> Ver Productos</a>
	<a href="{{route('lineas')}}" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-barcode"></i> Lineas y Sublineas </a>
    	</div>
		</div>


		<a href="#multiCollapseExample3" class=" list-group-item list-group-item-action bg-transparent"id="list-element" data-toggle="collapse" data-target="#multiCollapseExample3" aria-expanded="false" aria-controls="multiCollapseExample3"><i class="fas fa-truck"></i> Pedidos</a>

    	<div class="col">
      		<div class="collapse multi-collapse" id="multiCollapseExample3">
				<a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element" hidden><i class="fas fa-clipboard-list"></i> Crear Pedido</a>
				<a href="#" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-book-reader"></i> Pedidos y Compras</a>
        <a href="{{route('verPedidosReal')}}" class="list-group-item list-group-item-action bg-transparent" id="list-element" ><i class="fas fa-book-reader"></i>  Pedidos</a>
        		<a href="{{route('verPedidos')}}" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-clipboard-list"></i> Órdenes de Compra</a>
        	</div>
      	</div>

        <a href="#multiCollapseExample6" class=" list-group-item list-group-item-action bg-transparent"id="list-element" data-toggle="collapse" data-target="#multiCollapseExample6" aria-expanded="false" aria-controls="multiCollapseExample6"><i class="fas fa-chart-bar fa-1x"></i> Proveedores</a>
        <div class="col">
        	<div class="collapse multi-collapse" id="multiCollapseExample6">
            	<a  href="{{route('lproveedor')}}"class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-user fa-1x"></i>Ver Proveedores</a>
			</div>
		</div>

     <a href="#multiCollapseExample7" class=" list-group-item list-group-item-action bg-transparent"id="list-element" data-toggle="collapse" data-target="#multiCollapseExample7" aria-expanded="false" aria-controls="multiCollapseExample7"><i class="fas fa-dolly"></i> Sucursales</a>
        <div class="col">
          <div class="collapse multi-collapse" id="multiCollapseExample7">
        <a  href="Sucursales" class="list-group-item list-group-item-action bg-transparent" id="list-element" ><i class="fas fa-book-reader"></i> Ver sucursales</a>
      </div>
        </div>

		<a href="#multiCollapseExample5" class=" list-group-item list-group-item-action bg-transparent"id="list-element" data-toggle="collapse" data-target="#multiCollapseExample5" aria-expanded="false" aria-controls="multiCollapseExample5"><i class="fas fa-chart-bar fa-1x"></i> Reportes</a>
        <div class="col">
        	<div class="collapse multi-collapse" id="multiCollapseExample5">
				<a href="reportes" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-user fa-1x"></i>Reportes Inventario</a>

			</div>
		</div>

        <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element" hidden><i class="fas fa-file-alt"></i> Marbete</a>
      </div>
    </div>
    <div id="page-content-wrapper">

      <nav class="navbar navbar-expand-lg navbar-light bg-white border-bottom">
        <button class="btn btn-white" id="menu-toggle"><i class="fas fa-bars fa-1x"></i></button>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

  <style>
hello:hover{
    background-color:  #fff;
    width: 100%;
    height: 100%;
  }
  </style>



<h4 style="color:#344478" >  SGMC </h4>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
            <li class="nav-item active">

              <nav id="menu">
                <div class="btn-group">
                <button type="button" style="width:100px; text-align:right;"class="btn btn-transparent dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-bell campanita"></i>
                </button>
                <div class="dropdown-menu">
                 @foreach($notificacion as $noti)
                 @if($noti->idtipo_n==1)
                  <a href="#"class="dropdown-item" style="font-size:10px;"> <i class="fas fa-hand-point-right" style="color:green;"></i> <strong>Notificacion Entrada:</strong><br/>{{$noti->des}}<br/>  <strong>Destino: </strong>{{$noti->destino}}<br/>{{$noti->tiempo}}</a>
                  @else
                    <a href="#"class="dropdown-item" style="font-size:10px;"> <i class="fas fa-hand-point-left" style="color:blue;"></i> <strong>Notificacion Salida:</strong><br/> {{$noti->des}}<br/> <strong>origen:</strong>{{$noti->origen}}<br/> {{$noti->tiempo}}</a>
@endif
                  @endforeach
                </div>

              </div>
                <div class="btn-group">
                <button type="button" class="btn btn-transparent dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-user-tie fa-1x" ></i> Jefe almacen</a>
                </button>
                <div class="dropdown-menu">
                  <a href="" class="dropdown-item" data-toggle="modal" data-target="#verperfil" id="list-element"style="color:#344478"><i class="fas fa-user  fa-1" ></i>Mi Perfil</a>
  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="{{ url('/logout') }}"><i class="fas fa-sign-out-alt fa-1x" ></i> Cerrar Sesión</a>

                </div>
              </div>

                <div style="font-size:12px; margin-left:140px;">{{ date('Y-m-d H:i:s') }} </div>
              </nav>

            </li>
          </ul>
        </div>
      </nav>
@break

@case("0111100010100101011")
    <div class=" border-right" id="sidebar-wrapper" style="  background-color:  #344478;" >
      <div class="sidebar-heading bg-white">
		  <a href="{{route('home2')}}"><img src="{{ asset('assets/Images/logo.jpg') }}" style="max-height: 60px;"></a>
	  </div>
      <div class="list-group list-group-flush" id="sidebar-list">

    	<a href="Usuario" class=" list-group-item list-group-item-action bg-transparent" id="list-element" hidden><i class="fas fa-users"></i> Usuario</a>

		<a href="#multiCollapseExample2" class=" list-group-item list-group-item-action bg-transparent"id="list-element" data-toggle="collapse" data-target="#multiCollapseExample2" aria-expanded="false" aria-controls="multiCollapseExample2"><i class="fas fa-dolly"></i> Inventarios</a>
        <div class="col">
        	<div class="collapse multi-collapse" id="multiCollapseExample2">
				<a  href="ModificarInv" class="list-group-item list-group-item-action bg-transparent" id="list-element" ><i class="fas fa-book-reader"></i> Ver inventario</a>
			</div>
        </div>

		<a href="#multiCollapseExample4" class=" list-group-item list-group-item-action bg-transparent"id="list-element" data-toggle="collapse" data-target="#multiCollapseExample4" aria-expanded="false" aria-controls="multiCollapseExample4" hidden><i class="fas fa-box"></i> Productos</a>
		<div class="col">
    		<div class="collapse multi-collapse" id="multiCollapseExample4">
				<a href="{{route('CatalogProduc')}}" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-barcode"></i> Ver Productos</a>
	<a href="{{route('lineas')}}" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-barcode"></i> Lineas y Sublineas </a>
      </div>
		</div>


		<a href="#multiCollapseExample3" class=" list-group-item list-group-item-action bg-transparent"id="list-element" data-toggle="collapse" data-target="#multiCollapseExample3" aria-expanded="false" aria-controls="multiCollapseExample3"><i class="fas fa-truck"></i> Pedidos</a>

    	<div class="col">
      		<div class="collapse multi-collapse" id="multiCollapseExample3">
				<a href="{{route('verPedidos')}}" class="list-group-item list-group-item-action bg-transparent" id="list-element" hidden><i class="fas fa-clipboard-list"></i> Crear Pedido</a>
				<a href="{{route('verPedidos')}}" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-book-reader"></i> Ver Pedidos</a>

        	</div>
      	</div>

        <a href="#multiCollapseExample6" class=" list-group-item list-group-item-action bg-transparent"id="list-element" data-toggle="collapse" data-target="#multiCollapseExample6" aria-expanded="false" aria-controls="multiCollapseExample6"><i class="fas fa-chart-bar fa-1x"></i> Proveedores</a>
        <div class="col">
        	<div class="collapse multi-collapse" id="multiCollapseExample6">
            	<a  href="{{route('lproveedor')}}"class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-user fa-1x"></i>Ver Proveedores</a>
			</div>
		</div>

       <a href="#multiCollapseExample7" class=" list-group-item list-group-item-action bg-transparent"id="list-element" data-toggle="collapse" data-target="#multiCollapseExample7" aria-expanded="false" aria-controls="multiCollapseExample7"><i class="fas fa-dolly"></i> Sucursales</a>
        <div class="col">
          <div class="collapse multi-collapse" id="multiCollapseExample7">
        <a  href="Sucursales" class="list-group-item list-group-item-action bg-transparent" id="list-element" ><i class="fas fa-book-reader"></i> Ver sucursales</a>
      </div>
        </div>


		<a href="#multiCollapseExample5" class=" list-group-item list-group-item-action bg-transparent"id="list-element" data-toggle="collapse" data-target="#multiCollapseExample5" aria-expanded="false" aria-controls="multiCollapseExample5"><i class="fas fa-chart-bar fa-1x"></i> Reportes</a>
        <div class="col">
        	<div class="collapse multi-collapse" id="multiCollapseExample5">
				<a href="reportes" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-user fa-1x"></i>Reportes Inventario</a>

			</div>
		</div>

        <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element" hidden><i class="fas fa-file-alt"></i> Marbete</a>
      </div>
    </div>
    <div id="page-content-wrapper">

      <nav class="navbar navbar-expand-lg navbar-light bg-white border-bottom">
        <button class="btn btn-white" id="menu-toggle"><i class="fas fa-bars fa-1x"></i></button>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

  <style>
hello:hover{
    background-color:  #fff;
    width: 100%;
    height: 100%;
  }
  </style>

<h4 style="color:#344478" >  SGMC </h4>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
            <li class="nav-item active">

              <nav id="menu">
                <div class="btn-group">
                <button type="button" class="btn btn-transparent dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-user-tie fa-1x" ></i> Distrbuidora</a>
                </button>
                <div class="dropdown-menu">
                  <a href="" class="dropdown-item" data-toggle="modal" data-target="#verperfil" id="list-element"style="color:#344478"><i class="fas fa-user  fa-1" ></i>Mi Perfil</a>
  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="{{ url('/logout') }}"><i class="fas fa-sign-out-alt fa-1x" ></i> Cerrar Sesión</a>

                </div>
              </div>

                <div style="font-size:12px; margin-left:20px;">{{ date('Y-m-d H:i:s') }} </div>
              </nav>

            </li>
          </ul>
        </div>

      </nav>

@break
@case("000000001010010101")
    <div class=" border-right" id="sidebar-wrapper" style="  background-color:  #344478;" >
      <div class="sidebar-heading bg-white">
		  <a href="{{route('home3')}}"><img src="{{ asset('assets/Images/logo.jpg') }}" style="max-height: 60px;"></a>
	  </div>
      <div class="list-group list-group-flush" id="sidebar-list">

    	<a href="Usuario" class=" list-group-item list-group-item-action bg-transparent" id="list-element" hidden><i class="fas fa-users"></i> Usuario</a>

		<a href="#multiCollapseExample2" class=" list-group-item list-group-item-action bg-transparent"id="list-element" data-toggle="collapse" data-target="#multiCollapseExample2" aria-expanded="false" aria-controls="multiCollapseExample2" hidden><i class="fas fa-dolly"></i> Inventarios</a>
        <div class="col">
        	<div class="collapse multi-collapse" id="multiCollapseExample2">
				<a  href="ModificarInv" class="list-group-item list-group-item-action bg-transparent" id="list-element" ><i class="fas fa-book-reader"></i> Ver inventario</a>
			</div>
        </div>

		<a href="#multiCollapseExample4" class=" list-group-item list-group-item-action bg-transparent"id="list-element" data-toggle="collapse" data-target="#multiCollapseExample4" aria-expanded="false" aria-controls="multiCollapseExample4" hidden><i class="fas fa-box"></i> Productos</a>
		<div class="col">
    		<div class="collapse multi-collapse" id="multiCollapseExample4">
				<a href="{{route('CatalogProduc')}}" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-barcode"></i> Ver Productos</a>
	<a href="{{route('lineas')}}" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-barcode"></i> Lineas y Sublineas </a>
    	</div>
		</div>


		<a href="#multiCollapseExample3" class=" list-group-item list-group-item-action bg-transparent"id="list-element" data-toggle="collapse" data-target="#multiCollapseExample3" aria-expanded="false" aria-controls="multiCollapseExample3"><i class="fas fa-truck"></i> Pedidos</a>

    	<div class="col">
      		<div class="collapse multi-collapse" id="multiCollapseExample3">

        <a href="{{route('verPedidosReal')}}" class="list-group-item list-group-item-action bg-transparent" id="list-element" ><i class="fas fa-book-reader"></i>  Pedidos</a>
        		<a href="{{route('verPedidos')}}" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-clipboard-list"></i> Órdenes de Compra</a>
        	</div>
      	</div>

        <a href="#multiCollapseExample6" class=" list-group-item list-group-item-action bg-transparent"id="list-element" data-toggle="collapse" data-target="#multiCollapseExample6" aria-expanded="false" aria-controls="multiCollapseExample6" hidden><i class="fas fa-chart-bar fa-1x"></i> Proveedores</a>
        <div class="col">
        	<div class="collapse multi-collapse" id="multiCollapseExample6">
            	<a  href="{{route('lproveedor')}}"class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-user fa-1x"></i>Ver Proveedores</a>
			</div>
		</div>

         <a href="#multiCollapseExample7" class=" list-group-item list-group-item-action bg-transparent" hidden id="list-element" data-toggle="collapse" data-target="#multiCollapseExample7" aria-expanded="false" aria-controls="multiCollapseExample7"><i class="fas fa-dolly"></i> Sucursales</a>
        <div class="col">
          <div class="collapse multi-collapse" id="multiCollapseExample7">
        <a  href="Sucursales" class="list-group-item list-group-item-action bg-transparent" hidden id="list-element" ><i class="fas fa-book-reader"></i> Ver sucursales</a>
      </div>
        </div>


		<a href="#multiCollapseExample5" class=" list-group-item list-group-item-action bg-transparent"id="list-element" data-toggle="collapse" data-target="#multiCollapseExample5" aria-expanded="false" aria-controls="multiCollapseExample5"><i class="fas fa-chart-bar fa-1x"></i> Reportes</a>
        <div class="col">
        	<div class="collapse multi-collapse" id="multiCollapseExample5">
				<a href="reportes" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-user fa-1x"></i>Reportes Inventario</a>

			</div>
		</div>


      </div>
    </div>
    <div id="page-content-wrapper">

      <nav class="navbar navbar-expand-lg navbar-light bg-white border-bottom">
        <button class="btn btn-white" id="menu-toggle"><i class="fas fa-bars fa-1x"></i></button>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

  <style>
hello:hover{
    background-color:  #fff;
    width: 100%;
    height: 100%;
  }
  </style>

<h4 style="color:#344478" >  SGMC </h4>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
            <li class="nav-item active">

              <nav id="menu">
                <div class="btn-group">
                <button type="button" class="btn btn-transparent dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-user-tie fa-1x" ></i> Checador</a>
                </button>
                <div class="dropdown-menu">
                  <a href="" class="dropdown-item" data-toggle="modal" data-target="#verperfil" id="list-element"style="color:#344478"><i class="fas fa-user  fa-1" ></i>Mi Perfil</a>
  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="{{ url('/logout') }}"><i class="fas fa-sign-out-alt fa-1x" ></i> Cerrar Sesión</a>

                </div>
              </div>

                <div style="font-size:12px; margin-left:20px;">{{ date('Y-m-d H:i:s') }} </div>
              </nav>

            </li>
          </ul>
        </div>
      </nav>
@break

@case("011111101100010101")
    <div class=" border-right" id="sidebar-wrapper" style="  background-color:  #344478;" >
      <div class="sidebar-heading bg-white">
		  <a href="{{route('home3')}}"><img src="{{ asset('assets/Images/logo.jpg') }}" style="max-height: 60px;"></a>
	  </div>
      <div class="list-group list-group-flush" id="sidebar-list">

    	<a href="Usuario" class=" list-group-item list-group-item-action bg-transparent" id="list-element" hidden><i class="fas fa-users"></i> Usuario</a>

		<a href="#multiCollapseExample2" class=" list-group-item list-group-item-action bg-transparent"id="list-element" data-toggle="collapse" data-target="#multiCollapseExample2" aria-expanded="false" aria-controls="multiCollapseExample2"><i class="fas fa-dolly"></i> Inventarios</a>
        <div class="col">
        	<div class="collapse multi-collapse" id="multiCollapseExample2">
				<a  href="ModificarInv" class="list-group-item list-group-item-action bg-transparent" id="list-element" ><i class="fas fa-book-reader"></i> Ver inventario</a>
			</div>
        </div>

		<a href="#multiCollapseExample4" class=" list-group-item list-group-item-action bg-transparent"id="list-element" data-toggle="collapse" data-target="#multiCollapseExample4" aria-expanded="false" aria-controls="multiCollapseExample4"><i class="fas fa-box"></i> Productos</a>
		<div class="col">
    		<div class="collapse multi-collapse" id="multiCollapseExample4">
				<a href="{{route('CatalogProduc')}}" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-barcode"></i> Ver Productos</a>
	<a href="{{route('lineas')}}" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-barcode"></i> Lineas y Sublineas </a>
    	</div>
		</div>


		<a href="#multiCollapseExample3" class=" list-group-item list-group-item-action bg-transparent"id="list-element" data-toggle="collapse" data-target="#multiCollapseExample3" aria-expanded="false" aria-controls="multiCollapseExample3"><i class="fas fa-truck"></i> Compras</a>

    	<div class="col">
      		<div class="collapse multi-collapse" id="multiCollapseExample3">

				<a href="{{route('verPedidos')}}" class="list-group-item list-group-item-action bg-transparent" id="list-element" hidden><i class="fas fa-book-reader"></i> Ver Pedidos</a>
        	<a href="{{route('verPedidos')}}" class="list-group-item list-group-item-action bg-transparent" id="list-element" ><i class="fas fa-book-reader"></i> Órdenes de Compra</a>

        	</div>
      	</div>
  <a href="{{route('clave-sat')}}" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-scroll"></i> Cátalogo SAT </a>
        <a href="#multiCollapseExample6" class=" list-group-item list-group-item-action bg-transparent"id="list-element" data-toggle="collapse" data-target="#multiCollapseExample6" aria-expanded="false" aria-controls="multiCollapseExample6"><i class="fas fa-chart-bar fa-1x"></i> Proveedores</a>
        <div class="col">
        	<div class="collapse multi-collapse" id="multiCollapseExample6">
            	<a  href="{{route('lproveedor')}}"class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-user fa-1x"></i>Ver Proveedores</a>
			</div>
		</div>


   <a href="#multiCollapseExample7" class=" list-group-item list-group-item-action bg-transparent"id="list-element" data-toggle="collapse" data-target="#multiCollapseExample7" aria-expanded="false" aria-controls="multiCollapseExample7"><i class="fas fa-dolly"></i> Sucursales</a>
        <div class="col">
          <div class="collapse multi-collapse" id="multiCollapseExample7">
        <a  href="Sucursales" class="list-group-item list-group-item-action bg-transparent" id="list-element" ><i class="fas fa-book-reader"></i> Ver sucursales</a>
      </div>
        </div>

		<a href="#multiCollapseExample5" class=" list-group-item list-group-item-action bg-transparent"id="list-element" data-toggle="collapse" data-target="#multiCollapseExample5" aria-expanded="false" aria-controls="multiCollapseExample5"><i class="fas fa-chart-bar fa-1x"></i> Reportes</a>
        <div class="col">
        	<div class="collapse multi-collapse" id="multiCollapseExample5">
				<a href="reportes" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-user fa-1x"></i>Reportes Inventario</a>

			</div>
		</div>
        <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element" hidden><i class="fas fa-file-alt"></i> Marbete</a>
      </div>
    </div>
    <div id="page-content-wrapper">

      <nav class="navbar navbar-expand-lg navbar-light bg-white border-bottom">
        <button class="btn btn-white" id="menu-toggle"><i class="fas fa-bars fa-1x"></i></button>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

  <style>
hello:hover{
    background-color:  #fff;
    width: 100%;
    height: 100%;
  }
  </style>



<h4 style="color:#344478">  SGMC </h4>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
            <li class="nav-item active">

              <nav id="menu">
                <div class="btn-group">
                <button type="button" style="width:100px; text-align:right;"class="btn btn-transparent dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-bell campanita"></i>
                </button>
                <div class="dropdown-menu">
                 @foreach($notificacion as $noti)
                 @if($noti->idtipo_n==1)
                  <a href="#"class="dropdown-item" style="font-size:10px;"> <i class="fas fa-hand-point-right" style="color:green;"></i> <strong>Notificacion Entrada:</strong><br/>{{$noti->des}}<br/>  <strong>Destino: </strong>{{$noti->destino}}<br/>{{$noti->tiempo}}</a>
                  @else
                    <a href="#"class="dropdown-item" style="font-size:10px;"> <i class="fas fa-hand-point-left" style="color:blue;"></i> <strong>Notificacion Salida:</strong><br/> {{$noti->des}}<br/> <strong>origen:</strong>{{$noti->origen}}<br/> {{$noti->tiempo}}</a>
@endif
                  @endforeach
                </div>

              </div>
                <div class="btn-group">
                <button type="button" class="btn btn-transparent dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-user-tie fa-1x" ></i> Compras</a>
                </button>
                <div class="dropdown-menu">
                  <a href="" class="dropdown-item" data-toggle="modal" data-target="#verperfil" id="list-element"style="color:#344478"><i class="fas fa-user  fa-1" ></i>Mi Perfil</a>
  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="{{ url('/logout') }}"><i class="fas fa-sign-out-alt fa-1x" ></i> Cerrar Sesión</a>

                </div>
              </div>

                <div style="font-size:12px; margin-left:120px;">{{ date('Y-m-d H:i:s') }} </div>
              </nav>

            </li>
          </ul>
        </div>
      </nav>
@break

@case("000110001110100101")
    <div class=" border-right" id="sidebar-wrapper" style="  background-color:  #344478;" >
      <div class="sidebar-heading bg-white">
		  <a href=""><img src="{{ asset('assets/Images/logo.jpg') }}" style="max-height: 60px;"></a>
	  </div>
      <div class="list-group list-group-flush" id="sidebar-list">

    	<a href="Usuario" class=" list-group-item list-group-item-action bg-transparent" id="list-element" hidden><i class="fas fa-users"></i> Usuario</a>

		<a href="#multiCollapseExample2" class=" list-group-item list-group-item-action bg-transparent"id="list-element" data-toggle="collapse" data-target="#multiCollapseExample2" aria-expanded="false" aria-controls="multiCollapseExample2" hidden><i class="fas fa-dolly"></i> Inventarios</a>
        <div class="col">
        	<div class="collapse multi-collapse" id="multiCollapseExample2">
				<a  href="ModificarInv" class="list-group-item list-group-item-action bg-transparent" id="list-element" ><i class="fas fa-book-reader"></i> Ver inventario</a>
			</div>
        </div>

		<a href="#multiCollapseExample4" class=" list-group-item list-group-item-action bg-transparent"id="list-element" data-toggle="collapse" data-target="#multiCollapseExample4" aria-expanded="false" aria-controls="multiCollapseExample4" hidden><i class="fas fa-box"></i> Productos</a>
		<div class="col">
    		<div class="collapse multi-collapse" id="multiCollapseExample4">
				<a href="{{route('CatalogProduc')}}" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-barcode"></i> Ver Productos</a>
        	<a href="{{route('lineas')}}" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-barcode"></i> Lineas y Sublineas </a>
      </div>
		</div>


		<a href="#multiCollapseExample3" class=" list-group-item list-group-item-action bg-transparent"id="list-element" data-toggle="collapse" data-target="#multiCollapseExample3" aria-expanded="false" aria-controls="multiCollapseExample3"><i class="fas fa-truck"></i> Pedidos</a>

    	<div class="col">
      		<div class="collapse multi-collapse" id="multiCollapseExample3">


        <a href="{{route('verPedidosReal')}}" class="list-group-item list-group-item-action bg-transparent" id="list-element" ><i class="fas fa-book-reader"></i>  Pedidos</a>
        		<a href="{{route('verPedidos')}}" class="list-group-item list-group-item-action bg-transparent" id="list-element" hidden><i class="fas fa-clipboard-list"></i> Órdenes de Compra</a>
        	</div>
      	</div>

        <a href="#multiCollapseExample6" class=" list-group-item list-group-item-action bg-transparent"id="list-element" data-toggle="collapse" data-target="#multiCollapseExample6" aria-expanded="false" aria-controls="multiCollapseExample6"><i class="fas fa-chart-bar fa-1x"></i> Proveedores</a>
        <div class="col">
        	<div class="collapse multi-collapse" id="multiCollapseExample6">
            	<a  href="{{route('lproveedor')}}"class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-user fa-1x"></i>Ver Proveedores</a>
			</div>
		</div>




		<a href="#multiCollapseExample5" class=" list-group-item list-group-item-action bg-transparent"id="list-element" data-toggle="collapse" data-target="#multiCollapseExample5" aria-expanded="false" aria-controls="multiCollapseExample5"><i class="fas fa-chart-bar fa-1x"></i> Reportes</a>
        <div class="col">
        	<div class="collapse multi-collapse" id="multiCollapseExample5">
				<a href="reportes" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-user fa-1x"></i>Reportes Inventario</a>

		</div>

        <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element" hidden><i class="fas fa-file-alt"></i> Marbete</a>
      </div>
    </div>
  </div>
    <div id="page-content-wrapper">

      <nav class="navbar navbar-expand-lg navbar-light bg-white border-bottom">
        <button class="btn btn-white" id="menu-toggle"><i class="fas fa-bars fa-1x"></i></button>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

  <style>
hello:hover{
    background-color:  #fff;
    width: 100%;
    height: 100%;
  }
  </style>



<h4 style="color:#344478" >  SGMC </h4>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
            <li class="nav-item active">

              <nav id="menu">
                <div class="btn-group">
                <button type="button" style="width:100px; text-align:right;"class="btn btn-transparent dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-bell campanita"></i>
                </button>
                <div class="dropdown-menu">
                 @foreach($notificacion as $noti)
                 @if($noti->idtipo_n==1)
                  <a href="#"class="dropdown-item" style="font-size:10px;"> <i class="fas fa-hand-point-right" style="color:green;"></i> <strong>Notificacion Entrada:</strong><br/>{{$noti->des}}<br/>  <strong>Destino: </strong>{{$noti->destino}}<br/>{{$noti->tiempo}}</a>
                  @else
                    <a href="#"class="dropdown-item" style="font-size:10px;"> <i class="fas fa-hand-point-left" style="color:blue;"></i> <strong>Notificacion Salida:</strong><br/> {{$noti->des}}<br/> <strong>origen:</strong>{{$noti->origen}}<br/> {{$noti->tiempo}}</a>
@endif
                  @endforeach
                </div>

              </div>
                <div class="btn-group">
                <button type="button" class="btn btn-transparent dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-user-tie fa-1x" ></i> Ventas</a>
                </button>
                <div class="dropdown-menu">
                  <a href="" class="dropdown-item" data-toggle="modal" data-target="#verperfil" id="list-element"style="color:#344478"><i class="fas fa-user  fa-1" ></i>Mi Perfil</a>
  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="{{ url('/logout') }}"><i class="fas fa-sign-out-alt fa-1x" ></i> Cerrar Sesión</a>

                </div>
              </div>

                <div style="font-size:12px; margin-left:140px;">{{ date('Y-m-d H:i:s') }} </div>
              </nav>

            </li>
          </ul>
        </div>
      </nav>
@break
@default
  <span>Something went wrong, please try again</span>
  @break

@endswitch
@else
@endif




  <div class="container">
      @yield('content')

      <div class="modal fade" id="verperfil" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Mi Perfil: </h5>
              <button type="button" class="close" style="color: #FFFFFF;" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <form>
                  <div class="form-row">
                      <div class="form-group col-md-3">
                      <i class="fas fa-user-tie fa-7x" style="color:#0D4566;"></i></div>
                          <div class="form-group col-md-4">
                          <label for="Usuario">Usuario</label>
                          <input type="text" class="form-control" id="Usuario" readonly style="background-color: #fff;"placeholder="Usuario" value="{{Auth::user()->usuario}}">
                      </div>
                      @if(Auth::user()->id==1)
                      <div class="form-group col-md-4">
                        <label for="Rol">Rol</label>
                        <input type="text" class="form-control" id="Rol"  style="background-color: #fff;" readonly placeholder="Rol" value='Administrador'>
                          @elseif (Auth::user()->id==2)
                      <div class="form-group col-md-4">
                        <label for="Rol">Rol</label>
                        <input type="text" class="form-control" id="Rol"  style="background-color: #fff;" readonly placeholder="Rol" value='Jefe operaciones'>

                          @elseif (Auth::user()->id==3)
                      <div class="form-group col-md-4">
                        <label for="Rol">Rol</label>
                        <input type="text" class="form-control" id="Rol"  style="background-color: #fff;" readonly placeholder="Rol" value='Jefe de Almacén'>

                          @elseif (Auth::user()->id==4)
                      <div class="form-group col-md-4">
                        <label for="Rol">Rol</label>
                        <input type="text" class="form-control" id="Rol"  style="background-color: #fff;" readonly placeholder="Rol" value='Distribuidora'>
                        @elseif (Auth::user()->id==5)
                      <div class="form-group col-md-4">
                      <label for="Rol">Rol</label>
                      <input type="text" class="form-control" id="Rol"  style="background-color: #fff;" readonly placeholder="Rol" value='Checador'>

                      @elseif (Auth::user()->id==6)
                    <div class="form-group col-md-4">
                    <label for="Rol">Rol</label>
                    <input type="text" class="form-control" id="Rol"  style="background-color: #fff;" readonly placeholder="Rol" value='Compras'>

                          @endif
                      </div>
                    </div>
                    <div class="form-row">
                      <div class="form-group col-md-4">
                        <label for="Nombre">Nombre</label>
                        <input type="text" class="form-control" id="Nombre" style="background-color: #fff;" readonly placeholder="Nombre" value="{{Auth::user()->name}}">
                      </div>
                      <div class="form-group col-md-4">
                        <label for="Apellido">Apellido Paterno</label>
                        <input type="text" class="form-control" id="Apellido" style="background-color: #fff;" readonly placeholder=""value="{{Auth::user()->aPaterno}}">
                      </div>
                      <div class="form-group col-md-4">
                        <label for="Apellido">Apellido Materno</label>
                        <input type="text" class="form-control" id="Apellido" style="background-color: #fff;" readonly placeholder=""value="{{Auth::user()->aMaterno}}">
                      </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-5">
                      <label for="Email">Email</label>
                      <input type="email" class="form-control" id="Email" style="background-color: #fff;" readonly placeholder="Email" value="{{Auth::user()->email}}">
                    </div>
                      <div class="form-group col-md-5">
                      <label for="Sucursal">Sucursal</label>
                      <input type="text" class="form-control" id="Sucursal"style="background-color: #fff;"  readonly placeholder="Sucursal" value="Sucursal {{Auth::user()->id_sucursal}}">
                     </div>
                    </div>
                    <div class="form-row">
                      <div class="form-group col-md-4">
                        <label for="Calle">Calle</label>
                        <input type="text" class="form-control" id="Calle" style="background-color: #fff;" readonly placeholder="Calle" value="{{Auth::user()->calle}}">
                      </div>
                      <div class="form-group col-md-4">
                        <label for="Colonia">Colonia</label>
                        <input type="text" class="form-control" id="Colonia" style="background-color: #fff;" readonly placeholder="Colonia" value="{{Auth::user()->colonia}}">
                      </div>
                      <div class="form-group col-md-4">
                        <label for="Colonia">Ciudad</label>
                        <input type="text" class="form-control" id="Ciudad"  style="background-color: #fff;"readonly placeholder="Colonia" value="{{Auth::user()->ciudad}}">
                      </div>
                    </div>
                  </form>

            <div class="modal-footer">
              <button type="button" class="btn btn-danger"data-dismiss="modal"data-toggle="tooltip" data-placement="top" title="Cancelar"><i class="fas fa-eye-slash"></i></button>
  </div>  </div>

          </div>
      </div>
    </div>
  </div>

  <!-- Menu Toggle Script -->

  <script>
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
  </script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script href="{{ asset('assets/js/bootstrap.min.js')}}"></script>
  <script src="http://arquimo.com/lands/merca2/js/jquery.dataTables.min.js"></script>

</body>
</html>
