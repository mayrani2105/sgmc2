<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
    <div class="container-fluid">
        <div class="header-body">
            <!-- Card stats -->
            <div class="row">
                <div class="col-xl-3 col-lg-6">
                    <div class="card card-stats mb-4 mb-xl-0">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    @foreach($entradas as $ent)
                                    <h5 class="card-subtitle text-uppercase text-muted mb-0">Entradas</h5>
                                    <span class="h3 font-weight-bold mb-0">{!!$ent->entradas!!}</span>
                                    @endforeach
                                </div>
                                <div class="col-auto">
                                    <div class="icon icon-shape bg-danger text-white rounded-circle shadow">
                                        <i class="fas fa-chart-bar"></i>
                                    </div>
                                </div>
                            </div>

                            <p class="mt-3 mb-0 text-muted text-sm">







                            </p>

                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6">
                    <div class="card card-stats mb-4 mb-xl-0">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    @foreach($salidas as $sal)
                                    <h5 class="card-subtitle text-uppercase text-muted mb-0">Salidas</h5>
                                    <span class="h3 font-weight-bold mb-0">{!!$sal->salidas!!}</span>
                                    @endforeach
                                </div>
                                <div class="col-auto">
                                    <div class="icon icon-shape bg-warning text-white rounded-circle shadow">
                                        <i class="fas fa-chart-pie"></i>
                                    </div>
                                </div>
                            </div>

                            <p class="mt-3 mb-0 text-muted text-sm">
                         <!-- Aquí va lo que ya no funciona por su culpa -.-  -->

                            <p class="mt-3 mb-0 text-muted text-sm">

                            </p>

                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-9">
                    <div class="card card-stats mb-4 mb-xl-0">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    @foreach($totalp as $tot)
                                    <h5 class="card-subtitle text-uppercase text-muted mb-0">Productos</h5>
                                    <span class="h3 font-weight-bold mb-0">{!!$tot->total!!}</span>
                                    @endforeach
                                </div>
                                <div class="col-auto">
                                    <div class="icon icon-shape bg-success text-white rounded-circle shadow">
                                        <i class="fas fa-boxes"></i>
                                    </div>
                                </div>
                            </div>
                          
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
