
@extends('layouts.cabe')

@section('content')
<style type="text/css">
  .titulo-sat{
    margin: 35px 45px;

  }
  .titulo-sat p{
    margin: 35px 45px;
    font-size: 28px;
    text-align: center;
    font-weight: 300;
  }
</style>
<center>
<h3 class="display-4">
  Cátalogo SAT</h3>
</center>
<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #344478">

</nav><br/>
<table id="example"  class="table table-striped " style="width:100%">
        <thead>
            <tr>
                <th>id</th>
                <th>ID-SAT</th>
                <th>Descripción</th>
            </tr>
        </thead>
        <tbody>
          @foreach($res as $row)
            <tr>
              <td>{!!$row->idsat!!}</td>
              <td>{!!$row->clavesat!!}</td>
              <td>{!!$row->descripcion!!}</td>
            </tr>

          @endforeach
        </tbody>
            </table>

<script type="text/javascript">
  $(document).ready(function() {
    var table = $('#example').DataTable();

    $('#example tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    } );

    $('#button').click( function () {
        table.row('.selected').remove().draw( false );
    } );
} );
</script>
@endsection
