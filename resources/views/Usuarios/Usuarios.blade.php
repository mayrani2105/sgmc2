@extends('layouts.cabe')
    <!-- /#sidebar-wrapper -->
@section('content')
    <!-- Page Content -->
    <style>
    .btn-group-fab {
      position: fixed;
      width: 50px;
      height: auto;
      right: 20px; bottom: 20px;
    }
    .btn-group-fab div {
      position: relative; width: 100%;
      height: auto;
    }
    .js-pscroll {
      position: relative;
      overflow: hidden;
    }

    table tr th {
       cursor: pointer;
       -webkit-user-select: none;
       -moz-user-select: none;
       -ms-user-select: none;
       user-select: none;
    }
    tr:hover{
      background-color: #f0f1f3;
    }
    .has-search .form-control {
        padding-left: 2.375rem;
    }

    .has-search .form-control-feedback {
        position: absolute;
        z-index: 2;
        display: block;
        width: 2.375rem;
        height: 2.375rem;
        line-height: 2.375rem;
        text-align: center;
        pointer-events: none;
        color: #aaa;
    }

    .asc:after {
       content: ' ↑';
    }
    .desc:after {
       content: " ↓";
    }
    .btn-group-fab .btn {
      position: absolute;
      bottom: 0;
      border-radius: 50%;
      display: block;
      margin-bottom: 4px;
      width: 40px; height: 40px;
      margin: 4px auto;
    }
    .btn-group-fab .btn-main {
      width: 50px; height: 50px;
      right: 50%; margin-right: -25px;
      z-index: 9;
    }
    .btn-group-fab .btn-sub {
      bottom: 0; z-index: 8;
      right: 50%;
      margin-right: -20px;
      -webkit-transition: all 2s;
      transition: all 0.5s;
    }

    .nav-link,.nav-link-active{
    color:#aaaa;

    }
    .modal-header{
      background-color: #344478;
      color:#ffffff;
    }
    .nav-tabs{
    width: 100$;
    }
    .close{

    }


    .mr-auto{
      margin-right: 200px;
    }
    .hidden{ display: none; }
    .btn-group-fab.active .btn-sub:nth-child(2) {
      bottom: 60px;
    }
    .btn-group-fab.active .btn-sub:nth-child(3) {
      bottom: 110px;
    }
    .btn-group-fab.active .btn-sub:nth-child(4) {
      bottom: 160px;
    }
    .btn-group-fab .btn-sub:nth-child(5) {
      bottom: 210px;
    }
    </style>
<style>
.modal-header{
  background-color: #344478;
  color:#ffffff;
}

.btn-group-fab {
  position: fixed;
  width: 50px;
  height: auto;
  right: 20px; bottom: 20px;
}
.btn-group-fab div {
  position: relative; width: 100%;
  height: auto;
}
.js-pscroll {
  position: relative;
  overflow: hidden;
}
table tr th {
   cursor: pointer;
   -webkit-user-select: none;
   -moz-user-select: none;
   -ms-user-select: none;
   user-select: none;
}
tr:hover{
  background-color: #f0f1f3;
}
.has-search .form-control {
    padding-left: 2.375rem;
}

.has-search .form-control-feedback {
    position: absolute;
    z-index: 2;
    display: block;
    width: 2.375rem;
    height: 2.375rem;
    line-height: 2.375rem;
    text-align: center;
    pointer-events: none;
    color: #aaa;
}

.asc:after {
   content: ' ↑';
}
.desc:after {
   content: " ↓";
}
.btn-group-fab .btn {
  position: absolute;
  bottom: 0;
  border-radius: 50%;
  display: block;
  margin-bottom: 4px;
  width: 40px; height: 40px;
  margin: 4px auto;
}
.btn-group-fab .btn-main {
  width: 50px; height: 50px;
  right: 50%; margin-right: -25px;
  z-index: 9;
}
.btn-group-fab .btn-sub {
  bottom: 0; z-index: 8;
  right: 50%;
  margin-right: -20px;
  -webkit-transition: all 2s;
  transition: all 0.5s;
}

.nav-link,.nav-link-active{
color:#aaaa;

}
.nav-tabs{
width: 100$;
}
.close{

}


.mr-auto{
  margin-right: 200px;
}
.hidden{ display: none; }
.btn-group-fab.active .btn-sub:nth-child(2) {
  bottom: 60px;
}
.btn-group-fab.active .btn-sub:nth-child(3) {
  bottom: 110px;
}
.btn-group-fab.active .btn-sub:nth-child(4) {
  bottom: 160px;
}
.btn-group-fab .btn-sub:nth-child(5) {
  bottom: 210px;
}

</style>
  <div class="container" >

    <nav class="navbar navbar-expand-lg navbar-light" style="background-color: #344478">

	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">
		  <li class="nav-item active">
			<a class="nav-link dropdown-toggle" href="#" style="color: #FFFFFF" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<i class="fas fa-sliders-h"></i> Filtros
			</a>
		  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
			<a class="dropdown-item"  href="#"id="nombre">Nombre Usuario</a>
			<a class="dropdown-item"href="#" id="email">Email</a>
      <a class="dropdown-item" href="#"id="apellido">Apellidos</a>
      <a class="dropdown-item" href="#"id="usuario">usuario</a>
      <a class="dropdown-item" href="#"id="direccion">direccion</a>
		  </div>
		  </li>
		</ul>
    <h4 class="display-4" style="color:#fff; margin-right:160px;">Cátalogo Usuarios</h4>
    <form class="form-inline my-10 my-lg-0">
	  <div>
      <div class="form-group has-search" style="margin-Top:31px;">
      <span class="fa fa-search form-control-feedback"style="color:#344478;"></span>
      <input type="text" class="form-control"id="search"name="search" type="search" placeholder="Buscar" >
    </div>

	  </div>
    </form>
  </div>
	</nav>
    <div class="row form-row">
		<div class="col-md-12">
		<br/>
			<div class="table-responsive">

                <table  data-toggle="table" class="table table-striped" id="mitabla">
                	<thead>
                    	<tr>
							<th  class="text-center" data-field="folio" data-sortable="true">Nombre del usuario</th>
              				<th  class="text-center" data-field="folio" data-sortable="true">Apellidos</th>
                            <th  class="text-center" data-field="empresa" data-sortable="true">Email</th>
                            <th  class="text-center" data-field="rsocial" data-sortable="true">Usuario</th>
                            <th  class="text-center"  data-field="rfc" data-sortable="true">Direccion</th>
                            <th  class="text-center" data-field="saldo" data-sortable="true">Operaciones</th>
                         </tr>
					</thead>
				<tbody>
				@foreach($usuarios as $usuario)
				<tr>
				  <td>{{$usuario->name}}</td>
          		  <td>{{$usuario->aPaterno}} {{$usuario->aMaterno}}</td>
				  <td>{{$usuario->email}}</td>
				  <td>{{$usuario->usuario}}</td>
				  <td>{{$usuario->calle}}</td>
				  <td>
					  <button type="button" id="editar" name="editar" class="btn btn-primary btn-sm select" data-toggle="modal" data-target=".bd-example-modal-lg" onclick="modalUser('{!!$usuario ->id!!}')"><i class="fas fa-edit"></i></button>
					  <button type="button" id="eliminar" name="eliminar" class="btn btn-danger btn-sm select" data-toggle="modal" data-target=".bd-example-modal-lg" value=""><i class="fas fa-trash-alt"></i></button>
				  </td>
				</tr>
				@endforeach
			  </tbody>
			</table>
		</div>
    </div>
  </div>
 </div>
</div>

<div class="btn-group-fab" role="group" aria-label="FAB Menu">
  <div>
    <button type="button" class="btn btn-main btn-primary has-tooltip" data-placement="left" title="Menu"> <i class="fa fa-bars"></i> </button>
    <button type="button" class="btn btn-sub btn-info has-tooltip" data-toggle="modal" data-target="#crear" data-placement="left" title="Agregar Producto" > <i class="fas fa-plus"></i></button>
    <button type="button" class="btn btn-sub btn-dark has-tooltip" data-toggle="modal" data-target="#ayuda" data-placement="left" title="Ayuda" > <i class="fas fa-question"></i> </button>

  </div>
</div>
<!-----------------------MODALES------------------------>
<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" id="usuario">
	  <div class="modal-header">
        <h5 class="modal-title" id="usuario_edit">Editando al usuario: </h5>
        <button type="button" class="close" style="color: #FFFFFF;" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
	  <div class="modal-body">
	 <ul class="nav nav-tabs" id="MyTab" role="tablist">
        <li class="nav-item">
          <a class="nav-link active" id="datosinvE-tab" data-toggle="tab" href="#datosUsuario" role="tab" aria-controls="datosinvE">Datos del Usuario <i class="fas fa-user"></i></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="ubicacionE-tab" data-toggle="tab" href="#permisos" role="tab" aria-controls="ubicacionE">Permisos <i class="fas fa-user-shield"></i></a>
        </li>

      </ul>
	  <div class="tab-content" id="myTabContent">
		  <div role="tabpanel" class="tab-pane active" id="datosUsuario">
			  <form>
			  <div class="form-group">
				<label for="nom_user">Nombre del usuario</label>
				<input type="text" class="form-control" id="nom_user" name="nom_user">
			  </div>
			  <div class="form-group">
				<label for="mail">E-mail</label>
				<input type="text" class="form-control" id="mail" name="mail">
			  </div>
			  <div class="form-group">
				<label for="alias_usuario">Usuario</label>
				<input type="text" class="form-control" id="alias_usuario">
			  </div>
			  <div class="form-group">
				<label for="direccion">Direccion</label>
				<input type="text" class="form-control" id="direcc">
			  </div>
			  <div class="form-group">
				<label for="sucursal">Sucursal</label>
				<input type="text" class="form-control" id="sucursal">
			  </div>
			  <div class="form-group">
				<label for="puesto">Puesto</label>
				<input type="text" class="form-control" id="puesto">
			  </div>
			</form>
		  </div>
		  <div role="tabpanel" class="tab-pane" id="permisos">
			  <div class="form-group">
				<label for="puesto">Rol</label>
				<select class="custom-select" id="rol">
					<option selected>Selecciona un rol</option>
					<option value="1">Jefe de Operaciones</option>
					<option value="2">Administrador</option>
					<option value="3">Jefe de Almacen</option>
					<option value="4">Distribuidor</option>
					<option value="5">Checador</option>
					<option value="6">Compras</option>
					<option value="7">Ventas</option>
				  </select>
			  </div>
			  <!--<label for="puesto">Seleccione los permisos a otorgar</label>
			  <div class="form-group">
					<div class="row form-row">
						<div class="col-md-3">
							<input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
							<label class="form-check-label" for="defaultCheck1">
								Modulo Usuarios 
							</label>
						</div>
						<div class="col-md-3">
							<input class="form-check-input" type="checkbox" value="" id="defaultCheck2">
							<label class="form-check-label" for="defaultCheck2">
								Modulo Inventario 
							</label>
						</div>
						<div class="col-md-3">
							<input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
							<label class="form-check-label" for="defaultCheck1">
								Modulo Proveedores
							</label>
						</div>
					</div>
			  </div>
				
				  
				
				  
				<input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
			    <label class="form-check-label" for="defaultCheck1">
					Modulo Pedidos
			    </label>
				  
				<input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
			    <label class="form-check-label" for="defaultCheck1">
					Modulo Productos
			    </label>
				  
				<input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
			    <label class="form-check-label" for="defaultCheck1">
					Modulo Almacenes
			    </label>-->
			  
		  </div>
	  </div>        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary">Guardar Cambios</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
</div>
    <!-- /#page-content-wrapper -->
  <!-- /#wrapper -->

  <!-- Bootstrap core JavaScript -->
<!-------MODAL PARA AGREGAR USUARIO------->
<form  method="post" action="">

                  {{method_field('POST')}}
<div class="modal fade bd-example-modal-lg" id="crear" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title">Agregar Usuario: </h5>
        <button type="button" class="close" style="color: #FFFFFF;" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    <div class="modal-body">
		  <div class="form-group">
			<label for="formGroupExampleInput">Nombre del usuario</label>
			<input type="text" class="form-control" id="nom_user" name="nom_user">
		  </div>
		  <div class="form-group">
			<label for="formGroupExampleInput2">Email</label>
			<input type="text" class="form-control" id="email" name="email">
		  </div>
		  <div class="form-group">
			<label for="formGroupExampleInput3">Usuario</label>
			<input type="text" class="form-control" id="alias_usuario" name="alias_usuario">
		  </div>
		  <div class="form-group">
			<label for="formGroupExampleInput4">Direccion</label>
			<input type="text" class="form-control" id="direccion" name="direccion">
		  </div>
		  <div class="form-group">
			<label for="formGroupExampleInput5">Sucursal</label>
			<input type="text" class="form-control" id="sucursal" name="sucursal">
		  </div>
		  <div class="form-group">
			<label for="formGroupExampleInput6">Puesto</label>
			<input type="text" class="form-control" id="puesto" name="puesto">
		  </div>
  
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-eye-slash"></i></button> <!--Botón de cerrar-->
        <button type="submit" class="btn btn-primary" value=""><i class="far fa-save"></i></button>
      </div>
    </div>
  </div>
</div>
</form>
<script>

  function modalUser(id){
    $.ajax({
      url:'ModificarUser/'+id,
      type:'get'
    }).done(function (res){
	  $('#usuario_edit').val(res.datos[0].name);
      $('#nom_user').val(res.datos[0].name);
      $('#mail').val(res.datos[0].email);
      $('#alias_usuario').val(res.datos[0].usuario);
      $('#direcc').val(res.datos[0].calle+" "+res.datos[0].colonia);
      $('#sucursal').val(res.datos[0].id_sucursal);
      $('#puesto').val(res.datos[0].id_rol);
	  //alert(res.datos2[0].descripcion);
	  $('#rol').val(res.datos2[0].descripcion);
    });
  }
</script>
<!--- Scripts para la tabla-->
<script type="text/javascript">
   $('th').click(function() {
   var table = $(this).parents('table').eq(0)
   var rows = table.find('tr:gt(0)').toArray().sort(comparer($(this).index()))
   this.asc = !this.asc
   if (!this.asc) {
      rows = rows.reverse()
   }
   for (var i = 0; i < rows.length; i++) {
      table.append(rows[i])
   }
   setIcon($(this), this.asc);

});
function comparer(index) {
   return function(a, b) {
      var valA = getCellValue(a, index),
      valB = getCellValue(b, index)
      return $.isNumeric(valA) && $.isNumeric(valB) ? valA - valB : valA.localeCompare(valB)
   }
}
function getCellValue(row, index) {
   return $(row).children('td').eq(index).html()
}
function setIcon(element, asc) {
   $("th").each(function(index) {
      $(this).removeClass("sorting");
      $(this).removeClass("asc");
      $(this).removeClass("desc");
   });
   element.addClass("sorting");
   if (asc) element.addClass("asc");
   else element.addClass("desc");
}
  $(document).ready(function() {
	  
	(function ($) {
        $('#search1').keyup(function () {
             var rex = new RegExp($(this).val(), 'i');
             if(rex=='/(?:)/i'){
               $('#paginacion').show();
             }else{
             $('#paginacion').hide();}
             $('.busca tr').hide();
             $('.busca tr').filter(function () {
               return rex.test($(this).text());
             }).show();
        })

    }(jQuery));
	  
    var table = $('#example').DataTable();

//script en el cual se hace el filtrado por categoria seleccionada

$(document).ready(function(){
  let table = document.getElementById('mitabla');
    let table1 = document.getElementById('mitabla2');

  $("#nombre").click(function(){

    for (var i = 0, row; row = table.rows[i]; i++){
       row.cells[0].classList.toggle('hidden');
    }
    for (var i = 0, row; row = table1.rows[i]; i++){
       row.cells[0].classList.toggle('hidden');
    }
  });
      $("#apellido").click(function(){
        for (var i = 0, row; row = table.rows[i]; i++){
           row.cells[1].classList.toggle('hidden');
        }
        for (var i = 0, row; row = table1.rows[i]; i++){
           row.cells[1].classList.toggle('hidden');
        }
      });

      $("#email").click(function(){
        for (var i = 0, row; row = table.rows[i]; i++){
           row.cells[2].classList.toggle('hidden');
        }
        for (var i = 0, row; row = table1.rows[i]; i++){
           row.cells[2].classList.toggle('hidden');
        }
      });
      $("#usuario").click(function(){
        for (var i = 0, row; row = table.rows[i]; i++){
           row.cells[3].classList.toggle('hidden');
        }
        for (var i = 0, row; row = table1.rows[i]; i++){
           row.cells[3].classList.toggle('hidden');
        }
      });
      $("#direccion").click(function(){
        for (var i = 0, row; row = table.rows[i]; i++){
           row.cells[4].classList.toggle('hidden');
        }
        for (var i = 0, row; row = table1.rows[i]; i++){
           row.cells[4].classList.toggle('hidden');
        }
      });
      $("#ocultarAlmacen").click(function(){
        for (var i = 0, row; row = table.rows[i]; i++){
           row.cells[6].classList.toggle('hidden');
        }
        for (var i = 0, row; row = table1.rows[i]; i++){
           row.cells[6].classList.toggle('hidden');
        }
      });
      $("#ocultarCosto").click(function(){
        for (var i = 0, row; row = table.rows[i]; i++){
           row.cells[5].classList.toggle('hidden');
        }
        for (var i = 0, row; row = table1.rows[i]; i++){
           row.cells[5].classList.toggle('hidden');
        }
      });
      $("#ocultarEstado").click(function(){
        for (var i = 0, row; row = table.rows[i]; i++){
           row.cells[7].classList.toggle('hidden');
        }
        for (var i = 0, row; row = table1.rows[i]; i++){
           row.cells[7].classList.toggle('hidden');
        }
      });
});
	
	  
    $('#button').click( function () {
        table.row('.selected').remove().draw( false );
    } );
	  
	$('.btn-group-fab').on('click', '.btn', function() {
    $('.btn-group-fab').toggleClass('active');
  });
	  
} );
</script>
@endsection
