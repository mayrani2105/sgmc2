@extends('layouts.ca')
    <!-- /#sidebar-wrapper -->
@section('content')
    <!-- Page Content -->

    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">

      <nav class="navbar navbar-expand-lg navbar-light bg-white border-bottom">
        <button class="btn btn-white" id="menu-toggle"><i class="fas fa-bars fa-1x"></i></button>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <h4 class="text-transparent">  SGMC </h4>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
            <li class="nav-item active">
              <a class="nav-link" href=" "><i class="fas fa-user-tie fa-1x"></i>Pedidos </a>
            </li>
          </ul>
        </div>
      </nav>

      <div class="container-fluid">
        <nav class="navbar navbar-light bg-light">
        <form class="form-inline">
          <input class="form-control" type="search" placeholder="Buscar" aria-label="Search">
          <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar</button>
        </form>
        </nav>

  <div class="container" >
    <h2>Crear Pedido </h2>
    <h5>Proporcione los siguientes datos para crear un nuevo pedido </h5>
      <div class="row">
      <div class="col-md-6 col-md-offset-6">
        <div class="panel panel-login">
          <div class="panel-heading">

            <hr>
          </div>
          <div class="panel-body">
            <div class="row">
              <div class="col-lg-12">

                <form id="register-form" action=" " method="post" role="form">

                  <div class="form-group">

                    <input type="textarea" name="Provpedido" id="Provpedido"  class="form-control" placeholder="Proveedor del Pedido" maxlength="50px" value=""><br/>
                    <input type="textarea" name="Cantpedido" id="Cantpedido"  class="form-control" placeholder="Cantidad de Pedido" maxlength="50px" value=""><br/>
                    <input type="textarea" name="Fechapedido" id="Fechapedido"  class="form-control" placeholder="Fecha del Pedido" maxlength="50px" value=""><br/>
                    <input type="textarea" name="Resppedido" id="Resppedido"  class="form-control" placeholder="Responsable del Pedido" minlength="8px" maxlength="20px" value=""><br/>
                    <input type="textarea" name="Almpedido" id="Almpedido"  class="form-control" placeholder="Almacén para el pedido" minlength="8px" maxlength="20px" value=""><br/>
                    <input type="textarea" name="Sucursal" id="Sucursal"  class="form-control" placeholder="Sucursal" minlength="8px" maxlength="20px" value=""><br/>


                  <div class="form-group">
                    <div class="row">
                      <div class="col-sm-6 col-sm-offset-3">
                        <input type="submit" name="register-submit" id="register-submit" tabindex="4" class="btn btn-outline-success my-2 my-sm-0" value="                         Crear                      ">
                      </div>
                      <div class="col-sm-offset-3">
                        <input type="submit" name="register-submit" id="register-submit" tabindex="4" class="btn btn-outline-danger my-2 my-sm-0" value= "                      Cancelar                     ">
                      </div>
                    </div>
                  </div>


                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
      </div>
    </div>
    <!-- /#page-content-wrapper -->
  <!-- /#wrapper -->

  <!-- Bootstrap core JavaScript -->
  <script src="/jquery/jquery.min.js"></script>
  <script src="/js/bootstrap.bundle.min.js"></script>

  <!-- Menu Toggle Script -->
  <script>
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
  </script>


</body>

</html>
