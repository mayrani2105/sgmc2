@extends('layouts.app')

@section('content')
<script>
	    function showPage() {
    
		console.log("Entra al SP");
		setTimeout(function(){
			console.log("Espera..."); 
			document.getElementById("loader").style.display = "none"; 
			console.log("Termina el Stout");
			document.getElementById("myDiv").style.display = "block";
		
		},5000);
		
		

    }
</script>	
<body class="app header-fixed sidebar-fixed sidebar-lg-show" style="background-image: url('assets/Images/administracion.jpg');" onload="showPage()" style="margin:0;">
<div class="center">

    <div id="loader"></div>

</div>	

<link rel="stylesheet" href="{{ asset('assets/css/loader.css') }}" />
<div style="display:none;" id="myDiv" class="animate-bottom">
<div class="container">
  <div class="row align-items-center justify-content-center auth">
      <div class="col-md-6 col-lg-5">
            <div class="card">
                <div class="card-block">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="auth-header" >
                          <img class="mb-4"  src="{{ asset('assets/Images/logo.jpg') }}" alt="" width="250" height="88.5">
                            <h1 class="text-primary" >SGMC
                          </h1>
                                              </div>
                                                <div class="auth-body">

                              <div class="form-group" class="{'has-danger': errors.has('email'), 'has-success': this.fields.email && this.fields.email.valid }">
                              <label for="email">Correo Electronico</label>
                                                      <div class="input-group input-group--custom">
                                                                        <div class="input-group-addon">
                                                                          <i class="input-icon input-icon--mail"></i>
                                                                        </div>

                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                        </div>
                      </div>

                        <div class="form-group">
                              <label for="password">Contraseña</label>
                                      <div class="input-group input-group--custom">
                                          <div class="input-group-addon"><i class="input-icon input-icon--lock"></i></div>
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group">

                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                      Recordar contraseña
                                    </label>
                                </div>

                        </div>

                        <div class="form-group ">

                                <button type="submit" class="btn btn-primary btn-block btn-spinner">
                                  Iniciar sesión
                                </button>
<br/>
                                @if (Route::has('password.request'))
                                <center>
                                    <a class="auth-ghost-link" href="{{ route('password.request') }}">
                                      ¿Se te olvidó tu contraseña?</a>
                                    </a>
                                  </center>
                                @endif
                            </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
@endsection
