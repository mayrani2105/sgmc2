@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row align-items-center justify-content-center auth">
      <div class="col-md-6 col-lg-5">
            <div class="card">
                <div class="card-block">


                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf
                        <div class="auth-header" >
                          <img class="mb-4"  src="{{ asset('assets/Images/logo.jpg') }}" alt="" width="250" height="88.5">
                            <h1 class="text-primary">SGMC
                          </h1></div>
                        <div class="auth-body">
                        <div class="form-group">
                            <label for="email" class="col-md-8 col-form-label text-md-right">Correo Electronico</label>
                            <div class="input-group input-group--custom">
                                <div class="input-group-addon">
                                    <i class="input-icon input-icon--mail"></i>
                                </div>

                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Correo Electronico">
  </div></div>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror




                        <div class="form-group ">
<center>
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Send Password Reset Link') }}
                                </button>
                              </center>
                                  </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
