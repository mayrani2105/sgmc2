@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><center> <h1 class="text-primary">SGMC Registro de Usuarios</center>
                </h1></div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                              <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="usuario" class="col-md-4 col-form-label text-md-right">Usuario</label>
                        <div class="col-md-6">

                            <input id="usuario" type="text" class="form-control @error('usuario') is-invalid @enderror" name="usuario" required autocomplete="name" autofocus>
                        </div>
                    </div>
                        <div class="form-group row">
                            <label for="aPaterno" class="col-md-4 col-form-label text-md-right">Apellido Paterno</label>
                        <div class="col-md-6">

                            <input id="aPaterno" type="text" class="form-control @error('aPaterno') is-invalid @enderror" name="aPaterno" required autocomplete="name" autofocus>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="aMaterno" class="col-md-4 col-form-label text-md-right">Apellido Materno</label>
                    <div class="col-md-6">

                        <input id="aMaterno" type="text" class="form-control @error('id_bitacora') is-invalid @enderror" name="aMaterno"  required autocomplete="name" autofocus>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="calle" class="col-md-4 col-form-label text-md-right">Calle</label>
                <div class="col-md-6">

                    <input id="calle" type="text" class="form-control @error('calle') is-invalid @enderror" name="calle"  required autocomplete="name" autofocus>
                </div>
            </div>
            <div class="form-group row">
                <label for="colonia" class="col-md-4 col-form-label text-md-right">Colonia</label>
            <div class="col-md-6">

                <input id="colina" type="text" class="form-control @error('colonia') is-invalid @enderror" name="colonia"  required autocomplete="name" autofocus>
            </div>
        </div>
        <div class="form-group row">
            <label for="ciudad" class="col-md-4 col-form-label text-md-right">Ciudad</label>
        <div class="col-md-6">

            <input id="ciudad" type="text" class="form-control @error('ciudad') is-invalid @enderror" name="ciudad"  required autocomplete="name" autofocus>
        </div>
    </div>
        <div class="form-group row">
            <label for="estado" class="col-md-4 col-form-label text-md-right">Estado</label>
        <div class="col-md-6">

            <input id="estado" type="text" class="form-control @error('calle') is-invalid @enderror" name="estado" value="1" required autocomplete="name" autofocus>
        </div>
    </div>

                        <div class="form-group row">
                          <label for="id_rol" class="col-md-4 col-form-label text-md-right">Rol</label>

                        <div class="col-md-6">
                            <select id="id_rol" type="text" class="form-control @error('id_rol') is-invalid @enderror" name="id_rol" value="1" required autocomplete="name" autofocus>
                              <option value ="1">Jefe Operaciones</option>
                              <option value ="2">Administrador</option>
                              <option value ="2">Jefe Almacén</option>
                              <option value ="3">Checador</option>
                              <option value ="4">Distribuidora</option></select>
                              </div>
                            </select>
                      </div>

                      <div class="form-group row">
                          <label for="permisos" class="col-md-4 col-form-label text-md-right">Permisos</label>
                      <div class="col-md-6">

                          <input id="permisos" type="text" class="form-control @error('municipio') is-invalid @enderror" name="permisos" value="111" >
                      </div>
                  </div>

                

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                  Registrar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
