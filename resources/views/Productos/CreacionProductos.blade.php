@extends('layouts.cabe')
<!-- /#sidebar-wrapper -->
@section('content')
<!-- Page Content -->

<div class="col-md-10">
    <center>
        <h3 class="display-4">Creación de productos</h3>
        <br>
    </center>
    <form class="form-inline">
        <input class="form-control col-md-5" type="search" placeholder="Buscar" aria-label="Search">
        <button class="btn btn-primary" type="submit">Buscar</button>
    </form><br>
    @if(session()->has('message'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ session()->get('message') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif

    
      
       
                <div class="bd-example">
        <center>
            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#crearLinea">Crear Linea</button>

            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#crearSublinea">Crear sublinea</button>
        </center><br>

                    <form id="register-form" action={{route('produc.crea')}} method="POST" role="form" class="form-group">
                        {{ csrf_field() }}
                        
                        <div class="row">
                        <div class="col-md-6">
                            <label for="descripcion">Descripcion del producto</label>
                            <input type="textarea" name="descripcion" id="Descripcion" class="form-control" placeholder="Descripción" maxlength="300px" required>
                        </div>
                        <div class="col-md-6">
                            <label for="unidad-base">Seleccione si es unidad o base</label>
                            <select type="text" name="unidadbase" id="Unidad-base" class="form-control" placeholder="Unidad/Base" value="Proveedor" required>
                                <option value="unidad">Unidad</option>
                                <option value="base">Base</option>
                            </select>
                        </div>
                        </div>
                        
                        <div class="row">
                        <div class="col-md-6">
                            <label for="costo">Costo del producto</label>
                            <input type="number" name="costo" id="costo" tabindex="2" class="form-control" placeholder="Costo" required>
                        </div>
                        <div class="col-md-6">
                            <label for="precio">Precio real del producto</label>
                            <input type="number" name="precioreal" id="Precio" tabindex="2" class="form-control" placeholder="Costo" required>
                        </div>
                        </div>
                        
                        <div class="row">
                        <div class="col-md-6">
                            <label for="clave">Clave interna del producto</label>
                            <input type="text" name="clave" id="Clave" tabindex="1" class="form-control" placeholder="Clave" value="" width="20%">
                        </div>


                        <div class="col-md-6">
                            <label for="marca">Marca del producto</label>
                            <select name="marca" id="Marca" class="form-control" maxlength="300px">
                                @foreach($marca as $marcas)
                                <option value="{{$marcas->idMarca}}">
                                    {{$marcas->marca}}
                                </option>
                                @endforeach
                            </select>
                        </div>

                        </div>
                        
                        <div class="row">
                        <div class="col-md-6">
                            <label for="ubicacion">Ubicación</label>
                            <select name="ubicacion" id="Ubicacion" class="form-control" maxlength="300px">
                                @foreach($ubicacion as $u)
                                <option value="{{$u->id_ubicacion}}">
                                    {{$u->nivel}}
                                </option>
                                @endforeach
                            </select>

                        </div>

                        <div class="col-md-6">
                            <label for="clavesat">Clave SAT</label>
                            <select name="clavesat" id="Clavesat" class="form-control" maxlength="300px">
                                @foreach($sat as $s)
                                <option value="{{$s->idsat}}">
                                    {{$s->descripcion}}
                                </option>
                                @endforeach

                            </select>

                        </div>
                        </div>
                        
                        <div class="row">
                        <div class="col-md-6">
                            <label for="almacen">Almacen</label>
                            <select name="almacen" id="Almacen" class="form-control" maxlength="300px">
                                @foreach($almacen as $a)
                                <option value="{{$a->idAlmacen}}">
                                    {{$a->descripcion}}
                                </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-md-6">
                            <label for="subcategoria">Sublinea</label>
                            <select name="subcategoria" id="Subcategoria" class="form-control" maxlength="300px">
                                @foreach($subcategoria as $sub)
                                <option value="{{$sub->idSubCategoria}}">
                                    {{$sub->descripcion}}
                                </option>
                                @endforeach
                            </select>
                        </div>
                            <!--
                            
    </div>-->
                        </div>
                        
                        <div class="row">
                        <div class="col-md-6">
                            <label for="status">Status</label>
                            <select name="status" id="Status" class="form-control" maxlength="300px">
                                @foreach($status as $st)
                                <option value="{{$st->idEstado}}">
                                    {{$st->descripcionEstado}}
                                </option>
                                @endforeach
                            </select>
                        </div>
                        </div>
<br>
<br>

                        <div class="form-group">

                            <div class="bd-example">
                                <center>
                                    <button type="button" class="btn btn-danger btn-lg">Cancelar</button>
                                    <button type="submit" class="btn btn-primary btn-lg">Crear nuevo producto</button>
                                </center>
                            </div>
                        </div>
                    </form>
     
   

<!-- MODAL CREAR LINEA -->
            <div class="modal fade" id="crearLinea" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Crear Linea</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form id="register-form" action="{{route('linea.crea')}}" method="post">

                            {{csrf_field()}}
                            <div class="modal-body">
                                @include('Lineas_Sublineas.CrearLinea')
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger my-2 my-sm-0" data-dismiss="modal">Cancelar</button>
                                <button type="submit" class="btn btn-primary my-2 my-sm-0" value="Crear linea">Guardar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!--MODAL CREAR SUBLINEA-->
            <div class="modal fade" id="crearSublinea" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Crear Sublinea</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form id="register-form" action="{{route('sublinea.crea')}}" method="post">
                            {{csrf_field()}}
                            <div class="modal-body">
                            
                            @include('Lineas_Sublineas.CrearSublinea')
                                </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger my-2 my-sm-0" data-dismiss="modal">Cancelar</button>
                                <button type="submit" class="btn btn-primary my-2 my-sm-0" value="Crear linea">Guardar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

    @endsection