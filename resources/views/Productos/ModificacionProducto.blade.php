<div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="verGeneralEd">
        <div class="row">
            <div class="col-md-11">
                <label for="Descripcion">Descripción</label>
                <input type="textarea" name="Descripcion" id="Descripcion" class="form-control" placeholder="Descripción" maxlength="300px" required>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <label for="status">status</label>
                <select type="text" name="status" id="Status" class="form-control">
                    <option selected value="{{$producto->idstatus}}" >{{$producto ->status}}</option>
                    @foreach($status as $st)
                    <option value="{{$st->idEstado}}">
                        {{$st->descripcionEstado}}
                    </option>
                    @endforeach
                </select>
            </div>

            <div class="col-md-6">
                <label for="Unidadbase">Unidad/Base</label>
                <select type="text" name="Unidadbase" id="Unidadbase" class="form-control">
                    <option value="{{$producto ->unidadbase}}" selected>{{$producto ->unidadbase}}</option>
                    <option value="Pieza">Pieza</option>
                    <option value="Base">Base</option>
                </select>
            </div>

        </div>

        <div class="row">

            <div class="col-md-6">
                <label for="costo">Costo</label>
                <input type="number" name="costo" id="Costo" class="form-control" required>
            </div>

            <div class="col-md-6">
                <label for="Precioreal">Precio Real</label>
                <input type="number" name="precioreal" id="Precioreal" class="form-control" required>
            </div>

        </div>


        <div class="row">
            <div class="col-md-6">
                <label for="Clave">Clave</label>
                <input type="text" name="Clave" id="Clave" tabindex="1" class="form-control" placeholder="Clave" width="20%" required>
            </div>


            <div class="col-md-6">
                <label for="Clavesat">Clave SAT</label>
                <select type="text" name="Clavesat" id="Clavesat" tabindex="1" class="form-control">
                    <option value="{{$producto ->idsat}}" selected>{{$producto ->descripcionsat}}</option>
                    @foreach($sat as $s)
                    <option value="{{$s->idsat}}">
                        {{$s->descripcion}}
                    </option>
                    @endforeach

                </select>
            </div>
        </div>

        <div class="row">

            <div class="col-md-6">
                <label for="Marca">Marca</label>
                <select type="text" name="Marca" id="Marca" class="form-control">
                    <option  selected value="{{$producto->idmarca}}">{{$producto ->marca}}</option>
                    @foreach($marca as $marcas)
                    <option value="{{$marcas->idMarca}}">
                        {{$marcas->marca}}
                    </option>
                    @endforeach
                </select>

            </div>


            <div class="col-md-6">
               
            </div>
        </div>


        <div class="row">
            
            <div class="col-md-6">
                <label for="status">Linea</label>
                <select type="text" name="categoria" id="Categoria" class="form-control">
                    <option selected value="{{$producto->idcategoria}}">{{$producto ->categoria}}</option>
                    @foreach($categorias as $cat)
                    <option value="{{$cat->idCategoria}}">
                        {{$cat->descripcion}}
                    </option>
                    @endforeach
                    
                </select>
            </div>
            
            <div class="col-md-6">
                <label for="categoria">Sublinea</label>
                <select type="" name="subcategoria" id="Subcategoria" class="form-control">
                    <option selected>{{$producto ->subcategoria}}</option>
                    
                </select>
            </div>

            

        </div>
    </div>
</div>
<script>
$(document).ready(function() {

    $('select[name="categoria"]').on('change', function(){
        var countryId = $(this).val();
        if(countryId) {
            $.ajax({
                url: '/ed/'+countryId,
                type:"GET",
                dataType:"json",
                beforeSend: function(){
                    $('#loader').css("visibility", "visible");
                },

                success:function(data) {

                    $('select[name="subcategoria"]').empty();

                    $.each(data, function(key, value){

                        $('select[name="subcategoria"]').append('<option value="'+ key +'">' + value + '</option>');

                    });
                },
                complete: function(){
                    $('#loader').css("visibility", "hidden");
                }
            });
        } else {
            $('select[name="categoria"]').append('Seleccione Linea');
        }

    });

});
</script>