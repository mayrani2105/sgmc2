@extends('layouts.cabe')
<!-- /#sidebar-wrapper -->
@section('content')
<!-- Page Content -->

<style>
    .btn-group-fab {
        position: fixed;
        width: 50px;
        height: auto;
        right: 20px;
        bottom: 20px;
    }

    .btn-group-fab div {
        position: relative;
        width: 100%;
        height: auto;
    }
.hidden{ display: none; }
    .btn-group-fab .btn {
        position: absolute;
        bottom: 0;
        border-radius: 50%;
        display: block;
        margin-bottom: 4px;
        width: 40px;
        height: 40px;
        margin: 4px auto;
    }
    .asc:after {
       content: ' ↑';
    }
    .desc:after {
       content: " ↓";
    }

    table tr th {
       cursor: pointer;
       -webkit-user-select: none;
       -moz-user-select: none;
       -ms-user-select: none;
       user-select: none;
    }

    .btn-group-fab .btn-main {
        width: 50px;
        height: 50px;
        right: 50%;
        margin-right: -25px;
        z-index: 9;
    }

    .btn-group-fab .btn-sub {
        bottom: 0;
        z-index: 8;
        right: 50%;
        margin-right: -20px;
        -webkit-transition: all 2s;
        transition: all 0.5s;
    }

    .btn-group-fab.active .btn-sub:nth-child(2) {
        bottom: 60px;
    }

    .btn-group-fab.active .btn-sub:nth-child(3) {
        bottom: 110px;
    }

    .btn-group-fab.active .btn-sub:nth-child(4) {
        bottom: 160px;
    }

    .btn-group-fab.active .btn-sub:nth-child(5) {
        bottom: 210px;
    }

    .nav-link,.nav-link-active{
    color:#aaaa;

    }
    .modal-header{
      background-color: #344478;
      color:#ffffff;
    }
    .has-search .form-control {
        padding-left: 2.375rem;
    }

    .has-search .form-control-feedback {
        position: absolute;
        z-index: 2;
        display: block;
        width: 2.375rem;
        height: 2.375rem;
        line-height: 2.375rem;
        text-align: center;
        pointer-events: none;
        color: #aaa;
    }
</style>
<div class="container-fluid">
<div class="flash-message">
  @foreach (['danger', 'warning', 'success', 'info'] as $msg)
    @if(Session::has('alert-' . $msg))
    <div class="alert alert-{{ $msg }} alert-dismissible fade show" role="alert">{{ Session::get('alert-' . $msg) }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
    @endif
  @endforeach
</div>

    <center>
      <!--  <h3 class="display-4">Catálogo de Productos</h3>-->
        <br>
    </center>
<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #344478">

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
   <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link dropdown-toggle" href="#" style="color: #FFFFFF" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			<i class="fas fa-sliders-h"></i> Filtros
		</a>
	  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
      <a class="dropdown-item" id="ocultarId" href="#">Id</a>
      <a class="dropdown-item" id="ocultar" href="#">Clave</a>
      <a class="dropdown-item" id="ocultarP"href="#">Descripción</a>
      <a class="dropdown-item" id="oculltarCorreo"href="#">Status</a>
      <a class="dropdown-item" id="ocultarPromos"href="#">Marca</a>
      <a class="dropdown-item" id="ocultardiascredito"href="#">Precio Real</a>
      <a class="dropdown-item" id="oculltarTelefono"href="#">Costo</a>
      <a class="dropdown-item" id="oculltarRFC"href="#">Clave Sat</a>
	  </div>
      </li>
    </ul>
          <h4 class="display-4" style="color:#fff; margin-right:150px;"> Catálogo Productos</h4>
    <form class="form-inline my-10 my-lg-0">
    <div>
    <div class="form-group has-search">
    <span class="fa fa-search form-control-feedback"style="color:#344478;"></span>
    <input type="text" class="form-control"id="search"name="search" type="search" placeholder="Buscar">
    </div>
    </div>
  </form><br/>
  </div>
</nav>
<div>
    <!-- @if(session()->has('message'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ session()->get('message') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif

-->
<br/>


@foreach($permiso as $permi)
    <table class="table table-striped hidden"data-toggle="table" id="mitabla2">
        <thead>

            <tr >

                <th  scope="col">ID </th>
                <th scope="col">Clave</th>
                <th  scope="col">Descripción</th>

                <th scope="col">Marca</th>
                <th scope="col">Precio Real</th>
                <th cope="col">Costo</th>
                <th scope="col">Clave Sat</th>
                <th class="text-center"scope="col">Opciones</th>

            </tr>

        </thead>
        <tbody class="busca">
            @foreach($producto as $producto)
            <tr>
                <td >{{$producto ->id_Producto}}</td>
                <td>{{$producto ->clave}}</td>
                <td >{{$producto ->descripcion}}</td>


  <td>{{$producto ->marca}}</td>
  <td>${{$producto ->precioreal}}.00</td>
  <td >${{$producto ->costo}}.00</td>
  <td >{{$producto ->descripcionsat}}</td>
                <td style="width:178px;" >

                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#vistaDetallada" data-id="{{$producto->id_Producto}}" data-descripcion="{{$producto->descripcion}}" data-unidadbase="{{$producto->unidadbase}}" data-costo="{{$producto->costo}}" data-clave="{{$producto->clave}}" data-marca="{{$producto->marca}}"  data-sat="{{$producto->sat}}" data-categoria="{{$producto->categoria}}" data-subcategoria="{{$producto->subcategoria}}" data-status="{{$producto->status}}" data-real="{{$producto->precioreal}}">
                      <i class="far fa-eye"></i></button>
@if($permi->editar==1)
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#edit" data-id="{{$producto->id_Producto}}" data-descripcion="{{$producto->descripcion}}" data-unidadbase="{{$producto->unidadbase}}" data-costo="{{$producto->costo}}" data-clave="{{$producto->clave}}" data-marca="{{$producto->marca}}"data-idmarca="{{$producto->idmarca}}" data-sat="{{$producto->sat}}" data-idsat="{{$producto->idsat}}" data-subcategoria="{{$producto->subcategoria}}"data-idsubcategoria="{{$producto->idsubcategoria}}" data-categoria="{{$producto->categoria}}"
                       data-idcategoria="{{$producto->idcategoria}}" data-status="{{$producto->status}}" data-idstatus="{{$producto->idstatus}}" data-real="{{$producto->precioreal}}" name="editar" onclick="buscar()"><i class="far fa-edit"></i></button>
@endif
@if($permi->borrar==01 )
 <a href="{{route('produc.destroy',$producto ->id_Producto)}}" class="btn btn-danger" onclick="return confirm('¿Está seguro de eliminar este producto de manera permanente?')"><i class="fas fa-trash-alt"></i></a>
          @endif @endforeach     </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <table class="table table-striped "data-toggle="table" id="mitabla">
        <thead>
            <tr >
                <th  scope="col">ID </th>
                <th scope="col">Clave</th>
                <th  scope="col">Descripción</th>

                <th scope="col">Marca</th>
                <th scope="col">Precio Real</th>
                <th cope="col">Costo</th>
                <th scope="col">Clave Sat</th>
                <th class="text-center"scope="col">Opciones</th>
            </tr>
        </thead>
        <tbody class="busca">
            @foreach($productos as $producto)
            <tr>
                <td >{{$producto ->id_Producto}}</td>
                <td>{{$producto ->clave}}</td>
                <td >{{$producto ->descripcion}}</td>


  <td>{{$producto ->marca}}</td>
  <td>${{$producto ->precioreal}}.00</td>
  <td >${{$producto ->costo}}.00</td>
  <td >{{$producto ->descripcionsat}}</td>
                <td style="width:178px;" >
@foreach($permiso as $permi)

                                     <button type="button" class="btn btn-success" data-toggle="modal" data-target="#vistaDetallada" data-id="{{$producto->id_Producto}}" data-descripcion="{{$producto->descripcion}}" data-unidadbase="{{$producto->unidadbase}}" data-costo="{{$producto->costo}}" data-clave="{{$producto->clave}}" data-marca="{{$producto->marca}}"  data-sat="{{$producto->sat}}" data-categoria="{{$producto->categoria}}" data-subcategoria="{{$producto->subcategoria}}" data-status="{{$producto->status}}" data-real="{{$producto->precioreal}}">
                                       <i class="far fa-eye"></i></button>
                 @if($permi->editar==1)
                                     <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#edit" data-id="{{$producto->id_Producto}}" data-descripcion="{{$producto->descripcion}}" data-unidadbase="{{$producto->unidadbase}}" data-costo="{{$producto->costo}}" data-clave="{{$producto->clave}}" data-marca="{{$producto->marca}}"data-idmarca="{{$producto->idmarca}}" data-sat="{{$producto->sat}}" data-idsat="{{$producto->idsat}}" data-subcategoria="{{$producto->subcategoria}}"data-idsubcategoria="{{$producto->idsubcategoria}}" data-categoria="{{$producto->categoria}}"
                                        data-idcategoria="{{$producto->idcategoria}}" data-status="{{$producto->status}}" data-idstatus="{{$producto->idstatus}}" data-real="{{$producto->precioreal}}" name="editar" onclick="buscar()"><i class="far fa-edit"></i></button>
                 @endif
                 @if($permi->borrar==01 )
                  <a href="{{route('produc.destroy',$producto ->id_Producto)}}" class="btn btn-danger" onclick="return confirm('¿Está seguro de eliminar este producto de manera permanente?')"><i class="fas fa-trash-alt"></i></a>
                           @endif @endforeach     </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="" id="paginacion">
    {{$productos->links()}}
    </div>






            <!-- /#page-content-wrapper -->
            <!-- /#wrapper -->

            <!-- Floating Action Button like Google Material -->
            <div class="btn-group-fab" role="group" aria-label="FAB Menu">
                <div>
                    <button type="button" class="btn btn-main btn-primary has-tooltip" data-placement="left" title="Menu"> <i class="fa fa-bars"></i> </button>
                    <button type="button" class="btn btn-sub btn-info has-tooltip" data-toggle="modal" data-target="#crearProducto" data-placement="left" title="Agregar Producto"> <i class="fas fa-plus"></i> </button>
                    <button type="button" class="btn btn-sub btn-warning has-tooltip"data-toggle="modal" data-target="#crearLinea" data-placement="left" title="Crear Linea"> <i class="fas fa-grip-lines"></i> </button>
                    <button type="button" class="btn btn-sub btn-info has-tooltip"data-toggle="modal" data-target="#crearSublinea" data-placement="left" title="Crear sublinea"> S<i class="fas fa-line-columns"></i> </button>
                    <button type="button" class="btn btn-sub btn-dark has-tooltip" data-toggle="modal" data-target="#ayuda" data-placement="left" title="Ayuda"> <i class="fas fa-question"></i></button>
                </div>
            </div>
            <script>
                $(function() {
                    $('.btn-group-fab').on('click', '.btn', function() {
                        $('.btn-group-fab').toggleClass('active');
                    });
                    $('has-tooltip').tooltip();
                });
            </script>


            <!-- MODAL VISTA DETALLADA PRODUCTO-->
            <div class="modal fade" id="vistaDetallada" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Informacion detallada del producto</h5>
                            <button type="button"style="  color: #FFFFFF;" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>


                        {{csrf_field()}}
                        <div class="modal-body">
                            <div role="tabpanel">

                            <input type="hidden" name="id" id="id">

                            @include('Productos.VistaDetalladaProducto')

                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-eye-slash"></i></button>

                        </div>

                    </div>
                </div>
            </div>

            <!-- MODAL EDITAR PRODUCTO-->
            <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Editar producto</h5>
                            <button type="button"style="  color: #FFFFFF;" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form id="register-form" action="{{route('produc.actualiza',$producto->id_Producto)}}" method="post">

                            {{csrf_field()}}
                            <div class="modal-body"><div role="tabpanel">

  <input type="hidden" name="usuario" id="usuario" value="{{Auth::user()->id}}">
                                <input type="hidden" name="id" id="id">

                                @include('Productos.ModificacionProducto')
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger my-2 my-sm-0" data-dismiss="modal"><i class="fas fa-eye-slash"></i></button>
                                <button type="submit" class="btn btn-primary my-2 my-sm-0" value="Modificar Producto"><i class="far fa-save"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!--MODAL ELIMINAR PRODUCTO-->
            <div class="modal fade" id="eliminar" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Eliminar Producto</h5>
                            <button type="button"style="  color: #FFFFFF;" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>¿Seguro que deseas Eliminar el producto?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-danger">Eliminar</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- MODAL CREAR LINEA -->
            <div class="modal fade" id="crearLinea" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Crear Linea</h5>
                            <button type="button"style="  color: #FFFFFF;" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form id="register-form" action="{{route('linea.crea')}}" method="post">

                            {{csrf_field()}}
                            <div class="modal-body">
                                @include('Lineas_Sublineas.CrearLinea')
                                  <input type="hidden" name="usuario" id="usuario" value="{{Auth::user()->id}}">

                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger my-2 my-sm-0" data-dismiss="modal"><i class="fas fa-eye-slash"></i></button>
                                <button type="submit" class="btn btn-primary my-2 my-sm-0" value="Crear linea"><i class="far fa-check-square fa-lg"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!--MODAL CREAR SUBLINEA-->
            <div class="modal fade" id="crearSublinea" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog " role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Crear Sublinea</h5>
                            <button type="button" style="  color: #FFFFFF;"class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form id="register-form" action="{{route('sublinea.crea')}}" method="post">
                            {{csrf_field()}}
                            <div class="modal-body">

                                @include('Lineas_Sublineas.CrearSublinea')
                                  <input type="hidden" name="usuario" id="usuario" value="{{Auth::user()->id}}">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger my-2 my-sm-0" data-dismiss="modal"><i class="fas fa-eye-slash"></i></button>
                                <button type="submit" class="btn btn-primary my-2 my-sm-0" value="Crear linea"><i class="far fa-check-square fa-lg"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!--Modal crear producto-->

            <div class="modal fade bd-example-modal-lg" id="crearProducto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Crear Producto</h5>
                            <button type="button" style="  color: #FFFFFF;"class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form id="register-form" action="{{route('produc.crea')}}" method="post">
                            {{csrf_field()}}
                            <div class="modal-body">
                                <div role="tabpanel">
       <input type="hidden" name="usuario" id="usuario" value="{{Auth::user()->id}}">
                                    @include('Productos.CreacionProductosModal')
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>

            <!--Modal de Ayuda-->

<div class="modal fade bd-example-modal-lg" id="ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Ayuda Productos </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <input type="hidden" name="id" id="id">
      <h2>Bienvenido {{Auth::user()->name}} </h2>
        <br/>
      @if(Auth::user()->id_rol!=5)
       <button type="button" class="btn btn-success"><i class="fas fa-eye" style="height:4px;"></i></button><strong>  Ver Detalles</strong> <br/>
          <p>En esta opcion el usuario podra encontrar informacion detallada de cada producto<br/>
          La informacion que muestra los campos son :</p>
          <ul>
            <li>Nombre del Producto</li>
            <li>Clave del Producto</li>
            <li>Marca</li>
            <li>Proveedor</li>
            <li>Estado del producto</li>
            <li>Costos</li>
          </ul>
         @endif
          @if(Auth::user()->id_rol==2 || Auth::user()->id_rol==1)
            <button type="button" class="btn btn-primary"><i class="fas fa-edit"></i></button><strong> Modificar Producto </strong><br/>
            <p>En esta opcion el usuario podra modificar los datos de cada uno de los productos.</p><br/>
            <button type="button" class="btn btn-danger"><i class="fas fa-trash-alt"></i></button><strong> Eliminar Producto </strong><br/>
             <p>En esta opcion el usuario podra eliminar el producto</p> <br/>
        @endif
        <button class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="buscar"><i class="fas fa-search"></i></button>
        <strong>Buscar  </strong><br/><p>En esta opción el usuario podrá buscar en tiempo real los productos.</p><br/>
        @if(Auth::user()->id_rol==2 || Auth::user()->id_rol==1)
            <button type="button" class="btn btn-sub btn-info has-tooltip" data-placement="left" title="Agregar Proveedor"> <i class="fas fa-plus"></i></button>
            <strong>Agregar Producto Nuevo </strong><br/>
            <p>En esta opción el usuario podrá agregar un nuevo producto al sistema.</p> <br/>
            <button type="button" class="btn btn-sub btn-warning has-tooltip" data-placement="left" title="Crear Linea"> <i class="fas fa-grip-lines"></i></button><strong>Crear Linea</strong>
            <p>En esta opción el usuario podrá agregar una nueva línea al sistema.</p>
            <button type="button" class="btn btn-sub btn-info has-tooltip" data-placement="left" title="Crear sublinea"> S<i class="fas fa-line-columns"></i></button> <strong>Crear Subline</strong>
            <p>En esta opción el usuario podrá agregar una nueva sublinea al sistema, siempre y cuando la línea ya exista dentro del sistema.</p>
            <strong>  Atención: </strong><p>Para poder realizar un registro es necesario llenar todos los campos.</p><br/>
        @endif

      </div>
      <div class="modal-footer">

        <button type="button" class="btn btn-danger"data-dismiss="modal"data-toggle="tooltip" data-placement="top" title="Cerrar Ayuda"><i class="fas fa-eye-slash"></i></button>
      </div>
    </div>
  </div>
</div>
<!--Termina Modal de Ayuda-->
<script>
  $(document).ready(function () {
      (function ($) {
          $('#search').keyup(function () {

               var rex = new RegExp($(this).val(), 'i');

               if(rex=='/(?:)/i'){
                 $('#mitabla2').hide();
                 $('#mitabla').show();
                 $('#paginacion').show();
               }else{
                 $('#mitabla2').show();
                 $('#mitabla').hide();
               $('#paginacion').hide();}
               $('.busca tr').hide();
               $('.busca tr').filter(function () {
                 return rex.test($(this).text());
               }).show();
          })

      }(jQuery));

  });
</script>
<script>
$(document).ready(function(){
  $('th').click(function() {
  var table = $(this).parents('table').eq(0)
  var rows = table.find('tr:gt(0)').toArray().sort(comparer($(this).index()))
  this.asc = !this.asc
  if (!this.asc) {
     rows = rows.reverse()
  }
  for (var i = 0; i < rows.length; i++) {
     table.append(rows[i])
  }
  setIcon($(this), this.asc);

});
function comparer(index) {
  return function(a, b) {
     var valA = getCellValue(a, index),
     valB = getCellValue(b, index)
     return $.isNumeric(valA) && $.isNumeric(valB) ? valA - valB : valA.localeCompare(valB)
  }
}
function getCellValue(row, index) {
  return $(row).children('td').eq(index).html()
}
function setIcon(element, asc) {
  $("th").each(function(index) {
     $(this).removeClass("sorting");
     $(this).removeClass("asc");
     $(this).removeClass("desc");
  });
  element.addClass("sorting");
  if (asc) element.addClass("asc");
  else element.addClass("desc");
}
  let table = document.getElementById('mitabla2');
  let table2 = document.getElementById('mitabla');
  $("#ocultarId").click(function(){

    for (var i = 0, row; row = table.rows[i]; i++){
       row.cells[0].classList.toggle('hidden');
    }
    for (var i = 0, row; row = table2.rows[i]; i++){
       row.cells[0].classList.toggle('hidden');
    }
  });
      $("#ocultar").click(function(){
        for (var i = 0, row; row = table.rows[i]; i++){
           row.cells[1].classList.toggle('hidden');
        }
        for (var i = 0, row; row = table2.rows[i]; i++){
           row.cells[1].classList.toggle('hidden');
        }
      });

      $("#ocultarP").click(function(){
        for (var i = 0, row; row = table.rows[i]; i++){
           row.cells[2].classList.toggle('hidden');
        }
        for (var i = 0, row; row = table2.rows[i]; i++){
           row.cells[2].classList.toggle('hidden');
        }
      });
      $("#oculltarCorreo").click(function(){
        for (var i = 0, row; row = table.rows[i]; i++){
           row.cells[3].classList.toggle('hidden');
        }
        for (var i = 0, row; row = table2.rows[i]; i++){
           row.cells[3].classList.toggle('hidden');
        }

      });
      $("#ocultardiascredito").click(function(){
        for (var i = 0, row; row = table.rows[i]; i++){
           row.cells[4].classList.toggle('hidden');
        }
        for (var i = 0, row; row = table2.rows[i]; i++){
           row.cells[4].classList.toggle('hidden');
        }
      });
      $("#oculltarTelefono").click(function(){
        for (var i = 0, row; row = table.rows[i]; i++){
           row.cells[6].classList.toggle('hidden');
        }
        for (var i = 0, row; row = table2.rows[i]; i++){
           row.cells[6].classList.toggle('hidden');
        }
      });
      $("#ocultarPromos").click(function(){
        for (var i = 0, row; row = table.rows[i]; i++){
           row.cells[5].classList.toggle('hidden');
        }
        for (var i = 0, row; row = table2.rows[i]; i++){
           row.cells[5].classList.toggle('hidden');
        }
      });
      $("#oculltarRFC").click(function(){
        for (var i = 0, row; row = table.rows[i]; i++){
           row.cells[7].classList.toggle('hidden');
        }
        for (var i = 0, row; row = table2.rows[i]; i++){
           row.cells[7].classList.toggle('hidden');
        }
      });
      $("#oculltarcalle").click(function(){
        for (var i = 0, row; row = table.rows[i]; i++){
           row.cells[9].classList.toggle('hidden');
        }
        for (var i = 0, row; row = table2.rows[i]; i++){
           row.cells[9].classList.toggle('hidden');
        }
      });
      $("#oculltarcolonia").click(function(){
        for (var i = 0, row; row = table.rows[i]; i++){
           row.cells[8].classList.toggle('hidden');
        }
        for (var i = 0, row; row = table2.rows[i]; i++){
           row.cells[8].classList.toggle('hidden');
        }
      });
});
</script>
            <!--Script editar Producto-->
            <script>
                $('#edit').on('show.bs.modal', function(event) {

                    var button = $(event.relatedTarget) // Button that triggered the modal
                    var id = button.data('id')
                    var idu = button.data('idubicacion')
                    var descripcion = button.data('descripcion')
                    var unidadbase = button.data('unidadbase')
                    var costo = button.data('costo')
                    var real = button.data('real')
                    var clave = button.data('clave')
                    /*var marca = button.data('marca')
                    var nivel = button.data('nivel')
                    var piso = button.data('piso')
                    var anaquel = button.data('anaquel')
                    var pasillo = button.data('pasillo')
                    var sat = button.data('sat')
                    var almacen = button.data('almacen')
                    var categoria = button.data('categoria')
                    var status = button.data('status')*/
                    var modal = $(this)

                    modal.find('.modal-body #id').val(id)
                    modal.find('.modal-body #idubicacion').val(idu)
                    modal.find('.modal-body #Descripcion').val(descripcion)
                    modal.find('.modal-body #Unidadbase').val(unidadbase)
                    modal.find('.modal-body #Costo').val(costo)
                    modal.find('.modal-body #Precioreal').val(real)
                    modal.find('.modal-body #Clave').val(clave)
                   /* modal.find('.modal-body #Marca').val(marca)
                    modal.find('.modal-body #Nivel').val(nivel)
                    modal.find('.modal-body #Piso').val(piso)
                    modal.find('.modal-body #Anaquel').val(anaquel)
                    modal.find('.modal-body #Pasillo').val(pasillo)
                    modal.find('.modal-body #Clavesat').val(sat)
                    modal.find('.modal-body #Almacen').val(almacen)
                    modal.find('.modal-body #Categoria').val(categoria)
                    modal.find('.modal-body #Status').val(status)*/
                })

                 function buscar() {
               $('#edit').on('click', function(){
                   var id = button.data('id')

        alert(id);

               });
}
            </script>

            <!--Script vista detallada-->
            <script>
                $('#vistaDetallada').on('show.bs.modal', function(event) {

                    var button = $(event.relatedTarget) // Button that triggered the modal
                    var id = button.data('id')
                    var descripcion = button.data('descripcion')
                    var unidadbase = button.data('unidadbase')
                    var costo = button.data('costo')
                    var real = button.data('real')
                    var clave = button.data('clave')
                    var marca = button.data('marca')
                   /* var nivel = button.data('nivel')
                    var piso = button.data('piso')
                    var anaquel = button.data('anaquel')
                    var pasillo = button.data('pasillo')

                    var almacen = button.data('almacen')*/
                    var sat = button.data('sat')
                    var categoria = button.data('categoria')
                    var subcategoria = button.data('subcategoria')
                    var status = button.data('status')
                    var modal = $(this)

                    modal.find('.modal-body #id').val(id)
                    modal.find('.modal-body #Descripcion').val(descripcion)
                    modal.find('.modal-body #Unidadbase').val(unidadbase)
                    modal.find('.modal-body #Precioreal').val(real)
                    modal.find('.modal-body #Costo').val(costo)
                    modal.find('.modal-body #Clave').val(clave)
                    modal.find('.modal-body #Marca').val(marca)
                    /*modal.find('.modal-body #Nivel').val(nivel)
                    modal.find('.modal-body #Piso').val(piso)
                    modal.find('.modal-body #Anaquel').val(anaquel)
                    modal.find('.modal-body #Pasillo').val(pasillo)

                    modal.find('.modal-body #Almacen').val(almacen)*/
                    modal.find('.modal-body #Clavesat').val(sat)
                    modal.find('.modal-body #Categoria').val(categoria)
                    modal.find('.modal-body #SubCategoria').val(subcategoria)
                    modal.find('.modal-body #Status').val(status)
                })
            </script>
            <!--Script color del status-->
            <script type="text/javascript">
              $(document).ready(function() {
                var table = $('#example').DataTable();

                $('#example tbody').on( 'click', 'tr', function () {
                    if ( $(this).hasClass('selected') ) {
                        $(this).removeClass('selected');
                    }
                    else {
                        table.$('tr.selected').removeClass('selected');
                        $(this).addClass('selected');
                    }
                } );

                $('#button').click( function () {
                    table.row('.selected').remove().draw( false );
                } );
            } );
            </script>
            <script>
                var listaTD = tabla1.getElementsByClassName("autoColor");
                var valoresTD = [];
                var coloresTD = [0, 1, 2];
                var colores = ["#EC7063", "#58D68D", "#F5B041"];
                var colorIndice = 0;
                var i;
                for (let td = 0; td < listaTD.length; td++) {
                    i = valoresTD.findIndex(function(valor) {
                        return valor == listaTD[td].innerHTML
                    });
                    if (i == -1) {
                        valoresTD.push(listaTD[td].innerHTML);
                        i = valoresTD.length - 1;
                        coloresTD.push(colorIndice++);
                        if (colorIndice >= colores.length) colorIndice = 0;
                    }
                    listaTD[td].style.backgroundColor = colores[coloresTD[i]];
                }

            </script>

            <!--SCRIPT PARA REALIZAR BUSQUEDA EN TIEMPO REAL EN LA TABLA-->



</div>
</div>

            @endsection
