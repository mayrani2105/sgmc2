


<style>
    #loader{
   visibility:hidden;
   }
</style>
<div class="tab-content">
    <form id="register-form" action="{{route('produc.crea')}}" method="post">
                            {{csrf_field()}}
    <div role="tabpanel" class="tab-pane active" id="verGeneral">
        <div class="row">
            <div class="col-md-6">
                <label for="descripcion">Descripcion del producto</label>
                <input type="textarea" name="descripcion" id="Descripcion" class="form-control" placeholder="Descripción" maxlength="300px" required>
            </div>
        
        <div class="col-md-6">
                <label for="marca">Marca del producto</label>
                <select name="marca" id="Marca" class="form-control" maxlength="300px">
                    @foreach($marca as $marcas)
                    <option value="{{$marcas->idMarca}}">
                        {{$marcas->marca}}
                    </option>
                    @endforeach
                </select>
            </div> 
        </div>
        
        <div class="row">
            
            <div class="col-md-6">
                <label for="status">Status</label>
                <select name="status" id="Status" class="form-control" maxlength="300px">
                    @foreach($status as $st)
                    <option value="{{$st->idEstado}}">
                        {{$st->descripcionEstado}}
                    </option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-6">
                <label for="unidad-base">Seleccione si es unidad o base</label>
                <select type="text" name="unidadbase" id="Unidad-base" class="form-control" placeholder="Unidad/Base" value="Proveedor" required>
                    <option value="unidad">Unidad</option>
                    <option value="base">Base</option>
                </select>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <label for="costo">Costo del producto</label>
                <input type="number" name="costo" id="costo" tabindex="2" class="form-control" placeholder="Costo" required>
            </div>
            <div class="col-md-6">
                <label for="precio">Precio real del producto</label>
                <input type="number" name="precioreal" id="Precio" tabindex="2" class="form-control" placeholder="Precio real" required>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <label for="clave">Clave interna del producto</label>
                <input type="text" name="clave" id="Clave" tabindex="1" class="form-control" placeholder="Clave" value="" width="20%">
            </div>
            
            
            <div class="col-md-6">
                <label for="clavesat">Clave SAT</label>
                <select name="clavesat" id="Clavesat" class="form-control" maxlength="300px">
                    @foreach($sat as $s)
                    <option value="{{$s->idsat}}">
                        {{$s->descripcion}}
                    </option>
                    @endforeach

                </select>

            </div>


            

        </div>


        <div class="row">
            
                <div class="col-md-6">
                
                <label for="subcategoria">Linea</label>
                <select name="categoria" id="Categoria" class="form-control" maxlength="300px">
                    <option value=""> Seleccione Linea</option>
                    @foreach($categorias as $cat)
                    <option value="{{$cat->idCategoria}}">
                        {{$cat->descripcion}}
                    </option>
                    @endforeach
                </select>
            </div>
                <div class="col-md-6">
                <label for="subcategoria">Sublinea</label>
                <select name="subcategoria" id="Subcategoria" class="form-control" maxlength="300px">
                    <option>Seleccione Sublinea</option>
                </select>
                    
            </div>
            <div class="col-md-2"><span id="loader"><i class="fa fa-spinner fa-3x fa-spin"></i></span></div>
            
        </div>

        <div class="row">

        </div>
    </div>

    
        <div class="modal-footer">
                                <button type="button" class="btn btn-danger my-2 my-sm-0" data-dismiss="modal"><i class="fas fa-eye-slash"></i></button>
                                <button type="submit" class="btn btn-primary my-2 my-sm-0" value="Crear linea"><i class="far fa-check-square fa-lg"></i></button>
                            </div>
                        
                        </form>
    <div role="tabpanel" class="tab-pane " id="creacionSublinea">
        <form id="register-form" action="{{route('sublinea.crea')}}" method="post">
                        {{csrf_field()}}
                        <div class="modal-body">

                            @include('Lineas_Sublineas.CrearSublinea')
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger my-2 my-sm-0" data-dismiss="modal"><i class="fas fa-eye-slash"></i></button>
                            <button type="submit" class="btn btn-primary my-2 my-sm-0" value="Crear linea"><i class="far fa-check-square fa-lg"></i></button>
                        </div>
                    </form>
    </div>

      

    <div role="tabpanel" class="tab-pane " id="creacionLinea">
        <form id="register-form" action="{{route('linea.crea')}}" method="post">
                        {{csrf_field()}}
                        <div class="modal-body">

                            @include('Lineas_Sublineas.CrearLinea')
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger my-2 my-sm-0" data-dismiss="modal"><i class="fas fa-eye-slash"></i></button>
                            <button type="submit" class="btn btn-primary my-2 my-sm-0" value="Crear linea"><i class="far fa-check-square fa-lg"></i></button>
                        </div>
                    </form>
    </div>
    
    
</div>
<script src="{{ asset('js/app.js') }}"></script>
 <script src="{{ asset('js/custom.js') }}"></script>

 <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
<script>
$(document).ready(function() {

    $('select[name="categoria"]').on('change', function(){
        var countryId = $(this).val();
        if(countryId) {
            $.ajax({
                url: '/sub/'+countryId,
                type:"GET",
                dataType:"json",
                beforeSend: function(){
                    $('#loader').css("visibility", "visible");
                },

                success:function(data) {

                    $('select[name="subcategoria"]').empty();

                    $.each(data, function(key, value){

                        $('select[name="subcategoria"]').append('<option value="'+ key +'">' + value + '</option>');

                    });
                },
                complete: function(){
                    $('#loader').css("visibility", "hidden");
                }
            });
        } else {
            $('select[name="categoria"]').append('Seleccione Linea');
        }

    });

});
</script>


