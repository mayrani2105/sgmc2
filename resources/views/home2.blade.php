@extends('layouts.cabe')
@section('content')

<style>
.hovertown span {
  transition: all 0.9s ease;
}
.hovertown span:hover {
  text-shadow: 0 0 72px black;
  color: transparent;
  transition: all 0.2s ease;
}

</style>


		<div class="container">


<br/><br/><br/>
			  <!--Grid column-->



			  <!--Grid column-->



 		<h3 class="display-4  hovertown"style="color:#344478" > <strong><Center>Bienvenido {{Auth::user()->name}}</center></strong>
<br/>
			"Cuanto más tiempo pase sin que actúes, más dinero estás dejando de ganar."</h3>
        <p class="lead"style="margin-left:6rem;">      -Carrie Wilkerson</p>
        @if (session('status'))
        <div class="alert alert-success" role="alert">
        	{{ session('status') }}
        </div>
        @endif
     </div>

    </div>
		<script>
		function typeEffect(element, speed) {
			var text = element.innerHTML;
			element.innerHTML = "";

			var i = 0;
			var timer = setInterval(function() {
		    if (i < text.length) {
		      element.append(text.charAt(i));
		      i++;
		    } else {
		      clearInterval(timer);
		    }
		  }, speed);
		}


		// application
		var speed = 75;
		var h1 = document.querySelector('h1');
		var p = document.querySelector('p');
		var delay = h1.innerHTML.length * speed + speed;

		// type affect to header
		typeEffect(h1, speed);


		// type affect to body
		setTimeout(function(){
		  p.style.display = "inline-block";
		  typeEffect(p, speed);
		}, delay);
		</script>
</div>
@endsection