@extends('layouts.cabe')
    <!-- /#sidebar-wrapper -->
@section('content')

<style>
.btn-group-fab {
  position: fixed;
  width: 50px;
  height: auto;
  right: 20px; bottom: 20px;
}
.btn-group-fab div {
  position: relative; width: 100%;
  height: auto;
}
.js-pscroll {
  position: relative;
  overflow: hidden;
}

table tr th {
   cursor: pointer;
   -webkit-user-select: none;
   -moz-user-select: none;
   -ms-user-select: none;
   user-select: none;
}
tr:hover{
  background-color: #f0f1f3;
}
.has-search .form-control {
    padding-left: 2.375rem;
}

.has-search .form-control-feedback {
    position: absolute;
    z-index: 2;
    display: block;
    width: 2.375rem;
    height: 2.375rem;
    line-height: 2.375rem;
    text-align: center;
    pointer-events: none;
    color: #aaa;
}

.asc:after {
   content: ' ↑';
}
.desc:after {
   content: " ↓";
}
.btn-group-fab .btn {
  position: absolute;
  bottom: 0;
  border-radius: 50%;
  display: block;
  margin-bottom: 4px;
  width: 40px; height: 40px;
  margin: 4px auto;
}
.btn-group-fab .btn-main {
  width: 50px; height: 50px;
  right: 50%; margin-right: -25px;
  z-index: 9;
}
.btn-group-fab .btn-sub {
  bottom: 0; z-index: 8;
  right: 50%;
  margin-right: -20px;
  -webkit-transition: all 2s;
  transition: all 0.5s;
}

.nav-link,.nav-link-active{
color:#aaaa;

}
.modal-header{
  background-color: #344478;
  color:#ffffff;
}
.nav-tabs{
width: 100$;
}
.close{

}


.mr-auto{
  margin-right: 200px;
}
.hidden{ display: none; }
.btn-group-fab.active .btn-sub:nth-child(2) {
  bottom: 60px;
}
.btn-group-fab.active .btn-sub:nth-child(3) {
  bottom: 110px;
}
.btn-group-fab.active .btn-sub:nth-child(4) {
  bottom: 160px;
}
.btn-group-fab .btn-sub:nth-child(5) {
  bottom: 210px;
}
</style>

<div class="container-fluid">
  <div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <div class="alert alert-{{ $msg }} alert-dismissible fade show" role="alert">{{ Session::get('alert-' . $msg) }}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
          </button>
          </div>
      @endif
    @endforeach
  </div>

<div>
<br>
<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #344478">

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link dropdown-toggle" href="#" style="color: #FFFFFF" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			<i class="fas fa-sliders-h"></i> Filtros
		</a>
	  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
   		<a class="dropdown-item" id="ocultarId" href="#">Id</a>
   		<a class="dropdown-item" id="ocultarClave" href="#">Clave</a>
         <a class="dropdown-item" id="ocultarCalle" href="#">Calle</a>
   		<a class="dropdown-item" id="ocultarColonia" href="#">Colonia</a>
         <a class="dropdown-item" id="ocultarCiudad" href="#">Ciudad</a>
         <a class="dropdown-item" id="ocultarEstado" href="#">Estado</a>
	  </div>
      </li>
    </ul>
  <h4 class="display-4" style="color:#fff; margin-right:150px;">Sucursales</h4>
    <form class="form-inline my-10 my-lg-0" action="{{route('BuscarSuc')}}">
	  <div>
    	<div class="form-group has-search" style="margin-Top:31px;">
	      <span class="fa fa-search form-control-feedback"style="color:#344478;"></span>
	      <input type="text" class="form-control"id="search"name="search" type="search" placeholder="Buscar" >
    	</div>
	  </div>
    </form>
  </div>
  </nav>
</div>

<br>
<div>
   @foreach($permiso as $permi)
   @if($permi->ver==1)
<table class="table table-striped hidden" id="mitabla2" data-toggle="table">
<thead>
  <tr>
    <th  class="">Id</th>
    <th  class="" scope="col">Clave</th>
    <th  class="" scope="col">Calle</th>
    <th scope="col">Colonia</th>
    <th   scope="col">Ciudad</th>
    <th class="hidden" scope="col">Estado</th>
    <th class="text-center" scope = "col" >Opciones</th>
  </tr>
</thead>
<tbody class="busca">
@foreach($sucursal as $sucursal)
<tr>
  <td>{!!$sucursal ->id!!}</td>
  <td>{!!$sucursal ->clave!!}</td>
  <td >
  {!!$sucursal ->calle!!}
  </td>
  <td >{!!$sucursal ->colonia!!}</td>
  <td >{!!$sucursal ->ciudad!!}</td>
  <td class=" hidden">{!!$sucursal ->estado!!}</td>
  <td class="text-center" style="padding-left:5px;">
      <a class="btn btn-success" id="ver" name="ver" onclick="modalGen('{{$sucursal->id}}')" data-id="{{$sucursal->id}}" data-toggle="modal" data-target="#vermodal"><i class="fas fa-eye"  style="color:#FFFFFF;" data-toggle="tooltip" data-placement="top" title="Ver Detalles"></i></a>
@if($permi->editar==1)
      <a class="btn btn-primary" id="editar" name="editar"  onclick ="modalEdit('{{$sucursal->id}}')" data-id="{{$sucursal->id}}" data-toggle="modal" data-target="#editmodal" data-toggle="tooltip" data-placement="top" title="Modificar"><i class="far fa-edit"  style="color:#FFFFFF;"></i></a>
@endif

   </td>
</tr>
   @endforeach

</tbody>
</table>
@endif
@endforeach
<table class="table table-striped" id="mitabla" data-toggle="table">
<thead>
  <tr>
    <th  class="">Id</th>
    <th  class="" scope="col">Clave</th>
    <th  class="" scope="col">Calle</th>
    <th  scope="col">Colonia</th>
    <th  scope="col">Ciudad</th>
    <th class="hidden" scope="col">Estado</th>
    <th class="text-center" scope = "col" >Opciones</th>
  </tr>
</thead>
<tbody >
@foreach($sucursales as $sucursal)
<tr>
  <td>{!!$sucursal ->id!!}</td>
  <td>{!!$sucursal ->clave!!}</td>
  <td >
  {!!$sucursal ->calle!!}
  </td>
  <td>{!!$sucursal ->colonia!!}</td>
  <td>{!!$sucursal ->ciudad!!}</td>
  <td class=" hidden">{!!$sucursal ->estado!!}</td>
  <td class="text-center" style="padding-left:5px;">
     @foreach($permiso as $permi)
      <a class="btn btn-success" id="ver" name="ver" onclick="modalGen('{{$sucursal->id}}')" data-id="{{$sucursal->id}}" data-toggle="modal" data-target="#vermodal"><i class="fas fa-eye"  style="color:#FFFFFF;" data-toggle="tooltip" data-placement="top" title="Ver Detalles"></i></a>
@if($permi->editar==1)
      <a class="btn btn-primary" id="editar" name="editar"  onclick ="modalEdit('{{$sucursal->id}}')" data-id="{{$sucursal->id}}" data-toggle="modal" data-target="#editmodal" data-toggle="tooltip" data-placement="top" title="Modificar"><i class="far fa-edit"  style="color:#FFFFFF;"></i></a>
@endif
 
@endforeach

   </td>
</tr>
   @endforeach

</tbody>
</table>

<!--Modal Ver Sucursal -->
<div class="modal fade" id="vermodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title">Detalles: </h5>
        <button type="button" class="close" style="color: #FFFFFF;" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    <div class="modal-body">
    	<form class="form-horizontal">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
	    	<input type="hidden" name="usuario" id="usuario" value="{{Auth::user()->id}}">
            {{method_field('POST')}}

      <div class="col-md-3" style="padding-left: 0px;">
          <label for="clave">Clave</label>
          <input type="text" name="clave" id="claveV" class="form-control" placeholder="Clave de Sucursal" value="" style="background-color: white; color:#808B96;" readonly="true"><br/>
      </div>
      <div class="row">
		     <div class="col-md-8">
		        <label for="calle">Calle</label>
		        <input type="text" name="calle" id="calleV" class="form-control" placeholder="Calle"  value="" style="background-color: white; color:#808B96;" readonly="true"><br/>
		      </div>
		      <div class="col-md-3">
		        <label for="numero">Número</label>
		        <input type="number" name="numero" id="numeroV" class="form-control" placeholder="Numero"  value="" style="background-color: white; color:#808B96;" readonly="true"><br/>
		      </div>
      </div>
      <div class="row">
	      <div class="col-md-8">
	        <label for="colonia">Colonia</label>
	        <input type="text" name="colonia" id="coloniaV" class="form-control" placeholder="Colonia"  value="" style="background-color: white; color:#808B96;" readonly="true"><br/>
	      </div>
      </div>
    <div class="row">
   		<div class="col-md-5">
          <label for="ciudad">Ciudad</label>
          <input type="text" name="ciudad" id="ciudadV" class="form-control" placeholder="Ciudad" value="" style="background-color: white; color:#808B96;" readonly="true"><br/>
     	</div>
     	<div class="col-md-5">
     		<label for="estado">Estado</label>
     		<input type="text" name="estado" id="estadoV" placeholder="Estado" class="form-control" readonly="true" style="background-color: white; color:#808B96;">
     	</div>
    </div>
         </form>
    </div>
     <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal" data-toggle="tooltip" data-placement="top" title="Cerrar"><i class="fas fa-eye-slash"></i></button> <!--Botón de cerrar-->
     </div>
	</div>
</div>
</div>
<!--Termina modal ver -->

<!--Modal editar -->
<form  method="post"  action="{{route('Sucursales.update', $sucursal->id)}}">

                  {{method_field('PUT')}}
<!-- //Modal de Editar -->
<div class="modal fade bd-example-modal-lg" id="editmodal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" id="usuario">
    <div class="modal-header">
        <h5 class="modal-title">Editando inventario: </h5>
        <button type="button" class="close" style="color: #FFFFFF;"data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    <div class="modal-body">
		<form class="form-horizontal" action="{{route('Sucursales.update', $sucursal->id)}}" onsubmit="">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
	    	<input type="hidden" name="usuario" id="usuario" value="{{Auth::user()->id}}">
            {{method_field('PUT')}}

	  <input type="hidden" name="id" id="idE" class="form-control" value="">
	  <div class="col-md-3" style="padding-left: 0px;">
          <label for="almacen">Almacen</label>
          <input type="text" name="almacen" id="almacenE" class="form-control" placeholder="Nombre de Almacen" value="" style="background-color: white; color:#808B96;" required="required"><br/>
      </div>
      <div class="col-md-3" style="padding-left: 0px;">
          <label for="clave">Clave</label>
          <input type="text" name="clave" id="claveE" class="form-control" placeholder="Clave de Sucursal" value="" style="background-color: white; color:#808B96;" required="required"><br/>
      </div>
      <div class="row">
		     <div class="col-md-8">
		        <label for="calle">Calle</label>
		        <input type="text" name="calle" id="calleE" class="form-control" placeholder="Calle"  value="" style="background-color: white; color:#808B96;" required="required"><br/>
		      </div>
		      <div class="col-md-3">
		        <label for="numero">Número</label>
		        <input type="number" name="numero" id="numeroE" class="form-control" placeholder="Numero"  value="" style="background-color: white; color:#808B96;" required="required"><br/>
		      </div>
      </div>
      <div class="row">
	      <div class="col-md-8">
	        <label for="colonia">Colonia</label>
	        <input type="text" name="colonia" id="coloniaE" class="form-control" placeholder="Colonia"  value="" style="background-color: white; color:#808B96;" required="required"><br/>
	      </div>
      </div>
    <div class="row">
   		<div class="col-md-5">
          <label for="ciudad">Ciudad</label>
          <input type="text" name="ciudad" id="ciudadE" class="form-control" placeholder="Ciudad" value="" style="background-color: white; color:#808B96;"><br/>
     	</div>
     	<div class="col-md-5">
     		<label for="estado">Estado</label>
     		<select name="estado" id="estadoE" class="form-control" placeholder="Estado">
     			<option value="">Selecciona un estado</option>
     			@foreach($estado as $es)
     			<option value="{!!$es->id!!}">{!!$es->estado!!}</option>
     			@endforeach
     		</select>
     	</div>
    </div>
         </form>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-eye-slash"></i></button> <!--Botón de cerrar-->
        <button type="submit" class="btn btn-primary"  value=""><i class="far fa-save"></i></button>
    </div>
    </div>
  </div>
</div>
</form>

<!--Termina odal editar -->

<!-- Floating Button -->
<div class="btn-group-fab" role="group" aria-label="FAB Menu">
  <div>
    <button type="button" class="btn btn-main btn-primary has-tooltip" data-placement="left" title="Menu"> <i class="fa fa-bars"></i> </button>
    <button type="button" class="btn btn-sub btn-info has-tooltip" data-toggle="modal" data-target="#crear" data-placement="left" title="Agregar Sucursal" > <i class="fas fa-plus"></i></button>
    <button type="button" class="btn btn-sub btn-dark has-tooltip" data-toggle="modal" data-target="#ayuda" data-placement="left" title="Ayuda" > <i class="fas fa-question"></i> </button>

  </div>
</div>

<!--Modal de Ayuda-->

<div class="modal fade bd-example-modal-lg" id="ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Ayuda Sucursales </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <input type="hidden" name="id" id="id">
      <h2>Bienvenido {{Auth::user()->name}} </h2>
        <br/>
      @if(Auth::user()->id_rol!=5)
       <button type="button" class="btn btn-success"><i class="fas fa-eye" style="height:4px;"></i></button><strong>  Ver Detalles</strong> <br/>
          <p>En esta opcion el usuario podra encontrar informacion detallada de cada registro de sucursales<br/>
          La informacion que muestra los campos son :</p>
          <ul>
            <li>Id Sucursale</li>
            <li>Clave de la Sucursal</li>
            <li>Calle</li>
            <li>Colonia</li>
            <li>Ciudad</li>
            <li>Estado</li>
          </ul>
 @endif
  @if(Auth::user()->id_rol==2 || Auth::user()->id_rol==1)
          <button type="button" class="btn btn-primary"><i class="fas fa-edit"></i></button><strong> Modificar Sucursal </strong><br/>
          <p>En esta opcion el usuario podra modificar los datos de cada una de las sucursales que se encuentran dentro del sistema.</p>
@endif
<button class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="buscar"><i class="fas fa-search"></i></button>
<strong>Buscar</strong><br/>En esta opción el usuario podrá buscar en tiempo real las sucursales.<br/>
  @if(Auth::user()->id_rol==2 || Auth::user()->id_rol==1)
        <button type="button" class="btn btn-sub btn-info has-tooltip" data-placement="left" title="Agregar Proveedor"> <i class="fas fa-plus"></i></i> </button>
         <strong>Agregar Sucursal</strong><br/>
         <p>En esta opción el usuario podrá agregar una nueva sucursal al sistema.</p> <br/>

      <strong>  Atención: </strong><p>Para poder realizar un registro es necesario llenar todos los campos.</p><br/>
  @endif

      </div>
      <div class="modal-footer">

        <button type="button" class="btn btn-danger"data-dismiss="modal"data-toggle="tooltip" data-placement="top" title="Cerrar Ayuda"><i class="fas fa-eye-slash"></i></button>
      </div>
    </div>
  </div>
</div>
<!--Termina Modal de Ayuda-->


<!--Modal Crear Sucursal -->
<form  method="post" id="store-form" action="{{route('Sucursales.store')}}">

                  {{method_field('POST')}}
<div class="modal fade bd-example-modal-lg" id="crear" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title">Agregar Sucursal: </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    <div class="modal-body">
      	<form class="form-horizontal" action="{{route('Sucursales.store')}}" onsubmit="">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
	    	<input type="hidden" name="usuario" id="usuario" value="{{Auth::user()->id}}">
            {{method_field('POST')}}
       <div class="row">
		   
	      <div class="col-md-3">
	          <label for="clave">Clave</label>
	          <input type="text" name="clave" id="clave" class="form-control" placeholder="Clave de Sucursal" value="" style="background-color: white; color:#808B96;" required="required"><br/>
	      </div>
	      <div class="col-md-3">
	          <label for="almacen">Almacen</label>
	          <input type="text" name="almacen" id="almacenE" class="form-control" placeholder="Nombre de Almacen" value="" style="background-color: white; color:#808B96;" required="required"><br/>
	      </div>
	  </div>   
      <div class="row">
		     <div class="col-md-8">
		        <label for="calle">Calle</label>
		        <input type="text" name="calle" id="calle" class="form-control" placeholder="Calle"  value="" style="background-color: white; color:#808B96;" required="required"><br/>
		      </div>
		      <div class="col-md-3">
		        <label for="numero">Número</label>
		        <input type="number" name="numero" id="numero" class="form-control" placeholder="Numero"  value="" style="background-color: white; color:#808B96;" required="required"><br/>
		      </div>
      </div>
      <div class="row">
	      <div class="col-md-8">
	        <label for="colonia">Colonia</label>
	        <input type="text" name="colonia" id="colonia" class="form-control" placeholder="Colonia"  value="" style="background-color: white; color:#808B96;" required="required"><br/>
	      </div>
      </div>
    <div class="row">
   		<div class="col-md-5">
          <label for="ciudad">Ciudad</label>
          <input type="text" name="ciudad" id="ciudad" class="form-control" placeholder="Ciudad" value="" style="background-color: white; color:#808B96;"><br/>
     	</div>
     	<div class="col-md-5">
     		<label for="estado">Estado</label>
     		<select name="estado" id="estado" class="form-control" placeholder="Estado">
     			<option value="">Selecciona un estado</option>
     			@foreach($estado as $es)
     			<option value="{!!$es->id!!}">{!!$es->estado!!}</option>
     			@endforeach
     		</select>
     	</div>


    </div>
         </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-eye-slash"></i></button> <!--Botón de cerrar-->
        <button type="submit" class="btn btn-primary" value=""><i class="far fa-check-square fa-lg"></i></button>
      </div>
    </div>
  </div>
</div>
</form>
<!--Termina modal Crear Sucursal -->
</div>
</div>


<script>

	  $(document).ready(function () {
      (function ($) {
          $('#search').keyup(function () {
               var rex = new RegExp($(this).val(), 'i');
               if(rex=='/(?:)/i'){
                 $('#mitabla2').hide();
                 $('#mitabla').show();
                 $('#paginacion').show();
               }else{
                 $('#mitabla2').show();
                 $('#mitabla').hide();
               $('#paginacion').hide();}
               $('.busca tr').hide();
               $('.busca tr').filter(function () {
                 return rex.test($(this).text());
               }).show();
          })
      }(jQuery));
  });

  $('th').click(function() {
   var table = $(this).parents('table').eq(0)
   var rows = table.find('tr:gt(0)').toArray().sort(comparer($(this).index()))
   this.asc = !this.asc
   if (!this.asc) {
      rows = rows.reverse()
   }
   for (var i = 0; i < rows.length; i++) {
      table.append(rows[i])
   }
   setIcon($(this), this.asc);

});
function comparer(index) {
   return function(a, b) {
      var valA = getCellValue(a, index),
      valB = getCellValue(b, index)
      return $.isNumeric(valA) && $.isNumeric(valB) ? valA - valB : valA.localeCompare(valB)
   }
}
function getCellValue(row, index) {
   return $(row).children('td').eq(index).html()
}
function setIcon(element, asc) {
   $("th").each(function(index) {
      $(this).removeClass("sorting");
      $(this).removeClass("asc");
      $(this).removeClass("desc");
   });
   element.addClass("sorting");
   if (asc) element.addClass("asc");
   else element.addClass("desc");
}


  $(document).ready(function(){
  let table = document.getElementById('mitabla');
    let table1 = document.getElementById('mitabla2');

  $("#ocultarId").click(function(){
    for (var i = 0, row; row = table.rows[i]; i++){
       row.cells[0].classList.toggle('hidden');
    }
    for (var i = 0, row; row = table1.rows[i]; i++){
       row.cells[0].classList.toggle('hidden');
    }
  });

  $("#ocultarClave").click(function(){
        for (var i = 0, row; row = table.rows[i]; i++){
           row.cells[1].classList.toggle('hidden');
        }
        for (var i = 0, row; row = table1.rows[i]; i++){
           row.cells[1].classList.toggle('hidden');
        }
      });

 $("#ocultarCalle").click(function(){
        for (var i = 0, row; row = table.rows[i]; i++){
           row.cells[2].classList.toggle('hidden');
        }
        for (var i = 0, row; row = table1.rows[i]; i++){
           row.cells[2].classList.toggle('hidden');
        }
      });

  $("#ocultarColonia").click(function(){
        for (var i = 0, row; row = table.rows[i]; i++){
           row.cells[3].classList.toggle('hidden');
        }
        for (var i = 0, row; row = table1.rows[i]; i++){
           row.cells[3].classList.toggle('hidden');
        }
      });
      $("#ocultarCiudad").click(function(){
        for (var i = 0, row; row = table.rows[i]; i++){
           row.cells[4].classList.toggle('hidden');
        }
        for (var i = 0, row; row = table1.rows[i]; i++){
           row.cells[4].classList.toggle('hidden');
        }
      });
     
     $("#ocultarEstado").click(function(){
        for (var i = 0, row; row = table.rows[i]; i++){
           row.cells[5].classList.toggle('hidden');
        }
        for (var i = 0, row; row = table1.rows[i]; i++){
           row.cells[5].classList.toggle('hidden');
        }
      });
});


	$(document).ready(function() {
  $('.btn-group-fab').on('click', '.btn', function() {
    $('.btn-group-fab').toggleClass('active');
  });

});

function modalGen(id){
    $.ajax({
      url:'Sucursales/'+ id,
      type:'get'
    }).done(function (res){
      $('#claveV').val(res.datos[0].clave);
      $('#calleV').val(res.datos[0].calle);
      $('#numeroV').val(res.datos[0].numero);
      $('#coloniaV').val(res.datos[0].colonia);
      $('#ciudadV').val(res.datos[0].ciudad);
      $('#estadoV').val(res.datos[0].estado);
    });
  }

 function modalEdit(id){
    $.ajax({
      url:'Sucursales/'+ id + '/edit',
      type:'get'
    }).done(function (res){
   	  $('#idE').val(res.datos[0].id);
      $('#claveE').val(res.datos[0].clave);
      $('#calleE').val(res.datos[0].calle);
      $('#numeroE').val(res.datos[0].numero);
      $('#coloniaE').val(res.datos[0].colonia);
      $('#ciudadE').val(res.datos[0].ciudad);
      $('#estadoE').val(res.datos[0].id_Estados);
    });
  }

</script>

@endsection