<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>SMGC | Panel de administración</title>

  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
	
  <script src="https://kit.fontawesome.com/da3b6a12fa.js"></script>
	
  <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script> 
  <script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
	
  <script src="https://code.highcharts.com/highcharts.js"></script>
  <script src="https://code.highcharts.com/modules/data.js"></script>
  <script src="https://code.highcharts.com/modules/drilldown.js"></script>

  <!-- Custom styles for this template -->
  <link href="css/simple-sidebar.css" rel="stylesheet">
  <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
  <link rel="icon" href="/favicon.ico" type="image/x-icon">

</head>

<body>

  <div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class="bg-dark border-right" id="sidebar-wrapper">
      <div class="sidebar-heading bg-white"><a href=""><img src="logo.jpg" style="max-height: 60px;"></a></div>
      <div class="list-group list-group-flush" id="sidebar-list">
        <a href="" class="list-group-item list-group-item-action bg-dark" id="list-element"><i class="fas fa-user fa-1x"></i> Algo</a>
        <a href="" class="list-group-item list-group-item-action bg-dark" id="list-element"><i class="fas fa-user-tie fa-1x"></i> Algo mas</a>
        <a href="" class="list-group-item list-group-item-action bg-dark" id="list-element"><i class="fas fa-envelope-open fa-1x"></i> Otra cosa</a>
		<a href="" class="list-group-item list-group-item-action bg-dark" id="list-element"><i class="fas fa-chart-bar fa-1x"></i> Otra cosa mas</a>
		<a href="" class="list-group-item list-group-item-action bg-dark" id="list-element"><i class="fas fa-sign-out-alt fa-1x"></i> Cerrar sesión</a>
      </div>
    </div>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">

      <nav class="navbar navbar-expand-lg navbar-light bg-white border-bottom">
        <button class="btn btn-white" id="menu-toggle"><i class="fas fa-bars fa-1x"></i></button>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
            <li class="nav-item active">
              <a class="nav-link" href=" "><i class="fas fa-user-tie fa-1x"></i> Usuario</a>
            </li>
          </ul>
        </div>
      </nav>

      <div class="container-fluid">

      </div>
    </div>
    <!-- /#page-content-wrapper -->
  <!-- /#wrapper -->

  <!-- Bootstrap core JavaScript -->
  <script src="/jquery/jquery.min.js"></script>
  <script src="/js/bootstrap.bundle.min.js"></script>

  <!-- Menu Toggle Script -->
  <script>
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
  </script>
	
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>

</body>

</html>
