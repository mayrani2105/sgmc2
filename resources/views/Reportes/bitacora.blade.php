@extends('layouts.cabe')

@section('content')
<style>
.btn-group-fab {
  position: fixed;
  width: 50px;
  height: auto;
  right: 20px; bottom: 20px;
}
.btn-group-fab div {
  position: relative; width: 100%;
  height: auto;
}
.js-pscroll {
  position: relative;
  overflow: hidden;
}

table tr th {
   cursor: pointer;
   -webkit-user-select: none;
   -moz-user-select: none;
   -ms-user-select: none;
   user-select: none;
}
tr:hover{
  background-color: #f0f1f3;
}

.asc:after {
   content: ' ↑';
}
.desc:after {
   content: " ↓";
}
.btn-group-fab .btn {
  position: absolute;
  bottom: 0;
  border-radius: 50%;
  display: block;
  margin-bottom: 4px;
  width: 40px; height: 40px;
  margin: 4px auto;
}
.btn-group-fab .btn-main {
  width: 50px; height: 50px;
  right: 50%; margin-right: -25px;
  z-index: 9;
}
.btn-group-fab .btn-sub {
  bottom: 0; z-index: 8;
  right: 50%;
  margin-right: -20px;
  -webkit-transition: all 2s;
  transition: all 0.5s;
}

.nav-link,.nav-link-active{
color:#aaaa;

}
.modal-header{
  background-color: #344478;
  color:#ffffff;
}
.nav-tabs{
width: 100$;
}
.close{

}
.has-search .form-control {
    padding-left: 2.375rem;
}

.has-search .form-control-feedback {
    position: absolute;
    z-index: 2;
    display: block;
    width: 2.375rem;
    height: 2.375rem;
    line-height: 2.375rem;
    text-align: center;
    pointer-events: none;
    color: #aaa;
}


.mr-auto{
  margin-right: 200px;
}
.hidden{ display: none; }
.btn-group-fab.active .btn-sub:nth-child(2) {
  bottom: 60px;
}
.btn-group-fab.active .btn-sub:nth-child(3) {
  bottom: 110px;
}
.btn-group-fab.active .btn-sub:nth-child(4) {
  bottom: 160px;
}
.btn-group-fab .btn-sub:nth-child(5) {
  bottom: 210px;
}
</style>

<div class="container-fluid">
  <div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <div class="alert alert-{{ $msg }} alert-dismissible fade show" role="alert">{{ Session::get('alert-' . $msg) }}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
          </button>
          </div>
      @endif
    @endforeach
  </div>
<center>
<h3 class="display-4">Bitacoras de Usuarios</h3>
</center><br/>

<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #344478">

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
  <!--    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link dropdown-toggle" href="#" style="color: #FFFFFF" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			<i class="fas fa-sliders-h"></i> Filtros
		</a>
	<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
    <a class="dropdown-item" id="ocultarId" href="#">Bitacora</a>
    <a class="dropdown-item" id="ocultar" href="#">Modulo</a>
		<a class="dropdown-item" id="ocultarP"href="#">usuario</a>
    	<a class="dropdown-item" id="oculltarCorreo"href="#">Movimiento</a>
		<a class="dropdown-item" id="ocultarPromos"href="#">Dato Modificado</a>
    <a class="dropdown-item" id="ocultardiascredito"href="#">Fecha y Hora</a>
    <a class="dropdown-item" id="oculltarTelefono"href="#">Sucursal</a>

	  </div>
      </li>
    </ul>
    <form class="form-inline mr-auto" action="{{route('BuscarPro')}}">
  <div>
		</div>
    <!--<div class="form-group has-search">
      <span class="fa fa-search form-control-feedback"style="color:#344478;"></span>
      <input type="text" class="form-control"id="search"name="search" type="search" placeholder="Buscar">
    </div>
-->

    </form>
  </div>
</nav>
<br/>
<div>
  <form action="{{route('reporteUFecha')}}" name="ejemplo2" method="post" >
    {{csrf_field()}}
  <div class="row">
      <div class="col-md-4">
        <label for="fecha">Fecha de inicio </label>
  <input class="form-control mr-sm-1 my-sm-0" required name="inicio"type="date" id="inicio" name="trip-start"

       min="2019-01-01"></div>
  <div class="col-md-4">
    <label for="Credito">Fecha de Fin </label>
  <input class="form-control mr-sm-1 my-sm-0" required name="fin" type="date" id="fin" name="trip-start"
    min="2019-01-01" ></div>


            <div class="col-md-1">
              <button class="btn btn-primary" style="margin-Top:30px;" data-toggle="tooltip" data-placement="top" title="buscar"type="submit"><i class="fas fa-search"></i></button>
</div>
<div class="col-md-3">
<label for="Estado">Sucursal</label>

<select  id="ejemplo2"name="estado" class="form-control" value="">
<option value="">Todas las Sucursales</option>
@foreach($sucursal as $sucu)
<option value="sucursal {{$sucu->descripcionS}}">Sucursal {{$sucu->descripcionS}}</option>
@endforeach
</select>
</div></div>
        </form>
          <br/>


<table class="table table-striped" id="example" data-toggle="table">
 <thead>
    <tr>
      <th  class=" ">Bitacora</th>
      <th class"">Modulo</th>
      <th  class=""scope="col">Usuario</th>
      <th  class=""scope="col">Movimiento </th>
<th  class=""scope="col">Dato Modificado</th>
      <th  class=""scope="col">Fecha y hora </th>
      <th class="" scope="col">Sucursal</th>
    </tr>
 </thead>
 <tbody class="buscar">
   @foreach($bitacora as $producto)
   <tr >
     <td> {!!$producto->id_bitacora!!}</td>
     <td> {!!$producto ->modulo!!}</td>
     <td> {!!$producto ->name!!}</td>
     @if($producto->id_movimiento == 2)
     <td><env class="fas fa-edit" style="color: blue;"></env> {!!$producto ->descripcion!!} </td>
     @elseif($producto->id_movimiento ==3)
     <td><i class="fas fa-trash-alt"style="color:red;"></i> {!!$producto ->descripcion!!} </td>
     @elseif($producto->id_movimiento==4)
      <td/><i class="fas fa-plus"style="color:green;"></i> {!!$producto ->descripcion!!} </td>
@elseif($producto->id_movimiento==5)
  <td/><i class="fas fa-arrow-left"></i> {!!$producto ->descripcion!!} </td>
@else
  <td/><i class="fas fa-arrow-right"></i> {!!$producto ->descripcion!!} </td>
      @endif
     <td>{{$producto->datomodificado}}</td>
     <td>{{$producto->created_at}}</td>
   <td class=""> Sucursal {{$producto->descripcionS}}</td>
 </tr>
      @endforeach
 </tbody>
</table>


<center><br/></center>
  <div class="btn-group-fab" role="group" aria-label="FAB Menu">
    <div>
      <button type="button" class="btn btn-main btn-primary has-tooltip" data-placement="left" title="Menu"> <i class="fa fa-bars"></i> </button>
      <button type="button" class="btn btn-sub btn-dark has-tooltip"data-toggle="modal" data-target="#Ayuda" data-placement="left" title="Ayuda"><i class="fas fa-question"></i></button>
    </div>
  </div>

  <div class="modal fade bd-example-modal-lg" id="Ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Ayuda Bitacoras </h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <input type="hidden" name="id" id="id">
        <h2>Bienvenido {{Auth::user()->name}} </h2>
          <br/>
        @if(Auth::user()->id_rol!=5)
      En esta seccion, la cual es la mas importante usted podra ver todas las acciones hechas por los usuarios.
  @endif
  <br/><br/>
  <button class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="buscar"><i class="fas fa-search"></i></button>
  <strong>Tipos de Busqueda</strong><br/>Manejamos 3 formas de realizar busquedas las cuales son:<br/>

    @if(Auth::user()->id_rol==2 || Auth::user()->id_rol==1)<br/>
<strong>1.Busqueda por Fecha</strong><br/>
Esta busqueda se realiza de acuerdo a rangos de fechas.<br/>Para lo cual debera llenar ambos campos con las fechas que le gustaria ver.<br/>
    <div class="row">
        <div class="col-md-4">
          <label for="fecha">Fecha de inicio </label>
    <input class="form-control mr-sm-1 my-sm-0" style="background-color: #fff;"  name="inicio"type="date" readonly id="inicio" name="trip-start"
value="2019-11-11"
         min="2019-01-01"></div>
    <div class="col-md-4">
      <label for="Credito">Fecha de Fin </label>
    <input class="form-control mr-sm-1 my-sm-0"  style="background-color: #fff;" name="fin" type="date" readonly id="fin" name="trip-start"
              value="2019-11-21"
              min="2019-01-01" ></div>
              <div class="col-md-4">

  </div><br/>
            </div>
            <strong>2.Busqueda Normal</strong><br/>
            En este tipo de busqueda usted podra buscar en todos los campos de tabla en tiempo real.
      <br/><br/>
      <strong>3.Busqueda por Sucursal</strong><br/>
      En este tipo de busqueda usted podra buscar de acuerdo a la sucursal que decida.

<br/><br/>
<strong>Tipos de Movimentos</strong><br/>
<i class="fas fa-arrow-right"></i> Entradas <br/>
Dicho movimiento se refiere a las entradas que se hagan a productos de inventario.<br/>
<i class="fas fa-arrow-left"></i> Salidas <br/>
Dicho movimiento se refiere a las salidas que se hagan a productos de inventario.<br/>
<env class="fas fa-edit" style="color: blue;"></env>  Modificación <br/>
Dicho movimiento se refiere a las modificaciones en los catalogos<br/>
<i class="fas fa-plus"style="color:green;"></i> Creación <br/>
Dicho movimiento se refiere al movimiento de agregar un nuevo registro<br/>
<i class="fas fa-trash-alt"style="color:red;"></i> Eliminación <br/>
Dicho movimiento se refiere al movimiento de eliminar un registro.<br/><br/>
<strong>  Atención: </strong>Solo los usuarios Administrador y jefe de operaciones pueden ver las bitacoras de todos los usuarios<br/>
@endif
        </div>
        <div class="modal-footer">

          <button type="button" class="btn btn-danger"data-dismiss="modal"data-toggle="tooltip" data-placement="top" title="Cerrar Ayuda"><i class="fas fa-eye-slash"></i></button>
        </div>
      </div>
    </div>
  </div>
  <script>
  $(function() {
    $('.btn-group-fab').on('click', '.btn', function() {
      $('.btn-group-fab').toggleClass('active');
    });
    $('has-tooltip').tooltip();
  });
  </script>
  <script type="text/javascript">
    $(document).ready(function() {
      var table = $('#example').DataTable();

      $('#example tbody').on( 'click', 'tr', function () {
          if ( $(this).hasClass('selected') ) {
              $(this).removeClass('selected');
          }
          else {
              table.$('tr.selected').removeClass('selected');
              $(this).addClass('selected');
          }
      } );

      $('#button').click( function () {
          table.row('.selected').remove().draw( false );
      } );
  } );
  </script><script>
  $(document).ready(function(){
    let table = document.getElementById('example');

    $("#ocultarId").click(function(){

      for (var i = 0, row; row = table.rows[i]; i++){
         row.cells[0].classList.toggle('hidden');
      }
    });
        $("#ocultar").click(function(){
          for (var i = 0, row; row = table.rows[i]; i++){
             row.cells[1].classList.toggle('hidden');
          }
        });

        $("#ocultarP").click(function(){
          for (var i = 0, row; row = table.rows[i]; i++){
             row.cells[2].classList.toggle('hidden');
          }
        });
        $("#oculltarCorreo").click(function(){
          for (var i = 0, row; row = table.rows[i]; i++){
             row.cells[3].classList.toggle('hidden');
          }
        });
        $("#ocultardiascredito").click(function(){
          for (var i = 0, row; row = table.rows[i]; i++){
             row.cells[5].classList.toggle('hidden');
          }
        });
        $("#oculltarTelefono").click(function(){
          for (var i = 0, row; row = table.rows[i]; i++){
             row.cells[6].classList.toggle('hidden');
          }
        });
        $("#ocultarPromos").click(function(){
          for (var i = 0, row; row = table.rows[i]; i++){
             row.cells[4].classList.toggle('hidden');
          }
        });


  });
  </script>
  <script>
  $(document).ready(function(){
    (function ($) {
    $("#ejemplo2").change(function(){
       var rex = new RegExp($('select[id=ejemplo2]').val(), 'i');
if(rex=='/(?:)/i'){

}
                $('#valor2').val($(this).val());
                $('.buscar tr').hide();
                $('.buscar tr').filter(function () {
                  return rex.test($(this).text());
                }).show();
           })

       }(jQuery));
    	});
  </script>

  @endsection
