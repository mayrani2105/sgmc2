
<center>
<div class="col-md-10" >
                <form id="crearpedidos" action="/CrearPedido" method="post"role="form" class="form-group">
                    {{ csrf_field() }}

                    <div id="myAlert" class="alert alert-success collapse">   Producto añadido al carrito de compras
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <span aria-hidden="true">&times;</span>
                    </div>
                    <div class="row" align ="right">
                    <div class="col-lg-12">
                  <strong>  <label for="folioPedido">Folio: P0{{$folio}}</label></strong>
                   <input name="folio" hidden value="P0{{$folio}}">
                    </div>
                    </div>

                    <div class="row">
                    <div class="col-md-5">
                     <input hidden name="tipo"value="1">
                     <input hidden name="origen"value="{{Auth::user()->id_sucursal}}">
                    <label for="pProveedor" >Tipo de Orden de Compra</label>
                    <select name="tipos" id="tipos" tabindex="" class="form-control" placeholder="Proveedor" min="0"required>
                      <option value="0">Seleccione tipo de Compra</option>

                      <option value="1">Compra Sucursal</option>
                      <option value="2"> Compra Proveedor</option>
                    </select></div>
                    <div id="sucursales"class="col-md-4">

                    <label for="pProveedor" > Sucursal origen </label>
                    <select name="sucursal"  tabindex="" class="form-control" placeholder="Proveedor" min="0"required>
                   <option value="" selected></option>
                     @foreach($sucursal as $suc)
                      <option value="{{$suc->descripcionS}}">Sucursal {{$suc->descripcionS}}</option>
                    @endforeach
                    </select></div>
                    <div class="col-md-2">
                      <button id="autorizar" type="button" style="margin-Top:35px;" class="btn btn-success"><i class="fas fa-check"></i></button>
                    </div></div>
                                  </form>
                    <form id="agregarproductos" method="post"role="form" action=""class="form-group">
                       {{ csrf_field() }}
                       <div class="row" >
                    <div  id="datosproveeedor"class="col-md-9">
                      <input name="folio" id="folioo"hidden value="P0{{$folio}}">
                    <label for="pProveedor" >Proveedor:</label>
                    <select name="proveedor" id="Proveedor" tabindex="" class="form-control" placeholder="Proveedor" min="0">
                    <option>Elige un proveedor</option>
                     @foreach($proveedorPedido as $proveedorPedido)
                    <option value ="{{$proveedorPedido ->idProveedor}}">
                       {{$proveedorPedido->proveedor}}
                        </option>
                    @endforeach
                    </select>
                  </div></div>
                  <div id="productodatos" class="row" >
                  <div  class="col-md-5"><br/>

                    <input id="ubicacion" hidden name="ubicacion">
                  <label for="pProveedor" >Productos:</label>
                  <select name="productos" id="Proveedor" tabindex="" class="form-control" placeholder="Proveedor" min="0">
                  </select></div>
                  <div class="col-md-3"><br/>
                    <label for="pProveedor" >Cantidad:</label>
                  <input type="number" name="cantidadS" class="form-control mr-sm-2" min="0">
                  </div>
                  <div class="col-md-3"><br/>
                    <button id="proagrega1" name="agrega" type="button" style="margin-Top:35px;" class="btn btn-success"><i class="fas fa-plus"></i></button>
                      <button id="elimina" name="elimina" type="button" style="margin-Top:35px;" class="btn btn-danger"><i class="fas fa-minus"></i></button>
                  </div> </div><br/>

                  <div id="datosproducto"class="row">
<div  class="col-md-5"><br/>
                    <label for="pProveedor" >Productos Proveedor:</label>
                    <select name="prodProveedor" id="Proveedor2" tabindex=""class=" form-control" placeholder="Proveedor" min="0"required>
                    </select>
</div>
                    <div class="col-md-3"><br/>
                      <label for="pProveedor" >Cantidad:</label>
                    <input type="number" name="cantidad" class="form-control mr-sm-2" min="0">
                    </div>
                    <div class="col-md-3">
                      <button id="proagrega" name="agrega" type="button" style="margin-Top:35px;" class="btn btn-success"><i class="fas fa-plus"></i></button>
                        <button id="elimina" name="elimina" type="button" style="margin-Top:35px;" class="btn btn-danger"><i class="fas fa-minus"></i></button>
                    </div>
                    </div>
              </form>
                    <br/>
                    <label for="pProveedor" >Productos del Pedido:</label>
    <nav class="navbar navbar-expand-xl navbar-light" style="background-color: #344478">
	  <div class="collapse navbar-collapse" id="navbarSupportedContent">
		<form class="form-inline my-10 my-lg-0" action="#">
		</form>
	  </div>
	</nav>
                    <div class="table-responsive">

                        <table name="tablaProduc" data-toggle="table" class="table table-hover" data-sort-name="pagare" data-sort-order="desc" id="tablaProduc">
                        <thead>
                            <tr >
                    <th  data-field="id" data-sortable="true">Clave </th>
                    <th  data-field="clave" data-sortable="true">Producto </th>
                    <th >Cantidad</th>

                            </tr>
                            </thead>
<tbody class="cuerpo" id="cuerpo">
</tbody>
                        </table>
                        <br>
                    </div>

    </div>
</center>


<!--
<script>
$(document).ready(function() {

    $('select[name="proveedor"]').on('change', function(){
        var countryId = $(this).val();

        if(countryId) {
            $.ajax({
                url: '/subP/'+countryId,
                type:"GET",
                dataType:"json",
                beforeSend: function(){
                    $('#loader').css("visibility", "visible");
                },

                success:function(data) {
                    //aplicar los valores de la tabla
                    $('table[name="tablaProduc"]').empty();

                    $('table[name="tablaProduc"]').append('<thead><tr><th class="text-center" data-field="id" data-sortable="true">ID </th><th class="text-center" data-field="id" data-sortable="true">Nombre </th><th class="text-center" data-field="clave" data-sortable="true">Cantidad</th><th class="text-center">Operaciones</th></tr></thead>');


                    $.each(data, function(key, value){

                        $('table[name="tablaProduc"]').append('<tbody><tr><td>' + key + '</td><td>' + value + '</td><td><center><input type="number" class="form-control mr-sm-2"></center></td><td><center><button id="btnSubmit" onclick="alert("asd")" class="btn btn-success"><i class="fas fa-plus"></button></center></td></tr></tbody>');

                    });
                },
                complete: function(){
                    $('#loader').css("visibility", "hidden");
                }
            });
        } else {
            $('table[name="tablaProduc"]').append('<tr><td><center>Seleccione proveedor</center></td></tr>');
        }

    });

});
</script>
-->
<script>
$(document).ready(function() {


        $('select[name="proveedor"]').on('change', function(){
            var countryId = $(this).val();

            if(countryId) {
                $.ajax({
                    url: '/subP/'+countryId,
                    type:"GET",
                    dataType:"json",
                    beforeSend: function(){
                        $('#loader').css("visibility", "visible");
                    },

                    success:function(data) {
                        //aplicar los valores de la tabla
                         $('select[name="prodProveedor"]').empty();
                        $.each(data, function(key, value){

                            $('select[name="prodProveedor"]').append('<option value="' + key + '">' + value + '</option>');

                        });
                    },
                    complete: function(){
                        $('#loader').css("visibility", "hidden");
                    }
                });
            } else {
                $('table[name="tablaProduc"]').append('<tr><td><center>Seleccione proveedor</center></td></tr>');
            }

    });

});
</script>


 <script type="text/javascript">
    $(document).ready(function () {
      $('#datosproveeedor').hide();
      $('#datosproducto').hide();
        $('#productodatos').hide();
$('#sucursales').hide();
      $('select[name="tipos"]').on('change', function(){
        var countryId = $(this).val();

        if(countryId==2){
$('#sucursales').hide();

    }else{
      $('#sucursales').show();
      $('#datosproveeedor').hide();
      $('#datosproducto').hide();


    }
      });




    });
</script>

<script>
$('#autorizar').click(function () {
  var frm=$("#crearpedidos");
  var datos = frm.serialize();
var res=  document.getElementById('tipos').value;
var re=document.getElementById('tipos');
var folio=document.getElementById('folioo');
$.ajaxSetup({
     headers: {
       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
     }
 });
$.ajax({
    type:'POST',
    url: '/CrearPedido/',
    data:datos,
    beforeSend: function(){
        $('#loader').css("visibility", "visible");
    },

    success:function(data) {
      folio.value=data;


    },
    complete: function(){
        $('#loader').css("visibility", "hidden");
    },
    error:function(x,xs,xt){
      alert(xt)
    }
});
  $('#autorizar').hide();

if(res==1){

  re.disabled=true;
//  $('#datosproducto').show();
    $('#productodatos').show();
}else if(res==2){
  re.disabled=true;
  $('#tipos').readonly=true;
  $('#datosproveeedor').show();
  $('#datosproducto').show();
    $('#productodatos').hide();
}

});

</script>
<script>
$('select[name="sucursal"]').on('change', function(){
    var countryId = $(this).val();
var sucursal=document.getElementById('ubicacion');
sucursal.value=countryId;
    if(countryId) {
        $.ajax({
            url: '/ProductosP/'+countryId,
            type:"GET",
            dataType:"json",
            beforeSend: function(){
                $('#loader').css("visibility", "visible");
            },

            success:function(data) {
                //aplicar los valores de la tabla
                 $('select[name="productos"]').empty();
                $.each(data, function(key, value){

                    $('select[name="productos"]').append('<option value="' + key + '">' + value + '</option>');

                });
            },
            complete: function(){
                $('#loader').css("visibility", "hidden");
            }
        });
      }
    });
</script>
<script>
$('#proagrega').click( function(){
  var frm=$("#agregarproductos");
  var datos = frm.serialize();
var x = $("#folioo").val();

  //alert(datos);
 $.ajaxSetup({
       headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
       }
   });
        $.ajax({
            type:'post',
            url: '/AgregarP/',
            data:datos,
            beforeSend: function(){
                $('#loader').css("visibility", "visible");
            },
            success:function(data) {
                //aplicar los valores de la tabla
           alert(data);
            },
            complete: function(){
                $('#loader').css("visibility", "hidden");
            }
            ,
            error:function(x,xs,xt){
              alert(xt);
            }
        });
        $.ajax({
        type : 'get',
        url : '/VerDetalle/'+x,
        success:function(data){
          $('#cuerpo').html(data);
    },
      error:function(x,xs,xt,data){
        alert("Error al registrar la marca")
        }
      });
    });

</script>

<script>
$('#proagrega1').click( function(){
  var frm=$("#agregarproductos");
  var datos = frm.serialize();
var x = $("#folioo").val();
var folio=document.getElementById('folioo');

  //alert(datos);
 $.ajaxSetup({
       headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
       }
   });
        $.ajax({
            type:'post',
            url: '/AgregarS/',
            data:datos,
            beforeSend: function(){
                folio.value=x;
                $('#loader').css("visibility", "visible");
            },
            success:function(data) {
                //aplicar los valores de la tabla
           alert(data);
            },
            complete: function(){
                $('#loader').css("visibility", "hidden");
            }
            ,
            error:function(x,xs,xt){
              alert(xt);
            }
        });
        $.ajax({
        type : 'get',
        url : '/VerDetalle/'+x,
        success:function(data){
          $('#cuerpo').html(data);
    },
      error:function(x,xs,xt,data){
        alert("Error al registrar la marca")
        }
      });
    });

</script>

<script>
$('#elimina').click( function(){
  var frm=$("#agregarproductos");
  var datos = frm.serialize();
var x = $("#folioo").val();
var folio=document.getElementById('folioo');

  //alert(datos);
 $.ajaxSetup({
       headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
       }
   });
        $.ajax({
            type:'post',
            url: '/borrarOrden/',
            data:datos,
            beforeSend: function(){
                folio.value=x;
                $('#loader').css("visibility", "visible");
            },
            success:function(data) {
                //aplicar los valores de la tabla
           alert(data);
            },
            complete: function(){
                $('#loader').css("visibility", "hidden");
            }
            ,
            error:function(x,xs,xt){
              alert(xt);
            }
        });
        $.ajax({
        type : 'get',
        url : '/VerDetalle/'+x,
        success:function(data){
          $('#cuerpo').html(data);
    },
      error:function(x,xs,xt,data){
        alert("Error al registrar la marca");
        }
      });
    });

</script>
<script>
var boton=document.getElementById('borrar');
boton.click(function(){
alert('error');
});




</script>
