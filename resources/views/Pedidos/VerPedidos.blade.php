@extends('layouts.cabe')
<!-- /#sidebar-wrapper -->
@section('content')
<!-- Page Content -->
<style>
    .btn-group-fab {
        position: fixed;
        width: 50px;
        height: auto;
        right: 20px;
        bottom: 20px;
    }
    .has-search .form-control {
        padding-left: 2.375rem;
    }

    .has-search .form-control-feedback {
        position: absolute;
        z-index: 2;
        display: block;
        width: 2.375rem;
        height: 2.375rem;
        line-height: 2.375rem;
        text-align: center;
        pointer-events: none;
        color: #aaa;
    }

    .btn-group-fab div {
        position: relative;
        width: 100%;
        height: auto;
    }

    .js-pscroll {
        position: relative;
        overflow: hidden;
    }

    table tr th {
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    tr:hover {
        background-color: #f0f1f3;
    }

    .asc:after {
        content: ' ↑';
    }

    .desc:after {
        content: " ↓";
    }

    .btn-group-fab .btn {
        position: absolute;
        bottom: 0;
        border-radius: 50%;
        display: block;
        margin-bottom: 4px;
        width: 40px;
        height: 40px;
        margin: 4px auto;
    }

    .btn-group-fab .btn-main {
        width: 50px;
        height: 50px;
        right: 50%;
        margin-right: -25px;
        z-index: 9;
    }

    .btn-group-fab .btn-sub {
        bottom: 0;
        z-index: 8;
        right: 50%;
        margin-right: -20px;
        -webkit-transition: all 2s;
        transition: all 0.5s;
    }

    .nav-link,
    .nav-link-active {
        color: #aaaa;

    }

    .modal-header {
        background-color: #344478;
        color: #ffffff;
    }

    .nav-tabs {
        width: 100$;
    }

    .close {}


    .mr-auto {
        margin-right: 200px;
    }

    .hidden {
        display: none;
    }

    .btn-group-fab.active .btn-sub:nth-child(2) {
        bottom: 60px;
    }

    .btn-group-fab.active .btn-sub:nth-child(3) {
        bottom: 110px;
    }

    .btn-group-fab.active .btn-sub:nth-child(4) {
        bottom: 160px;
    }

    .btn-group-fab .btn-sub:nth-child(5) {
        bottom: 210px;
    }

</style>
<div class="container-fluid">
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))
        <div class="alert alert-{{ $msg }} alert-dismissible fade show" role="alert">{{ Session::get('alert-' . $msg) }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif
        @endforeach
    </div>


    <div class="container">
        <center>

        </center><br /><br />
        <nav class="navbar navbar-expand-lg navbar-light" style="background-color: #344478">

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link dropdown-toggle" href="#" style="color: #FFFFFF" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-sliders-h"></i> Filtros
                        </a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                          <a class="dropdown-item" id="ocultarFolio1" href="#">Folio</a>
                    			<a class="dropdown-item" id="ocultarTipo1" href="#">Tipo pedido</a>
                          <a class="dropdown-item" id="ocultarOrigen" href="#">Origen Compra</a>
                          <a class="dropdown-item" id="ocultarCliente" href="#">Destino Compra</a>
                          <a class="dropdown-item" id="ocultarEstado" href="#">Estado de la Órden</a>


                    </li>
                </ul>
                <form class="form-inline my-10 my-lg-0" action="/ModPed">
                    <h4 class="display-4" style="color:#fff; margin-right:150px;"> Órdenes de compra</h4>

                    <div>
                        <div class="form-group has-search">
                            <span class="fa fa-search form-control-feedback" style="color:#344478;"></span>
                            <input type="text" class="form-control" id="search" name="search" type="search" placeholder="Buscar">
                        </div>
                    </div>

                </form>
            </div>
        </nav>
        <div class="row form-row">
            <div class="col-md-12">
                <br />


                    <table data-toggle="table " class="table table-striped hidden" name="prueba" id="mitabla2">
                        <thead>
                            <tr>

                                <th>Folio</th>
                                <th>Tipo pedido</th>
                                <th>Origen Compras</th>
                                <th>Destino Compra</th>
                                <th>Estado de Órden</th>
                                <th class="text-center">Opciones</th>
                            </tr>

                        </thead>
                        <tbody class="busca">
                            @foreach($pedidoss as $pedido)
                            <tr>
                                <td class="hidden">{!!$pedido ->idPedido!!}</td>
                                <td>{!!$pedido ->folio!!}</td>
                                <td>{!!$pedido ->tipoP!!}</td>
                                <td>{!!$pedido ->origen!!}</td>
                                <td>{!!$pedido ->destino!!}</td>
                                <td>{!!$pedido ->descripcionP!!}</td>

                                <td>

                                    <?php if($pedido -> descripcionP == "En captura"){?>
                                    <button type="button" id="cancelar" name="cancelar" class="btn btn-danger" data-target="#modalCancela" data-id="{{$pedido ->idPedido}}" data-peticion="Cancelar" data-tipo="{{$pedido ->tipoP}}" onclick="#modalCancela" data-toggle="modal"><i class="fas fa-times"></i></button>

                                    <button type="button" id="upgrade" name="upgrade" class="btn btn-warning" data-target="#modalCancela" data-id="{{$pedido ->idPedido}}" data-peticion="Upgrade" data-tipo="{{$pedido ->tipoP}}" onclick="#modalCancela" data-toggle="modal"><i class="fas fa-level-up-alt"></i></button>

                                    <?php }?>
                                    <?php if($pedido -> descripcionP == "Creado"){?>

                                    <button type="button" id="cancelar" name="cancelar" class="btn btn-danger" data-target="#modalCancela" data-id="{{$pedido ->idPedido}}" data-peticion="Cancelar" data-tipo="{{$pedido ->tipoP}}" onclick="#modalCancela" data-toggle="modal"><i class="fas fa-times"></i></button>

                                    <?php if($pedido -> tipoP == "Entrada"){?>
                                    <button type="button" id="aceptar" name="aceptar" class="btn btn-info" data-target="#modalAceptaE" data-id="{{$pedido ->idPedido}}" data-peticion="Aceptar" data-tipo="{{$pedido ->tipoP}}" onclick="modalAcepPe('{{$pedido ->idPedido}}')" data-toggle="modal"><i class="fas fa-check"></i></button><?php } else{?>
                                    <button type="button" id="aceptar" name="aceptar" class="btn btn-info" data-target="#modalAcepta" data-id="{{$pedido ->idPedido}}" data-peticion="Aceptar" data-tipo="{{$pedido ->tipoP}}" onclick="#modalCancela" data-toggle="modal"><i class="fas fa-check"></i></button><?php }?>

                                    <?php }?>

                                    <button type="button" id="detalles" name="detalles" onclick="modalPe('{{$pedido ->idPedido}}')" data-id="{{$pedido ->idPedido}}" class="btn btn-success" data-toggle="modal" data-target="#modalDetalles"><i class="fas fa-eye"></i></button>


                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

            </div>
        </div>
    </div>

                        <table data-toggle="table " class="table table-striped" name="prueba" id="mitabla">
                            <thead>
                                <tr>

                                    <th>Folio</th>
                                    <th>Tipo pedido</th>
                                    <th>Origen Compra</th>
                                    <th>Destino Compra</th>
                                    <th>Estado de Órden</th>
                                    <th class="text-center">Opciones</th>
                                </tr>

                            </thead>
                            <tbody>
                                @foreach($pedidos as $pedido)
                                <tr>

                                    <td>{!!$pedido ->folio!!}</td>
                                    <td>{!!$pedido ->tipoP!!}</td>
                                    <td>{!!$pedido ->origen!!}</td>
                                    <td>{!!$pedido ->destino!!}</td>
                                    <td>{!!$pedido ->descripcionP!!}</td>

                                    <td>

                                        <?php if($pedido -> descripcionP == "En captura"){?>
                                        <button type="button" id="cancelar" name="cancelar" class="btn btn-danger" data-target="#modalCancela" data-id="{{$pedido ->idPedido}}" data-peticion="Cancelar" data-tipo="{{$pedido ->tipoP}}" onclick="#modalCancela" data-toggle="modal"><i class="fas fa-times"></i></button>

                                        <button type="button" id="upgrade" name="upgrade" class="btn btn-warning" data-target="#modalCancela" data-id="{{$pedido ->idPedido}}" data-peticion="Upgrade" data-tipo="{{$pedido ->tipoP}}" onclick="#modalCancela" data-toggle="modal"><i class="fas fa-level-up-alt"></i></button>

                                        <?php }?>
                                        <?php if($pedido -> descripcionP == "Creado"){?>

                                        <button type="button" id="cancelar" name="cancelar" class="btn btn-danger" data-target="#modalCancela" data-id="{{$pedido ->idPedido}}" data-peticion="Cancelar" data-tipo="{{$pedido ->tipoP}}" onclick="#modalCancela" data-toggle="modal"><i class="fas fa-times"></i></button>

                                        <?php if($pedido -> tipoP == "Entrada"){?>
                                        <button type="button" id="aceptar" name="aceptar" class="btn btn-info" data-target="#modalAceptaE" data-id="{{$pedido ->idPedido}}" data-peticion="Aceptar" data-tipo="{{$pedido ->tipoP}}" onclick="modalAcepPe('{{$pedido ->idPedido}}')" data-toggle="modal"><i class="fas fa-check"></i></button><?php } else{?>
                                        <button type="button" id="aceptar" name="aceptar" class="btn btn-info" data-target="#modalAceptaE" data-id="{{$pedido ->idPedido}}" data-peticion="Aceptar" data-tipo="{{$pedido ->tipoP}}" onclick="#modalAceptaE" data-toggle="modal"><i class="fas fa-check"></i></button><?php }?>

                                        <?php }?>

                                        <button type="button" id="detalles" name="detalles" onclick="modalPe('{{$pedido ->idPedido}}')" data-id="{{$pedido ->idPedido}}" class="btn btn-success" data-toggle="modal" data-target="#modalDetalles"><i class="fas fa-eye"></i></button>


                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="" id="paginacion">
                        {{$pedidos->links()}}
                        </div>
    <!--Modal detalles-->
    <div class="modal" id="modalDetalles" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Detalles del pedido</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <table class="table" name="tablaDetails">
                        <thead class="thead" style="background: #0E1A78; color: #FFFFFF;">
                            <tr>
                                <th scope="col">IdPedido</th>
                                <th scope="col">descripcion</th>
                                <th scope="col">Cantidad</th>
                                <th scope="col">Unidad Base</th>
                                <th scope="col">Costo</th>
                                <th scope="col">Costo Venta</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="far fa-eye-slash"></i></button>

                </div>
            </div>
        </div>
    </div>

    <!-- Floating Action Button like Google Material -->

    <div class="btn-group-fab" role="group" aria-label="FAB Menu">
        <div>
            <button type="button" class="btn btn-main btn-primary has-tooltip" data-placement="left" title="Menu"> <i class="fa fa-bars"></i> </button>
            <button type="button" class="btn btn-sub btn-info has-tooltip" data-toggle="modal" data-target="#modalCrear" data-placement="left" title="Agregar Pedido"> <i class="fas fa-plus"></i></button>
            <button type="button" class="btn btn-sub btn-dark has-tooltip" data-toggle="modal" data-target="#ayudaPedidos" data-placement="left" title="Ayuda"><i class="fas fa-question"></i></button>

        </div>
    </div>
    <script>
        $(function() {
            $('.btn-group-fab').on('click', '.btn', function() {
                $('.btn-group-fab').toggleClass('active');
            });
            $('has-tooltip').tooltip();
        });

    </script>



    <!--Modal ayuda general-->
    <div class="modal" id="ayudaPedidos" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Ayuda Pedidos </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <center>
                        <h2>Bienvenido</h2>
                    </center>
                    <button type="button" class="btn btn-success"><i class="fas fa-eye" style="height:4px;"></i></button><strong> Ver Detalles </strong> <br />

                    En esta opcion el usuario podra encontrar informacion detallada sobre los productos que se encuentran en cada pedido<br />
                    La informacion que muestra los campos son :
                    <ul>
                        <li>Id del pedido</li>
                        <li>Descripción del producto</li>
                        <li>Cantidad de productos en el pedido</li>
                        <li>Unidad base del producto</li>
                        <li>Costo de cada producto</li>
                        <li>Costo de venta de cada producto</li>
                    </ul>

                    <button type="button" class="btn btn-primary"><i class="fas fa-edit" style="height:4px;"></i></button><strong> Editar pedido </strong> <br />

                    En esta opción el usuario podrá editar el pedido, cabe recalcar que esta opción <strong>solo está habilitada si el pedido está en captura. </strong>

                    <br /><br />

                    <button type="button" class="btn btn-warning"><i class="fas fa-level-up-alt" style="height:4px;"></i></button><strong> Autorizar pedido </strong> <br />
                    En esta opción el usuario podrá autorizar un pedido haciendo que este cambie de un estado de <strong>En captura</strong> a un estado de <strong>Creado</strong>.

                    <br /><br />

                    <button type="button" class="btn btn-danger"><i class="fas fa-times" style="height:4px;"></i></button><strong> Cancelar pedido </strong> <br />
                    En esta opción el usuario podrá cancelar un pedido haciendo que este cambie de un estado de <strong>En captura</strong> o <strong>Creado</strong> a un estado de <strong>Cancelado</strong>.

                    <br /><br />

                    <button type="button" class="btn btn-info"><i class="fas fa-check" style="height:4px;"></i></button><strong> Marcar pedido como exitoso </strong> <br />
                    En esta opción el usuario podrá marcar como exitoso un pedido haciendo que este cambie de un estado de <strong>Creado</strong>a un estado de <strong>Exitoso</strong>.



                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-eye-slash"></i></button>
                </div>

            </div>
        </div>
    </div>

    <!--Modal crear-->
    <div class="modal fade bd-example-modal" id="modalCrear" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Crear Órden de Compra</h5>

                    <button type="button" style="  color: #FFFFFF;" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div role="tabpanel">
                        <!-- Pestañas productos -->
                        <div class="nav nav-tabs " role="tablist">

                            <a role="presentation" class="nav-item nav-link active " href="#datosPedido" aria-controls="datosPedido" role="tab" data-toggle="tab">Datos del nuevo pedido</a>

                        </div>
                        <!--Pestaña datos del pedido-->
                        <div class="tab-content" id="nav-tabContent">
                            <div role="tabpanel" class="tab-pane active" id="datosPedido">
                                <br />
                                @include('Pedidos.CrearPedidos')
                                <br />

                            </div>
                            <!--Pestaña productos del pedido-->
                            <!--  <div role="tabpanel" class="tab-pane" id="productosPedido">
                            <br/>
                              @include('Pedidos.PedidoCarrito')
                            <br/>
-->

                            <div class="modal-footer">
                              <a href="{{route('verPedidos')}}" class="btn btn-primary my-2 my-sm-0"><i class="fas fa-edit"></i></a>
                                <button type="button" id="upgrade" name="upgrade" class="btn btn-warning" data-target="#modalCancela" data-id="{{$pedido ->idPedido}}" data-peticion="Upgrade" data-tipo="{{$pedido ->tipoP}}" onclick="#modalCancela" data-toggle="modal"><i class="fas fa-level-up-alt"></i></button>
                            </div>


                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>

    <form method="post" action="{{route('ModPed.update', 1)}}" onsubmit="return envia(this)">
        {{method_field('PUT')}}
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <!--Modal cancela-->
        <div class="modal" id="modalCancela" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Operaciones: </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p class="form-inline">¿Está seguro de
                            <input type="text" name="idPeticion" id="idPeticion" class="form-control" size="4" readonly> el pedido
                            <input type="text" name="id" id="id" class="form-control" size="1" readonly>
                            <input type="text" name="tipoP" id="tipoP" class="form-control" size="5" readonly>
                            ?</p>

                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-eye-slash"></i></button>

                        <button type="submit" id="cancelar" name="cancelar" class="btn btn-primary"><i class="far fa-check-square fa-lg"></i></button>
                    </div>

                </div>
            </div>
        </div>
    </form>

    <form method="post" action="{{route('ModPed.update', 1)}}" onsubmit="return envia(this)">
        {{method_field('PUT')}}
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <!--Modal Aceptar entrada-->
        <div class="modal" id="modalAceptaE" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Operaciones: </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <center>
                          <p class="form-inline">¿Está seguro de
                              <input type="text" name="idPeticion" id="idPeticion" class="form-control" size="4" readonly> el pedido
                              <input type="text" name="id" id="id" class="form-control" size="1" readonly>
                              <input type="text" name="tipoP" id="tipoP" class="form-control" size="5" readonly>
                              ?</p>
                            <table class="table" name="AceptarE">
                                <thead class="thead" style="background: #0E1A78; color: #FFFFFF;">
                                    <tr>
                                        <th scope="col">Id Pedido</th>
                                        <th scope="col">Producto</th>
                                        <th scope="col">Cantidad</th>
                                        <th scope="col">Unidad base</th>
                                        <th scope="col">Costo compra</th>
                                        <th scope="col">Costo venta</th>
                                    </tr>
                                </thead>
                                <tbody>


                                </tbody>

                            </table>

                        </center>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-eye-slash"></i></button>

                        <button type="submit" id="cancelar" name="cancelar" class="btn btn-primary"><i class="far fa-check-square fa-lg"></i></button>
                    </div>

                </div>
            </div>
        </div>
    </form>





    <script>
        function modalPe(id) {
            $.ajax({
                url: '/VerDetalles/' + id,
                type: "GET",
            }).done(function(res) {

                $('table[name="tablaDetails"]').empty();
                $('table[name="tablaDetails"]').append('<thead class="thead" style="background: #0E1A78; color: #FFFFFF;"><tr><th scope="col">Folio Pedido</th><th scope="col">Descripcion</th><th scope="col">Cantidad</th><th scope="col">Unidad Base</th><th scope="col">Costo</th><th scope="col">Costo Venta</th></tr></thead>');




                for ($i = 0; $i <= 10; $i++) {
                    $('table[name="tablaDetails"]').append('<tr><td>' + res.pedidosProductos[$i].idPedido + '</td>      <td>' + res.pedidosProductos[$i].Descripcion + '</td>  <td>' + res.pedidosProductos[$i].Cantidad + '</td>      <td>' + res.pedidosProductos[$i].unidadbase + '</td><td>' + res.pedidosProductos[$i].costo + '</td><td>' + res.pedidosProductos[$i].precioreal + '</td></tr>');

                }
            });
        }

    </script>
  <!--  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>-->
<script>
     function modalAcepPe(id) {
         $.ajax({
             url: '/VerDetalles/' + id,
             type: "GET",
         }).done(function(res) {

             $('table[name="AceptarE"]').empty();
             $('table[name="AceptarE"]').append('<thead class="thead" style="background: #0E1A78; color: #FFFFFF;"><tr><th scope="col">Folio Pedido</th><th scope="col">Descripcion</th><th scope="col">Cantidad</th><th scope="col">Unidad Base</th><th scope="col">Costo</th><th scope="col">Costo Venta</th><th scope="col">Aceptar</th><th scope="col">Revisados</th></tr></thead>');




             for ($i = 0; $i <= 10; $i++) {
                 $('table[name="AceptarE"]').append('<tr><td>' + res.pedidosProductos[$i].idPedido + '</td>      <td>' + res.pedidosProductos[$i].Descripcion + '</td>  <td>' + res.pedidosProductos[$i].Cantidad + '</td>      <td>' + res.pedidosProductos[$i].unidadbase + '</td><td>' + res.pedidosProductos[$i].costo + '</td><td>' + res.pedidosProductos[$i].precioreal + '</td><td>' + '<div class="form-check form-check-inline"><input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1"><label class="form-check-label" for="inlineCheckbox1">Aceptar</label></div></td>' + '<td><div class="incrementer-class-name"><button type="button" id="decremeta" onClick="dec(event)" class="remove-button md-btn md-btn-success" title="Quitar"><i class="fas fa-minus"></i></button>' + ' <input type="text" value="0" name="revisados" class="md-input form-control" onClick="this.setSelectionRange(0, this.value.length);" style="width: 88%;" readonly>' + '<button type="button" id="incrementa" onClick="add(event)"class="add-button md-btn md-btn-success" title="Agregar"><i class="fas fa-plus"></i></button>' + '</div>' + '</td></tr>');

             }
         });
     }

     function dec(event) {
         // alert("hola");

        // alert(parseInt(event.target.value));
             var inputField = $(event.target).next('input'),
                 value = parseInt(inputField.val());
             value -= 1;
             if (value < 0) value = 0;
             inputField.val(value);

     }

     function add(event) {
         var inputField = $(event.target).prev('input'),
             value = parseInt(inputField.val());
         value += 1;
         inputField.val(value);
     }

 </script>
<script>
$(document).ready(function(){
 let table = document.getElementById('mitabla');
   let table1 = document.getElementById('mitabla2');

 $("#ocultarFolio1").click(function(){
   for (var i = 0, row; row = table.rows[i]; i++){
      row.cells[0].classList.toggle('hidden');
   }
   for (var i = 0, row; row = table1.rows[i]; i++){
      row.cells[0].classList.toggle('hidden');
   }
 });

 $("#ocultarTipo1").click(function(){
       for (var i = 0, row; row = table.rows[i]; i++){
          row.cells[1].classList.toggle('hidden');
       }
       for (var i = 0, row; row = table1.rows[i]; i++){
          row.cells[1].classList.toggle('hidden');
       }
     });

$("#ocultarOrigen").click(function(){
       for (var i = 0, row; row = table.rows[i]; i++){
          row.cells[2].classList.toggle('hidden');
       }
       for (var i = 0, row; row = table1.rows[i]; i++){
          row.cells[2].classList.toggle('hidden');
       }
     });

    $("#ocultarCliente").click(function(){
       for (var i = 0, row; row = table.rows[i]; i++){
          row.cells[3].classList.toggle('hidden');
       }
       for (var i = 0, row; row = table1.rows[i]; i++){
          row.cells[3].classList.toggle('hidden');
       }
     });
    $("#ocultarEstado").click(function(){
       for (var i = 0, row; row = table.rows[i]; i++){
          row.cells[4].classList.toggle('hidden');
       }
       for (var i = 0, row; row = table1.rows[i]; i++){
          row.cells[4].classList.toggle('hidden');
       }
     });});
</script>
<script>
  $(document).ready(function () {
      (function ($) {
          $('#search').keyup(function () {

               var rex = new RegExp($(this).val(), 'i');

               if(rex=='/(?:)/i'){
                 $('#mitabla2').hide();
                 $('#mitabla').show();
                 $('#paginacion').show();
               }else{
                 $('#mitabla2').show();
                 $('#mitabla').hide();
               $('#paginacion').hide();}
               $('.busca tr').hide();
               $('.busca tr').filter(function () {
                 return rex.test($(this).text());
               }).show();

})
      }(jQuery));


  });
</script>
    <script>
        $('#modalCancela').on('show.bs.modal', function(event) {
            var button = $(event.relatedTarget)
            var id = button.data('id')
            var peticion = button.data('peticion')
            var tipoP = button.data('tipo')
            var modal = $(this)
            modal.find('.modal-body #id').val(id)
            modal.find('.modal-body #idPeticion').val(peticion)
            modal.find('.modal-body #tipoP').val(tipoP)


        })

    </script>
    <script>
        $('#modalAceptaE').on('show.bs.modal', function(event) {
            var button = $(event.relatedTarget)
            var id = button.data('id')
            var peticion = button.data('peticion')
            var tipoP = button.data('tipo')
            var modal = $(this)
            modal.find('.modal-body #id').val(id)
            modal.find('.modal-body #idPeticion').val(peticion)
            modal.find('.modal-body #tipoP').val(tipoP)


        })

    </script>
    <script>
    $('th').click(function() {
    var table = $(this).parents('table').eq(0)
    var rows = table.find('tr:gt(0)').toArray().sort(comparer($(this).index()))
    this.asc = !this.asc
    if (!this.asc) {
       rows = rows.reverse()
    }
    for (var i = 0; i < rows.length; i++) {
       table.append(rows[i])
    }
    setIcon($(this), this.asc);

    });
    function comparer(index) {
    return function(a, b) {
       var valA = getCellValue(a, index),
       valB = getCellValue(b, index)
       return $.isNumeric(valA) && $.isNumeric(valB) ? valA - valB : valA.localeCompare(valB)
    }
    }
    function getCellValue(row, index) {
    return $(row).children('td').eq(index).html()
    }
    function setIcon(element, asc) {
    $("th").each(function(index) {
       $(this).removeClass("sorting");
       $(this).removeClass("asc");
       $(this).removeClass("desc");
    });
    element.addClass("sorting");
    if (asc) element.addClass("asc");
    else element.addClass("desc");
    }

    </script>

    @endsection
