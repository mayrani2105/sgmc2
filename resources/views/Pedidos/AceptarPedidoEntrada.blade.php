<form  method="post" action="{{route('ModPed.update', 1)}}" onsubmit="return envia(this)">
                  {{method_field('PUT')}}
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<!--Modal cancela-->
    <div class="modal" id="modalAceptaE" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Operaciones: </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body">
        <center>
            <table class="table">
			  <thead class="thead" style="background: #0E1A78; color: #FFFFFF;">
                        <tr>
                          <th scope="col">Id Pedido</th>
                          <th scope="col">Producto</th>
                          <th scope="col">Cantidad</th>
                          <th scope="col">Unidad base</th>
                          <th scope="col">Costo compra</th>
                          <th scope="col">Costo venta</th>
                        </tr>
			  </thead>
			  <tbody> 
                  @foreach($pedidosProductos as $pedidosProductos)
				<tr>
				    <td>{!!$pedidosProductos ->idPedido!!}</td>
                    <td>{!!$pedidosProductos ->Descripcion!!}</td>
		            <td>{!!$pedidosProductos ->Cantidad!!}</td>
                    <td>{!!$pedidosProductos ->unidadbase!!}</td>
                    <td>{!!$pedidosProductos ->costo!!}</td>
                    <td>{!!$pedidosProductos ->precioreal!!}</td>
                    
				</tr>
                  @endforeach
            
			  </tbody>
			  
                </table>
            
  </center>

        </div>

        <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>

                <button type="submit" id="cancelar" name="cancelar" class="btn btn-success" >Sí, estoy deacuerdo</button>
        </div>

    </div>
  </div>
</div>
</form>
