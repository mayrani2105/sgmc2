    <center>
            <form id="register-form" action="/CrearPedido" method="POST" role="form" class="form-group">
                <div class="form-group">
                    <p>Descripción
                        <input type="text" name="Descripcion" id="descripcionPedido" tabindex="1" class="form-control" placeholder="Descripción del pedido" value="" width="20%"></p>
                </div>
                <div class="form-group">
                        <p>Fecha del pedido
                    <input type="date" name="fechaPedido" id="fechaPedido" tabindex="1" class="form-control"></p>
                </div>
                <div class="form-group">
                    <p>Persona a cargo del pedido
                    <select type="" name="encargado" id="Encargado" tabindex="" class="form-control" placeholder="Persona a cargo" min="0">
                    <option selected>Persona a cargo</option>
                    <option value ="1">José</option>    
                    <option value ="2">Donald</option>   
                    <option value ="3">Marco</option></select></p>                               
                </div>
                
                <div class="form-group">
                    <p>Proveedor <p>(Al modificar el proovedor se eliminarán automáticamente todos lor productos registrados por el pedido)
                    <select type="" name="encargado" id="Encargado" tabindex="" class="form-control" placeholder="Persona a cargo" min="0" data-toggle="modalEditarProv" data-target="#modalEdita">
                    <option selected>Persona a cargo</option>
                    <option value ="1">Bic</option>    
                    <option value ="2">Pelican</option>   
                    <option value ="3">Baco</option></select></p>
                </div>
                
            <table class="table">
			  <thead class="thead" style="background: #0E1A78; color: #FFFFFF;">
				<tr>
                  <th scope="col">Id Pedido</th>
				  <th scope="col">Producto</th>
				  <th scope="col">Cantidad</th>
				  <th scope="col">Unidad base</th>
				  <th scope="col">Costo compra</th>
                  <th scope="col">Costo venta</th>
				</tr>
			  </thead>
			  <tbody> 
              
			  </tbody>
			</table>
                
                
                
                <button type="button"  id="detalles" name="detalles" class="btn btn-secondary" data-toggle="modal" data-target="#modalEditarProv" value="">Agregar productos al pedido</button>
                </form>
  </center>

 <!--Modal editar-->
<div class="modal" id="modalEditarProv" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Advertencia</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        
      <div class="modal-body">
        Al modificar el proovedor se eliminarán todos los productos previamente seleccionados.
      </div>
        
      <div class="modal-footer">
        <button type="button" class="btn btn-primary">Guardar cambios</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div> 