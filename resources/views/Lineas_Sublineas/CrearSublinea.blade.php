<div class="row">
    <div class="col-md-6">
    <label for="Linea">Linea a la que pertenece</label>
    <select  name="Linea" id="Linea" class="form-control" maxlength="300px" >
        @foreach($categorias as $linea)
        <option value="{{$linea->idCategoria}}">
            {{$linea->descripcion}}
        </option>
        @endforeach
    </select>

</div>
</div>
<div class="row">
    <div class="col-md-6">
    <label for="Sublinea">Nombre de la nueva sublinea</label>
    <input type="text" name="Sublinea" id="Sublinea"  class="form-control" placeholder="Nombre de la sublinea" value="" width="20%" required>
</div>
</div>
