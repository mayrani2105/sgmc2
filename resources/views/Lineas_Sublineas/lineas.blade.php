@extends('layouts.cabe')
    <!-- /#sidebar-wrapper -->
@section('content')
<style>
.btn-group-fab {
  position: fixed;
  width: 50px;
  height: auto;
  right: 20px; bottom: 20px;
}
.btn-group-fab div {
  position: relative; width: 100%;
  height: auto;
}
.js-pscroll {
  position: relative;
  overflow: hidden;
}

table tr th {
   cursor: pointer;
   -webkit-user-select: none;
   -moz-user-select: none;
   -ms-user-select: none;
   user-select: none;
}
tr:hover{
  background-color: #f0f1f3;
}

.asc:after {
   content: ' ↑';
}
.desc:after {
   content: " ↓";
}
.btn-group-fab .btn {
  position: absolute;
  bottom: 0;
  border-radius: 50%;
  display: block;
  margin-bottom: 4px;
  width: 40px; height: 40px;
  margin: 4px auto;
}
.btn-group-fab .btn-main {
  width: 50px; height: 50px;
  right: 50%; margin-right: -25px;
  z-index: 9;
}
.btn-group-fab .btn-sub {
  bottom: 0; z-index: 8;
  right: 50%;
  margin-right: -20px;
  -webkit-transition: all 2s;
  transition: all 0.5s;
}

.nav-link,.nav-link-active{
color:#aaaa;

}
.modal-header{
  background-color: #344478;
  color:#ffffff;
}
.nav-tabs{
width: 100$;
}
.close{

}


.mr-auto{
  margin-right: 200px;
}
.hidden{ display: none; }
.btn-group-fab.active .btn-sub:nth-child(2) {
  bottom: 60px;
}
.btn-group-fab.active .btn-sub:nth-child(3) {
  bottom: 110px;
}
.btn-group-fab.active .btn-sub:nth-child(4) {
  bottom: 160px;
}
.btn-group-fab.active .btn-sub:nth-child(5) {
  bottom: 210px;
}
.btn-group-fab .btn-sub:nth-child(6) {
  bottom: 260px;
}</style>
    <div class="row">
        <h3 style="margin-top: 20px;margin-bottom: 40px; margin-left:40px;">
        Catálogo de Lineas y Sublineas


        </h3>
    </div>
    {!!$listas!!}
    <div class="btn-group-fab" role="group" aria-label="FAB Menu">
      <div>
        <button type="button" class="btn btn-main btn-primary has-tooltip" data-placement="left" title="Menu"> <i class="fa fa-bars"></i> </button>
        <button type="button" class="btn btn-sub btn-dark has-tooltip"data-toggle="modal" data-target="#Ayuda" data-placement="left" title="Ayuda"><i class="fas fa-question"></i></button>
      </div>
    </div>


    <div class="modal fade bd-example-modal-lg" id="Ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Ayuda Lineas y sublineas </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
          <h2>Bienvenido {{Auth::user()->name}} </h2>
            <br/>
          @if(Auth::user()->id_rol!=5)
<i class="fas fa-drafting-compass"></i> Catálogo de Lineas y Sublineas<br/><br/>
          En este apartado va poder checar las lineas y Sublineas con las que contamos y asi mismo las lineas que nosotros creemos.<br/>
          <br/>Para poder checar las sublineas correspondientes a cada linea basta con hacer click sobre la linea que queremos consultar.
    @endif
          </div>
          <div class="modal-footer">

            <button type="button" class="btn btn-danger"data-dismiss="modal"data-toggle="tooltip" data-placement="top" title="Cerrar Ayuda"><i class="fas fa-eye-slash"></i></button>
          </div>
        </div>
      </div>
    </div>




    <script>
    $(function() {
      $('.btn-group-fab').on('click', '.btn', function() {
        $('.btn-group-fab').toggleClass('active');
      });
      $('has-tooltip').tooltip();
    });
    </script>


    <!-- /#page-content-wrapper -->
  <!-- /#wrapper -->


@endsection
