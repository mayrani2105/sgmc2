<div class="row">
  <div class="col-md-5">
<label for="Clave">Clave</label>
<input type="number" name="claveProveedor" id="claveProveedorcrear" tabindex="1" class="form-control" placeholder="Clave del Proveedor" width="20%" min="0" required>
</div>
<div class="col-md-6">
<label for="proveedor">Proveedor</label>
   <input type="text" name="proveedor" id="proveedorCrear" tabindex="1" class="form-control" placeholder="Nombre del Proveedor"  width="20%" required><br/>
</div>
</div><div class="row">
  <div class="col-md-5">
        <label for="calle">Calle</label>
              <input type="text" name="calle" id="calle" tabindex="1" class="form-control" placeholder="Calle" width="20%" required><br/>
      </div>
  <div class="col-md-5">
          <label for="colonia">Colonia</label>
   <input type="text" name="colonia" id="coloniaCrear" tabindex="1" class="form-control" placeholder="Colonia" width="20%" required><br/></div>
   </div>

   <div class="row">
     <div class="col-md-3">
         <label for="noExterior"> Numero Exterior</label>
     <input type="number" name="noExterior" id="noExteriorCrear" tabindex="2" class="form-control" placeholder="numero Exterior"min="0" required><br/>
   </div>
     <div class="col-md-4">
       <label for="Estado">Estado</label>
      <select name="estado" class="form-control" value=""required>
          <option value="no">Estado</option>
          <option value="Aguascalientes">Aguascalientes</option>
          <option value="Baja California">Baja California</option>
          <option value="Baja California Sur">Baja California Sur</option>
          <option value="Campeche">Campeche</option>
          <option value="Chiapas">Chiapas</option>
          <option value="Chihuahua">Chihuahua</option>
          <option value="Coahuila">Coahuila</option>
          <option value="Colima">Colima</option>
          <option value="Distrito Federal">Distrito Federal</option>
          <option value="Durango">Durango</option>
          <option value="Estado de México">Estado de México</option>
          <option value="Guanajuato">Guanajuato</option>
          <option value="Guerrero">Guerrero</option>
          <option value="Hidalgo">Hidalgo</option>
          <option value="Jalisco">Jalisco</option>
          <option value="Michoacán">Michoacán</option>
          <option value="Morelos">Morelos</option>
          <option value="Nayarit">Nayarit</option>
          <option value="Nuevo León">Nuevo León</option>
          <option value="Oaxaca">Oaxaca</option>
          <option value="Puebla">Puebla</option>
          <option value="Querétaro">Querétaro</option>
          <option value="Quintana Roo">Quintana Roo</option>
          <option value="San Luis Potosí">San Luis Potosí</option>
          <option value="Sinaloa">Sinaloa</option>
          <option value="Sonora">Sonora</option>
          <option value="Tabasco">Tabasco</option>
          <option value="Tamaulipas">Tamaulipas</option>
          <option value="Tlaxcala">Tlaxcala</option>
          <option value="Veracruz">Veracruz</option>
          <option value="Yucatán">Yucatán</option>
          <option value="Zacatecas">Zacatecas</option>
      </select><br/></div>
      <div class="col-md-4">
           <label for="Pais">País</label>
 <input type="text" name="pais" id="paisCrear"  class="form-control" placeholder="Pais" required><br/>

</div> </div>
<div class="row">
  <div class="col-md-4">
      <label for="rfc">RFC</label>
<input type="text" name="rfc" id="rfcCrear"  class="form-control" placeholder="RFC" maxlength="100px" required><br/></div>
  <div class="col-md-4">
      <label for="Correo">Correo Electronico</label>
<input type="email" name="correo" id="correoCrear"  class="form-control" placeholder="Email" maxlength="100px" required ><br/></div>
  <div class="col-md-4">
    <label for="telefono">Telefono</label>
<input type="text" name="telefono" id="telefonoCrear" tabindex="1" class="form-control" placeholder="Numero de Telefono" min="0" max="15"><br/></div></div>
