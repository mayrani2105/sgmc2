
@extends('layouts.cabe')

@section('content')
<style>
.btn-group-fab {
  position: fixed;
  width: 50px;
  height: auto;
  right: 20px; bottom: 20px;
}
.btn-group-fab div {
  position: relative; width: 100%;
  height: auto;
}
.js-pscroll {
  position: relative;
  overflow: hidden;
}

table tr th {
   cursor: pointer;
   -webkit-user-select: none;
   -moz-user-select: none;
   -ms-user-select: none;
   user-select: none;
}
tr:hover{
  background-color: #f0f1f3;
}

.asc:after {
   content: ' ↑';
}
.desc:after {
   content: " ↓";
}
.btn-group-fab .btn {
  position: absolute;
  bottom: 0;
  border-radius: 50%;
  display: block;
  margin-bottom: 4px;
  width: 40px; height: 40px;
  margin: 4px auto;
}
.btn-group-fab .btn-main {
  width: 50px; height: 50px;
  right: 50%; margin-right: -25px;
  z-index: 9;
}
.btn-group-fab .btn-sub {
  bottom: 0; z-index: 8;
  right: 50%;
  margin-right: -20px;
  -webkit-transition: all 2s;
  transition: all 0.5s;
}
.has-search .form-control {
    padding-left: 2.375rem;
}

.has-search .form-control-feedback {
    position: absolute;
    z-index: 2;
    display: block;
    width: 2.375rem;
    height: 2.375rem;
    line-height: 2.375rem;
    text-align: center;
    pointer-events: none;
    color: #aaa;
}

.nav-link,.nav-link-active{
color:#aaaa;

}
.modal-header{
  background-color: #344478;
  color:#ffffff;
}
.nav-tabs{
width: 100$;
}
.close{

}


.mr-auto{
  margin-right: 200px;
}
.hidden{ display: none; }
.btn-group-fab.active .btn-sub:nth-child(2) {
  bottom: 60px;
}
.btn-group-fab.active .btn-sub:nth-child(3) {
  bottom: 110px;
}
.btn-group-fab.active .btn-sub:nth-child(4) {
  bottom: 160px;
}
.btn-group-fab.active .btn-sub:nth-child(5) {
  bottom: 210px;
}
.btn-group-fab .btn-sub:nth-child(6) {
  bottom: 260px;
}
</style>


<div class="container-fluid">
  <div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <div class="alert alert-{{ $msg }} alert-dismissible fade show" role="alert">{{ Session::get('alert-' . $msg) }}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
          </button>
          </div>
      @endif
    @endforeach
  </div>
<center><!--
<h3 class="display-4">Panel de Proveedores</h3>-->
</center><br/>

<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #344478">

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link dropdown-toggle" href="#" style="color: #FFFFFF" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			<i class="fas fa-sliders-h"></i> Filtros
		</a>
	  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
    <a class="dropdown-item" id="ocultarId" href="#">Id</a>
    <a class="dropdown-item" id="ocultar" href="#">Clave</a>
		<a class="dropdown-item" id="ocultarP"href="#">Proveedor</a>
    	<a class="dropdown-item" id="oculltarCorreo"href="#">Correo</a>
		<a class="dropdown-item" id="ocultarPromos"href="#">Promociones</a>
    <a class="dropdown-item" id="ocultardiascredito"href="#">diascredito</a>
    <a class="dropdown-item" id="oculltarTelefono"href="#">Telefono</a>
    <a class="dropdown-item" id="oculltarRFC"href="#">RFC</a>
    <a class="dropdown-item" id="oculltarcalle"href="#">Calle</a>
    <a class="dropdown-item" id="oculltarcolonia"href="#">Colonia</a>
	  </div>
      </li>
    </ul>
      <h4 class="display-4" style="color:#fff; margin-right:150px;"> Catálogo Proveedores</h4>
    <form class="form-inline my-10 my-lg-0" action="{{route('BuscarInve')}}">
    <div>
    <div class="form-group has-search">
    <span class="fa fa-search form-control-feedback"style="color:#344478;"></span>
    <input type="text" class="form-control"id="search"name="search" type="search" placeholder="Buscar">
    </div>
  </div>
</form>
</nav>

<br/>
<div>
</div>

  <table class="table table-striped hidden" id="mitabla2" data-toggle="table"style="width:100%;">
   <thead>
      <tr>
        <th  class=" ">Id</th>
        <th  class=""scope="col">Clave</th>
        <th  class=""scope="col">Proveedor</th>
        <th  class="hidden"scope="col">Email</th>
        <th  class=""scope="col">Dias Credito</th>
        <th  class=""scope="col">Promos %</th>
        <th  class=" hidden" scope="col">Telefono</th>
        <th  class=" hidden" scope="col">RFC</th>
        <th  class=" hidden"  scope="col">Colonia</th>
        <th  class=" hidden" scope="col">Calle</th>
        <th  class="text-center " style="width:140px;"scope = "col">Opciones</th>
      </tr>
   </thead>
   <tbody class="busca">
     @foreach($post as $producto)
     <tr >
       <td > {!!$producto ->idProveedor!!}</td>
       <td>{!!$producto ->claveProveedor!!}</th>
       <td >{!!$producto ->proveedor!!}</td>
       <td class=" hidden">{!!$producto ->correo!!}</td>
       <td >{!!$producto ->diascredito!!}</td>
       <td >{!!$producto ->promociones!!}</td>
       <td class="  hidden">{!!$producto ->telefono!!}</td>
       <td class="  hidden">{!!$producto ->rfc!!}</td>
       <td class=" hidden">{!!$producto ->colonia!!}</td>
       <td class=" hidden">{!!$producto ->calle!!}</td>

       <td style="width:170px;">

         @foreach($permiso as $permi)
        @if($permi->ver==1)
         <button type="button" class="btn btn-success "data-toggle="modal" data-target="#detalles" id="detallitos" name="detallitos"   data-id="{{$producto->idProveedor}}"
         data-clave="{{$producto->claveProveedor}}" data-proveedor="{{$producto->proveedor}}"data-diascredito="{{$producto->diascredito}}" data-promociones="{{$producto->promociones}}"
         data-rfc="{{$producto->rfc}}" data-telefono="{{$producto->telefono}}" data-correo="{{$producto->correo}}"
         data-colonia="{{$producto->colonia}}" data-estado="{{$producto->estado}}" data-calle="{{$producto->calle}}"data-toggle="tooltip" data-placement="top" title="Ver detalles"
         data-pais="{{$producto->pais}}"
         data-noexterior="{{$producto->noExterior}}"  ><i class="fas fa-eye"></i></button>
@endif
   @if($permi->editar==1)
   <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#edit"data-toggle="tooltip" data-placement="top" title="Modificar"
           data-id="{{$producto->idProveedor}}"
    data-clave="{{$producto->claveProveedor}}" data-proveedor="{{$producto->proveedor}}"
       data-rfc="{{$producto->rfc}}" data-telefono="{{$producto->telefono}}" data-correo="{{$producto->correo}}"
       data-colonia="{{$producto->colonia}}" data-estado="{{$producto->estado}}" data-calle="{{$producto->calle}}"
       data-pais="{{$producto->pais}}" data-diascredito="{{$producto->diascredito}}" data-promociones="{{$producto->promociones}}"
       data-noexterior="{{$producto->noExterior}}"><i class="fas fa-edit"></i></button>

             @endif

              @if($permi->borrar==01 )
             <a href="{{route('eliminarProv',$producto->idProveedor)}}" class="btn btn-danger" onclick="return confirm('¿Está seguro de eliminar este producto de manera permanente?')"><i class="fas fa-trash-alt"></i></a>

@endif
@endforeach

</td>
       </tr>
        @endforeach
   </tbody>
</table>


<table class="table table-striped" id="mitabla" data-toggle="table"style="width:100%;">
 <thead>
    <tr>
      <th  class=" ">Id</th>
      <th  class=""scope="col">Clave</th>
      <th  class=""scope="col">Proveedor</th>
      <th  class="hidden"scope="col">Email</th>
      <th  class=""scope="col">Dias Credito</th>
      <th  class=""scope="col">Promos %</th>
      <th  class=" hidden" scope="col">Telefono</th>
      <th  class=" hidden" scope="col">RFC</th>
      <th  class=" hidden"  scope="col">Colonia</th>
      <th  class=" hidden" scope="col">Calle</th>
      <th  class="text-center " style="width:140px;"scope = "col">Opciones</th>
    </tr>
 </thead>
 <tbody>
   @foreach($proveedores as $producto)
   <tr >
     <td > {!!$producto ->idProveedor!!}</td>
     <td>{!!$producto ->claveProveedor!!}</th>
     <td >{!!$producto ->proveedor!!}</td>
     <td class=" hidden">{!!$producto ->correo!!}</td>
     <td >{!!$producto ->diascredito!!}</td>
     <td >{!!$producto ->promociones!!}</td>
     <td class="  hidden">{!!$producto ->telefono!!}</td>
     <td class="  hidden">{!!$producto ->rfc!!}</td>
     <td class=" hidden">{!!$producto ->colonia!!}</td>
     <td class=" hidden">{!!$producto ->calle!!}</td>

     <td  style="width:170px;">

       @foreach($permiso as $permi)

       <button type="button" class="btn btn-success "data-toggle="modal" data-target="#detalles" id="detallitos" name="detallitos"   data-id="{{$producto->idProveedor}}"
       data-clave="{{$producto->claveProveedor}}" data-proveedor="{{$producto->proveedor}}"data-diascredito="{{$producto->diascredito}}" data-promociones="{{$producto->promociones}}"
       data-rfc="{{$producto->rfc}}" data-telefono="{{$producto->telefono}}" data-correo="{{$producto->correo}}"
       data-colonia="{{$producto->colonia}}" data-estado="{{$producto->estado}}" data-calle="{{$producto->calle}}"data-toggle="tooltip" data-placement="top" title="Ver detalles"
       data-pais="{{$producto->pais}}"
       data-noexterior="{{$producto->noExterior}}"  ><i class="fas fa-eye"></i></button>
 @if($permi->editar==1 )
 <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#edit"data-toggle="tooltip" data-placement="top" title="Modificar"
         data-id="{{$producto->idProveedor}}"
  data-clave="{{$producto->claveProveedor}}" data-proveedor="{{$producto->proveedor}}"
     data-rfc="{{$producto->rfc}}" data-telefono="{{$producto->telefono}}" data-correo="{{$producto->correo}}"
     data-colonia="{{$producto->colonia}}" data-estado="{{$producto->estado}}" data-calle="{{$producto->calle}}"
     data-pais="{{$producto->pais}}" data-diascredito="{{$producto->diascredito}}" data-promociones="{{$producto->promociones}}"
     data-noexterior="{{$producto->noExterior}}"><i class="fas fa-edit"></i></button>

           @endif
            @if($permi->borrar==01)
           <a href="{{route('eliminarProv',$producto->idProveedor)}}" class="btn btn-danger" onclick="return confirm('¿Está seguro de eliminar este producto de manera permanente?')"><i class="fas fa-trash-alt"></i></a>
@endif
@endforeach

     </tr>
      @endforeach
 </tbody>
</table>
<div class="" id="paginacion">
{{$proveedores->links()}}
</div>
<center><br/>
<br/></br>
<!-- scripts que me permite hacer la ordenacion de los campos-->


<!-- Floating Action Button like Google Material -->
<div class="btn-group-fab" role="group" aria-label="FAB Menu">
  <div>
    <button type="button" class="btn btn-main btn-primary has-tooltip" data-placement="left" title="Menu"> <i class="fa fa-bars"></i> </button>
    <button type="button" class="btn btn-sub btn-info has-tooltip"data-toggle="modal" data-target="#crear" data-placement="left" title="Agregar Proveedor"> <i class="fas fa-plus"></i> </button>
    <button type="button" class="btn btn-sub btn-warning has-tooltip"data-toggle="modal" data-target="#ver" data-placement="left" title="Mostrar Marcas"> <i class="fas fa-shopping-bag"></i> </button>
    <button type="button" class="btn btn-sub btn-dark has-tooltip"data-toggle="modal" data-target="#Ayuda" data-placement="left" title="Ayuda"><i class="fas fa-question"></i></button>
  </div>
</div>
<script>
$(function() {
  $('.btn-group-fab').on('click', '.btn', function() {
    $('.btn-group-fab').toggleClass('active');
  });
  $('has-tooltip').tooltip();
});
</script>

</center>
<!-- Modal de Agregar Proveedores-->
<div class="modal fade bd-example-modal-lg" id="crear" tabindex="-1" role="dialog"aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog  modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Agregar Proveedor</h5>
        <button type="button" class="close"style="  color: #FFFFFF;" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div role="tabpanel">
                  <!-- Nav tabs -->
                  <div class="nav nav-tabs " role="tablist">
                      <a class=" nav-item nav-link active" href="#uploadTab" aria-controls="nav-uploadTab" role="tab" data-toggle="tab">Datos Proveedores <i class="fas fa-users" style="color:#344478;"></i></a></li>
                        <a class="nav-item nav-link" href="#datoscredito" aria-controls="nav-datoscredito" role="tab" data-toggle="tab">Datos Credito y Promociones <i class="fas fa-sort-numeric-up"style="color:#344478;"></i></a>
                  </div>
      <div class="tab-content"  id="nav-tabContent">
      <div role="tabpanel" class="tab-pane fade show active" aria-labelledby="nav-upload-tab"id="uploadTab">
        <br/>  <div class="row">
      <div class="col-md-12 col-md-offset-3">
        <div class="panel panel-login">
          <div class="panel-body">
            <div class="row">
              <div class="col-lg-12">


    <form  action="{{route('CrearPro')}}" method="POST" role="form"id="registro" class="form-group" >
        <input type="hidden" name="id" id="id">
      <input type="hidden" name="usuario" id="usuario" value="{{Auth::user()->id}}">
  {{csrf_field()}}
      @include('Proveedores.crearProveedor')

      </div>
    </div>
  </div>

</div>
</div>
</div>
</div><div role="tabpanel" class="tab-pane fade "aria-labelledby="nav-datoscredito-tab" id="datoscredito">
<br/>
        <div class="row">
          <div class="col-md-5">
        <label for="Credito">Dias de credito </label>
        <input type="text" name="tipocredito" id="tipocredito" tabindex="1" class="form-control" placeholder="Dias de credito" width="20%" required>

        </div>
        <div class="col-md-5">
      <label for="Descuento">Descuento de Proveedor % </label>
      <input type="number" name="Descuento" id="Descuento" tabindex="1" class="form-control" placeholder="%" width="20%" max="100" required>

      </div>
    </div><br/></br></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger"data-dismiss="modal" data-toggle="tooltip" data-placement="top" title="Cancelar"><i class="fas fa-eye-slash"></i></button>
          <button type="submit" id="agregarproveedor" tabindex="4"class="btn btn-primary" value=""data-toggle="tooltip" data-placement="top" title="Agregar"><i class="far fa-check-square fa-lg"></i></button>
  </form>
      </div>

</div>
</div>
</div></div></div>
<!--Modal para crear Proveedor-->


<!--Modal de modificar Proveeedores-->
<div class="modal fade bd-example-modal-lg" id="edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header" >
<h5 class="modal-title" id="exampleModalLabel">Modificar Proveedor</h5>
        <button type="button" class="close"style="  color: #FFFFFF;" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

            <div role="tabpanel">
        <div class="nav nav-tabs " role="tablist">
            <a role="presentation" class="nav-item nav-link active" href="#modificar" aria-controls="modificar" role="tab" data-toggle="tab">Datos Proveedores <i class="fas fa-users" style="color:#344478;"></i></a>
              <a role="presentation" class="nav-item nav-link" href="#datocredito" aria-controls="datoscredito" role="tab" data-toggle="tab">Datos Credito y Promociones <i class="fas fa-sort-numeric-up"style="color:#344478;"></i></a>
            </div>
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="modificar">
        <div class="row">
      <div class="col-md-12 col-md-offset-3">
        <div class="row">
          <div class="col-lg-12">
        <form id="idFormulario" action="{{route('modificarPro','nada')}}" method="post" class="form-group">
            {{csrf_field()}}
            <br/>
              <input type="hidden" name="id" id="id">
                <input type="hidden" name="usuario" id="usuario" value="{{Auth::user()->id}}">
    @include('Proveedores.Modificar')
  </div></div></div></div>
</div>
<div role="tabpanel" class="tab-pane " id="datocredito">
  <div class="row">
    <div class="col-md-5">
  <label for="Credito">Dias de credito </label>
  <input type="number" name="diascredito" id="diascredito" tabindex="1" class="form-control" placeholder="Dias de credito" width="20%" required>
  </div>
  <div class="col-md-5">
<label for="Descuento">Descuento de Proveedor % </label>
<input type="number" name="promociones" id="promociones" tabindex="1" class="form-control" placeholder="%" width="20%" min="0" max="100" required>
</div>
</div><br/></br></div>

    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-danger"data-dismiss="modal"data-toggle="tooltip" data-placement="top" title="Cancelar"><i class="fas fa-eye-slash"></i></button>
        <button  type="submit"class="btn btn-primary" value=""data-toggle="tooltip" data-placement="top"title="Modificar"name="editar" id="editar"><i class="fas fa-edit"></i></button>
    </div>
    </form>
  </div>
</div>
</div>
</div></div>
<!-- Ajax para modificar proveedor-- >


<!--MODAL PARA VER DETALLES-->

<div class="modal fade" id="detalles" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Detalles del Proveedor</h5>
        <button type="button" class="close"style="  color: #FFFFFF;" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <div role="tabpanel">
    <div class="nav nav-tabs " role="tablist">
        <a role="presentation" class="nav-item nav-link active "href="#verProveedor" aria-controls="verProveedor" role="tab" data-toggle="tab">Datos Proveedores <i class="fas fa-users" style="color:#344478;"></i></a>
          <a role="presentation" class="nav-item nav-link"href="#verdatoscredito" aria-controls="verdatoscredito" role="tab" data-toggle="tab">Datos Credito y Promociones <i class="fas fa-sort-numeric-up"style="color:#344478;"></i></a>

    </div>
    <div class="tab-content">
      <div role="tabpanel" class="tab-pane active" id="verProveedor">
        <br/>
            @include('Proveedores.visiondetallada')
            <br/>
    <div class="row">
  <div class="col-md-12 col-md-offset-3">
    <div class="row">
      <div class="col-lg-12">
  </div></div></div></div>
  </div>
  <div role="tabpanel" class="tab-pane " id="verdatoscredito"><br/>
  <div class="row">
  <div class="col-md-5">
  <label for="Credito">Dias de credito </label>
  <input type="number" name="diascredito" id="diascredito" tabindex="1" class="form-control" placeholder="Dias de credito" width="20%" readonly style="background-color: #fff;">

  </div>
  <div class="col-md-5">
  <label for="Descuento">Descuento de Proveedor % </label>
  <input type="number" name="promociones" id="promociones" tabindex="1" class="form-control" placeholder="%" width="20%" max="100" readonly style="background-color: #fff;">
<br/>
  </div>
  </div><br/></br></div>

  </div>
  </div>
  <div class="modal-footer">
  <button type="button" class="btn btn-danger"data-dismiss="modal"data-toggle="tooltip" data-placement="top" title="Cerrar detalles"><i class="fas fa-eye-slash"></i></button>


  </div>
  </div>
  </div>
  </div>
  </div></div>


<!--MODAL PARA VER Marcas-->
<div class="modal fade bd-example-modal-lg" id="ver" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Marcas</h5>
        <button type="button" class="close" style="  color: #FFFFFF;"data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
<center><h3>Nuestra variedad de Marcas</h3></center><br/>
        <form id="marcaform" action="" method="post"role="form" class="form-group">

            <div class="row">
              <div class="col-md-4">
            <label for="Marca">Marca</label>
                <input type="hidden" name="usuario" id="usuario" value="{{Auth::user()->id}}">
            <input type="text" name="marca" id="tipocredito" tabindex="1" class="form-control" placeholder="Nombre de la marca" width="20%"  min="0" max="200"required>
</div>  <div class="col-md-5">
<label for="Proveedor">Proveedores</label>
<select name="proveedores" id="proveedores" class="form-control" maxlength="300px" required>
    @foreach($post as $marcas)
    <option value="{{$marcas->idProveedor}}">
 {{$marcas->proveedor}}
    </option>

    @endforeach
  </select>  </div>
      <div class="col-md-3">
        <br/><label></label>
  <button type="submit" class="btn btn-info" style="margin-top:10px;" width="100px" id="agregarmarca" name="agregarmarca" value="Agregar Marca"data-toggle="tooltip" data-placement="top" title="Agregar Marca"><i class="fas fa-plus"></i></button></div></div>
</form>
<hr style="border: 1;
    height: 0;
    border-top: 1px solid rgba(0, 0, 0, 0.1);
    border-bottom: 1px solid rgba(255, 255, 255, 0.3);">
        <label for="Proveedor"style="margin-left:170px;">Panel de Busqueda</label>  <i class="fas fa-search"></i>
       <input class="form-control mr-sm-5 col-md-6"id="searchmarca" style="margin-left:150px;"type="search" placeholder="Buscar" name="buscador"aria-label="Search"required>  <br/>
  </br>{{ $marca->links() }}
        <table  id="example" class="table" >
        <thead >
        <tr>
        <th scope="col">Id</th>
        <th scope="col">Marca</th>
        <th scope="col">Proveedor</th>
        </tr>
        </thead>
        <tbody class="buscar cuerpo" id="cuerpo">
        <tr>
          @foreach($marca as $marca)
          <tr>
          <td>{!!$marca->idMarca!!}</td>
          <td>{!!$marca->marca!!}</th>
              <td>{!!$marca->proveedor!!}</th>
        @endforeach
      </tr>
    </tbody>

  </table>


      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal"data-toggle="tooltip" data-placement="top" title="Cerrar detalles"><i class="fas fa-eye-slash"></i></button>

      </div>
    </div>
  </div>
</div>
</div>
<!--Falta modificarlo para que funcione bien-->


<div class="modal fade bd-example-modal-lg" id="Ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Ayuda Proveedores </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <input type="hidden" name="id" id="id">
      <h2>Bienvenido {{Auth::user()->name}} </h2>
        <br/>
      @if(Auth::user()->id_rol!=5)
       <button type="button" class="btn btn-success"><i class="fas fa-eye" style="height:4px;"></i></button><strong>  Ver Detalles   </strong> <br/>

          En esta opcion el usuario podra encontrar informacion detallada de cada registro de los proveedores<br/>
          La informacion que muestra los campos son :
          <ul>
            <li>Nombre Proveedor</li>
              <li>Clave del Proveedor</li>
                <li>Datos de contacto</li>
                  <li>Dias de credito</li>
                    <li>Promociones</li>
          </ul>
@endif
  @if(Auth::user()->id_rol==2 || Auth::user()->id_rol==1)
          <button type="button" class="btn btn-primary"><i class="fas fa-edit"></i></button><strong> Modificar Proveedor </strong><br/>
En esta opcion el usuario podra modificar los datos de los proveedores antes mencionados.<br/>
     <button type="button" class="btn btn-danger"><i class="fas fa-trash-alt"></i></button><strong> Eliminar Proveedor </strong><br/>
          En esta opcion el usuario podra eliminar el proveedor
          <br/>
          <button type="button" class="btn btn-sub btn-warning has-tooltip" data-placement="left" title="Mostrar Marcas"> <i class="fas fa-shopping-bag"></i></i> </button>
          <strong>Panel de Marcas </strong><br/>
          En esta opción el usuario podrá ver las marcas con las que se cuenta y asi mismo podrá agregar nuevas.
<br/>@endif
<button class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="buscar"><i class="fas fa-search"></i></button>
<strong>Buscar  </strong><br/>En esta opción el usuario podrá buscar en tiempo real los proveedores.<br/>
  @if(Auth::user()->id_rol==2 || Auth::user()->id_rol==1)
        <button type="button" class="btn btn-sub btn-info has-tooltip" data-placement="left" title="Agregar Proveedor"> <i class="fas fa-plus"></i></i> </button>
         <strong>Agregar Proveedor </strong><br/>
         En esta opción el usuario podrá agregar un nuevo proveedor al sistema.
        <br/>

      <strong>  Atención: </strong> para poder realizar un registro es necesario llenar todos los campos.<br/>
   @endif

      </div>
      <div class="modal-footer">

        <button type="button" class="btn btn-danger"data-dismiss="modal"data-toggle="tooltip" data-placement="top" title="Cerrar Ayuda"><i class="fas fa-eye-slash"></i></button>
      </div>
    </div>
  </div>
</div>
<!--Modal de eliminar Proveedores-->



<div class="modal fade" id="eliminar" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Eliminar Producto</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <input type="hidden" name="id" id="id">
        <p>¿Seguro que deseas Eliminar el producto?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-danger">Eliminar</button>
      </div>
    </div>
  </div>
</div>

</hr>
</br><br/>
</div>

  <script>
    $(document).ready(function () {
        (function ($) {
            $('#search').keyup(function () {

                 var rex = new RegExp($(this).val(), 'i');

                 if(rex=='/(?:)/i'){
                   $('#mitabla2').hide();
                   $('#mitabla').show();
                   $('#paginacion').show();
                 }else{
                   $('#mitabla2').show();
                   $('#mitabla').hide();
                 $('#paginacion').hide();}
                 $('.busca tr').hide();
                 $('.busca tr').filter(function () {
                   return rex.test($(this).text());
                 }).show();
            })

        }(jQuery));

    });
  </script>
  <script>
  $('th').click(function() {
  var table = $(this).parents('table').eq(0)
  var rows = table.find('tr:gt(0)').toArray().sort(comparer($(this).index()))
  this.asc = !this.asc
  if (!this.asc) {
     rows = rows.reverse()
  }
  for (var i = 0; i < rows.length; i++) {
     table.append(rows[i])
  }
  setIcon($(this), this.asc);

});
function comparer(index) {
  return function(a, b) {
     var valA = getCellValue(a, index),
     valB = getCellValue(b, index)
     return $.isNumeric(valA) && $.isNumeric(valB) ? valA - valB : valA.localeCompare(valB)
  }
}
function getCellValue(row, index) {
  return $(row).children('td').eq(index).html()
}
function setIcon(element, asc) {
  $("th").each(function(index) {
     $(this).removeClass("sorting");
     $(this).removeClass("asc");
     $(this).removeClass("desc");
  });
  element.addClass("sorting");
  if (asc) element.addClass("asc");
  else element.addClass("desc");
}
  $(document).ready(function(){
    let table = document.getElementById('mitabla2');
   let table2=document.getElementById('mitabla');
    $("#ocultarId").click(function(){

      for (var i = 0, row; row = table.rows[i]; i++){
         row.cells[0].classList.toggle('hidden');
      }
      for (var i = 0, row; row = table2.rows[i]; i++){
         row.cells[0].classList.toggle('hidden');
      }
    });
        $("#ocultar").click(function(){
          for (var i = 0, row; row = table.rows[i]; i++){
             row.cells[1].classList.toggle('hidden');
          }
          for (var i = 0, row; row = table2.rows[i]; i++){
             row.cells[1].classList.toggle('hidden');
          }
        });

        $("#ocultarP").click(function(){
          for (var i = 0, row; row = table.rows[i]; i++){
             row.cells[2].classList.toggle('hidden');
          }
          for (var i = 0, row; row = table2.rows[i]; i++){
             row.cells[2].classList.toggle('hidden');
          }
        });
        $("#oculltarCorreo").click(function(){
          for (var i = 0, row; row = table.rows[i]; i++){
             row.cells[3].classList.toggle('hidden');
          }
          for (var i = 0, row; row = table2.rows[i]; i++){
             row.cells[3].classList.toggle('hidden');
          }
        });
        $("#ocultardiascredito").click(function(){
          for (var i = 0, row; row = table.rows[i]; i++){
             row.cells[4].classList.toggle('hidden');
          }
          for (var i = 0, row; row = table2.rows[i]; i++){
             row.cells[4].classList.toggle('hidden');
          }
        });
        $("#oculltarTelefono").click(function(){
          for (var i = 0, row; row = table.rows[i]; i++){
             row.cells[6].classList.toggle('hidden');
          }
          for (var i = 0, row; row = table2.rows[i]; i++){
             row.cells[6].classList.toggle('hidden');
          }
        });
        $("#ocultarPromos").click(function(){
          for (var i = 0, row; row = table.rows[i]; i++){
             row.cells[5].classList.toggle('hidden');
          }
          for (var i = 0, row; row = table2.rows[i]; i++){
             row.cells[5].classList.toggle('hidden');
          }
        });
        $("#oculltarRFC").click(function(){
          for (var i = 0, row; row = table.rows[i]; i++){
             row.cells[7].classList.toggle('hidden');
          }
          for (var i = 0, row; row = table2.rows[i]; i++){
             row.cells[7].classList.toggle('hidden');
          }
        });
        $("#oculltarcalle").click(function(){
          for (var i = 0, row; row = table.rows[i]; i++){
             row.cells[9].classList.toggle('hidden');

          }
          for (var i = 0, row; row = table2.rows[i]; i++){
             row.cells[9].classList.toggle('hidden');
          }
        });
        $("#oculltarcolonia").click(function(){
          for (var i = 0, row; row = table.rows[i]; i++){
             row.cells[8].classList.toggle('hidden');
          }
          for (var i = 0, row; row = table2.rows[i]; i++){
             row.cells[8].classList.toggle('hidden');
          }
        });
  });
  </script>

  <script>
      $('#detalles').on('show.bs.modal', function(event) {
          var button = $(event.relatedTarget) // Button that triggered the modal
          var clave = button.data('clave')
          var id = button.data('id')
          var proveedor = button.data('proveedor')
          var rfc = button.data('rfc')
          var telefono = button.data('telefono')
          var colonia = button.data('colonia')
          var correo = button.data('correo')
          var promociones = button.data('promociones')
          var diascredito=button.data('diascredito')
          var pais = button.data('pais')
          var estado = button.data('estado')
          var calle = button.data('calle')
          var noexterior = button.data('noexterior')
          var modal = $(this)
          modal.find('.modal-body #id').val(id)
         modal.find('.modal-body #Clave').val(clave)
          modal.find('.modal-body #proveedor').val(proveedor)
          modal.find('.modal-body #rfc').val(rfc)
          modal.find('.modal-body #telefono').val(telefono)
          modal.find('.modal-body #colonia').val(colonia)
            modal.find('.modal-body #calle').val(calle)
          modal.find('.modal-body #promociones').val(promociones)
            modal.find('.modal-body #diascredito').val(diascredito)
          modal.find('.modal-body #correo').val(correo)
          modal.find('.modal-body #Estado').val(estado)
          modal.find('.modal-body #pais').val(pais)
          modal.find('.modal-body #calle').val(calle)
          modal.find('.modal-body #noExterior').val(noexterior)
      })

  </script>
  <script>
      $('#edit').on('show.bs.modal', function(event) {
          var button = $(event.relatedTarget) // Button that triggered the modal
          var clave = button.data('clave')
          var id = button.data('id')
          var proveedor = button.data('proveedor')
          var rfc = button.data('rfc')
          var telefono = button.data('telefono')
          var colonia = button.data('colonia')
          var correo = button.data('correo')
          //var ubicacion = button.data('ubicacion')
          var promociones = button.data('promociones')
          var diascredito=button.data('diascredito')
          var pais = button.data('pais')
          var estado = button.data('estado')
          var calle = button.data('calle')
          var noexterior = button.data('noexterior')
          var modal = $(this)
          modal.find('.modal-body #id').val(id)
         modal.find('.modal-body #Clave').val(clave)
          modal.find('.modal-body #proveedor').val(proveedor)
          modal.find('.modal-body #rfc').val(rfc)
          modal.find('.modal-body #telefono').val(telefono)
          modal.find('.modal-body #colonia').val(colonia)
            modal.find('.modal-body #calle').val(calle)
          // modal.find('.modal-body #Ubicacion').val(ubicacion)
          modal.find('.modal-body #promociones').val(promociones)
            modal.find('.modal-body #diascredito').val(diascredito)
          modal.find('.modal-body #correo').val(correo)
          modal.find('.modal-body #Estado').val(estado)
          modal.find('.modal-body #pais').val(pais)
          modal.find('.modal-body #calle').val(calle)
          modal.find('.modal-body #noExterior').val(noexterior)
      })

  </script>
  <!--SCRIPT PARA REALIZAR BUSQUEDA EN TIEMPO REAL EN LA TABLA-->




  <script type="text/javascript">
    $("body").on("click","#agregarmarca",function(event){
      event.preventDefault();

      var frm=$("#marcaform");
      var datos = frm.serialize();
      $.ajaxSetup({
           headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
           }
       });
       $.ajax({
        type:'POST',
        url:"/Crearmar/",
        data:datos,
      success:function(data){
              alert(data);
            },
            error:function(x,xs,xt){
              alert("Error al agregar")
            }
      });
      $.ajax({
      type : 'get',
      url : '/vermarca',
      success:function(data){
        $('#cuerpo').on('show.bs.modal').html(data);
  },
    error:function(x,xs,xt,data){
      alert("Error al registrar la marca")
      }
    });
    });
  </script>

  <script>
  $(document).ready(function () {
      (function ($) {
          $('#searchmarca').keyup(function () {

               var rex = new RegExp($(this).val(), 'i');
               if(rex=='/(?:)/i'){
              //   $('#paginacion').show();
               }else{
              // $('#paginacion').hide();
            }
               $('.buscar tr').hide();
               $('.buscar tr').filter(function () {
                 return rex.test($(this).text());
               }).show();
          })

      }(jQuery));

  });
  </script>
  <script>
  $("body").on("submit","#agregarproveedor",function(event){
    event.preventDefault();
    var frm=$("#registro");
    var datos = frm.serialize();
    $.ajaxSetup({
         headers: {
           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
     });
     $.ajax({
      type:'POST',
      url:'/CrearProveedor/',
      data:datos,
    success:function(data){
            alert(data);
          },
          error:function(x,xs,xt){
            alert("Error al agregar el proveedor");
          }
    });
    $.ajax({
    type : 'get',
    url : '/lproveedor',
    success:function(data){
    $('body').html(data);
    }
    });

  });
  </script>

  <!-- script en el cual se hace el filtrado por categoria seleccionada-->
  <script type="text/javascript">
    $(document).ready(function() {
      var table = $('#example').DataTable();

      $('#example tbody').on( 'click', 'tr', function () {
          if ( $(this).hasClass('selected') ) {
              $(this).removeClass('selected');
          }
          else {
              table.$('tr.selected').removeClass('selected');
              $(this).addClass('selected');
          }
      } );

      $('#button').click( function () {
          table.row('.selected').remove().draw( false );
      } );
  } );
  </script>


</center>
</div>
</div>
<!-- /#page-content-wrapper -->
<!-- /#wrapper -->

@endsection
