<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>SMGC | Panel de administración de usuarios</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet"
  href="{{ asset('assets/css/bootstrap.min.css') }}" />  <link rel="stylesheet" href="{{ asset('assets/css/simple-sidebar.css') }}" />
  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <script src="https://kit.fontawesome.com/da3b6a12fa.js"></script>

  <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
  <script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
afasd
  <script src="https://code.highcharts.com/highcharts.js"></script>
  <script src="https://code.highcharts.com/modules/data.js"></script>
  <script src="https://code.highcharts.com/modules/drilldown.js"></script>

  <!-- Custom styles for this template -->
  <link href="css/simple-sidebar.css" rel="stylesheet">
  <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
  <link rel="icon" href="/favicon.ico" type="image/x-icon">

</head>
  <style>
hello:hover{
    background-color:  #fff;
    width: 100%;
    height: 100%;
  }
  </style>
<body>

  <div class="d-flex" id="wrapper">

    <!-- Sidebar -->

    <div class=" border-right" id="sidebar-wrapper" style="  background-color:  #489bee;" >

      <div class="sidebar-heading bg-white"><a href=""><img src="{{ asset('assets/Images/logo.jpg') }}" style="max-height: 60px;"></a></div>
      <div class="list-group list-group-flush" id="sidebar-list">


<div class="dropdown">
        <a href="" class=" list-group-item list-group-item-action bg-transparent" id="list-element" onclick="myFunctiond"><i class="fas fa-dolly"></i> Inventarios</a>
        <ul id="drop" class="dropdown-content">
          <a class="list-group-item list-group-item-action bg-transparent" id="list-element" hidden><i class="fas fa-user fa-1x"></i> Ver inventario</a>
              <a class="list-group-item list-group-item-action bg-transparent" id="list-element"hidden ><i class="fas fa-user fa-1x"></i> Modificar inventario</a>

</ul></div>
<script>
function myFunctiond(){
  document.getElementById("list-element").classList.toggle("show");
}
</script>

        <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-user-shield"></i>Usuarios</a>
        <ul>
        <a class="list-group-item list-group-item-action bg-transparent" id="list-element" hidden><i class="fas fas fa-user-shield"></i> Modificar roles</a></ul>
		<a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-truck"></i> Pedidos</a>
        <ul>

        <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element"hidden><i class="fas fa-user-tie fa-1x"></i> Editar Pedido</a>
      <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element" hidden><i class="fas fa-user-tie fa-1x"></i> Ver Pedidos</a>
          <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element"hidden><i class="fas fa-user-tie fa-1x"></i> Generar Cancelacion</a></ul>
          <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-box"></i> Productos</a>
          <ul>

          <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element"hidden><i class="far fa-scanner"></i> Agregar Producto</a>
          <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element" hidden><i class="fas fa-user-tie fa-1x"></i>Modificar Producto</a>
          <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element"hidden><i class="fas fa-user-tie fa-1x"></i>Eliminar Producto</a></ul>
          <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-chart-bar fa-1x"></i> Reportes</a>
          <ul>
          <a class="list-group-item list-group-item-action bg-transparent" id="list-element"hidden><i class="fas fa-user fa-1x"></i>Reporte inventarios</a>
          </ul>

      <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-file-alt"></i> Marbete</a>
      <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-user  fa-1"></i> Perfil</a>
      <a href="" class="list-group-item list-group-item-action bg-transparent" id="list-element"><i class="fas fa-sign-out-alt fa-1x"></i> Cerrar Sesión</a>
      </div>
    </div>

    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">

      <nav class="navbar navbar-expand-lg navbar-light bg-white border-bottom">
        <button class="btn btn-white" id="menu-toggle"><i class="fas fa-bars fa-1x"></i></button>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <h4 class="text-transparent">  SGMC</h4>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
            <li class="nav-item active">
              <a class="nav-link" href=" "><i class="fas fa-user-tie fa-1x"></i> Administrador</a>
            </li>
          </ul>
        </div>
      </nav>

      <div class="container-fluid">
        <nav class="navbar navbar-light bg-light">
        <form class="form-inline">
          <input class="form-control" type="search" placeholder="Buscar" aria-label="Search">
          <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar</button>
        </form>
        </nav>

  <div class="container" >
    <h2>Crear-usuario</h2>
    <h5>Proporcione los siguientes datos para crear un nuevo usuario</h5>
      <div class="row">
      <div class="col-md-6 col-md-offset-6">
        <div class="panel panel-login">
          <div class="panel-heading">

            <hr>
          </div>
          <div class="panel-body">
            <div class="row">
              <div class="col-lg-12">

                <form id="register-form" action=" " method="post" role="form">

                  <div class="form-group">

                    <input type="textarea" name="aPaternoUser" id="aPaternoUser"  class="form-control" placeholder="Apellido paterno" maxlength="50px" value=""><br/>
                    <input type="textarea" name="aMaternoUser" id="aMaternoUser"  class="form-control" placeholder="Apellido materno" maxlength="50px" value=""><br/>
                    <input type="textarea" name="nombreUser" id="nombreUser"  class="form-control" placeholder="Nombre(s)" maxlength="50px" value=""><br/>
                    <input type="textarea" name="telefonoUser" id="telefonoUser"  class="form-control" placeholder="Teléfono" maxlength="10px" value=""><br/>
                    <input type="textarea" name="correoUser" id="corrreoUser"  class="form-control" placeholder="Correo electrónico" maxlength="300px" value=""><br/>
                    <input type="textarea" name="calleUser" id="calleUser"  class="form-control" placeholder="Calle" maxlength="50px" value=""><br/>
                    <input type="textarea" name="numeroCasaUser" id="numeroCasaUser"  class="form-control" placeholder="Número de casa" maxlength="3px" value=""><br/>
                    <input type="textarea" name="coloniaUser" id="coloniaUser"  class="form-control" placeholder="Colonia" maxlength="20px" value=""><br/>
                    <input type="textarea" name="municipioUser" id="municipioUser"  class="form-control" placeholder="Municipio" maxlength="20px" value=""><br/>
                    <input type="password" name="contraUser" id="contraUser"  class="form-control" placeholder="Contraseña" minlength="8px" maxlength="20px" value=""><br/>
                    <input type="password" name="recontraUser" id="recontraUser"  class="form-control" placeholder="Comprueba tu contraseña" minlength="8px" maxlength="20px" value=""><br/>

                  </div>

                  <div class="form-group">

                    <select type="" name="Roles" id="Roles" tabindex="" class="form-control" placeholder="Roles" value="">
                      <option selected>Rol</option>
                      <option value ="1">Jefe de almacén de entradas</option>
                      <option value ="2">Jefe de almacén de salidas</option>
                      <option value ="3">Jefe de operaciones</option>
                      <option value ="4">Checador</option></select><br/>

                  </div>



                  <div class="form-group">
                    <div class="row">
                      <div class="col-sm-6 col-sm-offset-3">
                        <input type="submit" name="register-submit" id="register-submit" tabindex="4" class="btn btn-outline-success my-2 my-sm-0" value="                         Crear                      ">
                      </div>
                      <div class="col-sm-offset-3">
                        <input type="submit" name="register-submit" id="register-submit" tabindex="4" class="btn btn-outline-danger my-2 my-sm-0" value= "                      Cancelar                     ">
                      </div>
                    </div>
                  </div>


                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
      </div>
    </div>
    <!-- /#page-content-wrapper -->
  <!-- /#wrapper -->

  <!-- Bootstrap core JavaScript -->
  <script src="/jquery/jquery.min.js"></script>
  <script src="/js/bootstrap.bundle.min.js"></script>

  <!-- Menu Toggle Script -->
  <script>
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
  </script>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>

</body>

</html>
