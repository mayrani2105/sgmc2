@extends('layouts.cabe')
    <!-- /#sidebar-wrapper -->
@section('content')
    <!-- Page Content -->
<style>
.btn-group-fab {
  position: fixed;
  width: 50px;
  height: auto;
  right: 20px; bottom: 20px;
}
.has-search .form-control {
    padding-left: 2.375rem;
}

.has-search .form-control-feedback {
    position: absolute;
    z-index: 2;
    display: block;
    width: 2.375rem;
    height: 2.375rem;
    line-height: 2.375rem;
    text-align: center;
    pointer-events: none;
    color: #aaa;
}
.btn-group-fab div {
  position: relative; width: 100%;
  height: auto;
}
.js-pscroll {
  position: relative;
  overflow: hidden;
}

table tr th {
   cursor: pointer;
   -webkit-user-select: none;
   -moz-user-select: none;
   -ms-user-select: none;
   user-select: none;
}
tr:hover{
  background-color: #f0f1f3;
}

.asc:after {
   content: ' ↑';
}
.desc:after {
   content: " ↓";
}
.btn-group-fab .btn {
  position: absolute;
  bottom: 0;
  border-radius: 50%;
  display: block;
  margin-bottom: 4px;
  width: 40px; height: 40px;
  margin: 4px auto;
}
.btn-group-fab .btn-main {
  width: 50px; height: 50px;
  right: 50%; margin-right: -25px;
  z-index: 9;
}
.btn-group-fab .btn-sub {
  bottom: 0; z-index: 8;
  right: 50%;
  margin-right: -20px;
  -webkit-transition: all 2s;
  transition: all 0.5s;
}

.nav-link,.nav-link-active{
color:#aaaa;

}
.modal-header{
  background-color: #344478;
  color:#ffffff;
}
.nav-tabs{
width: 100$;
}
.close{

}


.mr-auto{
  margin-right: 200px;
}
.hidden{ display: none; }
.btn-group-fab.active .btn-sub:nth-child(2) {
  bottom: 60px;
}
.btn-group-fab.active .btn-sub:nth-child(3) {
  bottom: 110px;
}
.btn-group-fab.active .btn-sub:nth-child(4) {
  bottom: 160px;
}
.btn-group-fab .btn-sub:nth-child(5) {
  bottom: 210px;
}
</style>

<div class="container-fluid">
  <div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <div class="alert alert-{{ $msg }} alert-dismissible fade show" role="alert">{{ Session::get('alert-' . $msg) }}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
          </button>
          </div>
      @endif
    @endforeach
  </div>



    <center>
    </center><br /><br />
	<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #344478">

	  <div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">
		  <li class="nav-item active">
			<a class="nav-link dropdown-toggle" href="#" style="color: #FFFFFF" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<i class="fas fa-sliders-h"></i> Filtros
			</a>
		  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
			<a class="dropdown-item" id="ocultarFolio" href="#">Folio</a>
			<a class="dropdown-item" id="ocultarTipo" href="#">Tipo pedido</a>
      <a class="dropdown-item" id="ocultarOrigen" href="#">Origen del pedido</a>
      <a class="dropdown-item" id="ocultarCliente" href="#">Destino del pedido</a>
      <a class="dropdown-item" id="ocultarEstado" href="#">Estado del pedido</a>
		  </div>
		  </li>
		</ul>
                    <h4 class="display-4" style="color:#fff; margin-right:180px;"> Pedidos</h4>
                    <form class="form-inline my-10 my-lg-0">
                    <div>
                        <div class="form-group has-search">
                            <span class="fa fa-search form-control-feedback" style="color:#344478;"></span>
                            <input type="text" class="form-control" id="search" name="search" type="search" placeholder="Buscar">
                        </div>
                    </div>
                </form>
	  </div>
	</nav>

<table class="table table-striped hidden" id="mitabla2" data-toggle="table">

                	<thead>
                    	<tr>

                            <th>Folio</th>
                            <th>Tipo pedido</th>
                            <th>Origen del pedido</th>
                            <th>Destino del pedido</th>
                            <th>Estado del pedido</th>
							<th class="text-center">Opciones</th>
                        </tr>

				</thead>
				<tbody class="busca">
				@foreach($pedidos as $pedido)
                <tr>

                      <td >{!!$pedido ->folio!!}</td>
                      <td>{!!$pedido ->tipoP!!}</td>
                      <td>{!!$pedido ->origen!!}</td>
                      <td>{!!$pedido ->destino!!}</td>
                      <td>{!!$pedido ->descripcionP!!}</td>

            <td>
                      <?php if($pedido -> descripcionP == "En espera"){?>

                      <button type="button" id="cancelar" name="cancelarReal" class="btn btn-danger" data-target="#modalCancelaReal" data-id="{{$pedido ->idPedido}}" data-peticion="Cancelar" data-tipo="{{$pedido ->tipoP}}" data-cliente="{{$pedido ->destino}}" data-origen="{{$pedido ->origen}}" onclick="#modalCancelaReal" data-toggle="modal"><i class="fas fa-times"></i></button>

                      <button type="button" id="aceptar" name="aceptarReal" class="btn btn-info" data-target="#modalCancelaReal" data-id="{{$pedido ->idPedido}}" data-cliente="{{$pedido ->destino}}" data-origen="{{$pedido ->origen}}" data-peticion="Aceptar" data-tipo="{{$pedido ->tipoP}}" onclick="#modalCancelaReal" data-toggle="modal"><i class="fas fa-check"></i></button>

                      <?php }?>

                      <button type="button" id="detalles" name="detallesReal" onclick="modalPe('{{$pedido ->idPedido}}')" data-id="{{$pedido ->idPedido}}" class="btn btn-success" data-toggle="modal" data-target="#modalDetalles"><i class="fas fa-eye"></i></button>


            </td>
          </tr>
				@endforeach
			  </tbody>
			</table>
       <table  data-toggle="table " id="mitabla" class="table table-striped" name="prueba">
                  <thead>
                      <tr>
                            <th>Folio</th>
                            <th>Tipo pedido</th>
                            <th>Origen del pedido</th>
                            <th>Destino del pedido</th>
                            <th>Estado del pedido</th>
              <th class="text-center">Opciones</th>
                        </tr>
        </thead>
        <tbody>
        @foreach($pedidos2 as $pedido)
                <tr>

                      <td >{!!$pedido ->folio!!}</td>
                      <td>{!!$pedido ->tipoP!!}</td>
                      <td>{!!$pedido ->origen!!}</td>
                      <td>{!!$pedido ->destino!!}</td>
                      <td>{!!$pedido ->descripcionP!!}</td>

            <td>
                      <?php if($pedido -> descripcionP == "En espera"){?>

                      <button type="button" id="cancelar" name="cancelarReal" class="btn btn-danger" data-target="#modalCancelaReal" data-id="{{$pedido ->idPedido}}" data-peticion="Cancelar" data-tipo="{{$pedido ->tipoP}}" data-cliente="{{$pedido ->destino}}" data-origen="{{$pedido ->origen}}" onclick="#modalCancelaReal" data-toggle="modal"><i class="fas fa-times"></i></button>

                      <button type="button" id="aceptar" name="aceptarReal" class="btn btn-info" data-target="#modalCancelaReal" data-id="{{$pedido ->idPedido}}" data-cliente="{{$pedido ->destino}}" data-origen="{{$pedido ->origen}}" data-peticion="Aceptar" data-tipo="{{$pedido ->tipoP}}" onclick="#modalCancelaReal" data-toggle="modal"><i class="fas fa-check"></i></button>
                      <?php }?>

                      <button type="button" id="detalles" name="detallesReal" onclick="modalPe('{{$pedido ->idPedido}}')" data-id="{{$pedido ->idPedido}}" class="btn btn-success" data-toggle="modal" data-target="#modalDetalles"><i class="fas fa-eye"></i></button>


            </td>
          </tr>
        @endforeach
        </tbody>
      </table>
		</div>
      </div>
    </div>
  </div>

<!-- modal detalles-->
<div class="modal" id="modalDetalles" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Detalles del pedido</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <table class="table" name="tablaDetails">
                        <thead class="thead" style="background: #0E1A78; color: #FFFFFF;">
                            <tr>
                                <th scope="col">IdPedido</th>
                                <th scope="col">descripcion</th>
                                <th scope="col">Cantidad</th>
                                <th scope="col">Unidad Base</th>
                                <th scope="col">Costo</th>
                                <th scope="col">Costo Venta</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="far fa-eye-slash"></i></button>

                </div>
            </div>
        </div>
    </div>


<!--Modal operaciones.-->
<form  method="post" action="{{route('ModPedReal.update', 1)}}" onsubmit="return envia(this)">
             {{method_field('PUT')}}
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<!--Modal cancela-->
    <div class="modal" id="modalCancelaReal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Operaciones: </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body">
        <p class="form-inline" >¿Está seguro de
        <input type="text" name="idPeticion" id="idPeticion" class="form-control" size="4" readonly> el pedido
        <input type="text" name="id" id="id" class="form-control" size="1" readonly>
        <input type="text" name="tipoP" id="tipoP" class="form-control" size="5" readonly>
        <input type="text" name="cliente" id="cliente" class="form-control" size="5" readonly>
        <input type="text" name="origen" id="origen" class="form-control" size="5" readonly>
        ?</p>

        </div>

        <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-eye-slash"></i></button>

                <button type="submit" id="cancelar" name="cancelar" class="btn btn-primary" ><i class="far fa-check-square fa-lg"></i></button>
        </div>

    </div>
  </div>
</div>
</form>



<script>
    $(document).ready(function () {
      (function ($) {
          $('#search').keyup(function () {

               var rex = new RegExp($(this).val(), 'i');
               if(rex=='/(?:)/i'){

                 $('#mitabla2').hide();
                 $('#mitabla').show();
                 $('#paginacion').show();
               }else{
                 $('#mitabla2').show();
                 $('#mitabla').hide();
               $('#paginacion').hide();}
               $('.busca tr').hide();
               $('.busca tr').filter(function () {
                 return rex.test($(this).text());
               }).show();
          })

      }(jQuery));

  });

  $('th').click(function() {
   var table = $(this).parents('table').eq(0)
   var rows = table.find('tr:gt(0)').toArray().sort(comparer($(this).index()))
   this.asc = !this.asc
   if (!this.asc) {
      rows = rows.reverse()
   }
   for (var i = 0; i < rows.length; i++) {
      table.append(rows[i])
   }
   setIcon($(this), this.asc);

});
function comparer(index) {
   return function(a, b) {
      var valA = getCellValue(a, index),
      valB = getCellValue(b, index)
      return $.isNumeric(valA) && $.isNumeric(valB) ? valA - valB : valA.localeCompare(valB)
   }
}
function getCellValue(row, index) {
   return $(row).children('td').eq(index).html()
}
function setIcon(element, asc) {
   $("th").each(function(index) {
      $(this).removeClass("sorting");
      $(this).removeClass("asc");
      $(this).removeClass("desc");
   });
   element.addClass("sorting");
   if (asc) element.addClass("asc");
   else element.addClass("desc");
}

 $(document).ready(function(){
  let table = document.getElementById('mitabla');
    let table1 = document.getElementById('mitabla2');

  $("#ocultarFolio").click(function(){
    for (var i = 0, row; row = table.rows[i]; i++){
       row.cells[0].classList.toggle('hidden');
    }
    for (var i = 0, row; row = table1.rows[i]; i++){
       row.cells[0].classList.toggle('hidden');
    }
  });

  $("#ocultarTipo").click(function(){
        for (var i = 0, row; row = table.rows[i]; i++){
           row.cells[1].classList.toggle('hidden');
        }
        for (var i = 0, row; row = table1.rows[i]; i++){
           row.cells[1].classList.toggle('hidden');
        }
      });

 $("#ocultarOrigen").click(function(){
        for (var i = 0, row; row = table.rows[i]; i++){
           row.cells[2].classList.toggle('hidden');
        }
        for (var i = 0, row; row = table1.rows[i]; i++){
           row.cells[2].classList.toggle('hidden');
        }
      });

     $("#ocultarCliente").click(function(){
        for (var i = 0, row; row = table.rows[i]; i++){
           row.cells[3].classList.toggle('hidden');
        }
        for (var i = 0, row; row = table1.rows[i]; i++){
           row.cells[3].classList.toggle('hidden');
        }
      });
     $("#ocultarEstado").click(function(){
        for (var i = 0, row; row = table.rows[i]; i++){
           row.cells[4].classList.toggle('hidden');
        }
        for (var i = 0, row; row = table1.rows[i]; i++){
           row.cells[4].classList.toggle('hidden');
        }
      });
});

      $('#modalCancelaReal').on('show.bs.modal', function(event) {
       var button = $(event.relatedTarget)
      var id = button.data('id')
      var peticion = button.data('peticion')
      var tipoP = button.data('tipo')
      var cliente = button.data('cliente')
      var origen = button.data('origen')
      var modal = $(this)
      modal.find('.modal-body #id').val(id)
      modal.find('.modal-body #idPeticion').val(peticion)
      modal.find('.modal-body #tipoP').val(tipoP)
      modal.find('.modal-body #cliente').val(cliente)
      modal.find('.modal-body #origen').val(origen)
      })
</script>


    <script>
        function modalPe(id) {
            $.ajax({
                url: '/VerDetalles2/' + id,
                type: "GET",
            }).done(function(res) {

                $('table[name="tablaDetails"]').empty();
                $('table[name="tablaDetails"]').append('<thead class="thead" style="background: #0E1A78; color: #FFFFFF;"><tr><th scope="col">Folio Pedido</th><th scope="col">Descripcion</th><th scope="col">Cantidad</th><th scope="col">Unidad Base</th><th scope="col">Costo</th><th scope="col">Costo Venta</th></tr></thead>');




                for ($i = 0; $i <= 10; $i++) {
                    $('table[name="tablaDetails"]').append('<tr><td>' + res.pedidosProductos[$i].idPedido + '</td>      <td>' + res.pedidosProductos[$i].Descripcion + '</td>  <td>' + res.pedidosProductos[$i].Cantidad + '</td>      <td>' + res.pedidosProductos[$i].unidadbase + '</td><td>' + res.pedidosProductos[$i].costo + '</td><td>' + res.pedidosProductos[$i].precioreal + '</td></tr>');

                }
            });
        }

    </script>


@endsection
