<div class="col-lg-12">
  <form  method="post" id="store-form" action="{{route('ModInv.store')}}" onsubmit="return envia(this)">
                  {{method_field('POST')}}

    <input type="hidden" name="_token" value="{{ csrf_token() }}">              
    <input type="hidden" name="id" id="id">
    <input type="hidden" name="usuario" id="usuario" value="{{Auth::user()->id}}">
	<select class="form-control" id="buscador" onchange="verproduct()">
    <option value="">Selecciona un producto</option>
		@foreach($productosGeneral as $d)
		  <option value="{!!$d->id_Producto!!}">{!!$d->descripcion!!}</option>
		@endforeach
	</select> 
		<div class="row">
                      <div class="col-md-3">
                        <label for="idC">ID Producto</label>
                        <input type="text" name="id" id="idC" class="form-control" placeholder="ID Producto" value="" readonly="readonly" style="background-color: white; color:#808B96;"><br/>
                    </div>
                    <div class="col-md-3">
                        <label for="ClaveC">Clave</label>
                        <input type="text" name="Clave" id="ClaveC" class="form-control" placeholder="Clave"  value="" disabled="disabled" style="background-color: white; color:#808B96;"><br/>
                    </div>
                    <div class="col-md-6">
                      <label for="ClavesatC">Clave SAT</label>
                      <input type="text" name="Clavesat" id="ClavesatC" class="form-control" placeholder="Clave Sat"  value="" disabled="disabled" style="background-color: white; color:#808B96;"><br/>
                    </div>
                  </div>
           <div class="row">
           		<div class="col-md-4">
                        <label for="marcaC">Marca</label>
                        <input type="text" name="marca" id="marcaC" class="form-control" placeholder="ID Producto" value="" readonly="readonly" style="background-color: white; color:#808B96;"><br/>
                    </div>
                    <div class="col-md-6">
                        <label for="proveedorC">Proveedor</label>
                        <input type="text" name="proveedor" id="proveedorC" class="form-control" placeholder="Clave"  value="" disabled="disabled" style="background-color: white; color:#808B96;"><br/>
                    </div>
           </div>
 </div>


 <script type="text/javascript">
   function verproduct(){
var cod = document.getElementById("buscador").value;

  $.ajax({
    url: 'ModN/'+ cod,
    type:'get'
  }).done(function(res){
    console.log(res);
    $('#idC').val(res.datos[0].id_Producto);
    $('#idCr').val(res.datos[0].id_Producto);
    $('#ClaveC').val(res.datos[0].clave);
    $('#ClavesatC').val(res.datos[0].sat);
    $('#marcaC').val(res.datos[0].marca);
    $('#proveedorC').val(res.datos[0].proveedor);
  });
}

 </script>
