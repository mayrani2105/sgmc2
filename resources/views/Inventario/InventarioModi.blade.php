@extends('layouts.cabe')
    <!-- /#sidebar-wrapper -->
@section('content')
    <!-- Page Content -->
     @include('layouts.headers.cards')
     <br>

<style>
.btn-group-fab {
  position: fixed;
  width: 50px;
  height: auto;
  right: 20px; bottom: 20px;
}
.btn-group-fab div {
  position: relative; width: 100%;
  height: auto;
}
.js-pscroll {
  position: relative;
  overflow: hidden;
}

table tr th {
   cursor: pointer;
   -webkit-user-select: none;
   -moz-user-select: none;
   -ms-user-select: none;
   user-select: none;
}
tr:hover{
  background-color: #f0f1f3;
}
.has-search .form-control {
    padding-left: 2.375rem;
}

.has-search .form-control-feedback {
    position: absolute;
    z-index: 2;
    display: block;
    width: 2.375rem;
    height: 2.375rem;
    line-height: 2.375rem;
    text-align: center;
    pointer-events: none;
    color: #aaa;
}

.asc:after {
   content: ' ↑';
}
.desc:after {
   content: " ↓";
}
.btn-group-fab .btn {
  position: absolute;
  bottom: 0;
  border-radius: 50%;
  display: block;
  margin-bottom: 4px;
  width: 40px; height: 40px;
  margin: 4px auto;
}
.btn-group-fab .btn-main {
  width: 50px; height: 50px;
  right: 50%; margin-right: -25px;
  z-index: 9;
}
.btn-group-fab .btn-sub {
  bottom: 0; z-index: 8;
  right: 50%;
  margin-right: -20px;
  -webkit-transition: all 2s;
  transition: all 0.5s;
}

.nav-link,.nav-link-active{
color:#aaaa;

}
.modal-header{
  background-color: #344478;
  color:#ffffff;
}
.nav-tabs{
width: 100$;
}
.close{

}


.mr-auto{
  margin-right: 200px;
}
.hidden{ display: none; }
.btn-group-fab.active .btn-sub:nth-child(2) {
  bottom: 60px;
}
.btn-group-fab.active .btn-sub:nth-child(3) {
  bottom: 110px;
}
.btn-group-fab.active .btn-sub:nth-child(4) {
  bottom: 160px;
}
.btn-group-fab .btn-sub:nth-child(5) {
  bottom: 210px;
}
</style>

<div class="container-fluid">
  <div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <div class="alert alert-{{ $msg }} alert-dismissible fade show" role="alert">{{ Session::get('alert-' . $msg) }}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
          </button>
          </div>
      @endif
    @endforeach
  </div>

<div>

<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #344478">

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link dropdown-toggle" href="#" style="color: #FFFFFF" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			<i class="fas fa-sliders-h"></i> Filtros
		</a>
	  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
   		<a class="dropdown-item" id="ocultarId" href="#">Id</a>
         <a class="dropdown-item" id="ocultarNombre" href="#">Nombre</a>
   		<a class="dropdown-item" id="ocultarClave" href="#">Clave</a>
         <a class="dropdown-item" id="ocultarCantidad" href="#">Cantidad</a>
   		<a class="dropdown-item" id="ocultarSat" href="#">Clave SAT</a>
         <a class="dropdown-item" id="ocultarCosto" href="#">Costo</a>
         <a class="dropdown-item" id="ocultarAlmacen" href="#">Almacen</a>
         <a class="dropdown-item" id="ocultarEstado" href="#">Estado</a>
         <a class="dropdown-item" id="ocultarMarca" href="#">Marca</a>
         <a class="dropdown-item" id="ocultarProveedor" href="#">Proveedor</a>
         <a class="dropdown-item" id="ocultarSucursal" href="#">Sucursal</a>

	  </div>
      </li>
    </ul>
  <h4 class="display-4" style="color:#fff; margin-right:60px;">Inventarios</h4>
    <form class="form-inline my-10 my-lg-0" action="{{route('BuscarInve')}}">
	  <div>
      <div class="form-group has-search" style="margin-Top:31px;">
      <span class="fa fa-search form-control-feedback"style="color:#344478;"></span>
      <input type="text" class="form-control"id="search"name="search" type="search" placeholder="Buscar" >
    </div>

	  </div>
    </form>

    @if(Auth::user()->id_rol<=2)<div class="col-md-3">
    <label for="Estado" style="color:#fff;">Sucursal</label>

    <select  id="ejemplo2"name="estado" class="form-control" value="">
    <option value="">Todas las Sucursales</option>
    @foreach($sucursal as $sucu)
    <option value="Sucursal-{{$sucu->descripcionS}}">Sucursal {{$sucu->descripcionS}}</option>
    @endforeach
    </select>
    </div>
    @endif
  </div>
</nav>
<br>
<div>
   @foreach($permiso as $permi)
   @if($permi->ver==1)
<table class="table table-striped hidden" id="mitabla2" data-toggle="table">
<thead>
  <tr>
    <th  class="">Id</th>
    <th  class="" scope="col">Nombre</th>
    <th  class="" scope="col">Clave</th>
    <th  class="" scope="col">Cantidad</th>

    <th class="hidden" scope="col">Clave SAT</th>
    <th  class="hidden" scope="col">Costo</th>
    <th class="hidden" scope="col">Almacen</th>
    <th class="hidden" scope="col">Estado</th>
    <th class="hidden" scope="col">Marca</th>
    <th class="hidden" scope="col">Proveedor</th>
    <th  class="hidden" scope="col">Sucursal</th>
    <th class="text-center" scope = "col" >Opciones</th>
  </tr>
</thead>
<tbody class="busca">
@foreach($producto as $producto)
<tr>
  <td>{!!$producto ->id_Producto!!}</td>
  <td>{!!$producto ->nombre!!}</td>
  <td>{!!$producto ->clave!!}</td>
  <td >
   @php
        $Maximo = $producto->cantMaxima;
        $Minimo = $producto->cantMin;
        $Cantidad = $producto->cantidad;
        $promedio = ($Maximo + $Minimo)/2;
        $promedioM = ($promedio + $Minimo)/2;

        if($Cantidad > $promedio){
        @endphp
          <button class="btn btn-success btn-xs"  disable= "disable" style="border-radius:50%"></button>
          @php
      } else if($Cantidad < $promedio  && $Cantidad > $promedioM){
      @endphp
       <button class="btn btn-warning btn-xs" disable= "disable" style="border-radius:50%"></button>
        @php

      } else {
      @endphp
        <button class="btn btn-danger btn-xs" disable= "disable" style="border-radius:50%"></button>
      @php
      }
  @endphp
  {!!$producto ->cantidad!!}
  </td>
  <td class=" hidden">{!!$producto ->sat!!}</td>
  <td class=" hidden">{!!$producto ->costo!!}</td>
  <td class=" hidden">{!!$producto ->almacen!!}</td>
  <td class=" hidden">{!!$producto ->descripcion!!}</td>
  <td class=" hidden">{!!$producto ->marca!!}</td>
  <td class=" hidden">{!!$producto ->proveedor!!}</td>
  <td class="hidden">Sucursal-{{$producto->descripcionS}}</td>
  <td class="text-center" style="padding-left:5px;">
      <a class="btn btn-success" id="ver" name="ver" onclick="modalGen('{{$producto->id_Producto}}')" data-id="{{$producto->id_Producto}}" data-toggle="modal" data-target="#vermodal"><i class="fas fa-eye"  style="color:#FFFFFF;" data-toggle="tooltip" data-placement="top" title="Ver Detalles"></i></a>
@if($permi->editar==1)
      <a class="btn btn-primary" id="editar" name="editar"  onclick ="modalEdit('{{$producto->id_Producto}}')" data-id="{{$producto->id_Producto}}" data-toggle="modal" data-target="#editmodal" data-toggle="tooltip" data-placement="top" title="Modificar"><i class="far fa-edit"  style="color:#FFFFFF;"></i></a>
@endif
@if($permi->borrar==01 )
      <button class="btn btn-danger" id="eliminar" name="eliminar" data-id="{{$producto->id_Producto}}" data-toggle="modal" data-target="#exampleModalCenter"><i class="far fa-trash-alt" style="color:#FFFFFF;" ></i></button>
      <!--<a href="{{route('eliminar',$producto->id_Producto)}}"  class="btn btn-danger" onclick="return confirm('¿Está seguro de eliminar este producto de manera permanente? ' + {!!$producto->id_Producto!!})"data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="far fa-trash-alt" style="color:#FFFFFF;"></i></a>-->
@endif

   </td>
</tr>
   @endforeach

</tbody>
</table>
@endif
@endforeach
<table class="table table-striped" id="mitabla" data-toggle="table">
<thead>
  <tr>
    <th  class="">Id</th>
    <th  class="" scope="col">Nombre</th>
    <th  class="" scope="col">Clave</th>
    <th  class="" scope="col">Cantidad</th>
    <th class="hidden" scope="col">Clave SAT</th>
    <th  class="hidden" scope="col">Costo</th>
    <th class="hidden" scope="col">Almacen</th>
    <th class="hidden" scope="col">Estado</th>
    <th class="hidden" scope="col">Marca</th>
    <th class="hidden" scope="col">Proveedor</th>
    <th  class="hidden" scope="col">Sucursal</th>
    <th class="text-center" scope = "col" >Opciones</th>
  </tr>
</thead>
<tbody >
@foreach($productos as $producto)
<tr>
  <td>{!!$producto ->id_Producto!!}</td>
  <td>{!!$producto ->nombre!!}</td>
  <td>{!!$producto ->clave!!}</td>
  <td >
   @php
        $Maximo = $producto->cantMaxima;
        $Minimo = $producto->cantMin;
        $Cantidad = $producto->cantidad;
        $promedio = ($Maximo + $Minimo)/2;
        $promedioM = ($promedio + $Minimo)/2;

        if($Cantidad > $promedio){
        @endphp
          <button class="btn btn-success btn-xs"  disable= "disable" style="border-radius:50%"></button>
          @php
      } else if($Cantidad < $promedio  && $Cantidad > $promedioM){
      @endphp
       <button class="btn btn-warning btn-xs" disable= "disable" style="border-radius:50%"></button>
        @php

      } else {
      @endphp
        <button class="btn btn-danger btn-xs" disable= "disable" style="border-radius:50%"></button>
      @php
      }
  @endphp
  {!!$producto ->cantidad!!}
  </td>
  <td class=" hidden">{!!$producto ->sat!!}</td>
  <td class=" hidden">{!!$producto ->costo!!}</td>
  <td class=" hidden">{!!$producto ->almacen!!}</td>
  <td class=" hidden">{!!$producto ->descripcion!!}</td>
  <td class=" hidden">{!!$producto ->marca!!}</td>
  <td class=" hidden">{!!$producto ->proveedor!!}</td>
  <td class="hidden"> Sucursal-{{$producto->descripcionS}}</td>
  <td class="text-center" style="padding-left:5px;">
     @foreach($permiso as $permi)
      <a class="btn btn-success" id="ver" name="ver" onclick="modalGen('{{$producto->id_Producto}}')" data-id="{{$producto->id_Producto}}" data-toggle="modal" data-target="#vermodal"><i class="fas fa-eye"  style="color:#FFFFFF;" data-toggle="tooltip" data-placement="top" title="Ver Detalles"></i></a>
@if($permi->editar==1)
      <a class="btn btn-primary" id="editar" name="editar"  onclick ="modalEdit('{{$producto->id_Producto}}')" data-id="{{$producto->id_Producto}}" data-toggle="modal" data-target="#editmodal" data-toggle="tooltip" data-placement="top" title="Modificar"><i class="far fa-edit"  style="color:#FFFFFF;"></i></a>
@endif
      <button class="btn btn-danger" id="eliminar" name="eliminar" data-id="{{$producto->id_Producto}}" data-toggle="modal" data-target="#exampleModalCenter"><i class="far fa-trash-alt" style="color:#FFFFFF;" ></i></button>
@if($permi->borrar==01 )
@endif
@endforeach

      <!--<a href="{{route('eliminar',$producto->id_Producto)}}"  class="btn btn-danger" onclick="return confirm('¿Está seguro de eliminar este producto de manera permanente? ' + {!!$producto->id_Producto!!})"data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="far fa-trash-alt" style="color:#FFFFFF;"></i></a>-->
   </td>
</tr>
   @endforeach

</tbody>
</table>
<div class="" id="paginacion">
{{$productos->links()}}</div>

    <!-- /#page-content-wrapper -->
  <!-- /#wrapper -->

<!-- //Modal de Eliminar -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Eliminar Producto</h5>
        <button type="button" class="close" style="color: #FFFFFF;"data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>¿Seguro que deseas Eliminar el producto?</p>
        <form action="{{route('ModInv.destroy', $producto->id_Producto)}}">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <input type="hidden" name="usuario" id="usuario" value="{{Auth::user()->id}}">
          <input type="hidden" name="id" id="id" value="" >
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-eye-slash"></i></button>
        <a type="button" class="btn btn-danger" id="eliminar" data-method="post" method="delete" href="{{route('ModInv.destroy',$producto->id_Producto)}}"></a>
      </div>
    </div>
  </div>
</div>
<!--Termina Modal ELIMINAR -->

<!-- Modal Ver Detalles-->
<div class="modal fade" id="vermodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title">Detalles: </h5>
        <button type="button" class="close" style="color: #FFFFFF;" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    <div class="modal-body">
      <ul class="nav nav-tabs" id="MyTab" role="tablist">
        <li class="nav-item">
          <a class="nav-link active" id="datosinv-tab" data-toggle="tab" href="#datosinv" role="tab" aria-controls="datosinv">Datos Inventario <i class="fas fa-boxes"></i></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="datosgenerales-tab" data-toggle="tab" href="#datosgenerales" role="tab" aria-controls="datosgenerales">Datos generales <i class="fas fa-dolly-flatbed"></i></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="ubicacion-tab" data-toggle="tab" href="#ubicacion" role="tab" aria-controls="ubicacion">Ubicación Producto <i class="fas fa-warehouse"></i></a>
        </li>

      </ul>
      <div class="tab-content" id="myTabContent">
         <div class="tab-pane fade show active" id="datosinv" role="tabpanel" aria-labelledby="datosinv-tab">
           @include('Inventario.ShowModal')
        </div>
          <div class="tab-pane fade" id="datosgenerales" role="tabpanel" aria-labelledby="datosgenerales-tab">
            <div role="tabpanel" class="tab-pane " id="datosgenerales">
                  <div class="row">
                      <div class="col-md-3">
                        <label for="id">ID Producto</label>
                        <input type="text" name="id" id="id" class="form-control" placeholder="ID Producto" disabled="disabled" style="background-color: white; color:#808B96;"><br/>
                    </div>
                    <div class="col-md-3">
                        <label for="Clave">Clave</label>
                        <input type="text" name="Clave" id="Clave" class="form-control" placeholder="Clave"disabled="disabled" style="background-color: white; color:#808B96;" ><br/>
                    </div>
                    <div class="col-md-3">
                      <label for="Clavesat">Clave SAT</label>
                      <input type="text" name="Clavesat" id="Clavesat" class="form-control" placeholder="Clave Sat" disabled="disabled" style="background-color: white; color:#808B96;"><br/>
                    </div>
                  </div>
                  <div class="form-group col-md-9" style="padding-left: 0px;">
                    <label for="productname">Nombre de Producto</label>
                    <input type="text" name="productname" id="productname" class="form-control" placeholder="Nombre del Producto" disabled="disabled" style="background-color: white; color:#808B96;" >
                  </div>
                  <div class="form-group col-md-9"  style="padding-left: 0px;">
                    <label for="estado">Estado de Producto</label>
                    <input type="text" name="estado" id="estado" class="form-control" placeholder="Estado del Producto" disabled="disabled" style="background-color: white; color:#808B96;">
                  </div>
                  <div class="row">
                    <div class="form-group col-md-4">
                      <label for="marca">Marca</label>
                      <input type="text" name="marca" id="marca" class="form-control" placeholder="Marca" disabled="disabled" style="background-color: white; color:#808B96;">
                    </div>
                    <div class="form-group col-md-8">
                      <label for="proveedor">Proveedor</label>
                      <input type="text" name="proveedor" id="proveedor" class="form-control" placeholder="Proveedor" disabled="disabled" style="background-color: white; color:#808B96;">
                    </div>
                  </div>
                  </div>
          </div>
          <div role="tabpanel" class="tab-pane " id="ubicacion">
            <div class="row">
              <div class="form-group col-md-3">
                <label for="almacen">Almacen</label>
                <input type="text" name="almacen" id="almacen" class="form-control" placeholder="Almacen" disabled="disabled" style="background-color: white; color:#808B96;"><br/>
              </div>
              <div class="form-group col-md-3">
                <label for="piso">Piso</label>
                <input type="text" name="piso" id="piso" class="form-control" placeholder="Piso" disabled="disabled" style="background-color: white; color:#808B96;"><br/>
              </div>
            </div>
            <div class="row">
              <div class="col-md-3">
                <label for="pastillo">Pasillo</label>
                <input type="text" name="pasillo" id="pasillo" class="form-control" placeholder="Pasillo" disabled="disabled" style="background-color: white; color:#808B96;"><br/>
              </div>
              <div class="col-md-3">
                <label for="anaquel">Anaquel</label>
                <input type="text" name="anaquel" id="anaquel" class="form-control" placeholder="Anaquel" disabled="disabled" style="background-color: white; color:#808B96;" ><br/>
              </div>
              <div class="col-md-3">
                <label for="Clavesat">Nivel</label>
                <input type="text" name="nivel" id="nivel" class="form-control" placeholder="Nivel" disabled="disabled" style="background-color: white; color:#808B96;"><br/>
                    </div>
              </div>
          </div>
        </div>
        <br>
    </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal" data-toggle="tooltip" data-placement="top" title="Cerrar"><i class="fas fa-eye-slash"></i></button> <!--Botón de cerrar-->
      </div>
    </div>
  </div>
</div>


<form  method="post"  action="{{route('ModInv.update', $producto->id_Producto)}}" onsubmit="return envia(this)">

                  {{method_field('PUT')}}
<!-- //Modal de Editar -->
<div class="modal fade bd-example-modal-lg" id="editmodal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" id="usuario">
    <div class="modal-header">
        <h5 class="modal-title">Editando inventario: </h5>
        <button type="button" class="close" style="color: #FFFFFF;" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    <div class="modal-body">
       <ul class="nav nav-tabs" id="MyTab" role="tablist">
        <li class="nav-item">
          <a class="nav-link active" id="datosinvE-tab" data-toggle="tab" href="#datosinvE" role="tab" aria-controls="datosinvE">Datos Inventario <i class="fas fa-boxes"></i></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="ubicacionE-tab" data-toggle="tab" href="#ubicacionE" role="tab" aria-controls="ubicacionE">Ubicación Producto <i class="fas fa-warehouse"></i></a>
        </li>

      </ul>
      <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade show active" id="datosinvE" role="tabpanel" aria-labelledby="datosinvE-tab">
         @include('Inventario.Editarmodal')
        </div>
   <!-- Ubicación-->
         <div class="tab-pane fade" id="ubicacionE" role="tabpanel" aria-labelledby="ubicacionE-tab">
            <div class="row">
               <div class="col-md-3">
                  <label for="almacen">Almacen</label>
                  <input type="text" name="almacen" id="almacenE" class="form-control" placeholder="Almacen" disabled="disabled" style="background-color: white; color:#808B96;"><br/>
               </div>
               <div class="col-md-3">
                  <label for="piso">Piso</label>
                  <input type="text" name="piso" id="pisoE" class="form-control" placeholder="Piso"  style="background-color: white; color:#808B96;"><br/>
               </div>
            </div>
            <div class="row">
               <div class="col-md-3">
                  <label for="pastillo">Pasillo</label>
                  <input type="text" name="pasillo" id="pasilloE" class="form-control" placeholder="Pasillo" style="background-color: white; color:#808B96;"><br/>
               </div>
               <div class="col-md-3">
                  <label for="anaquel">Anaquel</label>
                  <input type="text" name="anaquel" id="anaquelE" class="form-control" placeholder="Anaquel" style="background-color: white; color:#808B96;" ><br/>
               </div>
               <div class="col-md-3">
                  <label for="Clavesat">Nivel</label>
                  <input type="text" name="nivel" id="nivelE" class="form-control" placeholder="Nivel" style="background-color: white; color:#808B96;"><br/>
               </div>
            </div>
            <div>
               <input type="text" name="id_ubicacion" id="id_ubicacion" hidden="hidden">
            </div>
         </div>
         </form>
         <br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-eye-slash"></i></button> <!--Botón de cerrar-->
        <button type="submit" class="btn btn-primary"  value=""><i class="far fa-save"></i></button>
      </div>
    </div>
  </div>
</div>
</form>
<!--Termina Modal Editar-->


</div>



<!-- Floating Action Button like Google Material -->
<div class="btn-group-fab" role="group" aria-label="FAB Menu">
  <div>
    <button type="button" class="btn btn-main btn-primary has-tooltip" data-placement="left" title="Menu"> <i class="fa fa-bars"></i> </button>
    <button type="button" class="btn btn-sub btn-info has-tooltip" data-toggle="modal" data-target="#crear" data-placement="left" title="Agregar Producto" > <i class="fas fa-plus"></i></button>
    <button type="button" class="btn btn-sub btn-dark has-tooltip" data-toggle="modal" data-target="#ayuda" data-placement="left" title="Ayuda" > <i class="fas fa-question"></i> </button>

  </div>
</div>


<!--Modal de Ayuda-->

<div class="modal fade bd-example-modal-lg" id="ayuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Ayuda Inventario </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <input type="hidden" name="id" id="id">
      <h2>Bienvenido {{Auth::user()->name}} </h2>
        <br/>
      @if(Auth::user()->id_rol!=5)
       <button type="button" class="btn btn-success"><i class="fas fa-eye" style="height:4px;"></i></button><strong>  Ver Detalles</strong> <br/>
          <p>En esta opcion el usuario podra encontrar informacion detallada de cada registro de los productos dentro del inventario<br/>
          La informacion que muestra los campos son :</p>
          <ul>
            <li>Nombre del Producto</li>
            <li>Clave del Producto</li>
            <li>Ubicación del producto dentro del almacén</li>
            <li>Cantidad Actual</li>
            <li>Stocks</li>
            <li>Datos generales del producto</li>
          </ul>
 @endif
  @if(Auth::user()->id_rol==2 || Auth::user()->id_rol==1)
          <button type="button" class="btn btn-primary"><i class="fas fa-edit"></i></button><strong> Modificar Inventario </strong><br/>
          <p>En esta opcion el usuario podra modificar los datos de cada uno de los productos que se encuentran dentro del inventario antes mencionados.</p><br/>
     <button type="button" class="btn btn-danger"><i class="fas fa-trash-alt"></i></button><strong> Eliminar Producto de inventario </strong><br/>
     <p>En esta opcion el usuario podra eliminar el producto del inventario</p> <br/>
<br/>@endif
<button class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="buscar"><i class="fas fa-search"></i></button>
<strong>Buscar  </strong><br/>En esta opción el usuario podrá buscar en tiempo real los productos dentro del inventario.<br/>
  @if(Auth::user()->id_rol==2 || Auth::user()->id_rol==1)
        <button type="button" class="btn btn-sub btn-info has-tooltip" data-placement="left" title="Agregar Proveedor"> <i class="fas fa-plus"></i></i> </button>
         <strong>Agregar Producto a Inventario </strong><br/>
         <p>En esta opción el usuario podrá agregar un nuevo proveedor al sistema.</p> <br/>

      <strong>  Atención: </strong><p>Para poder realizar un registro es necesario llenar todos los campos.</p><br/>
  @endif

      </div>
      <div class="modal-footer">

        <button type="button" class="btn btn-danger"data-dismiss="modal"data-toggle="tooltip" data-placement="top" title="Cerrar Ayuda"><i class="fas fa-eye-slash"></i></button>
      </div>
    </div>
  </div>
</div>
<!--Termina Modal de Ayuda-->

<!--modal para NUEVO producto en inventario-->
<form  method="post" id="store-form" action="{{route('ModInv.store')}}" onsubmit="">

                  {{method_field('POST')}}
<div class="modal fade bd-example-modal-lg" id="crear" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title">Agregar a Inventario: </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    <div class="modal-body">
       <ul class="nav nav-tabs" id="MyTab" role="tablist">
         <li class="nav-item">
          <a class="nav-link active" id="productoC-tab" data-toggle="tab" href="#productoC" role="tab" aria-controls="productoC">Elegir Producto</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="datosinvC-tab" data-toggle="tab" href="#datosinvC" role="tab" aria-controls="datosinvC">Datos Inventario <i class="fas fa-boxes"></i></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="ubicacionC-tab" data-toggle="tab" href="#ubicacionC" role="tab" aria-controls="ubicacionC">Ubicación Producto <i class="fas fa-warehouse"></i></a>
        </li>

      </ul>
      <div class="tab-content" id="myTabContent">
         <div class="tab-pane fade show active" id="productoC" role="tabpanel" aria-labelledby="productoC-tab">
         @include('Inventario.ListaProductos')
         </div>
        <div class="tab-pane fade" id="datosinvC" role="tabpanel" aria-labelledby="datosinvC-tab">
         @include('Inventario.Crearmodal')
        </div>
   <!-- Ubicación-->
            <div class="tab-pane fade" id="ubicacionC" role="tabpanel" aria-labelledby="ubicacionC-tab">
               <div class="row">
                  <div class="col-md-4">
                    <label for="sucursal">Sucursal </label>
                    <select name="sucursal" id="sucursalC" class="form-control">
                      <option>Selecciona una sucursal</option>
                      @foreach($sucursales as $suc)
                      <option value="{!!$suc->id!!}">Sucursal {!!$suc->clave!!}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="col-md-3">
                     <label for="piso">Piso</label>
                     <input type="text" name="piso" id="pisoC" class="form-control" placeholder="Piso"  style="background-color: white; color:#808B96;" required="true"><br/>
                  </div>
               </div>
               <div class="row">
                  <div class="col-md-3">
                     <label for="pastillo">Pasillo</label>
                     <input type="text" name="pasillo" id="pasilloC" class="form-control" placeholder="Pasillo" style="background-color: white; color:#808B96;" required="true"><br/>
                  </div>
                  <div class="col-md-3">
                     <label for="anaquel">Anaquel</label>
                     <input type="text" name="anaquel" id="anaquelC" class="form-control" placeholder="Anaquel" style="background-color: white; color:#808B96;" required="true"><br/>
                  </div>
                  <div class="col-md-3">
                     <label for="Clavesat">Nivel</label>
                     <input type="text" name="nivel" id="nivelC" class="form-control" placeholder="Nivel" style="background-color: white; color:#808B96;" required="true"><br/>
                  </div>
               </div>
            </div>
         </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-eye-slash"></i></button> <!--Botón de cerrar-->
        <button type="submit" class="btn btn-primary " value="">  <i class="far fa-check-square fa-lg"></i>  </button>
      </div>
    </div>
  </div>
</div>
</form>
<!--Termina modal para nuevo producto en inventario-->

<script>
  $(document).ready(function () {
      (function ($) {
          $('#search').keyup(function () {

               var rex = new RegExp($(this).val(), 'i');
               if(rex=='/(?:)/i'){

                 $('#mitabla2').hide();
                 $('#mitabla').show();
                 $('#paginacion').show();
               }else{
                 $('#mitabla2').show();
                 $('#mitabla').hide();
               $('#paginacion').hide();}
               $('.busca tr').hide();
               $('.busca tr').filter(function () {
                 return rex.test($(this).text());
               }).show();
          })

      }(jQuery));

  });
</script>
<script>
$(document).ready(function(){
  (function ($) {
  $("#ejemplo2").change(function(){

     var rex = new RegExp($('select[id=ejemplo2]').val(), 'i');
if(rex=='/(?:)/i'){
  $('#mitabla2').hide();
  $('#mitabla').show();
  $('#paginacion').show();
}else{
  $('#mitabla2').show();
  $('#mitabla').hide();
$('#paginacion').hide();}
              $('.busca tr').hide();
              $('.busca tr').filter(function () {
                return rex.test($(this).text());
              }).show();
         })

     }(jQuery));
    });
</script>
<script type="text/javascript">
   $('th').click(function() {
   var table = $(this).parents('table').eq(0)
   var rows = table.find('tr:gt(0)').toArray().sort(comparer($(this).index()))
   this.asc = !this.asc
   if (!this.asc) {
      rows = rows.reverse()
   }
   for (var i = 0; i < rows.length; i++) {
      table.append(rows[i])
   }
   setIcon($(this), this.asc);

});
function comparer(index) {
   return function(a, b) {
      var valA = getCellValue(a, index),
      valB = getCellValue(b, index)
      return $.isNumeric(valA) && $.isNumeric(valB) ? valA - valB : valA.localeCompare(valB)
   }
}
function getCellValue(row, index) {
   return $(row).children('td').eq(index).html()
}
function setIcon(element, asc) {
   $("th").each(function(index) {
      $(this).removeClass("sorting");
      $(this).removeClass("asc");
      $(this).removeClass("desc");
   });
   element.addClass("sorting");
   if (asc) element.addClass("asc");
   else element.addClass("desc");
}

//script en el cual se hace el filtrado por categoria seleccionada

$(document).ready(function(){
  let table = document.getElementById('mitabla');
    let table1 = document.getElementById('mitabla2');

  $("#ocultarId").click(function(){

    for (var i = 0, row; row = table.rows[i]; i++){
       row.cells[0].classList.toggle('hidden');
    }
    for (var i = 0, row; row = table1.rows[i]; i++){
       row.cells[0].classList.toggle('hidden');
    }
  });
      $("#ocultarNombre").click(function(){
        for (var i = 0, row; row = table.rows[i]; i++){
           row.cells[1].classList.toggle('hidden');
        }
        for (var i = 0, row; row = table1.rows[i]; i++){
           row.cells[1].classList.toggle('hidden');
        }
      });

      $("#ocultarClave").click(function(){
        for (var i = 0, row; row = table.rows[i]; i++){
           row.cells[2].classList.toggle('hidden');
        }
        for (var i = 0, row; row = table1.rows[i]; i++){
           row.cells[2].classList.toggle('hidden');
        }
      });
      $("#ocultarCantidad").click(function(){
        for (var i = 0, row; row = table.rows[i]; i++){
           row.cells[3].classList.toggle('hidden');
        }
        for (var i = 0, row; row = table1.rows[i]; i++){
           row.cells[3].classList.toggle('hidden');
        }
      });
      $("#ocultarSat").click(function(){
        for (var i = 0, row; row = table.rows[i]; i++){
           row.cells[4].classList.toggle('hidden');
        }
        for (var i = 0, row; row = table1.rows[i]; i++){
           row.cells[4].classList.toggle('hidden');
        }
      });
      $("#ocultarAlmacen").click(function(){
        for (var i = 0, row; row = table.rows[i]; i++){
           row.cells[6].classList.toggle('hidden');
        }
        for (var i = 0, row; row = table1.rows[i]; i++){
           row.cells[6].classList.toggle('hidden');
        }
      });
      $("#ocultarCosto").click(function(){
        for (var i = 0, row; row = table.rows[i]; i++){
           row.cells[5].classList.toggle('hidden');
        }
        for (var i = 0, row; row = table1.rows[i]; i++){
           row.cells[5].classList.toggle('hidden');
        }
      });
      $("#ocultarEstado").click(function(){
        for (var i = 0, row; row = table.rows[i]; i++){
           row.cells[7].classList.toggle('hidden');
        }
        for (var i = 0, row; row = table1.rows[i]; i++){
           row.cells[7].classList.toggle('hidden');
        }
      });
      $("#ocultarProveedor").click(function(){
        for (var i = 0, row; row = table.rows[i]; i++){
           row.cells[9].classList.toggle('hidden');

        }
        for (var i = 0, row; row = table1.rows[i]; i++){
           row.cells[9].classList.toggle('hidden');
        }
      });
      $("#ocultarMarca").click(function(){
        for (var i = 0, row; row = table.rows[i]; i++){
           row.cells[8].classList.toggle('hidden');
        }
        for (var i = 0, row; row = table1.rows[i]; i++){
           row.cells[8].classList.toggle('hidden');
        }
      });
      $("#ocultarSucursal").click(function(){
        for (var i = 0, row; row = table.rows[i]; i++){
           row.cells[10].classList.toggle('hidden');
        }
        for (var i = 0, row; row = table1.rows[i]; i++){
           row.cells[10].classList.toggle('hidden');
        }
      });
});


$('#search1').on('keyup',function(){
   $value=$(this).val();
   if($value==""){
     $.ajax({
        type : 'get',
        url : '/ModificarInv',
        success:function(data){
            $('body').html(data);
        }
     });
   }else{
      $.ajax({
         type : 'get',
         url : '/searchI',
         data:{'search':$value},
         beforeSend: function(){
            console.log($value);
         } ,
         success:function(data){
            $('tbody').html(data);
         },error:function(x,xs,xt){
            alert("Error "+xt);
        }
      });
   }
});

</script>

<script type="text/javascript">
$.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
</script>


<script>
//Script para BUSCAR
$(document).ready(function () {
    (function ($) {
        $('#search1').keyup(function () {
             var rex = new RegExp($(this).val(), 'i');
             if(rex=='/(?:)/i'){
               $('#paginacion').show();
             }else{
             $('#paginacion').hide();}
             $('.busca tr').hide();
             $('.busca tr').filter(function () {
               return rex.test($(this).text());
             }).show();
        })

    }(jQuery));

});

$(document).ready(function() {
  $('.btn-group-fab').on('click', '.btn', function() {
    $('.btn-group-fab').toggleClass('active');
  });

});

//Modal para VER
function modalGen(id){
    $.ajax({
      url:'ModModificar/'+ id,
      type:'get'
    }).done(function (res){
      $('#id').val(res.datos[0].id_Producto);
      $('#Clave').val(res.datos[0].clave);
      $('#Clavesat').val(res.datos[0].sat);
      $('#CantidadT').val(res.datos[0].cantidad);
      $('#productname').val(res.datos[0].nombre);
      $('#estado').val(res.datos[0].descripcion);
      $('#cantidadMax').val(res.datos[0].cantMaxima);
      $('#cantidadMin').val(res.datos[0].cantMin);
      $('#marca').val(res.datos[0].marca);
      $('#proveedor').val(res.datos[0].proveedor);
      $('#entrada').val(res.datos[0].entrada);
      $('#salida').val(res.datos[0].salida);
      $('#almacen').val(res.datos[0].almacen);
      $('#piso').val(res.datos[0].piso);
      $('#anaquel').val(res.datos[0].anaquel);
      $('#nivel').val(res.datos[0].nivel)
      $('#pasillo').val(res.datos[0].pasillo);
      $('#CantidadD').val(res.datos[0].disponible);
      $('#CantidadB').val(res.datos[0].bloqueado);
    });
  }

//modal para EDITAR
function modalEdit(id){
    $.ajax({
      url:'ModInv/'+ id + '/edit',
      type:'get'
    }).done(function (res){
      $('#idE').val(res.datos[0].id_Producto);
      $('#ClaveE').val(res.datos[0].clave);
      $('#ClavesatE').val(res.datos[0].sat);
      $('#CantidadET').val(res.datos[0].cantidad);
      $('#productnameE').val(res.datos[0].nombre);
      $('#estadoE').val(res.datos[0].descripcion);
      $('#cantidadMaxE').val(res.datos[0].cantMaxima);
      $('#cantidadMinE').val(res.datos[0].cantMin);
      $('#almacenE').val(res.datos[0].almacen);
      $('#pisoE').val(res.datos[0].piso);
      $('#anaquelE').val(res.datos[0].anaquel);
      $('#nivelE').val(res.datos[0].nivel)
      $('#pasilloE').val(res.datos[0].pasillo);
      $('#id_ubicacion').val(res.datos[0].id_ubicacion);
      $('#CantidadED').val(res.datos[0].disponible);
      $('#CantidadEB').val(res.datos[0].bloqueado);
    });
  }
  $(document).ready(function() {
    var table = $('#example').DataTable();

    $('#example tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    } );

    $('#button').click( function () {
        table.row('.selected').remove().draw( false );
    } );
  } );



function envia(obj){
  if((parseInt(document.getElementById("cantidadMinE").value,10)<parseInt(document.getElementById("cantidadMaxE").value,10)) && !isNaN(parseInt(document.getElementById("cantidadMinE").value,10)) && !isNaN(parseInt(document.getElementById("cantidadMaxE").value,10))){
       return true;
    }else{
      alert("El Stock mínimo es mayor que el Stock Máximo");
      return false;
     }
 }

$('#exampleModalCenter').on('show.bs.modal', function(event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var id = button.data('id')
    var modal = $(this)
    modal.find('.modal-body #id').val(id)
});


</script>


@endsection
