     <div class="col-lg-12">
                <form class="form-horizontal" id="store-form"  method="post" role="form" action="{{route('ModInv.store')}}">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  {{method_field('POST')}}
                    <div class="row">
                      <input type="hidden" name="id" id="idCr" class="form-control">
                      <div class="form-group col-md-3">
                        <label for="cantidad">Cantidad</label>
                        <input type="number" name="Cantidad" id="CantidadC" class="form-control" placeholder="Cantidad"  value="" style="background-color: white; color:#808B96;" required="required">
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6 ">
                        <label for="cantidadMax">Stock Máximo</label>
                        <input type="text" name="cantidadMax" id="cantidadMaxC" class="form-control" placeholder="Stock Máximo"  value="" required="required">
                      </div>
                      <div class="col-md-6 ">
                        <label for="cantidadMin">Stock Mínimo</label>
                        <input type="text" name="cantidadMin" id="cantidadMinC" class="form-control" placeholder="Stock Mínimo"  value="" required="required">
                      </div>
                    </div>
      </div>
