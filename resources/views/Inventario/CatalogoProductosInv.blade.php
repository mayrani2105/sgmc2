@extends('layouts.cabe')
    <!-- /#sidebar-wrapper -->
@section('content')
    <!-- Page Content -->





  <table class="table table-sm">
  <thead>
    <tr>
      <th scope="col">Id</th>
      <th scope="col">Nombre</th>
      <th scope="col">Clave</th>
      <th scope="col">Marca</th>
      <th scope="col">Proveedor</th>
      <th scope="col">Costo</th>
      <th scope="col">Cantidad</th>
      <th scope="col">Estado</th>
    </tr>
  </thead>
  <tbody>
    @foreach($productos as $producto)
    <tr>
      <th>{!!$producto ->id_Producto!!}</th>
      <td>{!!$producto ->nombre!!}</th>
      <td>{!!$producto ->clave!!}</td>
      <td>{!!$producto ->marca!!}</td>
      <td>{!!$producto ->proveedor!!}</td>
      <td>{!!$producto ->costo!!}</td>
      <td>{!!$producto ->cantidad!!}</td>
      <td>{!!$producto ->descripcion!!}</td>
    </tr>
       @endforeach

  </tbody>
</table>
</div>
      </div>
    </div>
    <!-- /#page-content-wrapper -->
  <!-- /#wrapper -->


@endsection
