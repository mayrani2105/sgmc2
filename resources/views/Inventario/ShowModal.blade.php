<div class="col-md-12">
          <div class="panel-body">
              <div>
                <form class="form-horizontal" id="edit-form"  method="post" role="form" action="/ModInv">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <div role="tabpanel" class="tab-pane " id="datosinv">
                  <div class="row">
                    <div class="form-group col-md-3">
                      <label for="entradas">Entradas</label>
                      <input type="number" name="entrada" id="entrada" class="form-control" placeholder="Entradas" disabled="disabled" style="background-color: white; color:#808B96;">
                    </div>
                    <div class="form-group col-md-3">
                      <label for="salidas">Salidas</label>
                      <input type="number" name="salida" id="salida" class="form-control" placeholder="Salidas" disabled="disabled" style="background-color: white; color:#808B96;">
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-md-3">
                      <label for="cantidad">Cantidad Total</label>
                      <input type="text" name="CantidadT" id="CantidadT" class="form-control" placeholder="Cantidad"  value="" disabled="disabled" style="background-color: white; color:#808B96;">
                    </div>
                    <div class="form-group col-md-3">
                      <label for="cantidad">Cantidad Bloqueada</label>
                      <input type="text" name="Cantidad" id="CantidadB" class="form-control" placeholder="Cantidad"  value="" disabled="disabled" style="background-color: white; color:#808B96;">
                    </div>
                    <div class="form-group col-md-3">
                      <label for="cantidad">Cantidad Disponible</label>
                      <input type="text" name="Cantidad" id="CantidadD" class="form-control" placeholder="Cantidad"  value="" disabled="disabled" style="background-color: white; color:#808B96;">
                    </div>
                  </div>
                  <div class="row">
                  <div class="col-md-6 ">
                    <label for="cantidadMax">Stock Máximo</label>
                    <input type="text" name="cantidadMax" id="cantidadMax" class="form-control" placeholder="Stock Máximo" disabled="disabled" style="background-color: white; color:#808B96;">
                  </div>

                  <div class="col-md-6 ">
                    <label for="cantidadMin">Stock Mínimo</label>
                    <input type="text" name="cantidadMin" id="cantidadMin" class="form-control" placeholder="Stock Mínimo" disabled="disabled" style="background-color: white; color:#808B96;">
                  </div>
                  </div>
                </div>
                </form>
              </div>
      </div>
    </div>
      