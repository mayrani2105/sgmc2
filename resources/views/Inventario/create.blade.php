<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>

 <table class="table table-sm">
  <thead>
    <tr>
      <th scope="col">Id</th>
      <th scope="col">Nombre</th>
      <th scope="col">Clave</th>
      <th scope="col">Marca</th>
      <th scope="col">Proveedor</th>
      <th scope="col">Costo</th>
      <th scope="col">Cantidad</th>
      <th scope="col">Estado</th>
      <th scope = "col" style="padding: 0;">Opciones </th>
    </tr>
  </thead>
  <tbody>
    @foreach($datos as $producto)
    <tr>
      <th>{!!$producto ->id_Producto!!}</th>
      <td>{!!$producto ->nombre!!}</th>
      <td>{!!$producto ->clave!!}</td>
      <td>{!!$producto ->marca!!}</td>
      <td>{!!$producto ->proveedor!!}</td>
      <td>{!!$producto ->costo!!}</td>
      <td>{!!$producto ->cantidad!!}</td>
      <td>{!!$producto ->descripcion!!}</td>
      <td style="padding: 0;">
            <a href="{{route('modificar',$producto->id_Producto)}}"><input type="submit" name="register-submit" id="register-submit" tabindex="4" class="btn btn-outline-success my-1 my-sm-0" value="Modificar"></a>

            <a href="{{route('eliminar',$producto->id_Producto)}}"><input type="submit" name="register-submit" id="register-submit" tabindex="4" class="btn btn-outline-danger my-1 my-sm-0 openBtn" value="Eliminar"></a>
       </td>
    </tr>
       @endforeach

  </tbody>
</table>
</body>
</html>
