              <div class="col-lg-12">
                <form class="form-horizontal" id="edit-form"  method="post" role="form" action="{{route('ModInv.update', $producto->id_Producto)}}" onsubmit="return envia(this)">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  {{method_field('PUT')}}
                  <!--Datos Generales-->
                  <input type="hidden" name="id" id="id">
                  <input type="hidden" name="usuario" id="usuario" value="{{Auth::user()->id}}">
                  <div class="row">
                      <div class="col-md-3">
                        <label for="id">ID Producto</label>
                        <input type="text" name="id" id="idE" class="form-control" placeholder="ID Producto" value="" readonly="readonly" style="background-color: white; color:#808B96;"><br/>
                    </div>
                    <div class="col-md-3">
                        <label for="Clave">Clave</label>
                        <input type="text" name="Clave" id="ClaveE" class="form-control" placeholder="Clave"  value="" disabled="disabled" style="background-color: white; color:#808B96;"><br/>
                    </div>
                    <div class="col-md-6">
                      <label for="Clavesat">Clave SAT</label>
                      <input type="text" name="Clavesat" id="ClavesatE" class="form-control" placeholder="Clave Sat"  value="" disabled="disabled" style="background-color: white; color:#808B96;"><br/>
                    </div>
                  </div>
                   <div class="form-group">
                    <label for="productname">Nombre de Producto</label>
                    <input type="text" name="productname" id="productnameE" class="form-control" placeholder="Nombre del Producto"  value="" disabled="disabled" style="background-color: white; color:#808B96;">
                  </div>

                  <!--Inventario -->
                    <div class="row">
                      <div class="form-group col-md-3">
                        <label for="entradasE">Entradas</label>
                        <input type="number" name="entrada" id="entradaE" class="form-control" placeholder="Entradas">
                      </div>
                      <div class="form-group col-md-3">
                        <label for="salidasE">Salidas</label>
                        <input type="number" name="salida" id="salidaE" class="form-control" placeholder="Salidas">
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group col-md-3">
                        <label for="cantidad">Cantidad Total</label>
                        <input type="number" name="CantidadT" id="CantidadET" class="form-control" placeholder="Cantidad" readonly="readonly" value="" style="background-color: white; color:#808B96;">
                      </div>
                      <div class="form-group col-md-3">
                        <label for="cantidad">Cantidad Bloqueada</label>
                        <input type="number" name="CantidadB" id="CantidadEB" class="form-control" placeholder="Cantidad" readonly="readonly" value="" style="background-color: white; color:#808B96;">
                      </div>
                      <div class="form-group col-md-3">
                        <label for="cantidad">Cantidad Disponible</label>
                        <input type="number" name="CantidadD" id="CantidadED" class="form-control" placeholder="Cantidad" readonly="readonly" value="" style="background-color: white; color:#808B96;">
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6 ">
                        <label for="cantidadMax">Stock Máximo</label>
                        <input type="text" name="cantidadMax" id="cantidadMaxE" class="form-control" placeholder="Stock Máximo"  value="">
                      </div>
                      <div class="col-md-6 ">
                        <label for="cantidadMin">Stock Mínimo</label>
                        <input type="text" name="cantidadMin" id="cantidadMinE" class="form-control" placeholder="Stock Mínimo"  value="">
                      </div>
                    </div>
                
              </div>

<!--Para Bloqear los inputs de E/S de productos-->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#salidaE').val('');

                $('#entradaE').change(function () {
                    var value = $(this).val();
                    if (value.length != 0) {
                        $('#salidaE').val('');
                        $('#salidaE').attr("disabled", true);
                    }
                    else
                        $('#salidaE').removeAttr("disabled");
                });

                $('#salidaE').change(function () {
                    var value = $(this).val();
                    if (value.length != 0) {
                        $('#entradaE').val('');
                        $('#entradaE').attr("disabled", true);
                    }
                    else
                        $('#entradaE').removeAttr("disabled");
                });
            });

            

        </script>
      