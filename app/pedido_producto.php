<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pedido_producto extends Model
{
      public $timestamps = false;
      protected $table = 'pedido_producto';
}
