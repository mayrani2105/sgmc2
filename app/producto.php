<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class producto extends Model
{
  protected $table = 'producto';
    public $timestamps = false;
    protected $fillable = ['descripcion','unidadbase','costo','precioreal','clave','idMarca','idsat','idSubCategoria','idEstado'];

}
