<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class bitacora extends Model
{
    protected $table = 'bitacora';
    protected $dateFormat = 'Y-m-d H:i:s';
    
}
