<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class inventario extends Model
{
    protected $table = 'inventario';
    protected $primaryKey = 'id_Producto';
    protected $dateFormat = 'Y-m-d H:i:s';
    public $timestamps = false;
    protected $fillable = ['cantidad', 'cantMaxima','cantMin', 'estadoinventario', 'entrada', 'salida', 'idAlmacen', 'id_ubicacion', 'id_Producto', 'bloqueado', 'disponible'];


}
