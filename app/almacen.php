<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class almacen extends Model
{
    protected $table = 'almacen';
    protected $fillable = ['descripcion', 'calle', 'colonia', 'id_sucursal', 'id_Estados'];
    public $timestamps = false;
}
