<?php

namespace App\Http\Controllers;
use App\bitacora;
use App\rol;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
class BitacoraController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }
public function index(){
  $value =$sucursal=Db::table('sucursal')->select('*')->where('id_sucursal','=',Auth::user()->id_sucursal)->get();

  foreach ($value as $key) {
    $value="sucursal ".$key->descripcionS;
  }
  $notificacion=DB::table('notificacions')
  ->join('tipo_notificacion','tipo_notificacion.idtipo_n','=','notificacions.idtipo_n')
  ->select('tipo_notificacion.descripcion as des','notificacions.origen','notificacions.idtipo_n','notificacions.destino','notificacions.created_at as tiempo')->where('notificacions.destino','=',$value)->orwhere('notificacions.origen','=',$value)->orderBy('notificacions.created_at')->get();

$id=Auth::user()->id_rol;
if ($id<=2){
  $sucursal=DB::table('sucursal')->get();
  $bitacora=DB::table('bitacora')
  ->join('users','users.id','=','bitacora.id_usuario')
  ->join('sucursal','users.id_sucursal','=','sucursal.id_sucursal')
  ->join('tipomovimiento','tipomovimiento.id_movimiento','=','bitacora.id_movimiento')
  ->select('bitacora.id_bitacora','sucursal.descripcionS','bitacora.modulo','tipomovimiento.id_movimiento','bitacora.created_at','tipomovimiento.descripcion','users.name','bitacora.datomodificado')->get();
return view('Reportes.bitacora',['bitacora'=>$bitacora,'sucursal'=>$sucursal,'notificacion'=>$notificacion]);
}else{
   return redirect()->back()->with('alert-danger','No tiene accceso a esto');
}

}
public function busquedafecha(Request $request){
  $sucursal=DB::table('sucursal')->get();
  $bitacora=DB::table('bitacora')
  ->join('users','users.id','=','bitacora.id_usuario')
  ->join('tipomovimiento','tipomovimiento.id_movimiento','=','bitacora.id_movimiento')
  ->join('sucursal','users.id_sucursal','=','sucursal.id_sucursal')
  ->select('bitacora.id_bitacora','sucursal.descripcionS','bitacora.modulo','tipomovimiento.id_movimiento','bitacora.created_at','tipomovimiento.descripcion','users.name','bitacora.datomodificado')->whereBetween('bitacora.created_at',array($request->inicio,$request->fin))
  ->get();

  $value =$sucursal=Db::table('sucursal')->select('*')->where('id_sucursal','=',Auth::user()->id_sucursal)->get();

  foreach ($value as $key) {
    $value="sucursal ".$key->descripcionS;
  }
  $notificacion=DB::table('notificacions')
  ->join('tipo_notificacion','tipo_notificacion.idtipo_n','=','notificacions.idtipo_n')
  ->select('tipo_notificacion.descripcion as des','notificacions.origen','notificacions.idtipo_n','notificacions.destino','notificacions.created_at as tiempo')->where('notificacions.destino','=',$value)->orwhere('notificacions.origen','=',$value)->orderBy('notificacions.created_at')->get();

return view('Reportes.bitacora',['bitacora'=>$bitacora,'sucursal'=>$sucursal,'notificacion'=>$notificacion]);
}

}
