<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\clavesat;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
class Controlador_Sat extends Controller
{
  public function __construct()
    {
        $this->middleware('auth');
    }
    public function verSat()
    {
      $id=Auth::user()->id;
      if($id<=3 || $id==6){
    	$res = clavesat::all();
      $value =$sucursal=Db::table('sucursal')->select('*')->where('id_sucursal','=',Auth::user()->id_sucursal)->get();

      foreach ($value as $key) {
        $value="sucursal ".$key->descripcionS;
      }
      $notificacion=DB::table('notificacions')
      ->join('tipo_notificacion','tipo_notificacion.idtipo_n','=','notificacions.idtipo_n')
      ->select('tipo_notificacion.descripcion as des','notificacions.origen','notificacions.idtipo_n','notificacions.destino','notificacions.created_at as tiempo')->where('notificacions.destino','=',$value)->orwhere('notificacions.origen','=',$value)->orderBy('notificacions.created_at')->get();
    	return view('sat.versat',compact('res','notificacion'));
    }
    else {
      return redirect()->back()->with('alert-warning','No tiene Acceso');
    }
  }
}
