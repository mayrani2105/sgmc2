<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\proveedor;
use App\marca;
use App\Bitacora;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
class proveedorController extends Controller
{
  public $timestamps = false;
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {

    }
    public function __construct()
    {
        $this->middleware('auth');
    }

  public function index(/*$id*/){
$id=Auth::user()->id;
$permiso=DB::table('funcionesrol')->join('rol','rol.id_rol','=','funcionesrol.id_rol')->join('privilegios','funcionesrol.id_privilegio','=','privilegios.id_privilegio')->select('funcionesrol.ver','funcionesrol.editar','funcionesrol.borrar')->where('funcionesrol.id_rol','=',$id)->where('privilegios.id_privilegio','=',3)->get();
$permisoT=DB::table('privilegio_usuario')->join('users','users.id','=','privilegio_usuario.id_usuario')->join('privilegios','privilegio_usuario.id_privilegio','=','privilegios.id_privilegio')->select('privilegio_usuario.ver','privilegio_usuario.editar','privilegio_usuario.borrar')->where('privilegio_usuario.id_usuario','=',$id)->where('privilegios.id_privilegio','=',3)->get();
$marca=DB::table('proveedor')->join('marca', 'proveedor.idProveedor', '=', 'marca.idProveedor')->select('marca.idMarca','marca.marca','proveedor.proveedor','marca.idProveedor')->orderBy('idMarca')->Paginate(8);
$post = DB::table('proveedor')->select('proveedor.idProveedor','proveedor.claveProveedor','proveedor.proveedor','proveedor.telefono','proveedor.rfc','proveedor.calle','proveedor.colonia','proveedor.promociones','proveedor.diascredito','proveedor.estado','proveedor.correo','proveedor.pais','proveedor.noExterior','proveedor.estadoproveedor'/*,'marca.idMarca','marca.marca'*/)->where('proveedor.estadoproveedor',1)->orderBy('proveedor.idProveedor')->get();
$proveedores = DB::table('proveedor')->select('proveedor.idProveedor','proveedor.claveProveedor','proveedor.proveedor','proveedor.telefono','proveedor.rfc','proveedor.calle','proveedor.colonia','proveedor.promociones','proveedor.diascredito','proveedor.estado','proveedor.correo','proveedor.pais','proveedor.noExterior','proveedor.estadoproveedor')->where('proveedor.estadoproveedor',1)->orderBy('proveedor.idProveedor')->paginate(9);
$value =$sucursal=Db::table('sucursal')->select('*')->where('id_sucursal','=',Auth::user()->id_sucursal)->get();

foreach ($value as $key) {
  $value="sucursal ".$key->descripcionS;
}
$notificacion=DB::table('notificacions')
->join('tipo_notificacion','tipo_notificacion.idtipo_n','=','notificacions.idtipo_n')
->select('tipo_notificacion.descripcion as des','notificacions.origen','notificacions.idtipo_n','notificacions.destino','notificacions.created_at as tiempo')->where('notificacions.destino','=',$value)->orwhere('notificacions.origen','=',$value)->orderBy('notificacions.created_at')->get();
return view('Proveedores.verProveedores',['post'=>$post,'proveedores'=>$proveedores,
'marca'=>$marca,'permiso'=>$permiso,'permisoT'=>$permisoT,'notificacion'=>$notificacion]);}

    protected function validator(array $data)
 {

 }
    // Crea proveedor
    public function crear(Request $request)
{
  if($request->ajax()){
    $producto= new proveedor();
    $producto->idProveedor=0;
    $producto->claveProveedor= $request->input('claveProveedor');
    $producto->estadoproveedor=1;
    $producto->proveedor=$request->input('proveedor');
    $producto->rfc=$request->input('rfc');
    $producto->telefono=$request->input('telefono');
    $producto->calle=$request->input('calle');
    $producto->colonia=$request->input('colonia');
    $producto->noExterior=$request->input('noExterior');
    $producto->pais=$request->input('pais');
    $producto->correo=$request->input('correo');
    $producto->Estado=$request->input('estado');
    $producto->diascredito=$request->input('tipocredito');
    $producto->promociones=$request->input('Descuento');
    $producto->save();
    $operacion=new bitacora();
    $operacion->datomodificado=$request->input('proveedor');
    $operacion->id_usuario=$request->input('usuario');
    $operacion->modulo="proveedores";
    $operacion->id_movimiento=4;
    $operacion->save();
    echo" Agregado con exito";

}


}
// funcion que ayuda a crear una nueva marca
public function crearmarca(Request $request){
  if($request->ajax())
  {

  $consulta=DB::table('marca')->select('marca')->where('marca','=',$request->input('marca'))->count();
  if($consulta==0){
$marca=new Marca();
$marca->marca=$request->input('marca');
$marca->idProveedor=$request->input('proveedores');
$marca->save();
$operacion=new bitacora();
$operacion->datomodificado=$request->input('marca');
$operacion->id_usuario=$request->input('usuario');
$operacion->modulo="proveedores";
$operacion->id_movimiento=4;
$operacion->save();
echo"Agregado con exito";
}else{
  echo"Esta marca ya existe.";
  //return redirect()->back()->with('message',' Esta marca ya existe, Favor de verificar los datos ingresados');
}}
}
public function vermarcas(){
$output="";
    $marca=DB::table('proveedor')->join('marca', 'proveedor.idProveedor', '=', 'marca.idProveedor')->select('marca.idMarca','marca.marca','proveedor.proveedor','marca.idProveedor')->orderBy('idMarca','desc')->Paginate(8);
    foreach ($marca as $key => $product) {
    $output.='<tr>'.
    '<td>'.$product->idMarca.'</td>'.
    '<td>'.$product->marca.'</td>'.
    '<td>'.$product->proveedor.'</td>'.
    '</tr>';}

    return Response($output);

//return response()->json(['marca'=>$marca]);

}

public function destroy(Request $request ,$id)
{

$usuario=Auth::user()->id;
    //Eliminar proveedor que lo que hace es cambiar su estado y ya.
  $id_p=$request->id;
if($usuario<=3){
    $cambio=DB::table('proveedor')->select('proveedor')->where('idProveedor',$id_p)->get();
foreach($cambio as $reporusers){
$valor=$reporusers->proveedor;
}
  DB::table('proveedor')->where('idProveedor',$id_p)->update(['estadoproveedor'=>0]);
    $operacion=new bitacora();
    $operacion->datomodificado=$valor;
    $operacion->id_usuario=$usuario;
    $operacion->modulo="proveedores";
    $operacion->id_movimiento=3;
    $operacion->save();
    return redirect()->back()->with('alert-warning','El proveedor se ha eliminado');
}else{
    return redirect()->back()->with('alert-warning','No tiene acccesso a realizar esta funcion');
}
}
public function update(Request $request, $id)
{

      $id_p=$request->id;
      DB::table('proveedor')->where('idProveedor',$id_p)
          ->update(['claveProveedor'=>$request->Clave,
                  'proveedor'=>$request->proveedor,
                   'rfc'=>$request->rfc,
                   'telefono'=>$request->telefono,
                   'correo'=>$request->correo,
                   'noExterior'=>$request->noExterior,
                   'calle'=>$request->calle,
                   'diascredito'=>$request->diascredito,
                   'promociones'=>$request->promociones,
                   'colonia'=>$request->colonia,
                   'estado'=>$request->Estado,
                   'pais'=>$request->pais]
                 );
                 $operacion=new bitacora();
                 $operacion->datomodificado=$request->input('proveedor');
                 $operacion->id_usuario=$request->input('usuario');
                 $operacion->modulo="proveedores";
                 $operacion->id_movimiento=2;
                 $operacion->save();
  return redirect()->back()->with('alert-warning','Modificado con exito');


    }
}
