<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\inventario;
use App\ubicacion;
use App\bitacora;
use Illuminate\Support\Facades\Auth;
use Illuminate\Pagination\Paginator;

class InventoryController extends Controller
{
	public $timestamps = false;
    //Accion que devuelva una vista
	public function __construct()
    {
        $this->middleware('auth');
    }

	public function listamodificar(){
		 $sucursal=DB::table('sucursal')->get();
		$sucursales = Auth::user()->id_sucursal;
		$id=Auth::user()->id_rol;
		$id=Auth::user()->id;
		$value=Db::table('sucursal')->select('*')->where('id_sucursal','=',Auth::user()->id_sucursal)->get();

		foreach ($value as $key) {
			$value="sucursal ".$key->descripcionS;
		}
		$notificacion=DB::table('notificacions')
		->join('tipo_notificacion','tipo_notificacion.idtipo_n','=','notificacions.idtipo_n')
		->select('tipo_notificacion.descripcion as des','notificacions.origen','notificacions.idtipo_n','notificacions.destino','notificacions.created_at as tiempo')->where('notificacions.destino','=',$value)->orwhere('notificacions.origen','=',$value)->orderBy('notificacions.created_at')->get();
		$permiso=DB::table('funcionesrol')
		->join('rol','rol.id_rol','=','funcionesrol.id_rol')
		->join('privilegios','funcionesrol.id_privilegio','=','privilegios.id_privilegio')
		->select('funcionesrol.ver','funcionesrol.editar','funcionesrol.borrar')
		->where('funcionesrol.id_rol','=',$id)
		->where('privilegios.id_privilegio','=',2)
		->get();

	if($id<=2){
		$productos = DB::table('producto')
			->join('marca', 'marca.idMarca', '=', 'producto.idMarca')
			->join('proveedor', 'proveedor.idProveedor', '=', 'marca.idProveedor')
			->join('inventario', 'inventario.id_Producto', '=', 'producto.id_Producto')
			->join('clavesats', 'clavesats.idsat','=','producto.idsat')
			->join('status', 'status.idEstado', '=', 'producto.idEstado')
			->join('almacen',  'inventario.idAlmacen', '=', 'almacen.idAlmacen')
			->join('sucursal','sucursal.id_sucursal','=', 'almacen.id_sucursal')
			->where('estadoinventario', '=', 1)
			->select('producto.id_Producto','sucursal.descripcionS','producto.descripcion as nombre', 'marca.marca', 'proveedor.proveedor', 'inventario.cantidad', 'clave', 'costo', 'cantMaxima', 'cantMin', 'marca.marca', 'proveedor.proveedor', 'almacen.descripcion as almacen', 'clavesats.clavesat as sat', 'status as descripcion')
			->Paginate(8);

		$producto = DB::table('producto')
			->join('marca', 'marca.idMarca', '=', 'producto.idMarca')
			->join('proveedor', 'proveedor.idProveedor', '=', 'marca.idProveedor')
			->join('inventario', 'inventario.id_Producto', '=', 'producto.id_Producto')
			->join('clavesats', 'clavesats.idsat','=','producto.idsat')
			->join('status', 'status.idEstado', '=', 'producto.idEstado')
			->join('almacen',  'inventario.idAlmacen', '=', 'almacen.idAlmacen')
			->join('sucursal','sucursal.id_sucursal','=', 'almacen.id_sucursal')
			->where('estadoinventario', '=', 1)
			->select('producto.id_Producto','sucursal.descripcionS','producto.descripcion as nombre', 'marca.marca', 'proveedor.proveedor', 'inventario.cantidad', 'clave', 'costo', 'cantMaxima', 'cantMin', 'marca.marca', 'proveedor.proveedor', 'almacen.descripcion as almacen', 'clavesats.clavesat as sat', 'status as descripcion')
			->get();

		 $totproductos = DB::table('inventario')
		->join('almacen',  'inventario.idAlmacen', '=', 'almacen.idAlmacen')
		->join('sucursal','sucursal.id_sucursal','=', 'almacen.id_sucursal')
		->where('estadoinventario', '=', 1)
		->where('sucursal.id_sucursal','=',$sucursales)
		->selectRaw('count(*) as total')
		->get();

		$entradas = DB::table('inventario')
		->join('almacen',  'inventario.idAlmacen', '=', 'almacen.idAlmacen')
		->join('sucursal','sucursal.id_sucursal','=', 'almacen.id_sucursal')
		->where('estadoinventario', '=', 1)
		->where('sucursal.id_sucursal','=',$sucursales)
		->selectRaw('sum(entrada) as entradas')
		->get();

		$Ultimaent = DB::table('inventario')
		->join('almacen',  'inventario.idAlmacen', '=', 'almacen.idAlmacen')
		->join('sucursal','sucursal.id_sucursal','=', 'almacen.id_sucursal')
		->join('bitacora', 'bitacora.datomodificado', '=', 'inventario.id_Producto')
		->where('id_movimiento','=',1)
		->where('modulo', '=', 'inventario')
		->where('estadoinventario', '=', 1)
		->where('sucursal.id_sucursal','=',$sucursales)
		->select('updated_at')->latest('created_at')
		->first();


		$salidas = DB::table('inventario')
		->join('almacen',  'inventario.idAlmacen', '=', 'almacen.idAlmacen')
		->join('sucursal','sucursal.id_sucursal','=', 'almacen.id_sucursal')
		->where('estadoinventario', '=', 1)
		->where('sucursal.id_sucursal','=',$sucursales)
		->selectRaw('sum(salida) as salidas')
		->get();

		$Ultimasal = DB::table('inventario')
		->join('almacen',  'inventario.idAlmacen', '=', 'almacen.idAlmacen')
		->join('sucursal','sucursal.id_sucursal','=', 'almacen.id_sucursal')
		->join('bitacora', 'bitacora.datomodificado', '=', 'inventario.id_Producto')
		->where('id_movimiento','=',5)
		->where('modulo', '=', 'inventario')
		->where('estadoinventario', '=', 1)
		->where('sucursal.id_sucursal','=',$sucursales)
		->select('updated_at')->latest('updated_at')
		->first();

	}else{
		$productos = DB::table('producto')
			->join('marca', 'marca.idMarca', '=', 'producto.idMarca')
			->join('proveedor', 'proveedor.idProveedor', '=', 'marca.idProveedor')
			->join('inventario', 'inventario.id_Producto', '=', 'producto.id_Producto')
			->join('clavesats', 'clavesats.idsat','=','producto.idsat')
			->join('status', 'status.idEstado', '=', 'producto.idEstado')
			->join('almacen',  'inventario.idAlmacen', '=', 'almacen.idAlmacen')
			->join('sucursal','sucursal.id_sucursal','=', 'almacen.id_sucursal')
			->where('estadoinventario', '=', 1)
			->where('sucursal.id_sucursal','=',$sucursales)
			->select('producto.id_Producto','sucursal.descripcionS','producto.descripcion as nombre', 'marca.marca', 'proveedor.proveedor', 'inventario.cantidad', 'clave', 'costo', 'cantMaxima', 'cantMin', 'marca.marca', 'proveedor.proveedor', 'almacen.descripcion as almacen', 'clavesats.clavesat as sat', 'status as descripcion')
			->Paginate(8);
			$totproductos = DB::table('inventario')
		 ->join('almacen',  'inventario.idAlmacen', '=', 'almacen.idAlmacen')
		 ->join('sucursal','sucursal.id_sucursal','=', 'almacen.id_sucursal')
		 ->where('estadoinventario', '=', 1)
		 ->where('sucursal.id_sucursal','=',$sucursales)
		 ->selectRaw('count(*) as total')
		 ->get();

		 $entradas = DB::table('inventario')
		 ->join('almacen',  'inventario.idAlmacen', '=', 'almacen.idAlmacen')
		 ->join('sucursal','sucursal.id_sucursal','=', 'almacen.id_sucursal')
		 ->where('estadoinventario', '=', 1)
		 ->where('sucursal.id_sucursal','=',$sucursales)
		 ->selectRaw('sum(entrada) as entradas')
		 ->get();

		 $Ultimaent = DB::table('inventario')
		 ->join('almacen',  'inventario.idAlmacen', '=', 'almacen.idAlmacen')
		 ->join('sucursal','sucursal.id_sucursal','=', 'almacen.id_sucursal')
		 ->join('bitacora', 'bitacora.datomodificado', '=', 'inventario.id_Producto')
		 ->where('id_movimiento','=',1)
		 ->where('modulo', '=', 'inventario')
		 ->where('estadoinventario', '=', 1)
		 ->where('sucursal.id_sucursal','=',$sucursales)
		 ->select('updated_at')->latest('created_at')
		 ->first();


		 $salidas = DB::table('inventario')
		 ->join('almacen',  'inventario.idAlmacen', '=', 'almacen.idAlmacen')
		 ->join('sucursal','sucursal.id_sucursal','=', 'almacen.id_sucursal')
		 ->where('estadoinventario', '=', 1)
		 ->where('sucursal.id_sucursal','=',$sucursales)
		 ->selectRaw('sum(salida) as salidas')
		 ->get();

		 $Ultimasal = DB::table('inventario')
		 ->join('almacen',  'inventario.idAlmacen', '=', 'almacen.idAlmacen')
		 ->join('sucursal','sucursal.id_sucursal','=', 'almacen.id_sucursal')
		 ->join('bitacora', 'bitacora.datomodificado', '=', 'inventario.id_Producto')
		 ->where('id_movimiento','=',5)
		 ->where('modulo', '=', 'inventario')
		 ->where('estadoinventario', '=', 1)
		 ->where('sucursal.id_sucursal','=',$sucursales)
		 ->select('updated_at')->latest('updated_at')
		 ->first();

		$producto = DB::table('producto')
			->join('marca', 'marca.idMarca', '=', 'producto.idMarca')
			->join('proveedor', 'proveedor.idProveedor', '=', 'marca.idProveedor')
			->join('inventario', 'inventario.id_Producto', '=', 'producto.id_Producto')
			->join('clavesats', 'clavesats.idsat','=','producto.idsat')
			->join('status', 'status.idEstado', '=', 'producto.idEstado')
			->join('almacen',  'inventario.idAlmacen', '=', 'almacen.idAlmacen')
			->join('sucursal','sucursal.id_sucursal','=', 'almacen.id_sucursal')
			->where('estadoinventario', '=', 1)
			->where('sucursal.id_sucursal','=',$sucursales)
			->select('producto.id_Producto','sucursal.descripcionS','producto.descripcion as nombre', 'marca.marca', 'proveedor.proveedor', 'inventario.cantidad', 'clave', 'costo', 'cantMaxima', 'cantMin', 'marca.marca', 'proveedor.proveedor', 'almacen.descripcion as almacen', 'clavesats.clavesat as sat', 'status as descripcion')
			->get();
		}
		$productosGeneral = DB::table('producto')
            ->join('marca', 'marca.idMarca', '=', 'producto.idMarca')
            ->join('clavesats','clavesats.idsat', '=', 'producto.idsat')
            ->join('subcategoria', 'subcategoria.idSubCategoria', '=', 'producto.idSubCategoria')
            ->join('categoria', 'categoria.idCategoria', '=', 'subcategoria.idCategoria')
            ->join('status', 'status.idEstado', '=', 'producto.idEstado')
            ->select('producto.id_Producto','producto.descripcion','producto.unidadbase','producto.costo','producto.precioreal','producto.clave','marca.marca', 'clavesats.clavesat as sat','clavesats.descripcion as descripcionsat'
            ,'categoria.descripcion as categoria','subcategoria.descripcion as subcategoria','status.status','marca.idMarca as idmarca','clavesats.idsat as idsat'
            ,'categoria.idCategoria as idcategoria','subcategoria.idSubCategoria as idsubcategoria','status.idEstado as idstatus')->orderBy('producto.id_Producto')
            ->join('proveedor', 'proveedor.idProveedor', '=', 'marca.idProveedor')
            ->select('producto.id_Producto','producto.descripcion','producto.clave','marca.marca', 'clavesats.clavesat as sat','clavesats.descripcion as descripcionsat','marca.idMarca as idmarca','clavesats.idsat as idsat','proveedor.proveedor')->orderBy('producto.id_Producto')
            ->get();


       	$sucursales = DB::table('sucursal')
    	->select('id_sucursal as id', 'descripcionS as clave')
    	->get();



	return view('Inventario/InventarioModi', ['productos' => $productos, 'producto'=>$producto,'sucursal'=>$sucursal,
	'productosGeneral' => $productosGeneral, 'totalp'=>$totproductos, 'entradas'=>$entradas, 'salidas'=>$salidas, 'ultima'=>$Ultimasal, 'ultimae'=>$Ultimaent, 'permiso'=>$permiso, 'sucursales'=>$sucursales,'notificacion'=>$notificacion]);
	}

	public function modal($id){
		$datos = DB::table('producto')
				->join('marca', 'marca.idMarca', '=', 'producto.idMarca')
				->join('proveedor', 'proveedor.idProveedor', '=', 'marca.idProveedor')
				->join('inventario', 'inventario.id_Producto', '=', 'producto.id_Producto')
				->join('clavesats', 'clavesats.idsat','=','producto.idsat')
				->join('status','status.idEstado','=', 'producto.idEstado')
				->join('almacen', 'inventario.idAlmacen', '=', 'almacen.idAlmacen')
				->join('ubicacion','ubicacion.id_ubicacion', '=', 'inventario.id_ubicacion')
				->where('producto.id_Producto','=',$id)
				->select('producto.id_Producto','producto.descripcion as nombre', 'marca.marca', 'proveedor.proveedor', 'status as descripcion', 'inventario.cantidad', 'clave', 'costo', 'cantMaxima', 'cantMin', 'clavesats.clavesat as sat', 'entrada', 'salida', 'nivel', 'piso', 'anaquel', 'pasillo', 'almacen.descripcion as almacen', 'bloqueado', 'disponible')
				->get();
			return ($datos = array('datos' => $datos));
	}


	public function update($id, Request $request){
		$post = inventario::find($request->id);

		$post->cantMaxima = $request->cantidadMax;
		$post->cantMin = $request->cantidadMin;
		if ($request->has("entrada")) {
		//	$post->entrada = $post->entrada + $request->entrada;
			//$post->cantidad = $request->Cantidad + $request->entrada;
$post->cantidad = $request->CantidadT + $request->entrada;
$post->disponible = $request->CantidadD + $request->entrada;
			$operacion=new bitacora();
	    	$operacion->datomodificado = $request->id;
	    	$operacion->id_usuario = $request->input('usuario');
	    	$operacion->modulo="inventario";
	    	$operacion->id_movimiento=1;
	    	$operacion->save();
		}

		if ($request->has("salida")) {
		//	$post->salida = $post->salida + $request->salida;
			//$post->cantidad = $request->Cantidad - $request->salida;

			$post->cantidad = $request->CantidadT - $request->salida;
						$post->disponible = $request->CantidadD - $request->salida
			$operacion=new bitacora();
	    	$operacion->datomodificado = $request->id;
	    	$operacion->id_usuario = $request->input('usuario');
	    	$operacion->modulo="inventario";
	    	$operacion->id_movimiento=5;
	    	$operacion->save();
		}
		$post->save();


		DB::table('ubicacion')->where('id_ubicacion','=',$request->id_ubicacion)
            ->update(['nivel'=>$request->nivel,
                    'piso'=>$request->piso,
                     'anaquel'=>$request->anaquel,
                     'pasillo'=>$request->pasillo]
                    );

        $operacion=new bitacora();
    	$operacion->datomodificado = $request->id;
    	$operacion->id_usuario = $request->input('usuario');
    	$operacion->modulo="inventario";
    	$operacion->id_movimiento=2;
    	$operacion->save();




	return redirect()->back()->with('message','El inventario se ha actualizado');
	}

	public function edit($id){
		$inventario = DB::table('producto')
				->join('marca', 'marca.idMarca', '=', 'producto.idMarca')
				->join('proveedor', 'proveedor.idProveedor', '=', 'marca.idProveedor')
				->join('inventario', 'inventario.id_Producto', '=', 'producto.id_Producto')
				->join('status','status.idEstado','=', 'producto.idEstado')
				->join('clavesats', 'clavesats.idsat','=','producto.idsat')
				->join('almacen', 'inventario.idAlmacen', '=', 'almacen.idAlmacen')
				->join('ubicacion','ubicacion.id_ubicacion', '=', 'inventario.id_ubicacion')
				->where('producto.id_Producto','=',$id)
				->select('producto.id_Producto','producto.descripcion as nombre', 'marca.marca', 'proveedor.proveedor', 'status as descripcion', 'inventario.cantidad', 'clave', 'costo', 'cantMaxima', 'cantMin', 'clavesats.clavesat as sat', 'entrada', 'salida', 'nivel', 'piso', 'anaquel', 'pasillo', 'almacen.descripcion as almacen', 'inventario.id_ubicacion', 'bloqueado', 'disponible')
				->get();

		return ($datos  = array('datos' => $inventario));
	}

	public function destroy($id)
    {
        $producto= DB::table('inventario')
        ->where('id_Producto','=',$id)
       	->update(['estadoinventario' => 0]);
        return redirect()->back()->with('message','El producto se ha eliminado');
    }




    public function index(){

    }


    public function show(){

    }


    public function eliminar($id){
    	 $producto= DB::table('inventario')
        ->where('id_Producto','=',$id)
       	->update(['estadoinventario' => 0]);

       	DB::table('bitacora') ->insert(['datomodificado' => $id, 'updated_at' => '2019-11-19 10:02:01', 'id_movimiento' => 3, 'id_usuario' =>'1'
       	]);

        return redirect()->back()->with('message','El producto se ha eliminado');
    }

	public function filtrado(Request $request){
		$productos =DB::table('producto')
			->join('marca', 'marca.idMarca', '=', 'producto.idMarca')
			->join('proveedor', 'proveedor.idProveedor', '=', 'marca.idProveedor')
			->join('inventario', 'inventario.id_Producto', '=', 'producto.id_Producto')
			->join('clavesats', 'clavesats.idsat','=','producto.idsat')
			->join('status', 'status.idEstado', '=', 'producto.idEstado')
			->join('almacen',  'inventario.idAlmacen', '=', 'almacen.idAlmacen')
			->where('estadoinventario', '=', 1)
			->where($request->categoriad,'LIKE','%'.$request->buscador.'%')
			->select('producto.id_Producto','producto.descripcion as nombre', 'marca.marca', 'proveedor.proveedor', 'inventario.cantidad', 'clave', 'costo', 'cantMaxima', 'cantMin', 'marca.marca', 'proveedor.proveedor', 'almacen.descripcion as almacen', 'clavesats.clavesat as sat', 'status as descripcion')
			->Paginate(8);
		return view('Inventario/InventarioModi', $productos = array('productos' => $productos ));
	}

	public function create(){


	}

	public function store(Request $request){

		$almacen= DB::table('almacen')
		->where('almacen.id_sucursal', '=', $request->sucursal)
		->select('idAlmacen')
		->first();

		$ubicacion = ubicacion::create(
    	['pasillo' => $request->pasillo,
    	'piso' =>  $request->piso,
    	'anaquel' => $request->anaquel,
    	'nivel'=>$request->nivel]
		);

		$inventario = inventario::firstOrNew(
				[
					'cantidad'=>$request->Cantidad,
					'cantMaxima' => $request->cantidadMax,
					'cantMin' => $request->cantidadMin,
					'estadoinventario' => 1,
					'entrada' => 0,
					'salida' => 0,
					'bloqueado'=>0,
					'disponible'=>0,
					'idAlmacen' => $almacen->idAlmacen,
					'id_Producto' => $request->id,
					'id_ubicacion' => $ubicacion->id
		]);

			if($inventario->exists){
				return redirect()->back()->with('message','Ya existe');
			} else {
		$inventario->save();
		$operacion=new bitacora();
    	$operacion->datomodificado = $request->id;
    	$operacion->id_usuario = $request->input('usuario');
    	$operacion->modulo="inventario";
    	$operacion->id_movimiento=2;
    	$operacion->save();
    	return redirect()->back()->with('message','Se agregó producto a Inventario');
    	}


	}

  public function modalNuevo($id){
    	$datos = DB::table('producto')
            ->join('marca', 'marca.idMarca', '=', 'producto.idMarca')
            ->join('clavesats','clavesats.idsat', '=', 'producto.idsat')
            ->join('proveedor', 'proveedor.idProveedor', '=', 'marca.idProveedor')
            ->where('producto.id_Producto','=',$id)
            ->select('producto.id_Producto','producto.descripcion','producto.clave','marca.marca', 'clavesats.clavesat as sat','clavesats.descripcion as descripcionsat','marca.idMarca as idmarca','clavesats.idsat as idsat','proveedor.proveedor')
            ->get();
     return($datos = array('datos' => $datos));
    }



}
