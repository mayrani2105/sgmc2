<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Flash;
use App\producto;
use App\categoria;
use App\proveedor;
use App\estado_producto;
use App\ubicacion;
use App\clavesat;
use App\almacen;
use App\status;
use App\marca;
use App\bitacora;
use App\subcategoria;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class Productos extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }
    //Regresa los productos de la tabla productos
    public function listaProductos(){
        //Consulta tabla de productos
        $id=Auth::user()->id;
        $permiso=DB::table('funcionesrol')->join('rol','rol.id_rol','=','funcionesrol.id_rol')->join('privilegios','funcionesrol.id_privilegio','=','privilegios.id_privilegio')->select('funcionesrol.ver','funcionesrol.editar','funcionesrol.borrar')->where('funcionesrol.id_rol','=',$id)->where('privilegios.id_privilegio','=',5)->get();
        $permisoT=DB::table('privilegio_usuario')->join('users','users.id','=','privilegio_usuario.id_usuario')->join('privilegios','privilegio_usuario.id_privilegio','=','privilegios.id_privilegio')->select('privilegio_usuario.ver','privilegio_usuario.editar','privilegio_usuario.borrar')->where('privilegio_usuario.id_usuario','=',$id)->where('privilegios.id_privilegio','=',3)->get();
        $value =$sucursal=Db::table('sucursal')->select('*')->where('id_sucursal','=',Auth::user()->id_sucursal)->get();

        foreach ($value as $key) {
          $value="sucursal ".$key->descripcionS;
        }
        $notificacion=DB::table('notificacions')
        ->join('tipo_notificacion','tipo_notificacion.idtipo_n','=','notificacions.idtipo_n')
        ->select('tipo_notificacion.descripcion as des','notificacions.origen','notificacions.idtipo_n','notificacions.destino','notificacions.created_at as tiempo')->where('notificacions.destino','=',$value)->orwhere('notificacions.origen','=',$value)->orderBy('notificacions.created_at')->get();
        $productos = DB::table('producto')
            ->join('marca', 'marca.idMarca', '=', 'producto.idMarca')
            ->join('clavesats','clavesats.idsat', '=', 'producto.idsat')
            ->join('subcategoria', 'subcategoria.idSubCategoria', '=', 'producto.idSubCategoria')
            ->join('categoria', 'categoria.idCategoria', '=', 'subcategoria.idCategoria')
            ->join('status', 'status.idEstado', '=', 'producto.idEstado')
            ->select('producto.id_Producto','producto.descripcion','producto.unidadbase','producto.costo','producto.precioreal','producto.clave','marca.marca', 'clavesats.clavesat as sat','clavesats.descripcion as descripcionsat'
            ,'categoria.descripcion as categoria','subcategoria.descripcion as subcategoria','status.status','marca.idMarca as idmarca','clavesats.idsat as idsat'
            ,'categoria.idCategoria as idcategoria','subcategoria.idSubCategoria as idsubcategoria','status.idEstado as idstatus')->orderBy('producto.id_Producto')
            ->paginate(9);
            $producto = DB::table('producto')
                ->join('marca', 'marca.idMarca', '=', 'producto.idMarca')
                ->join('clavesats','clavesats.idsat', '=', 'producto.idsat')
                ->join('subcategoria', 'subcategoria.idSubCategoria', '=', 'producto.idSubCategoria')
                ->join('categoria', 'categoria.idCategoria', '=', 'subcategoria.idCategoria')
                ->join('status', 'status.idEstado', '=', 'producto.idEstado')
                ->select('producto.id_Producto','producto.descripcion','producto.unidadbase','producto.costo','producto.precioreal','producto.clave','marca.marca', 'clavesats.clavesat as sat','clavesats.descripcion as descripcionsat'
                ,'categoria.descripcion as categoria','subcategoria.descripcion as subcategoria','status.status','marca.idMarca as idmarca','clavesats.idsat as idsat'
                ,'categoria.idCategoria as idcategoria','subcategoria.idSubCategoria as idsubcategoria','status.idEstado as idstatus')->orderBy('producto.id_Producto')
                ->get();
        //Consulta tabla de categorias para el modal
        $categorias= categoria::all();
        $marca= marca::all();
        $sat=clavesat::all();
        $subcategoria=subcategoria::all();
        $status=status::all();

           // ('marca','ubicacion','sat','almacen','subcategoria','status'));
        return view('Productos.CatalogoProductos', ['productos'=>$productos,'producto'=>$producto, 'categorias'=>$categorias,'marca'=>$marca,'sat'=>$sat,'subcategoria'=>$subcategoria,'status'=>$status,'permiso'=>$permiso,'notificacion'=>$notificacion]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    //Regresa todos los elemntos para crear un producto
    public function listaNecesarioParaCreacionProducto(Request $request)
    {
        $categorias= categoria::all();
        $marca= marca::all();
        $sat=clavesat::all();
        $subcategoria=subcategoria::all();
        $status=status::all();

            return view ('Productos.CreacionProductos',['marca'=>$marca,'sat'=>$sat,'subcategoria'=>$subcategoria,'status'=>$status,'categorias'=>$categorias]);
    }



    //Controlador para crear un producto
    public function store(Request $request)
    {
            $producto= producto:: firstOrNew([
            'descripcion'=>$request->input('descripcion')
            ,'unidadbase'=>$request->input('unidadbase')
            ,'costo'=>$request->input('costo')
            ,'precioreal'=>$request->input('precioreal')
            ,'clave'=>$request->input('clave')
            ,'idMarca'=>$request->input('marca')
            ,'idsat'=>$request->input('clavesat')
            ,'idSubCategoria'=>$request->input('subcategoria')
            ,'idEstado'=>$request->input('status')

            ]);
        if ($producto->exists) {
    return redirect()->back()->with('alert-info','El producto ya existe');
} else {
   $producto->save();
   $operacion=new bitacora();
   $operacion->datomodificado=$request->input('descripcion');
   $operacion->id_usuario=$request->input('usuario');
    $operacion->modulo="productos";
   $operacion->id_movimiento=4;
   $operacion->save();
    return redirect()->back()->with('alert-success','Se ha creado el nuevo producto');
}




    }

    public function byCategoria($id) {
        $subcategoria=DB::table("subcategoria")->where("idCategoria",$id)->pluck("descripcion","idSubCategoria");
        return json_encode($subcategoria);
    }

    public function byAll($id) {

        //Consulta tabla de productos
        $producto = DB::table('producto')->where('id_Producto','=',$id_p)
            ->join('marca', 'marca.idMarca', '=', 'producto.idMarca')

            ->join('ubicacion','ubicacion.id_ubicacion', '=', 'producto.id_ubicacion')
            ->join('clavesats','clavesats.idsat', '=', 'producto.idsat')
            ->join('almacen', 'almacen.idAlmacen', '=', 'producto.idAlmacen')
            ->join('subcategoria', 'subcategoria.idSubCategoria', '=', 'producto.idSubCategoria')
            ->join('categoria', 'categoria.idCategoria', '=', 'subcategoria.idCategoria')
            ->join('status', 'status.idEstado', '=', 'producto.idEstado')
            ->select('producto.id_Producto','producto.descripcion','producto.unidadbase','producto.costo','producto.precioreal','producto.clave','marca.marca', 'ubicacion.nivel as nivel', 'ubicacion.piso as piso', 'ubicacion.anaquel as anaquel', 'ubicacion.pasillo as pasillo', 'clavesats.clavesat as sat', 'almacen.descripcion as almacen'
            ,'categoria.descripcion as categoria','subcategoria.descripcion as subcategoria','status.status','clavesats.idsat as idsat', 'almacen.idAlmacen as idalmacen'
            ,'categoria.idCategoria as idcategoria','subcategoria.idSubCategoria as idsubcategoria','status.idEstado as idstatus')->orderBy('producto.id_Producto');

        return json_encode($producto);
    }




    //Controlador para actualizar un producto
    public function update(Request $request, $id)
    {
        //Actualizar el producto


        $id_p=$request->id;
        //$producto=producto::findOrFail($request->id);

        DB::table('producto')->where('id_Producto','=',$id_p)
            ->update(['descripcion'=>$request->Descripcion,
                    'costo'=>$request->costo,
                     'clave'=>$request->Clave,
                     'descripcion'=>$request->Descripcion,
                     'idMarca'=>$request->Marca,
                     'idsat'=>$request->Clavesat,
                     'idSubCategoria'=>$request->categoria,
                     'idEstado'=>$request->status]
                    );
                    $operacion=new bitacora();
                    $operacion->datomodificado=$request->Descripcion;
                    $operacion->id_usuario=$request->usuario;
                     $operacion->modulo="productos";
                    $operacion->id_movimiento=2;
                    $operacion->save();



       // producto::update($request->all());
        return redirect()->back()->with('alert-warning','El producto se ha actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    //Controlador para eliminar un producto que no este aun utilizado
    public function destroy(Request $request,$id)
    {
        //Eliminar producto
      $usuario=Auth::user()->id;
      $dato=DB::table('producto')->select('*')->where('id_Producto','=',$id)->get();
    foreach ($dato as $key) {
         $datos=$key->descripcion;
}
        $producto= DB::table('producto')->where('id_Producto','=',$id)
        ->update(['idEstado'=>'2']);
        $operacion=new bitacora();
        $operacion->datomodificado=$datos;
        $operacion->id_usuario=$usuario;
        $operacion->modulo="productos";
        $operacion->id_movimiento=3;
        $operacion->save();
       //$producto->delete();


       return redirect()->back()->with('alert-danger','El producto se ha cambiado su status');
    }



}
