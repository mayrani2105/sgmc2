<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\categoria;
use App\subcategoria;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class LineasController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }
    public function listaCategorias(){
    	$categorias = categoria::all();

    	$html = '';
    	$html = '
      <div class="accordion" style="width:610px;   line-height: 1.6;border-top: 6px solid #344478; box-shadow: 5px 5px 10px 0px #a4bac1;" id="accordionExample">
    				<div class="card">';
    	$i = 0;
      $value =$sucursal=Db::table('sucursal')->select('*')->where('id_sucursal','=',Auth::user()->id_sucursal)->get();

      foreach ($value as $key) {
        $value="sucursal ".$key->descripcionS;
      }
      $notificacion=DB::table('notificacions')
      ->join('tipo_notificacion','tipo_notificacion.idtipo_n','=','notificacions.idtipo_n')
      ->select('tipo_notificacion.descripcion as des','notificacions.origen','notificacions.idtipo_n','notificacions.destino','notificacions.created_at as tiempo')->where('notificacions.destino','=',$value)->orwhere('notificacions.origen','=',$value)->orderBy('notificacions.created_at')->get();
    	foreach ($categorias as $key) {
    		$i++;
    		$idc = $key->idCategoria;
    		$subcat = DB::table('subcategoria')->where('idCategoria','=',$idc)->get();
    		$html .= '
    				<div class="card-heade" style="background-color: #fff;" id="headingOne">
      					<h2 class="mb-0" >
        					<button class="btn btn" type="button" data-toggle="collapse" style=" "data-target="#c'.$i.'" aria-expanded="true" >
          				<i class="fas fa-drafting-compass"></i>  '.$key->descripcion.'
        					</button>
      					</h2>
    				</div>
    				<div id="c'.$i.'" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample" >
      					<div class="card-body">
                <ul>
      					';
    		foreach ($subcat as $key2) {

    			        $html .='<li>'. $key2->descripcion . '<br>';
    		}
    		$html .= "	</div>
    				</div>";
    	}
    	$html .= '</div>';
    	return View('Lineas_Sublineas.lineas',['listas'=>$html,'notificacion'=>$notificacion]);
    }
}
