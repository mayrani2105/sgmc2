<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
      $value =$sucursal=Db::table('sucursal')->select('*')->where('id_sucursal','=',Auth::user()->id_sucursal)->get();

          foreach ($value as $key) {
            $value="sucursal ".$key->descripcionS;
          }


          $notificacion=DB::table('notificacions')
          ->join('tipo_notificacion','tipo_notificacion.idtipo_n','=','notificacions.idtipo_n')
          ->select('tipo_notificacion.descripcion as des','notificacions.origen','notificacions.idtipo_n','notificacions.destino','notificacions.created_at as tiempo')->where('notificacions.destino','=',$value)->orwhere('notificacions.origen','=',$value)->get();



       if(Auth::user()->id_rol <= 2){
          return view('home',['notificacion'=>$notificacion]);

      } else if(Auth::user()->id_rol >2 && Auth::user()->id_rol < 5 || Auth::user()->id_rol >= 6){
        return view('home2',['notificacion'=>$notificacion]);

      } else if(Auth::user()->id_rol >4){
        return view('home3');
      }


    }



}
