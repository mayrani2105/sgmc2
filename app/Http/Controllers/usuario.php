<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class usuario extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {

    }

  public function ver(){
    
    $usuario = DB::table('usuario')->select('id')->get();
    return view ('auth.login',['usuario'=>$usuario]);
}

public function verRoles(){
  $usuario= DB::table('rol')->select('descripcion','id_rol')->get();
    return view ('auth.register',['usuario'=>$usuario]);
}
public function verperfil(){
  $usuario= DB::table('users')->join('rol','users.id_rol','=','rol.id_rol')
  ->join('sucursal','users.id_sucursal','=','sucursal.id_sucursal')
  ->select('rol.descripcion','sucursal.direccion','sucursal.calle')->get();
    return view ('auth.register',['usuario'=>$usuario]);
}
}
