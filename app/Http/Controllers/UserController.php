<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
class UserController extends Controller
{
    //Regresa la vista del usuario
public function index(){
	return view('auth.login');
}

	public function registerUserAdmin(Request $request){
        // Se crea el usuario con los datos del registro
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);

        // Le asignamos el rol de Cliente
        $user->assignRole('adminstrador');
    }

	public function usuario(){

		$usuarios = DB::table('users')->get();
		$value =$sucursal=Db::table('sucursal')->select('*')->where('id_sucursal','=',Auth::user()->id_sucursal)->get();

		foreach ($value as $key) {
			$value="sucursal ".$key->descripcionS;
		}
		$notificacion=DB::table('notificacions')
		->join('tipo_notificacion','tipo_notificacion.idtipo_n','=','notificacions.idtipo_n')
		->select('tipo_notificacion.descripcion as des','notificacions.origen','notificacions.idtipo_n','notificacions.destino','notificacions.created_at as tiempo')->where('notificacions.destino','=',$value)->orwhere('notificacions.origen','=',$value)->orderBy('notificacions.created_at')->get();
		return view('Usuarios.Usuarios', array(
			"usuarios" => $usuarios,"notificacion"=>$notificacion
		));
	}

	public function actualizar($id_usuario){
	$datos = DB::table('users')
		->where('id', '=', $id_usuario)
		->select('*')
		->get();
		//dd($datos);
	/*$datos2 = DB::table('rol')
		//->where('users.id_rol', '=', 'rol.id_rol')
		->select('*')
		->get();*/
	$data=DB::table('rol')
       ->select('*')
       ->get();
		
	return ($datos=array('datos' => $datos));
		//return response()->json($datos);
	}
	
	public function select(){ $data=DB::table('roles')
       ->select('name')
       ->get();
        return view('Usuario.select')->with('data',$data);
    }
 
    public function postSelect(Request $request){
        dd($request->all());
    }
	
	public function newUser($id_usuario){

		$new->nomUser = $request->nom_user;
		$new->email = $request->email;
		$new->alias_usuario = $request->alias_usuario;
		$new->direccion = $request->direccion;
		$new->sucursal = $request->sucursal;
		$new->puesto = $request->puesto;

		$operacion=new bitacora();
    	$operacion->datomodificado = $request->id;
    	$operacion->id_usuario = $request->input('usuario');
    	$operacion->modulo="usuarios";
    	$operacion->id_movimiento=2;
    	$operacion->save();
	}
}
