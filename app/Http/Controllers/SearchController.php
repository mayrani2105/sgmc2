<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class SearchController extends Controller
{
  public function index()
{
  return view('search.search');
}
public function search(Request $request)
{
if($request->ajax())
{
  $output="";
  $products=DB::table('proveedor')->where('proveedor','LIKE','%'.$request->search."%")
  ->orwhere('idproveedor','LIKE','%'.$request->search."%")
  ->orwhere('claveProveedor','LIKE','%'.$request->search."%")
  ->orwhere('correo','LIKE','%'.$request->search."%")
  ->orwhere('diascredito','LIKE','%'.$request->search."%")
  ->orwhere('promociones','LIKE','%'.$request->search."%")
  ->orwhere('telefono','LIKE','%'.$request->search."%")
  ->orwhere('calle','LIKE','%'.$request->search."%")
  ->orwhere('colonia','LIKE','%'.$request->search."%")
  ->get();
  if($products)
  {

  foreach ($products as $key => $product) {
  $output.='<tr>'.
  '<td>'.$product->idProveedor.'</td>'.
  '<td>'.$product->claveProveedor.'</td>'.
  '<td>'.$product->proveedor.'</td>'.
  '<td>'.$product->correo.'</td>'.
  '<td>'.$product->diascredito.'</td>'.
  '<td>'.$product->telefono.'</td>'.
  '<td>'.$product->calle.'</td>'.
  '</tr>';
  }
  return Response($output);
   }
 }
}

  public function search2(Request $request){
    if($request->ajax())
    {
      $productos=DB::table('producto')
      ->join('marca', 'marca.idMarca', '=', 'producto.idMarca')
      ->join('proveedor', 'proveedor.idProveedor', '=', 'marca.idProveedor')
      ->join('inventario', 'inventario.id_Producto', '=', 'producto.id_Producto')
      ->join('clavesats', 'clavesats.idsat','=','producto.idsat')
      ->join('status', 'status.idEstado', '=', 'producto.idEstado')
      ->join('almacen',  'inventario.idAlmacen', '=', 'almacen.idAlmacen')
      ->where('estadoinventario', '=', 1) ->orwhere('producto.id_Producto', 'LIKE', '%'.$request->search."%")
      ->orwhere('producto.descripcion as nombre', 'LIKE', '%'.$request->search."%")
      ->orwhere('marca.marca', 'LIKE', '%'.$request->search."%")
      ->orwhere('proveedor.proveedor', 'LIKE', '%'.$request->search."%")
      ->orwhere('inventario.cantidad', 'LIKE', '%'.$request->search."%")
      ->orwhere('clave', 'LIKE', '%'.$request->search."%")
      ->orwhere('costo', 'LIKE', '%'.$request->search."%")
      ->orwhere('almacen.descripcion as almacen', 'LIKE', '%'.$request->search."%")
      ->orwhere('clavesats.clavesat as sat', 'LIKE', '%'.$request->search."%")
      ->orwhere('status as descripcion', 'LIKE', '%'.$request->search."%")
      ->select('producto.id_Producto','producto.descripcion as nombre', 'marca.marca', 'proveedor.proveedor', 'inventario.cantidad', 'clave', 'costo', 'cantMaxima', 'cantMin', 'almacen.descripcion as almacen', 'clavesats.clavesat as sat', 'status as descripcion')
      ->get();
      if($productos){
         foreach ($productos as $key => $product) {
            $output.='<tr>'.
            '<td>'.$product->id_Producto.'</td>'.
            '<td>'.$product->nombre.'</td>'.
            '<td>'.$product->clave.'</td>'.
            '<td>'.$product->cantidad.'</td>'.
            '<td>'.$product->sat.'</td>'.
            '<td>'.$product->costo.'</td>'.
            '<td>'.$product->almacen.'</td>'.
            '<td>'.$product->descripcion.'</td>'.
            '<td>'.$product->marca.'</td>'.
            '<td>'.$product->proveedor.'</td>'.
            '</tr>';
          }
          return Response($output);
      }

    }
  }

}
