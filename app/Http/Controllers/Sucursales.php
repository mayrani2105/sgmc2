<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\sucursal;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\bitacora;
use App\almacen;

class Sucursales extends Controller
{
    public function index(){
    	$id=Auth::user()->id_rol;
		$id=Auth::user()->id;
    $value =$sucursal=Db::table('sucursal')->select('*')->where('id_sucursal','=',Auth::user()->id_sucursal)->get();

    foreach ($value as $key) {
      $value="sucursal ".$key->descripcionS;
    }
    $notificacion=DB::table('notificacions')
    ->join('tipo_notificacion','tipo_notificacion.idtipo_n','=','notificacions.idtipo_n')
    ->select('tipo_notificacion.descripcion as des','notificacions.origen','notificacions.idtipo_n','notificacions.destino','notificacions.created_at as tiempo')->where('notificacions.destino','=',$value)->orwhere('notificacions.origen','=',$value)->orderBy('notificacions.created_at')->get();
    	$permiso=DB::table('funcionesrol')
		->join('rol','rol.id_rol','=','funcionesrol.id_rol')
		->join('privilegios','funcionesrol.id_privilegio','=','privilegios.id_privilegio')
		->select('funcionesrol.ver','funcionesrol.editar','funcionesrol.borrar')
		->where('funcionesrol.id_rol','=',$id)
		->where('privilegios.id_privilegio','=',2)
		->get();

    	$sucursal = DB::table('sucursal')
    	->join('estados', 'estados.id_Estados','=', 'sucursal.id_Estados')
    	->select('id_sucursal as id', 'descripcionS as clave', 'direccion as calle', 'colonia', 'ciudad', 'estados.estado')->orderBy('sucursal.id_sucursal')
    	->get();

    	$sucursales = DB::table('sucursal')
    	->join('estados', 'estados.id_Estados','=', 'sucursal.id_Estados')
    	->select('id_sucursal as id', 'descripcionS as clave', 'direccion as calle', 'colonia', 'ciudad', 'estados.estado')->orderBy('sucursal.id_sucursal')
    	->get();

    	$estado = DB::table('estados')
    	->select('id_Estados as id', 'estado')
    	->get();

		return view('Sucursales.VerSucursales', ['notificacion'=>$notificacion,'sucursal' => $sucursal, 'permiso'=>$permiso, 'sucursales'=>$sucursales, 'estado'=>$estado]);
	}

	public function show($id)
	{
		$sucursal = DB::table('sucursal')
    	->join('estados', 'estados.id_Estados','=', 'sucursal.id_Estados')
    	->where('sucursal.id_sucursal', '=', $id )
    	->select('id_sucursal as id', 'descripcionS as clave', 'direccion as calle', 'colonia', 'ciudad','numero', 'estados.estado')->orderBy('sucursal.id_sucursal')
    	->get();
    	return ($datos = array('datos' => $sucursal));
	}

	public function edit($id){
		$sucursal = DB::table('sucursal')
    	->join('estados', 'estados.id_Estados','=', 'sucursal.id_Estados')
    	->where('sucursal.id_sucursal', '=', $id )
    	->select('id_sucursal as id', 'descripcionS as clave', 'direccion as calle', 'colonia', 'ciudad','numero', 'estados.estado', 'sucursal.id_Estados')
    	->get();
    	return ($datos = array('datos' => $sucursal));
	}

	public function update(Request $request){
		$post = sucursal::find($request->id);


		$post->descripcionS = $request->clave;
		$post->direccion = $request->calle;
		$post->colonia = $request->colonia;
		$post->ciudad = $request->ciudad;
		$post->numero = $request->numero;
		$post->id_Estados = $request->estado;
		$post->save();

		$operacion=new bitacora();
    	$operacion->datomodificado = $request->clave;
    	$operacion->id_usuario = $request->input('usuario');
    	$operacion->modulo="sucursales";
    	$operacion->id_movimiento=2;
    	$operacion->save();

		return redirect()->back()->with('message','La sucursal se ha actualizado');
	}

	public function destroy(){

	}

	public function store(Request $request){
		$sucursal = sucursal::create([
			'descripcionS' => $request->clave,
			'direccion' => $request->calle,
			'colonia' => $request->colonia,
			'numero' => $request->numero,
			'ciudad' => $request->ciudad,
			'id_Estados' => $request->estado
		]);



		$almacen = almacen::create([
			'descripcion' => $request->almacen,
			'calle' => $request->calle,
			'colonia' => $request->colonia,
			'id_sucursal' => $sucursal->id_sucursal,
			'id_Estados' => $sucursal->id_Estados
		]);

		$operacion=new bitacora();
    	$operacion->datomodificado = $request->clave;
    	$operacion->id_usuario = $request->input('usuario');
    	$operacion->modulo="sucursales";
    	$operacion->id_movimiento=4;
    	$operacion->save();

    	return redirect()->back()->with('message','Se agregó nueva Sucursal');
	}

	public function BuscarSuc(){
		$sucursales = DB::table('sucursal')
    	->join('estados', 'estados.id_Estados','=', 'sucursal.id_Estados')
    	->select('id_sucursal as id', 'descripcionS as clave', 'direccion as calle', 'colonia', 'ciudad', 'estados.estado')->orderBy('sucursal.id_sucursal')
    	->get();
    	return view('Sucursales.VerSucursales', $sucursales  = array('sucursales' => $sucursales ));
	}



}
