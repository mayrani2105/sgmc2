<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\marca;
use Illuminate\Support\Facades\DB;

class PruebaController extends Controller
{
    //Accion que devuelva una vista
	public function compras(){
		//$productos = marca::all();
		$productos = DB::table('marca')->orderBy('marca', 'asc')->get();
	return view('CatalogoProductos', ['productos' => $productos]);
	}
	
}
