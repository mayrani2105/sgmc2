<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\pedido;
use App\pedido_producto;
use App\producto;
use App\proveedor;
use App\bitacora;
use App\inventario;
use App\notificacion;
use Illuminate\Support\Facades\Auth;
class PedidosController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

	public function verPedidos(){
        //Consulta tabla de Pedidos Se usa la siguiente sentencia sql

/*SELECT pedido.idPedido, pedido.folio, tipo_pedido.descripcion, users.name, status_pedido.descripcion FROM pedido
INNER JOIN tipo_pedido ON pedido.id_Tipo_Pedido = tipo_pedido.id_Tipo_Pedido
INNER JOIN pedido_usuario ON pedido.idPedido = pedido_usuario.idPedido
INNER JOIN status_pedido ON pedido.idStatusPedido = status_pedido.idStatusPedido
INNER JOIN users ON users.id = pedido_usuario.id_usuario*/

//Consulta para obtener proveedore
/*SELECT `idProveedor`,`proveedor`FROM `proveedor`*/
$value =Db::table('sucursal')->select('*')->where('id_sucursal','=',Auth::user()->id_sucursal)->get();

foreach ($value as $key) {
  $value="sucursal ".$key->descripcionS;
}

$sucursal=Db::table('sucursal')->select('*')->where('id_sucursal','!=',Auth::user()->id_sucursal)->get();
		$pedidos = DB::table('pedido')
            ->join('tipo_pedido', 'pedido.id_Tipo_Pedido', '=', 'tipo_pedido.id_Tipo_Pedido')
        //  ->join('pedido_producto','pedido.idPedido', '=', 'pedido_producto.idPedido')
			->join('status_pedido','pedido.idStatusPedido', '=', 'status_pedido.idStatusPedido')
          //  ->join('users','users.id', '=', 'pedido_usuario.id_usuario')
			->select('pedido.idPedido','tipo_pedido.descripcion as tipoP','pedido.folio','tipo_pedido.descripcion'/*'users.name'*/,'status_pedido.descripcion as descripcionP','pedido.destino','pedido.origen')
            ->where('tipo_pedido.id_Tipo_Pedido','=',1)->where('pedido.destino','like',"%".$value."%")->paginate(9);

  $pedidoss = DB::table('pedido')
                    ->join('tipo_pedido', 'pedido.id_Tipo_Pedido', '=', 'tipo_pedido.id_Tipo_Pedido')
                //  ->join('pedido_producto','pedido.idPedido', '=', 'pedido_producto.idPedido')
        			->join('status_pedido','pedido.idStatusPedido', '=', 'status_pedido.idStatusPedido')
                  //  ->join('users','users.id', '=', 'pedido_usuario.id_usuario')
        			->select('pedido.idPedido','tipo_pedido.descripcion as tipoP','pedido.folio','tipo_pedido.descripcion'/*'users.name'*/,'status_pedido.descripcion as descripcionP','pedido.destino','pedido.origen')
                    ->where('tipo_pedido.id_Tipo_Pedido','=',1)->where('pedido.destino','like',"%".$value."%")->paginate(9);




$folio=Db::table('pedido')->select('*')->count();
$folio=$folio+1;
        /*Sentencia SELECT pedido.idPedido, producto.descripcion, pedido_producto.Cantidad, producto.unidadbase, producto.costo, producto.precioreal FROM pedido_producto
        INNER JOIN pedido ON pedido.idPedido = pedido_producto.idPedido
        INNER JOIN producto ON producto.id_Producto = pedido_producto.id_Producto WHERE pedido.idPedido = 3   */
        $value=Db::table('sucursal')->select('*')->where('id_sucursal','=',Auth::user()->id_sucursal)->get();

        foreach ($value as $key) {
          $value="sucursal ".$key->descripcionS;
        }
        $notificacion=DB::table('notificacions')
        ->join('tipo_notificacion','tipo_notificacion.idtipo_n','=','notificacions.idtipo_n')
        ->select('tipo_notificacion.descripcion as des','notificacions.origen','notificacions.idtipo_n','notificacions.destino','notificacions.created_at as tiempo')->where('notificacions.destino','=',$value)->orwhere('notificacions.origen','=',$value)->orderBy('notificacions.created_at')->get();
        $pedidosProductos = DB::table('pedido_producto')
            ->join('pedido', 'pedido.idPedido', '=', 'pedido_producto.idPedido')
            ->join('producto', 'producto.id_Producto', '=', 'pedido_producto.id_Producto')
            ->select('pedido.idPedido','producto.descripcion as Descripcion','pedido_producto.Cantidad  as Cantidad','producto.unidadbase', 'producto.costo', 'producto.precioreal')
            ->where('pedido.idPedido','=', 1)
            //->groupBy('pedido_producto.idPedido')
            ->get();

        $usuariosPedido = DB::table('users')
        ->select('users.id','users.name')
        ->where('users.id_rol','=', 5)
        ->Where('users.id_sucursal','=', 1)
        ->get();

        $proveedorPedido = DB::table('proveedor')
        ->select('proveedor.idProveedor','proveedor.proveedor')
        ->get();

        $countPedidos = DB::table('pedido')->count();

       /* $contarPedidos = DB::table('pedido')
        ->select(count('idPedido'))
        ->get();*/
        //Consulta tabla de pedido para generar un folio
	return view('Pedidos.VerPedidos', ['pedidoss'=>$pedidoss,'folio'=>$folio,'sucursal'=>$sucursal,'pedidos' => $pedidos, 'pedidosProductos' => $pedidosProductos, 'usuariosPedido' => $usuariosPedido, 'proveedorPedido' => $proveedorPedido, 'countPedidos' => $countPedidos,'notificacion'=>$notificacion]);
	}

    /*Sentencia SELECT pedido.idPedido, producto.descripcion, pedido_producto.Cantidad, producto.unidadbase, producto.costo, producto.precioreal FROM pedido_producto INNER JOIN pedido ON pedido.idPedido = pedido_producto.idPedido INNER JOIN producto ON producto.id_Producto = pedido_producto.id_Producto WHERE pedido.idPedido = 3   */

    public function verPedidosProducto($id){
$output="";
         $pedidosProductos = DB::table('pedido_producto')
            ->join('pedido', 'pedido.idPedido', '=', 'pedido_producto.idPedido')
            ->join('producto', 'producto.id_Producto', '=', 'pedido_producto.id_Producto')
            ->select('pedido.idPedido','producto.descripcion as Descripcion','producto.clave','pedido_producto.Cantidad  as Cantidad','producto.unidadbase', 'producto.costo', 'producto.precioreal')
            ->where('pedido.idPedido','=', $id)
            ->get();
            foreach ($pedidosProductos as $product) {
              $output.='<tr>'.
              '<td>'.$product->clave.'</td>'.
              '<td>'.$product->Descripcion.'</td>'.
              '<td>'.$product->Cantidad.'</td>'

              .'</tr>'
              ;}

              return Response($output);
            }
       //

       public function verPedidosProductos($id){
   $output="";
            $pedidosProductos = DB::table('pedido_producto')
               ->join('pedido', 'pedido.idPedido', '=', 'pedido_producto.idPedido')
               ->join('producto', 'producto.id_Producto', '=', 'pedido_producto.id_Producto')
               ->select('pedido.folio as idPedido','producto.descripcion as Descripcion','producto.clave','pedido_producto.Cantidad  as Cantidad','producto.unidadbase', 'producto.costo', 'producto.precioreal')
               ->where('pedido.idPedido','=', $id)
               ->get();


              return ($pedidosProductos = array('pedidosProductos' => $pedidosProductos));
               }

    public function CrearPedidos(Request $request){
      $sucursal=DB::table('sucursal')->select('*')->where('id_sucursal','=',Auth::user()->id_sucursal)->get();
      foreach ($sucursal as $suc) {
       $lugar=$suc->descripcionS;
            }
    $pedido= new pedido();
      if($request->tipos==1){
    $pedido->idPedido=0;
    $pedido->folio=$request->folio;
    $pedido->id_Tipo_Pedido=$request->tipo;
    $pedido->origen="Sucursal ".$request->sucursal;
    $pedido->destino="Sucursal ".$lugar;
    $pedido->idStatusPedido=1;
   $pedido->save();
  //  echo"Guardado con exito";
  }else{
    $pedido->idPedido=0;
    $pedido->folio=$request->folio;
    $pedido->id_Tipo_Pedido=$request->tipo;
    $pedido->origen="Proveedor";
    $pedido->destino="Sucursal ".$lugar;
    $pedido->idStatusPedido=1;
   $pedido->save();
  //echo"Guardado con exito";

  }
  $pedido=DB::table('pedido')->select('idPedido')->where('folio','=',$request->folio)->pluck('idPedido');

  return json_decode($pedido);
    }
    /*Query para obtener valores SELECT producto.id_Producto, producto.descripcion, producto.unidadbase, marca.idMarca, proveedor.idProveedor FROM producto INNER JOIN marca ON producto.idMarca = marca.idMarca INNER JOIN proveedor ON marca.idProveedor = proveedor.idProveedor WHERE proveedor.idProveedor = 2 */

    public function byCategoria($id) {

        $subcategoria=DB::table("producto")
        ->join('marca', 'producto.idMarca', '=', 'marca.idMarca')
        ->join('proveedor', 'marca.idProveedor', '=', 'proveedor.idProveedor')
        ->select('producto.id_Producto','producto.descripcion','producto.unidadbase','marca.idMarca','proveedor.idProveedor')
        ->where("proveedor.idProveedor",$id)->pluck("producto.descripcion", "producto.id_Producto");
        return json_encode($subcategoria);
    }
    public function obtenerproductos($id){

      $productos = DB::table('producto')
        ->join('marca', 'marca.idMarca', '=', 'producto.idMarca')
        ->join('proveedor', 'proveedor.idProveedor', '=', 'marca.idProveedor')
        ->join('inventario', 'inventario.id_Producto', '=', 'producto.id_Producto')
        ->join('clavesats', 'clavesats.idsat','=','producto.idsat')
        ->join('status', 'status.idEstado', '=', 'producto.idEstado')
        ->join('almacen',  'inventario.idAlmacen', '=', 'almacen.idAlmacen')
        ->join('sucursal','sucursal.id_sucursal','=', 'almacen.id_sucursal')
        ->where('estadoinventario', '=', 1)
        ->where('sucursal.descripcionS','=',$id)->pluck("producto.descripcion", "producto.id_Producto");
        return json_encode($productos);
    }
    // se agregan pedidos a orden de compra de proveedores
public function agregarproductos(Request $request){
     $pro=new pedido_producto();

     $ca=$request->cantidad+0;
if($ca>0){
$pedido=DB::table('pedido')->select('*')->where('idPedido','=',$request->folio)->get();
foreach ($pedido as $k) {
  $id=$k->idPedido;
}
$can=DB::table("pedido_producto")->select('*')->where('id_Producto','=',$request->prodProveedor+0)->where('idPedido','=',$id)->count();

if($can==0){
  $pro->idPedido=$id;
  $pro->cantidad=$request->cantidad+0;
  $pro->id_Producto=$request->prodProveedor+0;
  $pro->save();
  }else{
    $numero=  DB::table("pedido_producto")->select('cantidad')
      ->where ('idPedido','=',$id)->where('id_Producto','=',$request->prodProveedor+0)->get();
$numero2=$request->cantidad;
$valor=0;
foreach ($numero as $key) {
  $valor=$key->cantidad;
}
$total=$numero2+$valor;
      DB::table("pedido_producto")
      ->where ('idPedido','=',$id)->where('id_Producto','=',$request->prodProveedor+0)
      ->update(['cantidad' =>$total ]);
}
}else{
  echo"Ingrese un valor valido.";
}
    }


public function eliminar(Request $request){
    $pedido=DB::table('pedido')->select('*')->where('idPedido','=',$request->folio)->get();
 $id=0;
      foreach ($pedido as $k) {
       $id=$k->idPedido;
               }
$can=DB::table("pedido_producto")->select('*')->where('id_Producto','=',$request->productos+0)->where('idPedido','=',$id)->count();
if($can==1){
  $cantidadinv= DB::table('producto')
    ->join('inventario', 'inventario.id_Producto', '=', 'producto.id_Producto')
    ->join('almacen',  'inventario.idAlmacen', '=', 'almacen.idAlmacen')
    ->join('sucursal','sucursal.id_sucursal','=', 'almacen.id_sucursal')
    ->where('sucursal.descripcionS','=',$request->ubicacion)
    ->where('producto.id_Producto','=',$request->productos+0)->select('inventario.disponible','inventario.cantidad','inventario.bloqueado')->get();
    $numero2=$request->cantidadS;
    $valo=0;
    $to=0;
    $bloque=0;
    foreach ($cantidadinv as $key) {
      $valo=$key->disponible;
        $to=$key->cantidad;
        $bloque=$key->bloqueado;
    }
DB::table('producto')
  ->join('inventario', 'inventario.id_Producto', '=', 'producto.id_Producto')
  ->join('almacen',  'inventario.idAlmacen', '=', 'almacen.idAlmacen')
  ->join('sucursal','sucursal.id_sucursal','=', 'almacen.id_sucursal')
  ->where('sucursal.descripcionS','=',$request->ubicacion)
  ->where('producto.id_Producto','=',$request->productos+0)->update(['disponible' =>$valo+$numero2 ,'bloqueado'=>$bloque-$numero2]);
//echo$bloque."  ".$valo;
 $producto=DB::table("pedido_producto")->select('*')->where('id_Producto','=',$request->productos+0)->where('idPedido','=',$id)->delete();
  echo"Borrado con exito";
}else{
echo"El producto no ha sido agreagado.\n\nPor tal motivo no se puede borrar";
}

    }

    public function eliminarprove(Request $request ){
    $pedido=DB::table('pedido')->select('*')->where('idPedido','=',$request->folio)->get();
    $id=0;
        foreach ($pedido as $k) {
         $id=$k->idPedido;
                 }
    $can=DB::table("pedido_producto")->select('*')->where('id_Producto','=',$request->prodProveedor+0)->where('idPedido','=',$id)->count();
    if($can==1){

    $producto=DB::table("pedido_producto")->select('*')->where('id_Producto','=',$request->prodProveedor+0)->where('idPedido','=',$id)->delete();
    echo"Borrado con exito";
    }else{
    echo"El producto no ha sido agreagado.\n\nPor tal motivo no se puede borrar";
    }
    }


    public function agregarsucursal(Request $request){
//  echo $request->folio;
$ca=$request->cantidadS+0;
if($ca>0){
  $cantidadinv= DB::table('producto')
    ->join('inventario', 'inventario.id_Producto', '=', 'producto.id_Producto')
    ->join('almacen',  'inventario.idAlmacen', '=', 'almacen.idAlmacen')
    ->join('sucursal','sucursal.id_sucursal','=', 'almacen.id_sucursal')
    ->where('sucursal.descripcionS','=',$request->ubicacion)
    ->where('producto.id_Producto','=',$request->productos+0)->select('inventario.disponible','inventario.cantidad','inventario.bloqueado')->get();
//echo$cantidadinv;
    $numero2=$request->cantidadS;
    $valo=0;
    $to=0;
    $bloque=0;
    foreach ($cantidadinv as $key) {
      $valo=$key->disponible;
        $to=$key->cantidad;
        $bloque=$key->bloqueado;
    }
//    echo $to;
    if($valo>$to){
    echo"no se hace nada";
    }
    //echo$numero2." ".$valo;
    $cantidadinv=$valo;
//echo"xd".$valo;
    if($numero2<=$cantidadinv){
    //  echo"llegaste aqui ".$request->folio;
      $pro=new pedido_producto();

   $pedido=DB::table('pedido')->select('*')->where('idPedido','=',$request->folio)->get();
//echo"pedido ". $pedido;
$id=0;
     foreach ($pedido as $k) {
      $id=$k->idPedido;
              }
          //    echo$id;
 $can=DB::table("pedido_producto")->select('*')->where('id_Producto','=',$request->productos+0)->where('idPedido','=',$id)->count();
 //echo "  repetido ".$can;
 if($can==0){
       $pro->idPedido=$id;
       $pro->cantidad=$request->cantidadS+0;
       $pro->id_Producto=$request->productos;
       $pro->save();
       $numero=$request->cantidadS+0;
      $valor=$numero;
      $total=$valo-$valor;
      echo"Agregado con exito";

      DB::table('producto')
        ->join('inventario', 'inventario.id_Producto', '=', 'producto.id_Producto')
        ->join('almacen',  'inventario.idAlmacen', '=', 'almacen.idAlmacen')
        ->join('sucursal','sucursal.id_sucursal','=', 'almacen.id_sucursal')
        ->where('sucursal.descripcionS','=',$request->ubicacion)
        ->where('producto.id_Producto','=',$request->productos+0)->update(['disponible' =>$total ,'bloqueado'=>$valor+$bloque]);
       }else{
         $numero=  DB::table("pedido_producto")->select('cantidad')
              ->where ('idPedido','=',$id)->where('id_Producto','=',$request->productos+0)->get();
//echo$numero;
        $numero2=$request->cantidadS;
        $valor=0;
        foreach ($numero as $key) {
          $valor=$key->cantidad;
        }

     $total=$numero2+$valor;
  $totale=$valo-$valor;
        DB::table("pedido_producto")
        ->where ('idPedido','=',$id)->where('id_Producto','=',$request->productos+0)
        ->update(['cantidad' =>$total ]);
           DB::table('producto')
             ->join('inventario', 'inventario.id_Producto', '=', 'producto.id_Producto')
             ->join('almacen',  'inventario.idAlmacen', '=', 'almacen.idAlmacen')
             ->join('sucursal','sucursal.id_sucursal','=', 'almacen.id_sucursal')
             ->where('sucursal.descripcionS','=',$request->ubicacion)
             ->where('producto.id_Producto','=',$request->productos+0)->update(['disponible' =>$totale ,'bloqueado'=>$valor+$bloque]);
      echo"Modificado con exito";
    }
}else{
   echo"No se puede agregar.\n Solo se cuenta con esta cantidad: ".$cantidadinv;
 }

}else{
  echo"Ingrese un valor Valido";
}}

   public function update(Request $request) {

       if($request->idPeticion == "Cancelar"){
         DB::table("pedido")
        ->where ('pedido.idPedido','=',$request->id)
        ->update(['pedido.idStatusPedido' => 4]);
       }
       if($request->idPeticion =="Upgrade"){
      $ped=  DB::table("pedido")
        ->where ('pedido.idPedido','=',$request->id)
        ->select('*')->get();

        foreach ($ped as $key) {
        $tipo=  $key->id_Tipo_Pedido;
        $origen= $key->origen;
        $destino= $key->destino;
         $folio=$key->folio;
        }

        $pedido= new pedido();
        if($tipo==1 && $origen!="Proveedor"){
          //echo"entramos";
          $value =$sucursal=Db::table('sucursal')->select('*')->where('id_sucursal','=',Auth::user()->id_sucursal)->get();
          foreach ($value as $key) {
            $value="sucursal ".$key->descripcionS;
          }
          $folio2=Db::table('pedido')
          ->select('*')->where('origen','like','%'.$value."%")->count();
          $folio2=$folio2+1;
          $pedido->idPedido=0;
          $pedido->folio="P0".$folio2;
          $pedido->id_Tipo_Pedido=2;
          $pedido->origen=$origen;
          $pedido->destino=$destino;
          $pedido->idStatusPedido=2;
         $pedido->save();
  $notificacion= new notificacion();
  $notificacion->origen=$origen;
  $notificacion->destino=$destino;
  $notificacion->idtipo_n=2;
  $notificacion->idPedido=$request->id;
  $notificacion->save();
  $operacion=new bitacora();
  $operacion->datomodificado=$pedido->folio;
  $operacion->id_usuario=Auth::user()->id;
  $operacion->modulo="Ordenes de Compra";
  $operacion->id_movimiento=4;
  $operacion->save();
/*  $notificacion= new notificacion();
  $notificacion->origen=$destino;
  $notificacion->destino=$origen;
  $notificacion->idtipo_n=1;
  $notificacion->idPedido=$request->id;
  $notificacion->save();*/
      //   echo$pedido;
      $idpp=DB::table('pedido')->select('idPedido')->where('folio','=',"P0".$folio2)->get();
      foreach ($idpp as $key) {
        $idpp=$key->idPedido;
      }

         $idpe=DB::table('pedido')
          ->join('pedido_producto','pedido_producto.idPedido','=','pedido.idPedido')
         ->select('pedido_producto.id_Producto','pedido_producto.idPedido as pe','pedido_producto.cantidad')
         ->where('folio','=',$folio)->get();
         //echo $idpe."asdfasdf";
foreach ($idpe as $key) {
   $pro=new pedido_producto();
  //echo$key->$pe;
  $pro->idPedido=$idpp;
   $pro->cantidad=$key->cantidad;
   $pro->id_Producto=$key->id_Producto+0;
   $pro->save();
   //echo$pro;
}
echo"guardado";


}else{
  $notificacion= new notificacion();
  $notificacion->idNotificacion=0;
  $notificacion->origen=$origen;
  $notificacion->destino=$destino;
  $notificacion->idtipo_n=1;
  $notificacion->idPedido=$request->id;
  $notificacion->save();
  $operacion=new bitacora();
  $operacion->datomodificado="";
  $operacion->id_usuario=Auth::user()->id;
  $operacion->modulo="Ordenes de Compra";
  $operacion->id_movimiento=4;
  $operacion->save();
}
        DB::table("pedido")
        ->where ('pedido.idPedido','=',$request->id)
        ->update(['pedido.idStatusPedido' => 2]);
       }
       if($request->idPeticion =="Aceptar"){
           $productosPedido=DB::table("pedido_producto")
            ->select('idPedido','Cantidad','id_Producto')
            ->where('idPedido','=', $request->id)
            ->get();
       $productosPedido = array('productosPedido' => $productosPedido);


//           dd($productosPedido->idPedido);
          /* foreach($productosPedido as $product){


          }

        /*foreach($productosPedido as $value){
        echo "Value is: $value";
        }

        if($request->tipoPedido =="Entrada"){
               echo "Entra a condición Entrada";
           }
           if($request->tipoPedido =="Salida"){
               echo "Entra a condición Salida";
           }

        */


        DB::table("pedido")
        ->where ('pedido.idPedido','=',$request->id)
        ->update(['pedido.idStatusPedido' => 3]);
       }



    return redirect()->back()->with('message','El pedido se ha actualizado');
   }






}
