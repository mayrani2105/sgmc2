<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\pedido;
use App\pedido_producto;
use App\producto;
use App\proveedor;
use App\bitacora;
use App\inventario;
use Illuminate\Support\Facades\Auth;
class PedidosRealController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

      public function verPedidosProductos($id){
   $output="";
            $pedidosProductos = DB::table('pedido_producto')
               ->join('pedido', 'pedido.idPedido', '=', 'pedido_producto.idPedido')
               ->join('producto', 'producto.id_Producto', '=', 'pedido_producto.id_Producto')
               ->select('pedido.folio as idPedido','producto.descripcion as Descripcion','producto.clave','pedido_producto.Cantidad  as Cantidad','producto.unidadbase', 'producto.costo', 'producto.precioreal')
               ->where('pedido.idPedido','=', $id)
               ->get();


              return ($pedidosProductos = array('pedidosProductos' => $pedidosProductos));
               }

	public function verPedidosReal(){

$value =$sucursal=Db::table('sucursal')->select('*')->where('id_sucursal','=',Auth::user()->id_sucursal)->get();

foreach ($value as $key) {
  $value="sucursal ".$key->descripcionS;
}


$value =$sucursal=Db::table('sucursal')->select('*')->where('id_sucursal','=',Auth::user()->id_sucursal)->get();

foreach ($value as $key) {
  $value="sucursal ".$key->descripcionS;
}
$notificacion=DB::table('notificacions')
->join('tipo_notificacion','tipo_notificacion.idtipo_n','=','notificacions.idtipo_n')
->select('tipo_notificacion.descripcion as des','notificacions.origen','notificacions.idtipo_n','notificacions.destino','notificacions.created_at as tiempo')->where('notificacions.destino','=',$value)->orwhere('notificacions.origen','=',$value)->orderBy('notificacions.created_at')->get();
$sucursal=Db::table('sucursal')->select('*')->where('id_sucursal','!=',Auth::user()->id_sucursal)->get();

    $pedidos2 = DB::table('pedido')
            ->join('tipo_pedido', 'pedido.id_Tipo_Pedido', '=', 'tipo_pedido.id_Tipo_Pedido')
			->join('status_pedido','pedido.idStatusPedido', '=', 'status_pedido.idStatusPedido')
			->select('pedido.idPedido','tipo_pedido.descripcion as tipoP','pedido.folio','tipo_pedido.descripcion','status_pedido.descripcion as descripcionP','pedido.destino','pedido.origen')
            ->where('tipo_pedido.id_Tipo_Pedido','=',2)->where('pedido.origen','like',"%".$value."%")->get();

     $pedidos = DB::table('pedido')
            ->join('tipo_pedido', 'pedido.id_Tipo_Pedido', '=', 'tipo_pedido.id_Tipo_Pedido')
            ->join('status_pedido','pedido.idStatusPedido', '=', 'status_pedido.idStatusPedido')
            ->select('pedido.idPedido','tipo_pedido.descripcion as tipoP','pedido.folio','tipo_pedido.descripcion','status_pedido.descripcion as descripcionP','pedido.destino','pedido.origen')
            ->where('tipo_pedido.id_Tipo_Pedido','=',2)->where('pedido.origen','like',"%".$value."%")->get();


	return view('PedidosReal.VerPedidosReal', ['notificacion'=>$notificacion,'sucursal'=>$sucursal,'pedidos2' => $pedidos2,'pedidos'=>$pedidos]);
	}




    /*Función de actualizar*/
    public function update(Request $request) {
    if($request->idPeticion == "Cancelar"){

        $cliente = $request->cliente;


        if($cliente[0] == "c" || "C" ){
        $origen = $request->origen;
        $origen = explode(" ", $origen);
        $origen = $origen[1] ;

         $productos=DB::table("pedido_producto")
        ->select('idPedido','Cantidad','id_Producto')
        ->where('idPedido','=', $request->id)
        ->get();
    foreach ($productos as $produc) {
    $productoInv = DB::table('inventario')
            ->join('almacen', 'almacen.idAlmacen', '=', 'inventario.idAlmacen')
            ->join('sucursal', 'sucursal.id_sucursal', '=', 'almacen.id_sucursal')
            ->where ('inventario.id_Producto','=',$produc->id_Producto)
            ->where ('sucursal.descripcionS','=', $origen)
           ->select('id_inventario','cantidad','entrada','salida', 'bloqueado', 'disponible')
            ->get();
    foreach ($productoInv as $proInv){
        $resta = (($produc->Cantidad)+0);

        DB::table('inventario')
        ->join('almacen', 'almacen.idAlmacen', '=', 'inventario.idAlmacen')
        ->join('sucursal', 'sucursal.id_sucursal', '=', 'almacen.id_sucursal')
        ->where ('inventario.id_Producto','=',$produc->id_Producto)
        ->where ('sucursal.descripcionS','=', $origen)
        ->update(['inventario.disponible' => $proInv->disponible+$resta, 'inventario.bloqueado' => $proInv->bloqueado-$resta]);
        //->update(['inventario.bloqueado' => $proInv->bloqueado-$resta2]);

   $operacion=new bitacora();
   $operacion->datomodificado=$request->input('tipoP');
   $operacion->id_usuario=1;
   $operacion->modulo="pedidos";
   $operacion->id_movimiento=5;
   $operacion->save();

    }



  }
        }

        else{

        $origen = $request->origen;
        $origen = explode(" ", $origen);
        $origen = $origen[1] ;

         $productos=DB::table("pedido_producto")
        ->select('idPedido','Cantidad','id_Producto')
        ->where('idPedido','=', $request->id)
        ->get();
            foreach ($productos as $produc) {
            $productoInv = DB::table('inventario')
            ->join('almacen', 'almacen.idAlmacen', '=', 'inventario.idAlmacen')
            ->join('sucursal', 'sucursal.id_sucursal', '=', 'almacen.id_sucursal')
            ->where ('inventario.id_Producto','=',$produc->id_Producto)
            ->where ('sucursal.descripcionS','=', $origen)
           ->select('id_inventario','cantidad','entrada','salida', 'bloqueado', 'disponible')
            ->get();
    foreach ($productoInv as $proInv){
        $resta2 = (($produc->Cantidad)+0);

        DB::table('inventario')
        ->join('almacen', 'almacen.idAlmacen', '=', 'inventario.idAlmacen')
        ->join('sucursal', 'sucursal.id_sucursal', '=', 'almacen.id_sucursal')
        ->where ('inventario.id_Producto','=',$produc->id_Producto)
        ->where ('sucursal.descripcionS','=', $origen)
        ->update(['inventario.disponible' => $proInv->disponible+$resta2, 'inventario.bloqueado' => $proInv->bloqueado-$resta2]);
        //->update(['inventario.bloqueado' => $proInv->bloqueado-$resta2]);

   $operacion=new bitacora();
   $operacion->datomodificado=$request->input('tipoP');
   $operacion->id_usuario=1;
   $operacion->modulo="pedidos";
   $operacion->id_movimiento=5;
   $operacion->save();

    }



  }



        }

         DB::table("pedido")
        ->where ('pedido.idPedido','=',$request->id)
        ->update(['pedido.idStatusPedido' => 4]);

       }

    if($request->idPeticion == "Aceptar"){
        $cliente = $request->cliente;


        if($cliente[0] == "s" || "S" ){

        $origen = $request->origen;
        $origen = explode(" ", $origen);
        $origen = $origen[1] ;

        $destino = $request->cliente;
        $destino = explode(" ", $destino);
        $destino = $destino[1] ;


         $productos=DB::table("pedido_producto")
        ->select('idPedido','Cantidad','id_Producto')
        ->where('idPedido','=', $request->id)
        ->get();

        foreach ($productos as $produc) {
        $productoInv = DB::table('inventario')
                ->join('almacen', 'almacen.idAlmacen', '=', 'inventario.idAlmacen')
                ->join('sucursal', 'sucursal.id_sucursal', '=', 'almacen.id_sucursal')
                ->where ('inventario.id_Producto','=',$produc->id_Producto)
               ->select('id_inventario','cantidad','entrada','salida', 'bloqueado', 'disponible')
                ->get();
            foreach ($productoInv as $proInv){
                $resta = (($produc->Cantidad)+0);

                $valor1 = DB::table('inventario')
                ->join('almacen', 'almacen.idAlmacen', '=', 'inventario.idAlmacen')
                ->join('sucursal', 'sucursal.id_sucursal', '=', 'almacen.id_sucursal')
                ->where ('inventario.id_Producto','=',$produc->id_Producto)
                ->where ('sucursal.descripcionS','=', $origen)
                ->update(['inventario.cantidad' => $proInv->cantidad-$resta, 'inventario.bloqueado' => $proInv->bloqueado-$resta,'inventario.disponible' => $proInv->cantidad-$proInv->bloqueado, 'inventario.salida' => $proInv->salida+$resta]);

                $valor2 = DB::table('inventario')
                ->join('almacen', 'almacen.idAlmacen', '=', 'inventario.idAlmacen')
                ->join('sucursal', 'sucursal.id_sucursal', '=', 'almacen.id_sucursal')
                ->where ('inventario.id_Producto','=',$produc->id_Producto)
                ->where ('sucursal.descripcionS','=', $destino)
                ->update(['inventario.cantidad' => $proInv->cantidad+$resta, 'inventario.disponible' => $proInv->cantidad+$proInv->bloqueado, 'inventario.entrada' => $proInv->entrada+$resta]);




   $operacion=new bitacora();
   $operacion->datomodificado=$request->input('tipoP');
   $operacion->id_usuario=1;
   $operacion->modulo="pedidos";
   $operacion->id_movimiento=5;
   $operacion->save();

            }


        }

        }

    else{

                $origen = $request->origen;
                $origen = explode(" ", $origen);
                $origen = $origen[1] ;

                 $productos=DB::table("pedido_producto")
                ->select('idPedido','Cantidad','id_Producto')
                ->where('idPedido','=', $request->id)
                ->get();
                foreach ($productos as $produc) {
                $productoInv = DB::table('inventario')
                        ->join('almacen', 'almacen.idAlmacen', '=', 'inventario.idAlmacen')
                        ->join('sucursal', 'sucursal.id_sucursal', '=', 'almacen.id_sucursal')
                        ->where ('inventario.id_Producto','=',$produc->id_Producto)
                        ->where ('sucursal.descripcionS','=', $origen)
                       ->select('id_inventario','cantidad','entrada','salida', 'bloqueado', 'disponible')
                        ->get();
                    foreach ($productoInv as $proInv){
                        $resta = (($produc->Cantidad)+0);

                        DB::table('inventario')
                        ->join('almacen', 'almacen.idAlmacen', '=', 'inventario.idAlmacen')
                        ->join('sucursal', 'sucursal.id_sucursal', '=', 'almacen.id_sucursal')
                        ->where ('inventario.id_Producto','=',$produc->id_Producto)
                        ->where ('sucursal.descripcionS','=', $origen)
                        ->update(['inventario.cantidad' => $proInv->cantidad-$resta, 'inventario.bloqueado' => $proInv->bloqueado-$resta,'inventario.disponible' => $proInv->cantidad-$proInv->bloqueado, 'inventario.salida' => $proInv->salida+$resta]);


                   $operacion=new bitacora();
                   $operacion->datomodificado=$request->input('tipoP');
                   $operacion->id_usuario=1;
                   $operacion->modulo="pedidos";
                   $operacion->id_movimiento=5;
                   $operacion->save();

                }
            }






    }
        DB::table("pedido")
        ->where ('pedido.idPedido','=',$request->id)
        ->update(['pedido.idStatusPedido' => 3]);

    }

    return redirect()->back()->with('message','El pedido se ha actualizado');


    }//Aquí termina la función update


    public function BuscarPed(){
         $pedidos = DB::table('pedido')
            ->join('tipo_pedido', 'pedido.id_Tipo_Pedido', '=', 'tipo_pedido.id_Tipo_Pedido')
            ->join('status_pedido','pedido.idStatusPedido', '=', 'status_pedido.idStatusPedido')
            ->select('pedido.idPedido','tipo_pedido.descripcion as tipoP','pedido.folio','tipo_pedido.descripcion','status_pedido.descripcion as descripcionP','pedido.destino','pedido.origen')
            ->where('tipo_pedido.id_Tipo_Pedido','=',2)
            ->where('pedido.origen','like',"%".$value."%")
            ->get();
        return view('PedidosReal.VerPedidosReal', $pedidos = array('pedidos' => $pedidos ));
    }










    

   }//Aquí acaba nuestro controlador.
