<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\inventario;
use App\ubicacion;
use App\bitacora;
use Illuminate\Support\Facades\Auth;
use Illuminate\Pagination\Paginator;

class ReportesController extends Controller
{
	public function Inventario(){
		$value =$sucursal=Db::table('sucursal')->select('*')->where('id_sucursal','=',Auth::user()->id_sucursal)->get();

    foreach ($value as $key) {
      $value="sucursal ".$key->descripcionS;
    }
    $notificacion=DB::table('notificacions')
    ->join('tipo_notificacion','tipo_notificacion.idtipo_n','=','notificacions.idtipo_n')
    ->select('tipo_notificacion.descripcion as des','notificacions.origen','notificacions.idtipo_n','notificacions.destino','notificacions.created_at as tiempo')->where('notificacions.destino','=',$value)->orwhere('notificacions.origen','=',$value)->orderBy('notificacions.created_at')->get();
		$sucursal = Auth::user()->id_sucursal;
		$totproductos = DB::table('inventario')
		->join('almacen',  'inventario.idAlmacen', '=', 'almacen.idAlmacen')
		->join('sucursal','sucursal.id_sucursal','=', 'almacen.id_sucursal')
		->where('estadoinventario', '=', 1)
		->where('sucursal.id_sucursal','=',$sucursal)
		->selectRaw('count(*) as total')
		->get();

		$entradas = DB::table('inventario')
		->join('almacen',  'inventario.idAlmacen', '=', 'almacen.idAlmacen')
		->join('sucursal','sucursal.id_sucursal','=', 'almacen.id_sucursal')
		->join('bitacora', 'bitacora.datomodificado', '=', 'inventario.id_Producto')
		->where('id_movimiento','=',1)
		->where('modulo', '=', 'inventario')
		->where('estadoinventario', '=', 1)
		->where('sucursal.id_sucursal','=',$sucursal)
		->selectRaw('sum(entrada) as entradas')
		->get();

		$Ultimaent = DB::table('inventario')
		->join('almacen',  'inventario.idAlmacen', '=', 'almacen.idAlmacen')
		->join('sucursal','sucursal.id_sucursal','=', 'almacen.id_sucursal')
		->join('bitacora', 'bitacora.datomodificado', '=', 'inventario.id_Producto')
		->where('id_movimiento','=',1)
		->where('modulo', '=', 'inventario')
		->where('estadoinventario', '=', 1)
		->where('sucursal.id_sucursal','=',$sucursal)
		->select('updated_at')->latest('created_at')
		->get();



		$salidas = DB::table('inventario')
		->join('almacen',  'inventario.idAlmacen', '=', 'almacen.idAlmacen')
		->join('sucursal','sucursal.id_sucursal','=', 'almacen.id_sucursal')
		->join('bitacora', 'bitacora.datomodificado', '=', 'inventario.id_Producto')
		->where('id_movimiento','=',5)
		->where('modulo', '=', 'inventario')
		->where('estadoinventario', '=', 1)
		->where('sucursal.id_sucursal','=',$sucursal)
		->selectRaw('sum(salida) as salidas')
		->get();

		$Ultimasal = DB::table('inventario')
		->join('almacen',  'inventario.idAlmacen', '=', 'almacen.idAlmacen')
		->join('sucursal','sucursal.id_sucursal','=', 'almacen.id_sucursal')
		->join('bitacora', 'bitacora.datomodificado', '=', 'inventario.id_Producto')
		->where('id_movimiento','=',5)
		->where('modulo', '=', 'inventario')
		->where('estadoinventario', '=', 1)
		->where('sucursal.id_sucursal','=',$sucursal)
		->select('updated_at')->latest('updated_at')
		->get();

		return view('Reportes.dashboard', ['notificacion'=>$notificacion,'totalp'=>$totproductos, 'entradas'=>$entradas, 'salidas'=>$salidas, 'ultima'=>$Ultimasal, 'ultimae'=>$Ultimaent]);
	}


}
