<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ListadoProductosController extends Controller
{
    //Regresa los productos de la tabla productos
	public function lista(){

		$productos = DB::table('producto')->get();

		return view('Productos.CatalogoProductos', array(
			"productos" => $productos
		));
	}
	public function __construct()
	{
			$this->middleware('auth');
	}
}
