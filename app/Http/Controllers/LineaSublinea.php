<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Flash;
use App\categoria;
use App\subcategoria;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\bitacora;
use Illuminate\Support\Facades\DB;

class LineaSublinea extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }
    public function listarCategorias(Request $request){

        $categorias = categoria::all();
        return view ('Lineas_Sublineas.CrearSublinea',compact('categorias'));
    }
    public function storeSublinea(Request $request)
    {
        //Guarda una nueva Linea



        $sublinea= new subcategoria;
        $sublinea->idCategoria=$request->input('Linea');
        $sublinea->descripcion=$request->input('Sublinea');

        $sublinea->save();
        $operacion=new bitacora();
        $operacion->datomodificado=$request->input('Sublinea');
        $operacion->id_usuario=$request->input('usuario');
         $operacion->modulo="productos";
        $operacion->id_movimiento=4;
        $operacion->save();

        return redirect()->back()->with('message','Se ha creado la nueva Sublinea');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeLinea(Request $request)
    {
        //Guarda una nueva Linea
        //dd($request->Linea);


        $linea= new categoria;

        $linea->descripcion=$request->input('Linea');

        $linea->save();
        $operacion=new bitacora();
        $operacion->datomodificado=$request->input('Linea');
        $operacion->id_usuario=$request->input('usuario');
         $operacion->modulo="productos";
        $operacion->id_movimiento=4;
        $operacion->save();

        return redirect()->back()->with('message','Se ha creado la nueva linea');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
