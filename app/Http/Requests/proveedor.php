<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProveedorV extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
      return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
      return  [
      'claveProveedor' =>'required|max:255',
      'proveedor' => 'required|unique|max:255',
      ];


    }
    public function messages()
 {
 return [
 'proveedor.required' => 'El campo nombre es obligatorio',
 'claveProveedor.required' => 'El campo apellido es obligatorio',
 ];
 }


}
