<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ubicacion extends Model
{
    protected $table = 'ubicacion';
    protected $fillable = ['nivel','piso','anaquel','pasillo'];
    public $timestamps = false;
    protected $primarykey = 'id_ubicacion';
}