<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sucursal extends Model
{
	protected $primaryKey = 'id_sucursal';
    protected $table = 'sucursal';
    protected $fillable = ['descripcionS', 'direccion', 'colonia', 'numero', 'ciudad', 'id_Estados'];
    public $timestamps = false;
}
