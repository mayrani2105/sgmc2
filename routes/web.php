<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');



Route::any('/', function () {

    return view('auth.login');
});



Auth::routes();
//Route::get('/', 'usuario@ver');
Route::get('/JefeOp', function () {
    return view('Operaciones');
});

//Ruta usuario
Route::any('/Usuario','UserController@usuario');
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::any('/ModificarUser/{id_user}', 'UserController@actualizar')->name ('modificar');
Route::resource('newUser', 'UserController');
//Ruta que sirve para mandar correo de verificacion de contraseña

Route::get('password/reset/{token?}', 'Auth\ForgotPasswordController@showLinkRequestForm');

Route::get('/marcas','PruebaController@compras');

Route::get('/home2', 'HomeController@index')->name('home3');
Route::get('/home3', 'HomeController@index')->name('home2');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/auth.register', 'usuario@verRoles');
Route::resource('/CrearProducto','Productos');
//Ruta que verifica el correo electronico
Auth::routes(['verify' => true]);
//Route::get('/modPro','proveedorController@mod')->name('modificarPro');
Route::any('/modPro/{id}','proveedorController@update')->name('modificarPro');
Route::any('/CrearProveedor','proveedorController@crear')->name('CrearPro');
Route::any('/lproveedor','proveedorController@index')->name('lproveedor');
Route::any('/BuscarPro','proveedorController@filtrado')->name('BuscarPro');
Route::any('/eliminarProv/{id}','proveedorController@destroy')->name('eliminarProv');
Route::any('/Crearmar','proveedorController@crearmarca')->name('Crearmar');
Route::get('/vermarca','proveedorController@vermarcas')->name('vermarca');
//Ruta para Proveedores

Route::get('/search','SearchController@search');
Route::resource('Provee','proveedorController',['only' => ['index', 'show']]);

//Controladores para Inventario
Route::any('/ModificarInv', 'InventoryController@listamodificar')->name('ModificarInv');
Route::any('/ModModificar/{id_Producto}','InventoryController@modal')->name ('modal');
Route::any('/BuscarInve','InventoryController@filtrado')->name('BuscarInve');
Route::get('/ModN/{id_Producto}', 'InventoryController@modalNuevo');
Route::any('/SucN/{id_sucursal}','InventoryController@almacen');
Route::resource('ModInv', 'InventoryController');


Route::any('/eliminarInv/{id_Producto}', 'InventoryController@eliminar')->name ('eliminar');
Route::get('/searchI','SearchController@search2');
//Terminan controladores para Inventario


    //rutas para obtener Reportes
    Route::any('/reporteU','BitacoraController@index')->name('reporteU');

    Route::any('/reportes','ReportesController@inventario');
    Route::any('/reporteUFecha','BitacoraController@busquedafecha')->name('reporteUFecha');
    Route::get('/graficaI', 'ReportesController@graficaI');

//Ruta para Proveedores
//Route::get('/modPro','proveedorController@mod')->name('modificarPro');




//Ruta para Proveedores



//Rutas para Pedidos (en realidad es Ordenes de compra)
Route::get('/verPedidos','PedidosController@VerPedidos')->name('verPedidos');
Route::any('/CrearPedido','PedidosController@CrearPedidos');
Route::get('/subP/{id}','PedidosController@byCategoria');
Route::get('/VerDetalles/{id}','PedidosController@verPedidosProductos');
Route::resource('ModPed', 'PedidosController');
Route::any('/borrarOrden','PedidosController@eliminar');
Route::get('/ProductosP/{id}', 'PedidosController@obtenerproductos');
Route::any('/AgregarP', 'PedidosController@agregarproductos');
Route::any('/AgregarS', 'PedidosController@agregarsucursal');
Route::get('/VerDetalle/{id}','PedidosController@verPedidosProducto');

//Rutas para PedidosReal
Route::get('/verPedidosReal','PedidosRealController@verPedidosReal')->name('verPedidosReal');
Route::resource('ModPedReal', 'PedidosRealController');
Route::any('/BuscarPed','PedidosRealController@BuscarPed')->name('BuscarPed');
Route::get('/VerDetalles2/{id}','PedidosRealController@verPedidosProductos');

//Rutas Productos

/////////////Vistas Productos


//Ruta para formulario crear producto
Route::get('/CreacionP', 'Productos@listaNecesarioParaCreacionProducto');

//Ruta para crear producto
Route::any('Produc/crear', [
    'uses'=>'Productos@store',
    'as'=>'produc.crea'
]);

//Controlador Catalogo de productos
Route::any('/CatalogProduc','Productos@listaProductos')->name('CatalogProduc');


//Ruta para eliminar producto
Route::any('Produc/{id}/destroy', [
    'uses'=>'Productos@destroy',
    'as'=>'produc.destroy'
]);
//Ruta para editar producto
//Ruta para obtener subcategorias
Route::get('/ed/{idP}','Productos@byAll');
Route::any('Produc/{id}/actualizar', [
    'uses'=>'Productos@update',
    'as'=>'produc.actualiza'
]);



Route::resource('Sucursales', 'Sucursales');
Route::any('/BuscarSuc','Sucursales@BuscarSuc')->name('BuscarSuc');



///////////Rutas para lineas y sublineas

//Ruta para crear linea
Route::any('Linea/crear', [
    'uses'=>'LineaSublinea@storeLinea',
    'as'=>'linea.crea'
]);

//Ruta para guardar una sublinea
Route::any('Sublinea/crear', [
    'uses'=>'LineaSublinea@storeSublinea',
    'as'=>'sublinea.crea'
]);

//Ruta para obtener subcategorias
Route::get('/sub/{id}','Productos@byCategoria');


Route::get('/lineas','LineasController@listaCategorias')->name('lineas');


Route::get('clave-sat','Controlador_Sat@verSat')->name('clave-sat');
